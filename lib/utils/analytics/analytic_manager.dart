import 'package:app/flavor/config.dart';
import 'package:app/utils/analytics_util.dart';
import 'package:app/utils/analytics/mix_panel_analytics.dart';

class AnalyticsManager {
  static FireBaseAnalytics? _fireBaseAnalytics;
  static MixPanelAnalyticsManager? _mixPanelAnalytics;

  static final AnalyticsManager _analyticsManager = AnalyticsManager._();

  factory AnalyticsManager() {
    return _analyticsManager;
  }

  AnalyticsManager._() {
    _fireBaseAnalytics = FireBaseAnalytics();
    _mixPanelAnalytics = MixPanelAnalyticsManager.init(FlavorConfig.instance!.values.MIX_PANEL_CODE);
  }

  logEvent(String eventName) async {
    _fireBaseAnalytics?.trackEvent(eventName);
    _mixPanelAnalytics?.trackEvent(eventName);
  }

  logEventProp(String eventName, Map<String, dynamic>? param) async {
    _fireBaseAnalytics?.trackEvent(eventName, properties: param);
    _mixPanelAnalytics?.trackEvent(eventName, properties: param);
  }

  logUserProp(String eventName, userProperties) async {
    _fireBaseAnalytics?.trackEvent(eventName);
    _mixPanelAnalytics?.trackUserProperties(userProperties);
  }

  logUserAlias(String eventName, String userId) async {
    _fireBaseAnalytics?.trackEvent(eventName);
    _mixPanelAnalytics?.aliasIdentity(userId);
  }

  screenViewEvent(pageName) async {
    _mixPanelAnalytics?.screenViewEvent(pageName);
    _fireBaseAnalytics?.firebaseScreenViewEvent(pageName);
  }
}
