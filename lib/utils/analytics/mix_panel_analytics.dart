import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/string_util.dart';
import 'package:mixpanel_flutter/mixpanel_flutter.dart';

class MixPanelAnalyticsManager {
  static Mixpanel? _instance;
  static MixPanelAnalyticsManager? _mixPanelAnalyticsManager;

  factory MixPanelAnalyticsManager.init(String? mixpanelToken) {
    _mixPanelAnalyticsManager = MixPanelAnalyticsManager._(mixpanelToken);
    return _mixPanelAnalyticsManager ??= MixPanelAnalyticsManager();
  }

  MixPanelAnalyticsManager();

  MixPanelAnalyticsManager._(_mixpanelToken) {
    if (_instance == null) {
      _MixpanelManager.init(_mixpanelToken ?? '').then((value) {
        _instance = value;
      });
    }
  }

  screenViewEvent(String pageName) async {
    if (_instance != null) {
      _instance!.track(pageName);
    }
  }

  trackEvent(String eventName, {Map<String, dynamic>? properties}) async {
    if (_instance != null) {
      _instance!.track(eventName, properties: properties);
    }
  }

  trackUserProperties(userProp) async {
    if (_instance != null) {
      People people = _instance!.getPeople();
      _instance!.identify(userProp.id.toString());
      people.set(UserProperties.USER_ROLE, UserProperties.USER_TYPE);
      people.set(UserProperties.USER_NAME, userProp.name ?? 'Admin');
      if (!isNullOrEmpty(userProp.email))
        people.set(UserProperties.EMAIL, userProp.email);
      if (!isNullOrEmpty(userProp.district))
        people.set(UserProperties.USER_DISTRICT, userProp.district);
      if (!isNullOrEmpty(userProp.mobileNumber))
        people.set(UserProperties.USER_MOBILE, userProp.mobileNumber);
    }
  }

  aliasIdentity(String userId) async {
    if (_instance != null) {
      _instance!.identify(userId);
    }
  }
}

class _MixpanelManager {
  static Mixpanel? _instance;

  static Future<Mixpanel> init(String mixPanelToken) async {
    if (_instance == null) {
      _instance =
          await Mixpanel.init(mixPanelToken, optOutTrackingDefault: false, trackAutomaticEvents: true);
    }
    return _instance!;
  }
}
