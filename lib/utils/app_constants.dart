import 'package:flutter/material.dart';
import 'package:app/model/master/goods_unit.dart';

class RouteConstants {
  static const String authErrorRoute = '/autherror';
  static const String homeRoute = '/home';
  static const String createTrip = '/createTrip';
  static const String viewTrip = '/viewTripList';
  static const String customerConfig = '/customerConfig';
  static const String uploadChallan = '/uploadChallan';
  static const String bookedTrips = '/bookedTrips';
}

List<String> tripTypeConst = ['All Trips', 'Bidded', 'Booked', 'Live'];
List<String> bookedStatusConst = [
  'ON_THE_WAY_TO_LOAD_POINT',
  'TRUCK_REACHED_AT_LOADING_POINT',
  'WAITING_FOR_LOADING',
  'LOADING_ON_PROCESS',
  'LOADING_DONE',
  'STARTED_FOR_DESTINATION'
];
List<String> liveStatusConst = [
  'UNLOADING_STARTED',
  'TRUCK_REACHED_AT_UNLOADING_POINT',
  'WAITING_FOR_UNLOADING',
  'UNLOADING_DONE_AND_IN_TRANSIT',
  'UNLOADING_COMPLETED'
];

final int NO_OF_REC = 5;
final int negIndex = -1;
const int insMaxCharCount = 1000;
final double containerWidth = 680;
const double horizontalPadding = 40.0;
const String mpTestCode = "588c834f5207074e7791b63c34ad7f51";
const String mpProdCode = "b75f01c77be7fdad6d872f8d4f7dace8";

const int UNLOAD_POINT_INS = 1;
const int GOOD_QTY_UNT_INS = 2;

final Map<String, dynamic> units = {
  'KG': GoodsUnit('KG', 'Kg', 'কেজি'),
  'TON': GoodsUnit('TON', 'Tons', 'টন'),
  'BAGS': GoodsUnit('BAGS', 'Bags', 'ব্যাগ'),
  'CARTON': GoodsUnit('CARTON', 'Cartons', 'কার্টন'),
  'PCS': GoodsUnit('PCS', 'Pieces', 'পিস'),
  'CBM': GoodsUnit('CBM', 'CBM', 'সিবিএম')
};

enum TripUpdateStatus {
  ETAUpdate,
  OnTheWayToLoadPoint,
  TruckReachedAtLoadingPoint,
  LoadingOnProcess,
  LoadingDone,
  StartedForDestination,
  LocationUpdate,
  TruckReachedAtUnloadingPoint,
  DriverHelperNotReachable,
  UnloadingStarted,
  UnloadingDoneTripInTransit,
  UnloadingCompleted,
  FollowupUpdate,
  WaitingForUnloading,
  WaitingForLoading,
  None
}

enum BookedTripStatus {
  OnTheWayToLoadPoint,
  TruckReachedAtLoadingPoint,
  LoadingOnProcess,
  LoadingDone,
  StartedForDestination,
  WaitingForLoading,
  None
}

enum LiveTripStatus {
  TruckReachedAtUnloadingPoint,
  UnloadingStarted,
  UnloadingDoneTripInTransit,
  UnloadingCompleted,
  WaitingForUnloading,
  None
}

enum Followup {
  TruckReachedAtUnloadingPoint,
  DriverTakingRest,
  DriverNotReachable,
  NoAnswerFromCall,
  DriverPhoneSwitchedOff,
  Other
}

extension FollowUpExt on Followup {
  String get text {
    switch (this) {
      case Followup.TruckReachedAtUnloadingPoint:
        return 'Truck reached at unloading point';
      case Followup.DriverTakingRest:
        return 'Driver taking rest';
      case Followup.DriverNotReachable:
        return 'Driver not reachable';
      case Followup.NoAnswerFromCall:
        return 'No answer from call';
      case Followup.DriverPhoneSwitchedOff:
        return 'Driver’s phone switched off';
      default:
        return 'Other';
    }
  }

  String get value {
    switch (this) {
      case Followup.TruckReachedAtUnloadingPoint:
        return 'TRUCK_REACHED_AT_UNLOADING_POINT';
      case Followup.DriverTakingRest:
        return 'DRIVER_TAKING_REST';
      case Followup.DriverNotReachable:
        return 'DRIVER_NOT_REACHABLE';
      case Followup.NoAnswerFromCall:
        return 'NO_ANSWER_FROM_CALL';
      case Followup.DriverPhoneSwitchedOff:
        return 'DRIVERS_PHONE_SWITCHED_OFF';
      default:
        return 'OTHER';
    }
  }
}

enum TripState { Available, Unbidded, Bidded, Booked, Live }

extension TripStateExt on TripState {
  String? filterValue() {
    switch (this) {
      case TripState.Available:
        return "AvailableTrips";
      case TripState.Unbidded:
        return "UNBIDDED";
      case TripState.Bidded:
        return "BIDDED";
      case TripState.Booked:
        return "BookedTrips";
      case TripState.Live:
        return "LiveTrips";
      default:
        return null;
    }
  }

  String? statusValue() {
    switch (this) {
      case TripState.Unbidded:
        return "UNBIDDED";
      case TripState.Bidded:
        return "BIDDED";
      case TripState.Booked:
        return "BOOKED";
      case TripState.Live:
        return "RUNNING";
      default:
        return null;
    }
  }
}

class DimensionResource {
  static const leftMargin = 18.0;
  static const rightMargin = 18.0;
  static const formFieldContentPadding =
      EdgeInsets.only(left: 15, top: 20, right: 10, bottom: 20);
  static const filterFormFieldContentPadding =
      EdgeInsets.only(left: 15, top: 18, right: 5, bottom: 18);
  static const hPad = 20.0;
  static const screenPadding =
      EdgeInsets.only(left: 18, top: 18, right: 18, bottom: 5);
  static const trip_widget_right_margin = 26.0;
  static const chipPadding = EdgeInsets.fromLTRB(14.0, 5.0, 14.0, 5.0);
}

//todo new admin type constant use to  constants
const ADMIN = "ADMIN";
const OPS = "OPS";
const KAM = "KAM";
const MAX_TRIP = 999;
