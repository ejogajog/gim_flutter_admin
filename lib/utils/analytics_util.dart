import 'package:firebase_analytics/firebase_analytics.dart';

class FireBaseAnalytics {
  static final FireBaseAnalytics _fireBaseAnalytic =
  FireBaseAnalytics._internal();

  factory FireBaseAnalytics() {
    return _fireBaseAnalytic;
  }

  FireBaseAnalytics._internal() {
    _analytics = FirebaseAnalytics.instance;
    _analyticsObserver = FirebaseAnalyticsObserver(analytics: _analytics);
  }

  late FirebaseAnalytics _analytics;
  FirebaseAnalyticsObserver? _analyticsObserver;

  get analyticObserver => _analyticsObserver;

  logFbSignUp() async {
    await _analytics.logSignUp(signUpMethod: "SignUp using mobile number.");
  }

  firebaseScreenViewEvent(eventName) async {
    await _analytics.setCurrentScreen(
        screenName: eventName, screenClassOverride: eventName);
  }

  trackEvent(String eventName, {Map<String, dynamic>? properties}) async {
    final stringProperties = properties?.map((key, value) => MapEntry(key, "$value"));
    await _analytics.logEvent(name: eventName, parameters: stringProperties);
  }
}
