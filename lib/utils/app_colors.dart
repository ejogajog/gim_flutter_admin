import 'package:flutter/material.dart';


class AppColors {
  static const Color Primary = Color(0xFF003250);
  static const Color TextOnPrimary = Color(0xFFFFFFFF);
  static const Color Secondary = Color(0xFFFBB03B);
  static const Color TextOnSecondary = Color(0xFFFFFFFF);
}

class AppFilterChip extends ChoiceChip {
  final selected;
  final text;
  final textColor;
  final selectedTextColor;
  final onSelected;

  static Widget _label(selected, text, textColor, selectedTextColor) {
    return Container(
      width: 56,
      child: Text(
        text,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: selected ? selectedTextColor : textColor,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  AppFilterChip({
    required this.selected,
    required this.text,
    required this.onSelected,
    this.textColor,
    this.selectedTextColor,
  }) : super(
          selected: selected,
          label: _label(selected, text, textColor, selectedTextColor),
          onSelected: onSelected,
        );

  @override
  Color get selectedColor => textColor;

  @override
  Color get backgroundColor => selectedTextColor;

  @override
  OutlinedBorder get shape => StadiumBorder(
        side: BorderSide(
          color: textColor,
          width: 2,
        ),
      );
}
