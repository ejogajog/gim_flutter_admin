import 'dart:convert';

bool isNullOrEmpty(String? value) {
  return value == null || value.trim().length == 0;
}

bool isNullOrEmptyInt(int? value) {
  return value == null || value == 0;
}

bool isNullOrEmptyDouble(double? value) {
  return value == null || value == 0.0 || value == 0;
}

bool isNullOrEmptyList(List? value) {
  return value == null || value.length == 0;
}

int bytesLength(String value) {
return utf8.encode(value).length;
}