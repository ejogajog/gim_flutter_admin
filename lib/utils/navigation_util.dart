import 'package:flutter/material.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/route.dart';
import 'package:app/utils/ui_util.dart';

navigateNextScreen(BuildContext context, page,
    {bool shouldFinishPreviousPages = false,
    bool shouldReplaceCurrentPage = false,
    String? pageName,
    Object? arguments}) {
  hideSoftKeyboard(context);
  pageName = pageName ?? page.runtimeType.toString();
  AnalyticsManager().screenViewEvent(pageName);
  if (!shouldFinishPreviousPages) {
    if (!shouldReplaceCurrentPage)
      return Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => page,
              settings: RouteSettings(name: pageName, arguments: arguments)));
    else
      return Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => page,
              settings: RouteSettings(name: pageName, arguments: arguments)));
  } else
    return Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
            builder: (context) => page,
            settings: RouteSettings(name: pageName, arguments: arguments)),
        (e) => false);
}

navigateToScreen(BuildContext context, pageUrl,
    {bool shouldFinishPreviousPages = false,
    bool shouldReplaceCurrentPage = false,
    String? pageName,
    Object? arguments}) {
  hideSoftKeyboard(context);
  pageName = pageName ?? pageUrl.runtimeType.toString();
  AnalyticsManager().screenViewEvent(pageName);
  if (!shouldFinishPreviousPages) {
    if (!shouldReplaceCurrentPage)
      return Navigator.pushNamed(context, pageUrl);
    else
      return Navigator.pushReplacement(
          context,
          AdminPageRouter.generateRoute(
              RouteSettings(name: pageUrl, arguments: arguments))!);
  } else
    return Navigator.pushAndRemoveUntil(
        context,
        AdminPageRouter.generateRoute(
            RouteSettings(name: pageUrl, arguments: arguments))!,
        (e) => false);
}

navigateNextTransparentScreen(context, page,
    {bool shouldFinishPreviousPages = false,
    bool shouldReplaceCurrentPage = false}) {
  return Navigator.of(context).push(PageRouteBuilder(
      opaque: false,
      pageBuilder: (BuildContext context, _, __) => page,
      settings: RouteSettings(name: page.runtimeType.toString())));
}
