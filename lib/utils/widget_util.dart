import 'package:flutter/material.dart';
import 'package:app/utils/date_time_util.dart';
import 'package:app/utils/ui_util.dart';
import 'package:intl/intl.dart';

Future<Null> selectDateTime(BuildContext context, Function callback) async {
  final DateTime? selDate = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      initialDatePickerMode: DatePickerMode.day,
      firstDate: DateTime(2021),
      lastDate: DateTime.now());
  if (selDate != null) selectTime(context, callback, selDate);
}

Future<Null> selectTime(
    BuildContext context, Function callback, DateTime selDate) async {
  final TimeOfDay? selTime = await showTimePicker(
    context: context,
    initialTime: TimeOfDay.now(),
  );
  DateTime datePicked = DateTime(
      selDate.year, selDate.month, selDate.day, selTime!.hour, selTime.minute);
  if(datePicked.isAfter(DateTime.now())){
    showToast("Please select valid time");
    return;
  }
  String dateTimeFormat = DateFormat(AppDateTimeFormat).format(datePicked);
  callback(dateTimeFormat);
}
