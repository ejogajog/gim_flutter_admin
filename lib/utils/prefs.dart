import 'dart:async';
import 'package:app/model/master/districts.dart';
import 'package:app/model/master/goods_type.dart';
import 'package:app/model/master/truck_dimension.dart';
import 'package:app/model/master/truck_size.dart';
import 'package:app/model/master/truck_type.dart';
import 'package:app/utils/string_util.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../model/project/project.dart';

class Prefs {
  static SharedPreferences? _prefsInstance;
  static const String PREF_USER_PWD = "pwd";
  static const String token = "TOKEN";
  static const String ROLE_ID = "roleId";
  static const String USER_ID = "userId";
  static const String IS_LOGGED_IN = "IS_LOGGED_IN";
  static const PREF_USER_EMAIL = "user_email";
  static const PREF_DEVICE_TOKEN = "PREF_DEVICE_TOKEN";
  static const IS_DISTRIBUTOR = "IsDistributor";
  static const PREF_IS_ENTERPRISE_USER = "PREF_IS_ENTERPRISE_USER";
  static const IS_APPROVED_USER = "IS_APPROVED_USER";
  static const language_code = "language_code";
  static const String DISTRICTS = "districts";
  static const String GOODS_TYPE = "goodsType";
  static const String TRUCK_TYPE = "truckType";
  static const String TRUCK_SIZE = "truckSize";
  static const String TRUCK_LENGTH = "truckLength";
  static const String MASTER_DATA_SYNCED = "isMdSync";
  static const String ADMIN_TYPE = "adminType";
  static const String PROJECTS = "projects";
  static const String CLS_DIST_ID = "dstClsId";

  static init() async {
    await getInstance();
  }

  static void dispose() {
    _prefsInstance = null;
  }

  static Future<SharedPreferences> getInstance() async {
    if (_prefsInstance == null)
      _prefsInstance = await SharedPreferences.getInstance();
    return _prefsInstance!;
  }

  static setString(String key, String? value) async {
    if (isNullOrEmpty(value)) return;
    _prefsInstance = await getInstance();
    _prefsInstance?.setString(key, value!);
  }

  static setInt(String key, int? value) async {
    if (value == null) return;
    _prefsInstance = await getInstance();
    _prefsInstance?.setInt(key, value);
  }

  static Future<int?> getInt(String key) async {
    await getInstance();
    return _prefsInstance?.getInt(key) == null
        ? null
        : _prefsInstance?.getInt(key);
  }

  static int getIntWithDefValue(String key, {defaultValue = 0}) {
    try {
      return _prefsInstance?.getInt(key) ?? defaultValue;
    } catch (ex) {
      return defaultValue;
    }
  }

  static setBoolean(String key, bool? value) async {
    if (value == null) return;
    _prefsInstance = await getInstance();
    _prefsInstance?.setBool(key, value);
  }

  static  addIntegerList(String key, List<int>? integerList) async {
    if(isNullOrEmptyList(integerList)) return null;
    _prefsInstance = await getInstance();
    _prefsInstance?.setStringList(key, integerList!.map((e) => e.toString()).toList());
  }

  static Future<List<int>> getIntegerList(String key) async {
    _prefsInstance = await getInstance();
    final stringList = _prefsInstance?.getStringList(key);
    if (stringList != null) {
      return stringList.map((e) => int.parse(e)).toList();
    }
    return [];
  }

  static Future<dynamic> getPrefValue(String key) async {
    return _prefsInstance?.get(key);
  }

  static String? getString(String key) {
    try {
      return _prefsInstance?.getString(key);
    } catch (ex) {
      return "";
    }
  }

  static Future<String> getStringAsync(String key) async {
    _prefsInstance =
        _prefsInstance == null ? await getInstance() : _prefsInstance;
    try {
      return _prefsInstance!.getString(key)!;
    } catch (ex) {
      return "";
    }
  }

  static Future<String?> getContactId(String key) async {
    try {
      return _prefsInstance?.getString(key);
    } catch (ex) {
      return "";
    }
  }

  static Future<String?> getFcmToken() async {
    try {
      return _prefsInstance?.getString(Prefs.PREF_DEVICE_TOKEN);
    } catch (ex) {
      return "";
    }
  }

  static String getStringWithDefaultValue(String key, {defaultValue = ""}) {
    try {
      return _prefsInstance?.getString(key) ?? defaultValue;
    } catch (ex) {
      return defaultValue;
    }
  }

  static Future<bool> getBool(String key, {defaultValue = false}) async {
    _prefsInstance =
        _prefsInstance == null ? await getInstance() : _prefsInstance;
    return _prefsInstance?.getBool(key) ?? defaultValue;
  }

  static bool getBoolean(String key, {defaultValue = false}) {
    try {
      return _prefsInstance?.getBool(key) ?? defaultValue;
    } catch (ex) {
      return defaultValue;
    }
  }

  static Future<bool?> remove(String key) async {
    return _prefsInstance?.remove(key);
  }

  static Future<bool?> clear() async {
    return _prefsInstance?.clear();
  }

  static List<Districts> getDistricts() {
    return Districts.decode(getString(Prefs.DISTRICTS)!);
  }

  static List<GoodsType> getGoodsType() {
    return GoodsType.decode(getString(Prefs.GOODS_TYPE)!);
  }

  static List<TruckType> getTrucksType() {
    return TruckType.decode(getString(Prefs.TRUCK_TYPE)!);
  }

  static List<TruckSize> getTrucksSizes() {
    return TruckSize.decode(getString(Prefs.TRUCK_SIZE)!);
  }

  static List<TruckDimensionLength> getTrucksLength() {
    return TruckDimensionLength.decode(getString(Prefs.TRUCK_LENGTH)!);
  }

  static List<Projects> getProjects() {
    return Projects.decode(getString(Prefs.PROJECTS)!);
  }
}
