import 'package:flutter/material.dart';
import 'package:app/ui/auth/login_screen.dart';
import 'package:app/ui/home/home.dart';
import 'package:app/ui/splash/navigation_splash_screen.dart';
import 'package:app/ui/trip/booked_trip_list.dart';
import 'package:app/ui/trip/search_customer.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/app_constants.dart';

class AdminPageRouter {
  static Route<dynamic>? generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case RouteConstants.createTrip:
        return MaterialPageRoute(
            builder: (_) => CustomerSearchPage(), settings: RouteSettings(name: AnalyticsEvents.CUSTOMER_SEARCH_SCREEN));
      case RouteConstants.viewTrip:
        return MaterialPageRoute(
            builder: (_) => HomeScreen(), settings: RouteSettings());
      case RouteConstants.authErrorRoute:
        return MaterialPageRoute(
            builder: (_) => LoginScreenContainer(), settings: RouteSettings());
      case RouteConstants.homeRoute:
        return MaterialPageRoute(
            builder: (_) => NavigationSplashScreen(),
            settings: RouteSettings());
      case RouteConstants.customerConfig:
        return MaterialPageRoute(
            builder: (_) => CustomerSearchPage(launchConfig: true), settings: RouteSettings(name: AnalyticsEvents.CUSTOMER_SEARCH_SCREEN));
      case RouteConstants.bookedTrips:
        return MaterialPageRoute(
            builder: (_) => BookedTripListScreen(), settings: RouteSettings(name: AnalyticsEvents.BOOKED_LIST_SCREEN));
    }
  }
}
