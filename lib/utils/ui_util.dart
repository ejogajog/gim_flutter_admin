import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:app/flavor/config.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/ui/widget/app_button.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/string_util.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:app/ui/widget/project_widget.dart';
import 'package:app/bloc/base_bloc.dart';
import 'package:app/bloc/base_pagination_bloc.dart';

showToast(String msg) {
  print('toast');
  showCustomSnackbar(msg);
  return;
}

OverlayEntry showOverLay(BuildContext context, builder) {
  OverlayState overlayState = Overlay.of(context);
  OverlayEntry overlayEntry = OverlayEntry(builder: builder);
  overlayState.insert(overlayEntry);

  return overlayEntry;
}

showSnackBar(ScaffoldState context, String? msg) {
  if (!isNullOrEmpty(msg)) showCustomSnackbar(msg);
}

showCustomSnackbar(msg, {bgColor = null, textColor = null}) async {
  OverlayState? overlayState = getGlobalState().overlay;
  OverlayEntry overlayEntry = OverlayEntry(
      builder: (context) => getSnackWidget(context,
          msg: msg, textColor: textColor, bgColor: bgColor));

  overlayState?.insert(overlayEntry);

  await new Future.delayed(const Duration(seconds: 4));

  overlayEntry.remove();
}

NavigatorState getGlobalState() {
  var state = FlavorConfig.instance!.values.navigatorKey.currentState;
  return state!;
}

Widget getSnackWidget(BuildContext context,
    {msg = "thanks", bgColor, textColor}) {
  var mediaQuery = MediaQuery.of(getGlobalContext());
  return Positioned(
    top: 0.0,
    left: 0.0,
    child: SafeArea(
      child: new Material(
          color: bgColor ?? Colors.black,
          child: Container(
              width: mediaQuery.size.width,
              padding: EdgeInsets.only(left: 18, right: 5, top: 15, bottom: 15),
              child: Text(
                msg,
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: textColor ?? ColorResource.colorWhite, fontSize: 18),
              ))),
    ),
  );
}

Future<Null> submitDialog(BuildContext context, {dismissible = false}) async {
  return await showDialog<Null>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async => true, //dismissible,
          child: SimpleDialog(
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            children: <Widget>[
              Center(
                child: SizedBox(
                  width: 40,
                  height: 40,
                  child: CircularProgressIndicator(
                    backgroundColor: ColorResource.colorMarineBlue,
                  ),
                ),
              )
            ],
          ),
        );
      });
}

showEmptyWidget<T extends BasePaginationBloc>({warning_msg}) {
  return Consumer<T>(
    builder: (context, bloc, _) => Visibility(
      visible: bloc.itemList.isEmpty && !bloc.isLoading,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 50, right: 50, top: 50),
            child: AspectRatio(
                aspectRatio: 468 / 144,
                child: Image.asset(
                  'images/trip_welcome.webp',
                )),
          ),
          SizedBox(
            height: 30,
          ),
          Text(
            'No Available Trips',
            style: TextStyle(
                fontSize: 18,
                color: ColorResource.semi_transparent_color_light),
          )
        ],
      ),
    ),
  );
}

showLoader<T extends BaseBloc>(T bloc) {
  return Center(
    child: Selector<T, bool>(
        selector: (context, bloc) => bloc.isLoading,
        builder: (context, builderVal, _) => Visibility(
            visible: bloc.isLoading,
            child: SizedBox(
              width: 40,
              height: 40,
              child: CircularProgressIndicator(
                backgroundColor: ColorResource.colorMarineBlue,
              ),
            ))),
  );
}

BuildContext getGlobalContext() {
  var context = FlavorConfig.instance!.values.navigatorKey.currentContext;
  return context!;
}

hideSoftKeyboard(context) {
  WidgetsBinding.instance.focusManager.primaryFocus?.unfocus();
}

Future<void> copyToClipboard(BuildContext context, String? text) async {
  await Clipboard.setData(ClipboardData(text: text!));
  showCustomSnackbar('Copied to clipboard');
}

void showCustomDialog(
    BuildContext context, String? title, String? description, Function? callback,
    {eventName, heading, withNegBtn = false}) {
  showGeneralDialog(
    context: context,
    barrierLabel: "Barrier",
    barrierDismissible: true,
    barrierColor: Colors.black.withOpacity(0.5),
    transitionDuration: Duration(milliseconds: 700),
    pageBuilder: (_, __, ___) {
      return Center(
        child: Container(
          height: 210,
          width: 260,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  padding: const EdgeInsets.all(14.0),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('$title',
                            style: GoogleFonts.roboto(
                                color: ColorResource.colorMarineBlue,
                                fontSize: 16.0,
                                fontWeight: FontWeight.w700,
                                decoration: TextDecoration.none))
                      ]),
                  decoration: BoxDecoration(
                      color: ColorResource.lightTrnBlue,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10)))),
              Padding(
                padding: const EdgeInsets.only(left: 4.0, right: 4.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    isNullOrEmpty(heading)
                        ? SizedBox()
                        : Text(localize(heading),
                        style: GoogleFonts.roboto(
                            fontSize: 12.0,
                            fontWeight: FontWeight.w500,
                            color: ColorResource.colorMarineBlue,
                            decoration: TextDecoration.none)),
                    Text('$description',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.roboto(
                            fontSize: 14.0,
                            fontWeight: FontWeight.w500,
                            color: ColorResource.colorMarineBlue,
                            decoration: TextDecoration.none)),
                  ],
                ),
              ),
              withNegBtn
                  ? Row(
                children: [
                  Expanded(
                    child: FilledColorButton(
                      fontSize: 16.0,
                      radius: 2.0,
                      horizonatalMargin: 5.0,
                      textColor: ColorResource.colorPrimary,
                      buttonText: translate(context, 'txt_btn_cancel'),
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                      backGroundColor: ColorResource.off_white,
                      eventName: eventName,
                    ),
                  ),
                  Expanded(
                    child: FilledColorButton(
                      fontSize: 16.0,
                      radius: 2.0,
                      horizonatalMargin: 5.0,
                      textColor: ColorResource.colorWhite,
                      buttonText: translate(context, 'txt_btn_ok'),
                      onPressed: () {
                        Navigator.pop(context, true);
                        callback!();
                      },
                      eventName: eventName,
                    ),
                  )
                ],
              )
                  : FilledColorButton(
                fontSize: 16.0,
                radius: 2.0,
                horizonatalMargin: 5.0,
                textColor: ColorResource.colorWhite,
                buttonText: translate(context, 'txt_btn_ok'),
                onPressed: () {
                  Navigator.pop(context, true);
                  callback!();
                },
                eventName: eventName,
              )
            ],
          ),
          margin: EdgeInsets.symmetric(horizontal: 20),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(6)),
        ),
      );
    },
    transitionBuilder: (_, anim, __, child) {
      Tween<Offset> tween;
      if (anim.status == AnimationStatus.reverse) {
        tween = Tween(begin: Offset(0, -1), end: Offset.zero);
      } else {
        tween = Tween(begin: Offset(0, 1), end: Offset.zero);
      }

      return SlideTransition(
        position: tween.animate(anim),
        child: FadeTransition(
          opacity: anim,
          child: child,
        ),
      );
    },
  );
}

void showEtDialog(BuildContext ctx, String ttl, String dsc, Widget widget,
    Function process, Function cancel,
    {eventName, heading}) {
  showGeneralDialog(
    context: ctx,
    barrierLabel: "Barrier",
    barrierDismissible: true,
    barrierColor: Colors.black.withOpacity(0.5),
    transitionDuration: Duration(milliseconds: 700),
    pageBuilder: (_, __, ___) {
      return Scaffold(
        backgroundColor: Colors.transparent,
        body: Center(
          child: Container(
            height: 300,
            width: 260,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    padding: const EdgeInsets.all(14.0),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(ttl,
                              style: GoogleFonts.roboto(
                                  color: ColorResource.colorMarineBlue,
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                  decoration: TextDecoration.none))
                        ]),
                    decoration: BoxDecoration(
                        color: ColorResource.lightTrnBlue,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10)))),
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 10.0, top: 4.0, right: 10.0, bottom: 4.0),
                        child: Text(dsc,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 14.0,
                                color: ColorResource.colorMarineBlue,
                                decoration: TextDecoration.none)),
                      ),
                      widget,
                      FilledColorButton(
                        fontSize: 14.0,
                        radius: 2.0,
                        innerPadding: 22,
                        verticalMargin: 0.0,
                        horizonatalMargin: 5.0,
                        textColor: ColorResource.colorWhite,
                        buttonText: localize('btn_save_tmp'),
                        onPressed: () {
                          process();
                        },
                        eventName: eventName,
                      )
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.pop(ctx);
                    cancel();
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10, bottom: 20.0),
                    child: Text(localize('skip_now'),
                        style: TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold,
                            color: ColorResource.colorMarineBlue,
                            decoration: TextDecoration.underline)),
                  ),
                ),
              ],
            ),
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(10)),
          ),
        ),
      );
    },
    transitionBuilder: (_, anim, __, child) {
      Tween<Offset> tween;
      if (anim.status == AnimationStatus.reverse) {
        tween = Tween(begin: Offset(0, -1), end: Offset.zero);
      } else {
        tween = Tween(begin: Offset(0, 1), end: Offset.zero);
      }

      return SlideTransition(
        position: tween.animate(anim),
        child: FadeTransition(
          opacity: anim,
          child: child,
        ),
      );
    },
  );
}

void showCustomFormDialog(BuildContext context, Function? onProject,
    Function onExpRate, Function onComment, Function createTrip) {
  showGeneralDialog(
    context: context,
    barrierLabel: "Barrier",
    barrierDismissible: false,
    barrierColor: Colors.black.withOpacity(0.5),
    transitionDuration: Duration(milliseconds: 700),
    pageBuilder: (_, __, ___) {
      return Center(
        child: Container(
          height: 540,
          width: 340,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                  padding: const EdgeInsets.all(18.0),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(localize('title_save_trip'),
                            style: GoogleFonts.roboto(
                                color: ColorResource.colorMarineBlue,
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                decoration: TextDecoration.none))
                      ]),
                  decoration: BoxDecoration(
                      color: ColorResource.lightTrnBlue,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10)))),
              ProjectWidget(onProject, onExpRate, onComment, createTrip),
            ],
          ),
          margin: EdgeInsets.symmetric(horizontal: 20),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(10)),
        ),
      );
    },
    transitionBuilder: (_, anim, __, child) {
      Tween<Offset> tween;
      if (anim.status == AnimationStatus.reverse) {
        tween = Tween(begin: Offset(0, -1), end: Offset.zero);
      } else {
        tween = Tween(begin: Offset(0, 1), end: Offset.zero);
      }

      return SlideTransition(
        position: tween.animate(anim),
        child: FadeTransition(
          opacity: anim,
          child: child,
        ),
      );
    },
  );
}

isMobileBrowser(constraint) => constraint.maxWidth < 760;


const double padding = 20;
const double avatarRadius =45;

void showEtDialogWithImg(BuildContext ctx, String ttl,String dsc1,String dsc2, String dsc, Widget widget,
    Function process, Function cancel, Function copy,
    {eventName, heading}) {
  showGeneralDialog(
    context: ctx,
    barrierLabel: "Barrier",
    barrierDismissible: true,
    barrierColor: Colors.black.withOpacity(0.5),
    transitionDuration: Duration(milliseconds: 700),
    pageBuilder: (_, __, ___) {

      return Material(
        type: MaterialType.transparency,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Center(
            child: Stack(

                children: <Widget>[

                  Container(

                    width: 260,
                    padding: EdgeInsets.only(left: padding,top: avatarRadius
                        , right: padding,bottom: padding
                    ),
                    margin: EdgeInsets.only(top: avatarRadius),
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(padding),
                        boxShadow: [
                          BoxShadow(color: Colors.black,offset: Offset(0,10),
                              blurRadius: 10
                          ),
                        ]
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(ttl,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 20,
                              fontFamily: 'sans',
                              fontWeight: FontWeight.w700,
                              color: ColorResource.headlineGrey,
                            ),),
                          SizedBox(height: 10,),

                          Text('$dsc1',
                              style:  GoogleFonts.roboto(fontSize: 14.0,
                                  fontWeight: FontWeight.w500,
                                  color: ColorResource.subtext,
                                  decoration: TextDecoration.none)),
                          Text('$dsc2',
                            style:  GoogleFonts.roboto(fontSize: 14.0,
                                fontWeight: FontWeight.w900,
                                color: ColorResource.subtext,
                                decoration: TextDecoration.none),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text('$dsc',
                                style:  GoogleFonts.roboto(fontSize: 14.0,
                                    fontWeight: FontWeight.w900,
                                    color: ColorResource.subtext,
                                    decoration: TextDecoration.none),
                              ),
                              IconButton(
                                icon: Icon(Icons.copy,
                                    color: HexColor('#8091A3')),
                                iconSize: 14.0,
                                onPressed: () => copy(),
                              ),
                            ],
                          ),
                          widget,
                          FilledColorButton(
                            fontSize: 14.0,
                            radius: 2.0,
                            innerPadding: 22,
                            verticalMargin: 0.0,
                            horizonatalMargin: 5.0,
                            textColor: ColorResource.colorWhite,
                            buttonText: localize('btn_save_tmp'),
                            onPressed: () {
                              process();
                            },
                            eventName: eventName,
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.pop(ctx);
                              cancel();
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(top: 10, bottom: 20.0),
                              child: Text(localize('skip_now'),
                                  style: TextStyle(
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.bold,
                                      color: ColorResource.colorMarineBlue,
                                      decoration: TextDecoration.underline)),
                            ),
                          ),

                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    left: padding,
                    right:padding,
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      radius: avatarRadius,
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(avatarRadius)),
                        child: SvgPicture.asset(
                          "svg_img/ic_success_green.svg",
                          height: 60,
                          width: 60,
                        ),
                      ),
                    ),
                  ),]),
          ),
        ),
      );

    },
    transitionBuilder: (_, anim, __, child) {
      Tween<Offset> tween;
      if (anim.status == AnimationStatus.reverse) {
        tween = Tween(begin: Offset(0, -1), end: Offset.zero);
      } else {
        tween = Tween(begin: Offset(0, 1), end: Offset.zero);
      }

      return SlideTransition(
        position: tween.animate(anim),
        child: FadeTransition(
          opacity: anim,
          child: child,
        ),
      );
    },
  );
}

void showCustomDialogWithImg(
    BuildContext context, String? title, String? description1,String? description2, String? description, Function? callback,Function? copy,
    {eventName, heading, withNegBtn = false}) {
  showGeneralDialog(
    context: context,
    barrierLabel: "Barrier",
    barrierDismissible: true,
    barrierColor: Colors.black.withOpacity(0.5),
    transitionDuration: Duration(milliseconds: 700),
    pageBuilder: (_, __, ___) {
      return  Material(
        type: MaterialType.transparency,
        child: Center(
          child: Stack(
              children: <Widget>[Container(

                width: 260,
                padding: EdgeInsets.only(left: padding,top: avatarRadius
                    , right: padding,bottom: padding
                ),
                margin: EdgeInsets.only(top: avatarRadius),
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(padding),
                    boxShadow: [
                      BoxShadow(color: Colors.black,offset: Offset(0,10),
                          blurRadius: 10
                      ),
                    ]
                ),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('$title',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 20,
                          fontFamily: 'sans',
                          fontWeight: FontWeight.w700,
                          color: ColorResource.headlineGrey,
                        ),),

                      isNullOrEmpty(heading)
                          ? SizedBox(height: 10,)
                          : Text(localize(heading),
                          style: GoogleFonts.roboto(
                              fontSize: 14.0,
                              fontWeight: FontWeight.w500,
                              color: ColorResource.colorMarineBlue,
                              decoration: TextDecoration.none)),
                      Text('$description1',
                          style:  GoogleFonts.roboto(fontSize: 14.0,
                          fontWeight: FontWeight.w500,
                          color: ColorResource.subtext,
                          decoration: TextDecoration.none)),
                      Text('$description2',
                          style:  GoogleFonts.roboto(fontSize: 14.0,
                              fontWeight: FontWeight.w900,
                              color: ColorResource.subtext,
                              decoration: TextDecoration.none),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('$description',
                            style:  GoogleFonts.roboto(fontSize: 14.0,
                                fontWeight: FontWeight.w900,
                                color: ColorResource.subtext,
                                decoration: TextDecoration.none),
                          ),
                          IconButton(
                            icon: Icon(Icons.copy,
                                color: HexColor('#8091A3')),
                            iconSize: 14.0,
                            onPressed: () => copy!(),
                          ),
                        ],
                      ),

                      withNegBtn
                          ? Row(
                        children: [
                          Expanded(
                            child: FilledColorButton(
                              fontSize: 16.0,
                              radius: 2.0,
                              horizonatalMargin: 5.0,
                              textColor: ColorResource.colorPrimary,
                              buttonText: translate(context, 'txt_btn_cancel'),
                              onPressed: () {
                                Navigator.pop(context, true);
                              },
                              backGroundColor: ColorResource.off_white,
                              eventName: eventName,
                            ),
                          ),
                          Expanded(
                            child: FilledColorButton(
                              fontSize: 16.0,
                              radius: 2.0,
                              horizonatalMargin: 5.0,
                              textColor: ColorResource.colorWhite,
                              buttonText: translate(context, 'txt_btn_ok'),
                              onPressed: () {
                                Navigator.pop(context, true);
                                callback!();
                              },
                              eventName: eventName,
                            ),
                          )
                        ],
                      )
                          : FilledColorButton(
                        fontSize: 16.0,
                        radius: 2.0,
                        horizonatalMargin: 5.0,
                        textColor: ColorResource.colorWhite,
                        buttonText: translate(context, 'txt_btn_ok'),
                        onPressed: () {
                          Navigator.pop(context, true);
                          callback!();
                        },
                        eventName: eventName,
                      )
                    ],
                  ),
                ),

              ),
                Positioned(
                  left: padding,
                  right:padding,
                  child: CircleAvatar(
                    backgroundColor: Colors.transparent,
                    radius: avatarRadius,
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(avatarRadius)),
                      child: SvgPicture.asset(
                        "svg_img/ic_success_green.svg",
                        height: 60,
                        width: 60,
                      ),
                    ),
                  ),
                ),]
          ),
        ),
      );
    },
    transitionBuilder: (_, anim, __, child) {
      Tween<Offset> tween;
      if (anim.status == AnimationStatus.reverse) {
        tween = Tween(begin: Offset(0, -1), end: Offset.zero);
      } else {
        tween = Tween(begin: Offset(0, 1), end: Offset.zero);
      }

      return SlideTransition(
        position: tween.animate(anim),
        child: FadeTransition(
          opacity: anim,
          child: child,
        ),
      );
    },
  );
}