class AnalyticsEvents {

  static const ADMIN_LOGIN_SCREEN = "Login_Screen";
  static const ACC_FEATURE_SCREEN = "Accessible_Feature_Screen";
  static const CREATE_TRIP_SCREEN = "Create_Trip_Screen";
  static const UPLOAD_CHALLAN_SCREEN = "Upload_Challan_Screen";
  static const TRIP_LIST_SCREEN = "Trip_List_Screen";
  static const BOOKED_LIST_SCREEN = "Booked_Trip_List_Screen";
  static const Customer_CNF_STG_SCREEN = "Customer_Configuration_Setting_Screen";

  static const CUSTOMER_SEARCH_SCREEN = "Customer_Search_Screen";

  static const CUSTOMER_CONFIG_SCREEN = "Customer_Configuration_Setting_Screen";
  static const TEMPLATE_LIST_SCREEN = "Template_List_Screen";

  static const LOGIN_FAILED = "Login_Failed";
  static const LOGIN_SUCCESS = "Login_Success";
  static const LOGOUT_FAILED = "Logout_Failed";
  static const LOGOUT_SUCCESS = "Logout_Success";
  static const LOGIN_BTN_CLK = "Login_Btn_Click";
  static const LOGOUT_BTN_CLK = "Logout_Btn_Click";

  static const TRIP_ITEM_CLICK = "Trip_Item_Click";
  static const TRIP_STATUS_CLICK = "Trip_Status_Btn_Click";
  static const TRIP_DETAIL_SCREEN = "Trip_Detail_Screen";

  static const ALL_TRIP_BTN_CLK = "All_Trips_Btn_Click";
  static const BID_TRIP_BTN_CLK = "Bidded_Trips_Btn_Click";
  static const BOOKED_TRIP_BTN_CLK = "Booked_Trips_Btn_Click";
  static const LIVE_TRIP_BTN_CLK = "Live_Trips_Btn_Click";

  static const CREATE_TRIP_BTN_CLICK = "Create_Trip_Btn_Click";
  static const SAVE_CONFIG_BTN_CLICK = "Save_Config_Btn_Click";
  static const CREATE_TEMP_BTN_CLICK = "Create_Template_Btn_Click";

  static const TRIP_CREATE_SUCCESS = "Trip_Create_Success";
  static const TRIP_CREATE_FAILED = "Trip_Create_Failed";
  static const TRIP_CREATE_SUCCESS_DLG = "Trip_Create_Success_Dialog";

  static const TMP_CREATE_SUCCESS = "Template_Create_Success";
  static const TMP_CREATE_FAILED = "Template_Create_Failed";
  static const TMP_CREATE_SUCCESS_DLG = "Template_Create_Success_Dialog";

  static const CONFIG_SAVE_SUCCESS = "Config_Save_Success";
  static const CONFIG_SAVE_FAILED = "Config_Save_Failed";

  static const GOODS_TYPE_ITEM_CLK = "Goods_Type_Item_Click";
  static const GOODS_TYPE_BTN_CLK = "Goods_Type_Btn_Click";

  static const LOADING_POINT_ITEM_CLK = "Loading_Point_Item_Click";
  static const LOADING_POINT_BTN_CLK = "Loading_Point_Btn_Click";
  static const UNLOADING_POINT_ITEM_CLK = "Unloading_Point_Item_Click";
  static const UNLOADING_POINT_BTN_CLK = "Unloading_Point_Btn_Click";

  static const ADD_UNLOAD_POINT_BTN_CLK = "Multiple_Unload_Point_Btn_Click";

  static const LOAD_TIME_HOUR_BTN_CLK = "Loading_Time_Hour_Btn_Click";
  static const CALENDER_BTN_CLK = "Calender_Btn_Click";
  static const TODAY_TOM_BTN_CLK = "_Btn_Click";
  static const TRUCK_TYPE_ITM_CLK = "Truck_Type_Item_Click";
  static const TRUCK_DIMEN_BTN_CLK = "Truck_Dimen_Btn_Click";

  static const TRUCK_Size_BTN_CLK = "Truck_Size_Btn_Click";
  static const TRUCK_Length_BTN_CLK = "Truck_Length_Btn_Click";

  static const NO_TRUCK_PLUS_BTN_CLK = "No_Of_Truck_Plus_Btn_Click";
  static const NO_TRUCK_MINUS_BTN_CLK = "No_Of_Truck_Minus_Btn_Click";

  static const ADD_NEW_INST_BTN_CLK = "Add_New_Instruction_Btn_Click";
  static const INST_EDIT_BTN_CLK = "Instruction_Edit_Btn_Click";
  static const INST_SAVE_BTN_CLK = "Instruction_Save_Btn_Click";
  static const INST_DEL_BTN_CLK = "Instruction_Delete_Btn_Click";
  static const INST_SEL_BTN_CLK = "Instruction_CheckBox_Btn_Click";
  static const STATUS_UPDATE_BTN_CLK = "Status_Update_Btn_Click";
  static const TRUCK_DRIVER_UPDATE_BTN_CLK = "Truck_Driver_Update_Btn_Click";

  static const START_TRIP_SUCCESS_DLG = "Start_Trip_Success_Dialog";
  static const START_TRIP_ERROR = "Start_Trip_Error";
}

class AnalyticProperties{
  static const TRIP_ID = "Trip_Id";
  static const GOODS_ID = "Goods_Id";
  static const TRUCK_TYPE = "Truck_Type";
  static const TRUCK_SIZE = "Truck_Size";
  static const TRUCK_Length = "Truck_Length";
  static const TRUCK_DIMEN = "Truck_Dimen";
  static const TRIP_STATUS = "Trip_Status";
  static const ADDRESS = "address";
  static const USER_EMAIL = "User_Email";
}

class UserProperties {
  static const USER_DISTRICT = "User_District";
  static const USER_ROLE = "User_Role";
  static const USER_ID = "User_Id";
  static const USER_NAME = "name";
  static const USER_MOBILE = "User_Mobile";
  static const EMAIL = "\$email";
  static const USER_EMAIL = "User_Email";
  static const USER_TYPE = "Admin";
  static const USER_REGULAR = "Regular";
  static const USER_ENTERPRISE = "Enterprise";
}