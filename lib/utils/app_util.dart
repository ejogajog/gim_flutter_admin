import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:app/ui/auth/login_response.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:mailto/mailto.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/foundation.dart';

isEnterprise(List<UserRolesItem> userRoles) {
  for (UserRolesItem uri in userRoles) {
    if (uri.id == 5 && (uri.enterprise??false)) return true;
    continue;
  }
  return false;
}

void launchEmailSubmission(content) async {
  final Email email = Email(
    body: content,
    subject: 'Payment approval request',
    recipients: ['krish.prvn@gmail.com'],
    isHTML: false,
  );
  await FlutterEmailSender.send(email);
  /*await FlutterEmailSender.send(email);
  final Uri params = Uri(
      scheme: 'mailto',
      path: 'krish.prvn@gmail.com',
      query: Uri.encodeFull(
          'subject=Payment approval request&body= $content'));
  String url = params.toString();
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    print('Could not launch $url');
  }*/
}

amountWithCurrencySign(amount, {shouldShowCurrencySign = true}) {
  var currencyFormat = NumberFormat("#,##0", 'en');
  return (shouldShowCurrencySign ? "৳ " : "") +
      currencyFormat.format(amount ?? 0.00).replaceAll("BDT", "").replaceAll("৳", "");
}

launchWebEmail(content) async{
  print('Mail content is $content');
  final url = Mailto(
    to: [
      'krish.prvn@gmail.com',
      'asingh.ejogajog@gmail.com',
    ],
    cc: [
      'krish.prvn@gmail.com',
      'asingh.ejogajog@gmail.com',
    ],
    bcc: [
      'asingh.ejogajog@gmail.com',
    ],
    subject: 'Payment approval request',
    body:content,
  ).toString();
  await launch(url);
}

final isWebMobile = kIsWeb && (defaultTargetPlatform == TargetPlatform.iOS || defaultTargetPlatform == TargetPlatform.android);
