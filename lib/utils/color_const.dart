import 'package:flutter/material.dart';

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

class ColorResource {
  static const colorPrimary = Color.fromRGBO(0, 50, 80, 1.0);
  static var colorMariGold = Color.fromRGBO(255, 200, 0, 1.0);
  static var colorWhite = HexColor("ffffff");
  static var divider_light = HexColor("fafafa");
  static var colorAccent = HexColor("002439");
  static var colorPrimaryDark = HexColor("003250");
  static var colorLightMarineBlue = HexColor("006789");
  static const divider_color = Color.fromRGBO(198, 202, 209, 1.0);
  static const orange_yellow = Color.fromRGBO(255, 189, 39, 1.0);
  static const colorMarineBlue = Color.fromRGBO(0, 50, 80, 1.0);
  static const colorMarineBlueAlpha = Color.fromRGBO(0, 50, 80, 0.7);
  static const dark_yellow = Color.fromRGBO(181, 152, 36, 1.0);
  static const colorMarineBlueLight = Color.fromRGBO(0, 162, 255, 1);
  static var colorMariGoldTran = Color.fromRGBO(255, 200, 0, 0.7);
  static const brownish_grey = Color.fromRGBO(100, 100, 100, 1);
  static const light_grey = Color.fromRGBO(230, 230, 230, 1);
  static const light_white = Color.fromRGBO(232, 232, 232, 1.0);
  static var warm_grey = HexColor("9b9b9b");
  static var colorFadedBlue = HexColor("609aba");
  static const colorBrownGrey = Color.fromRGBO(168, 168, 168, 1);
  static var colorBlack = HexColor("#000000");
  static var ColorDropOffBlue = HexColor("#609aba");
  static var colorTransparent = HexColor("00FFFFFF");
  static var marigold_opacity_20 = HexColor("#90ffc800");
  static var greyish_brown = HexColor("#ff4a4a4a");
  static var marigold_alpha = HexColor("ffffc800");
  static var labelColor = HexColor("#646464");
  static var greyIshBrown = HexColor("#4a4a4a");
  static var veryLightPink = HexColor("#dddddd");
  static var dropOffBlue = HexColor("#609aba");
  static var lightBlue = HexColor("#e8eff3");
  static var warmGrey = Color.fromRGBO(155, 155, 155, 1);
  static var lightBlueGrey = HexColor("#c6cad1");
  static var marigold_two = Color.fromRGBO(255, 189, 0, 1);
  static var marigoldAlpha = HexColor("#90ffbd00");
  static var buttonBgColor = HexColor("#262699fb");
  static var semi_transparent_color_light = HexColor('96003250');
  static var white_gray = Color.fromRGBO(244, 244, 244, 1);
  static var marine_blue_two = HexColor('79003250');
  static var grey_white = Color.fromRGBO(246, 246, 246, 1);
  static const trip_above_24hourBg = Color.fromRGBO(216, 0, 0, 1);
  static const not_read_notification = Color.fromRGBO(216, 223, 227, 1);
  static var dark_gray = HexColor('ff808080');
  static const colorLightBlueGrey = Color.fromRGBO(198, 202, 209, 1);
  static var very_light_blue = HexColor('e6ebee');
  static var pink_red = HexColor('ea1654');
  static var off_white = Color.fromRGBO(255, 255, 244, 1);
  static var lightBlu = Color.fromRGBO(136, 170, 203, 1);
  static var darkBackground = Color.fromRGBO(17, 29, 33, 1);
  static var bluBg = Color.fromRGBO(214, 222, 225, 1);
  static var lblClr = Color.fromRGBO(66, 123, 251, 1);
  static const txtClr = Color.fromRGBO(3, 50, 69, 1);
  static var lightBluBg = Color.fromRGBO(248, 250, 255, 1);
  static var transBluBg = Color.fromRGBO(235, 239, 240, 1);
  static var lightWhite = Color.fromRGBO(251, 251, 251, 1);
  static var lblBlueTxtClr = Color.fromRGBO(16, 118, 162, 1);
  static const iconClr = Color.fromRGBO(180, 197, 204, 1);
  static const lightGreen = Color.fromRGBO(244, 251, 242, 1);
  static const lightTrnBlue = Color.fromRGBO(242, 245, 246, 1);
  static const trnBlue = Color.fromRGBO(203, 218, 254, 1);
  static const disBtn = Color.fromRGBO(203, 218, 233, 1);
  static const btnColor = Color.fromRGBO(106, 157, 177, 1);
  static const cta_btn = Color.fromRGBO(0, 50, 80, 1);
  static const silver_chalice = Color.fromRGBO(159, 159, 159, 1);
  static const primary_text = Color.fromRGBO(41, 42, 41, 1);
  static const headlineGrey = Color.fromRGBO(77, 89, 131, 1);
  static const subtext = Color.fromRGBO(134, 141, 170, 1);


  //todo:New Color Added
  static var CreamWhite = HexColor("#FFFFF3");
  static var platinum = HexColor('#E3E3E3');
}
