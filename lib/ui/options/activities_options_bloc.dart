import 'package:app/api/api_response.dart';
import 'package:app/bloc/base_bloc.dart';
import 'package:app/ui/auth/auth_repository.dart';
import 'package:app/ui/auth/logout_response.dart';
import 'package:app/ui/options/features_model.dart';
import 'package:app/utils/prefs.dart';

class ActivityOptionsBloc extends BaseBloc {
  final FeaturesModel? _featuresModel;

  ActivityOptionsBloc(this._featuresModel);

  ApiResponse? _logoutResponse;
  ApiResponse? get logoutResponse => _logoutResponse;
  set logoutResponse(ApiResponse? value) {
    _logoutResponse = value;
  }

  get listOfFeatures => _featuresModel == null ? [] : _featuresModel?.features();

  signOut() async {
    print("Inside logout");
    isLoading = true;
    var response = await AuthRepository.signOut();
    logoutResponse = response;
    isLoading = false;
    switch (response.status) {
      case Status.LOADING:
        break;
      case Status.COMPLETED:
        print("Logout success");
        var logoutResponse = LogoutResponse.fromJson(response.data);
        if (logoutResponse.status??false) {
          Prefs.setBoolean(Prefs.IS_LOGGED_IN, false);
          Prefs.setBoolean(Prefs.IS_DISTRIBUTOR, false);
          Prefs.setBoolean(Prefs.PREF_IS_ENTERPRISE_USER, false);
        }
        break;
      case Status.ERROR:
        print("Logout ERROR");
        break;
    }
    return response;
  }

}
