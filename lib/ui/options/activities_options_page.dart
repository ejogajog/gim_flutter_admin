import 'package:flutter/material.dart';
import 'package:app/api/api_response.dart';
import 'package:app/ui/auth/login_screen.dart';
import 'package:app/ui/options/features_model.dart';
import 'package:app/ui/widget/app_button.dart';
import 'package:app/ui/widget/app_icon_box.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/navigation_util.dart';
import 'package:app/utils/prefs.dart';
import 'package:app/utils/ui_util.dart';
import 'package:provider/provider.dart';
import 'package:app/ui/widget/base_widget.dart';
import 'package:app/ui/options/activities_options_bloc.dart';

class ActivityOptionPage extends StatelessWidget {
  final FeaturesModel? _featuresModel;

  ActivityOptionPage(this._featuresModel);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ActivityOptionsBloc>(
            create: (_) => ActivityOptionsBloc(_featuresModel)),
      ],
      child: ActivityOptionScreen(),
    );
  }
}

class ActivityOptionScreen extends StatefulWidget {
  @override
  _ActivityOptionScreenState createState() => _ActivityOptionScreenState();
}

class _ActivityOptionScreenState
    extends BasePageWidgetState<ActivityOptionScreen, ActivityOptionsBloc> {
  @override
  List<Widget> getPageWidget() {
    return [
      Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: CustomButton(
                  iconSize: 16,
                  prefixIcon: Icons.power_settings_new_rounded,
                  text: 'Log Out',
                  onPressed: () async{
                    AnalyticsManager().logEventProp(AnalyticsEvents.LOGOUT_BTN_CLK,{UserProperties.USER_EMAIL: await Prefs.getStringAsync(Prefs.PREF_USER_EMAIL)});
                    _onLogoutButtonPressed();
                  },
                ),
              )
            ],
          ),
          Expanded(
            child: Center(
                child: SingleChildScrollView(
                    child: LayoutBuilder(
                        builder: (context, constraint) =>
                            isMobileBrowser(constraint)
                                ? Wrap(
                                    spacing: 20.0,
                                    runSpacing: 20.0,
                                    alignment: WrapAlignment.center,
                                    children: _featureOptions(),
                                  )
                                : _webResponsiveAppIcons(
                                    bloc!.listOfFeatures.length)))),
          ),
        ],
      )
    ];
  }

  List<Widget> _featureOptions() {
    return bloc!.listOfFeatures
        .map<Widget>((element) => AppBoxIcon(element))
        .toList();
  }

  Widget _webResponsiveAppIcons(int length) {
    List<FeatureItem> items = bloc!.listOfFeatures;
    return length <= 4
        ? Column(
            children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  AppBoxIcon(items.first),
                  SizedBox(width: 20.0),
                  AppBoxIcon(items.elementAt(1))
                ],
              ),
              SizedBox(height: 20.0),
            ]..add(length == 3
                ? Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      AppBoxIcon(items.elementAt(2)),
                    ],
                  )
                : length == 2
                    ? SizedBox()
                    : Column(
                        children: [
                          SizedBox(height: 20.0),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              AppBoxIcon(items.elementAt(2)),
                              SizedBox(width: 20.0),
                              AppBoxIcon(items.elementAt(3)),
                            ],
                          )
                        ],
                      )),
          )
        : Column(
            children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  AppBoxIcon(items.first),
                  SizedBox(width: 20.0),
                  AppBoxIcon(items.elementAt(1)),
                  SizedBox(width: 20.0),
                  AppBoxIcon(items.elementAt(2)),
                ],
              ),
              SizedBox(height: 20.0),
            ]..add(length == 5
                ? Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      AppBoxIcon(items.elementAt(3)),
                      SizedBox(width: 20.0),
                      AppBoxIcon(items.elementAt(4)),
                    ],
                  )
                : Column(
                    children: [
                      SizedBox(height: 20.0),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          AppBoxIcon(items.elementAt(3)),
                          SizedBox(width: 20.0),
                          AppBoxIcon(items.elementAt(4)),
                          SizedBox(width: 20.0),
                          AppBoxIcon(items.elementAt(5)),
                        ],
                      )
                    ],
                  )),
          );
  }

  _onLogoutButtonPressed() async {
    submitDialog(context, dismissible: false);
    var response = await bloc!.signOut();
    Navigator.pop(context);
    if (response.status == Status.COMPLETED) {
      AnalyticsManager().logEventProp(AnalyticsEvents.LOGOUT_SUCCESS,{UserProperties.USER_EMAIL: await Prefs.getStringAsync(Prefs.PREF_USER_EMAIL)});
      navigateNextScreen(context, LoginScreenContainer(),
          shouldFinishPreviousPages: true, pageName: AnalyticsEvents.ADMIN_LOGIN_SCREEN);
    } else{
      showCustomSnackbar(response.message);
      AnalyticsManager().logEventProp(AnalyticsEvents.LOGOUT_FAILED,{UserProperties.USER_EMAIL: await Prefs.getStringAsync(Prefs.PREF_USER_EMAIL)});
    }
  }

  @override
  scaffoldBackgroundColor() => ColorResource.darkBackground;
}
