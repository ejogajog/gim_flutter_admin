// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'features_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeaturesModel _$FeaturesModelFromJson(Map<String, dynamic> json) =>
    FeaturesModel()
      ..userId = (json['userId'] as num?)?.toInt()
      ..canSeeTrip = json['canSeeTrip'] as bool?
      ..canStartTrip = json['canStartTrip'] as bool?
      ..canCreateTrip = json['canCreateTrip'] as bool?
      ..canDoTripConfig = json['canDoTripConfig'] as bool?
      ..canUploadChallan = json['canUploadChallan'] as bool?;

Map<String, dynamic> _$FeaturesModelToJson(FeaturesModel instance) =>
    <String, dynamic>{
      'userId': instance.userId,
      'canSeeTrip': instance.canSeeTrip,
      'canStartTrip': instance.canStartTrip,
      'canCreateTrip': instance.canCreateTrip,
      'canDoTripConfig': instance.canDoTripConfig,
      'canUploadChallan': instance.canUploadChallan,
    };
