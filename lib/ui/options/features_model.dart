import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/app_constants.dart';
import 'package:json_annotation/json_annotation.dart';

part 'features_model.g.dart';

@JsonSerializable()
class FeaturesModel {
  int? userId;
  bool? canSeeTrip;
  bool? canStartTrip;
  bool? canCreateTrip;
  bool? canDoTripConfig;
  bool? canUploadChallan;

  FeaturesModel();

  factory FeaturesModel.fromJson(Map<String, dynamic> json) =>
      _$FeaturesModelFromJson(json);

  Map<String, dynamic> toJson() => _$FeaturesModelToJson(this);

  List<FeatureItem> features() {
    List<FeatureItem> features = <FeatureItem>[];
    if (canCreateTrip??false)
      features.add(FeatureItem('Create Trips', RouteConstants.createTrip,
          'Insert your text here', null, AnalyticsEvents.CREATE_TRIP_SCREEN));
    if (canSeeTrip??false)
      features.add(FeatureItem('Update Call Status', RouteConstants.viewTrip,
          'Insert your text here', null, AnalyticsEvents.TRIP_LIST_SCREEN));
    if (canDoTripConfig??false)
      features.add(FeatureItem(
          'Customer Configuration Setting',
          RouteConstants.customerConfig,
          'Insert your text here',
          null,
          AnalyticsEvents.Customer_CNF_STG_SCREEN));
    if (canStartTrip ?? true)
      features.add(FeatureItem('Update Trip Status', RouteConstants.bookedTrips,
          'Insert your text here', null, AnalyticsEvents.BOOKED_LIST_SCREEN));
    return features;
  }
}

class FeatureItem {
  String? title;
  String? pageUrl;
  String? description;
  String? bgFeatureImg;
  String? eventName;

  FeatureItem(this.title, this.pageUrl, this.description, this.bgFeatureImg,
      this.eventName);
}
