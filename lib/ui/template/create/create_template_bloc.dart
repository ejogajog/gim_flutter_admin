import 'package:flutter/cupertino.dart';
import 'package:app/api/api_response.dart';
import 'package:app/bloc/base_bloc.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/ui/template/create/template_model.dart';
import 'package:app/ui/template/repo/template_repo.dart';
import 'package:app/ui/trip/create/model/customer_model.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/ui/trip/create/model/preset_instruction.dart';
import 'package:app/ui/trip/create/model/truck_info.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/prefs.dart';
import 'package:app/utils/string_util.dart';
import 'package:app/model/master/goods_type.dart';

class CreateTemplateBloc extends BaseBloc {
  bool? isTempEdit;
  TruckInfo? _truckInfo;
  bool _isEdited = true;
  bool _validGood = true;
  bool _inValidTemp = true;
  CustomerInfo _customerInfo;
  TemplateModel? _template;
  CustomerPreferences? _customerPreferences;
  final txtEdtController = TextEditingController();

  CreateTemplateBloc(this._customerInfo, this._customerPreferences) {
    isTempEdit = !isNullOrEmptyInt(_customerPreferences?.templateId);
    _isEdited = !isTempEdit!;
    _inValidTemp = _isEdited;
    _template = TemplateModel();
    _template?.name = _customerPreferences?.templateName;
    txtEdtController.text = _template?.name??'';
    _template?.templateId = _customerPreferences?.templateId;
    _customerPreferences?.companyId = _customerInfo.companyId;
    _template?.quantityOfGoods = customerPreset?.qntOfGoods;
    if(_customerPreferences != null){
      if (!isNullOrEmptyList(customerPreset?.defaultUpList)) {
        _template?.unloadPoints = customerPreset?.defaultUpList;
      }
      if(!isNullOrEmptyList(_customerPreferences?.instructionSuggestions))
        _template?.instructions = _customerPreferences?.instructionSuggestions;
    }
  }

  onTemplateValidated(templateName){
    _inValidTemp = isNullOrEmpty(templateName);
    _template?.tempName = templateName;
    notifyIns();
  }

  onSelGoods(GoodsType? selGoods) {
    _template?.goodsId = selGoods?.id ?? 0;
  }

  get loadPoint => _template?.loadPoint;

  get unloadPoints => _template?.unloadPoints;

  notifyIns() {
    _isEdited = true;
    notifyListeners();
  }

  get isActivate => _isEdited;

  truckInfo(TruckInfo? truckDtl) {
    _truckInfo = truckDtl;
    _template?.trkType = _truckInfo?.type?.id;
    _template?.trkSize = _truckInfo?.size?.id;
    _template?.truckLen = _truckInfo?.length == null
        ? 0
        : double.tryParse(_truckInfo?.length.value);
  }

  updateIns(index, content) {
    _template?.instructions?.elementAt(index).instruction = content;
  }

  goodsFlag(value) {
    _validGood = value;
  }

  goodsQnt(value) {
    _template?.goodsQnt = int.tryParse(value);
  }

  goodsUnit(unit) {
    _template?.goodsUnit = unit;
  }

  get templateName => txtEdtController.text;

  get instructions => _template?.instructions;

  get truckInf => _truckInfo ?? TruckInfo();

  get isEnterprise => !isNullOrEmptyInt(_customerInfo.companyId);

  get goodsQty => _template?.quantityOfGoods;

  CustomerPreferences? get customerPreset => _customerPreferences;

  get cmpId => _customerInfo.companyId;

  get client =>
      '${_customerInfo.userName}-${_customerInfo.mobileNumber} (${_customerInfo.companyName ?? localize('regular')})';

  saveTemplate() async {
    validInstructions();
    _template?.companyId = cmpId;
    _template?.tempName = txtEdtController.text.trim();
    _template?.buildAddressPoints();
    print(_template?.toJson());
    ApiResponse apiResponse = isTempEdit??false
        ? await TemplateRepository.editTemplate(_template)
        : await TemplateRepository.addTemplate(_template);
    if (apiResponse.status == Status.ERROR) {
      _template?.tripDropoffList?.removeLast();
    }
    return apiResponse;
  }

  validInstructions() {
    _template?.instructions
        ?.retainWhere((element) => element.instruction?.trim().length != 0);
  }

  validateFields() {
    if (isNullOrEmpty(_template?.name) || _inValidTemp) {
      return isNullOrEmpty(txtEdtController.text) ? 'tmp_name_error' : 'tmp_exist_error';
    } else if (_template?.goodsTypeId == null || _template?.goodsTypeId == 0 || !_validGood) {
      return 'goods_emp_error';
    } else if (isNullOrEmpty(_template?.loadPoint?[0]?.address)) {
      return 'src_emp_error';
    } else if (_isInvalidUnloadPoint()) {
      return 'dst_emp_error';
    } else if (truckInf.type == null) {
      return 'truck_type_error';
    } else if (truckInf.size == null) {
      return 'truck_size_error';
    } else if (_saveIns()) {
      return 'ins_save_error';
    } else if (!validInstLength()) {
      return 'ins_len_error';
    }
  }

  validInstLength() {
    int length = 0;
    _template?.instructions?.forEach((element) {
      length += element.toString().trim().length;
    });
    return length <= insMaxCharCount;
  }

  _isInvalidUnloadPoint() {
    int? size = _template?.unloadPoints?.length;
    for (int loop = 0; loop < size!; loop++) {
      if (isNullOrEmpty(_template?.unloadPoints?[loop]?.address)) return true;
    }
    return false;
  }

  _saveIns() {
    for (int index = 0; index < _template!.instructions!.length; index++) {
      PresetInstruction instruction = _template!.instructions![index];
      if (!(instruction.isSaved ?? true)) return true;
    }
    return false;
  }

  get headerText => isTempEdit??false ? 'lbl_edit_temp' : 'lbl_new_temp';
  get successText => isTempEdit??false ? 'edit_template_suc_lbl' : 'new_template_suc_lbl';
  get buttonText => isTempEdit??false ? 'btn_edit_temp' : 'btn_new_temp';

}
