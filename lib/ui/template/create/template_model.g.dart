// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'template_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TemplateModel _$TemplateModelFromJson(Map<String, dynamic> json) =>
    TemplateModel()
      ..name = json['name'] as String?
      ..templateId = (json['templateId'] as num?)?.toInt()
      ..companyId = (json['companyId'] as num?)?.toInt()
      ..goodsTypeId = (json['goodsTypeId'] as num?)?.toInt()
      ..truckSizeId = (json['truckSizeId'] as num?)?.toInt()
      ..truckTypeId = (json['truckTypeId'] as num?)?.toInt()
      ..truckLength = (json['truckLength'] as num?)?.toDouble()
      ..pickUpFactoryId = (json['pickUpFactoryId'] as num?)?.toInt()
      ..pickUpDepotId = (json['pickUpDepotId'] as num?)?.toInt()
      ..pickUplat = (json['pickUplat'] as num?)?.toDouble()
      ..pickUplon = (json['pickUplon'] as num?)?.toDouble()
      ..dropOfflat = (json['dropOfflat'] as num?)?.toDouble()
      ..dropOfflon = (json['dropOfflon'] as num?)?.toDouble()
      ..pickUpAddress = json['pickUpAddress'] as String?
      ..pickUpDistrictId = (json['pickUpDistrictId'] as num?)?.toInt()
      ..dropOffDistrictId = (json['dropOffDistrictId'] as num?)?.toInt()
      ..dropoffAddress = json['dropoffAddress'] as String?
      ..dropOffFactoryId = (json['dropOffFactoryId'] as num?)?.toInt()
      ..dropOffDepotId = (json['dropOffDepotId'] as num?)?.toInt()
      ..noOfUnloadPoints = (json['noOfUnloadPoints'] as num?)?.toInt()
      ..goodType = json['goodType'] == null
          ? null
          : GoodsType.fromJson(json['goodType'] as Map<String, dynamic>)
      ..truckType = json['truckType'] == null
          ? null
          : TruckType.fromJson(json['truckType'] as Map<String, dynamic>)
      ..truckSize = json['truckSize'] == null
          ? null
          : TruckSize.fromJson(json['truckSize'] as Map<String, dynamic>)
      ..instructions = (json['instructions'] as List<dynamic>?)
          ?.map((e) => PresetInstruction.fromJson(e as Map<String, dynamic>))
          .toList()
      ..tripDropoffList = (json['tripDropoffList'] as List<dynamic>?)
          ?.map((e) => TripLocation.fromJson(e as Map<String, dynamic>))
          .toList()
      ..quantityOfGoods = (json['quantityOfGoods'] as num?)?.toDouble()
      ..goodsQuantityUnit = json['goodsQuantityUnit'] as String?;

Map<String, dynamic> _$TemplateModelToJson(TemplateModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'templateId': instance.templateId,
      'companyId': instance.companyId,
      'goodsTypeId': instance.goodsTypeId,
      'truckSizeId': instance.truckSizeId,
      'truckTypeId': instance.truckTypeId,
      'truckLength': instance.truckLength,
      'pickUpFactoryId': instance.pickUpFactoryId,
      'pickUpDepotId': instance.pickUpDepotId,
      'pickUplat': instance.pickUplat,
      'pickUplon': instance.pickUplon,
      'dropOfflat': instance.dropOfflat,
      'dropOfflon': instance.dropOfflon,
      'pickUpAddress': instance.pickUpAddress,
      'pickUpDistrictId': instance.pickUpDistrictId,
      'dropOffDistrictId': instance.dropOffDistrictId,
      'dropoffAddress': instance.dropoffAddress,
      'dropOffFactoryId': instance.dropOffFactoryId,
      'dropOffDepotId': instance.dropOffDepotId,
      'noOfUnloadPoints': instance.noOfUnloadPoints,
      'goodType': instance.goodType,
      'truckType': instance.truckType,
      'truckSize': instance.truckSize,
      'instructions': instance.instructions,
      'tripDropoffList': instance.tripDropoffList,
      'quantityOfGoods': instance.quantityOfGoods,
      'goodsQuantityUnit': instance.goodsQuantityUnit,
    };
