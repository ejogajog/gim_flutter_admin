import 'package:app/model/master/goods_type.dart';
import 'package:app/model/master/truck_size.dart';
import 'package:app/model/master/truck_type.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/ui/trip/create/model/trip_model.dart';
import 'package:app/ui/trip/create/model/preset_instruction.dart';
import 'package:json_annotation/json_annotation.dart';

part 'template_model.g.dart';

@JsonSerializable()
class TemplateModel{

  TemplateModel();
  String? name;
  int? templateId;
  int? companyId;
  int? goodsTypeId;
  int? truckSizeId;
  int? truckTypeId;
  double? truckLength;
  int? pickUpFactoryId;
  int? pickUpDepotId;
  double? pickUplat;
  double? pickUplon;
  double? dropOfflat;
  double? dropOfflon;
  String? pickUpAddress;
  int? pickUpDistrictId;
  int? dropOffDistrictId;
  String? dropoffAddress;
  int? dropOffFactoryId;
  int? dropOffDepotId;
  @JsonKey(ignore: true)
  String? pickUpName;
  @JsonKey(ignore: true)
  String? dropOffName;
  int? noOfUnloadPoints;
  GoodsType? goodType;
  TruckType? truckType;
  TruckSize? truckSize;
  List<PresetInstruction>? instructions = [];
  List<TripLocation>? tripDropoffList = [];
  double? quantityOfGoods;
  String? goodsQuantityUnit;
  @JsonKey(ignore: true)
  List<BaseAddressesSuggestion?>? loadPoint = [null];
  @JsonKey(ignore: true)
  List<BaseAddressesSuggestion?>? unloadPoints = [null];

  TemplateModel.withTrip(this.name, this.companyId, this.goodsTypeId, this.truckTypeId,this.truckSizeId,this.truckLength,this.pickUplat, this.pickUplon, this.pickUpAddress, this.pickUpFactoryId, this.pickUpDepotId, this.dropOfflat, this.dropOfflon, this.dropoffAddress, this.dropOffFactoryId, this.dropOffDepotId, this.instructions, this.tripDropoffList, this.quantityOfGoods, this.goodsQuantityUnit);

  factory TemplateModel.fromJson(Map<String, dynamic> json) => _$TemplateModelFromJson(json);

  Map<String, dynamic> toJson() => _$TemplateModelToJson(this);

  set tempName(tempName){
    name = tempName;
  }

  set goodsId(int selGoodsId) {
    goodsTypeId = selGoodsId;
  }

  set trkType(int? selTruckType) {
    truckTypeId = selTruckType;
  }

  set trkSize(int? selTruckSize) {
    truckSizeId = selTruckSize;
  }

  set truckLen(double? selTruckLen) {
    truckLength = selTruckLen;
  }

  set dstAddress(String selDstAdr) {
    dropoffAddress = selDstAdr;
  }

  set loadingPoint(address){
    pickUplat = address.lat;
    pickUplon = address.lon;
    pickUpName = address.name;
    pickUpAddress = address.address;
    if (address.depot ?? false) {
      pickUpDepotId = address.id ?? 0;
      pickUpFactoryId = 0;
    } else {
      pickUpDepotId = 0;
      pickUpFactoryId = address.id ?? 0;
    }
  }

  set unloadingPoint(address){
    dropOfflat = address.lat;
    dropOfflon = address.lon;
    dropOffName = address.name;
    dropoffAddress = address.address;
    if (address.depot ?? false) {
      dropOffDepotId = address.id ?? 0;
      dropOffFactoryId = 0;
    } else {
      dropOffDepotId = 0;
      dropOffFactoryId = address.id ?? 0;
    }
  }

  buildAddressPoints(){
    pickUplat = loadPoint?[0]?.lat;
    pickUplon = loadPoint?[0]?.lon;
    pickUpDepotId = loadPoint?[0]?.depot ?? false ? loadPoint![0]?.id : 0;
    pickUpFactoryId = loadPoint?[0]?.depot ?? true ? 0 : loadPoint?[0]?.id;
    pickUpName = loadPoint?[0]?.name;
    pickUpAddress = loadPoint?[0]?.address;
    tripDropoffList = [];
    unloadPoints?.forEach((element) {
      tripDropoffList?.add(element?.tripAddress);
    });
  }

  clearDropAddress() {
    dropOfflat = null;
    dropOfflon = null;
    dropOffDepotId = 0;
    dropOffFactoryId = 0;
    dropOffName = null;
    dropoffAddress = null;
  }

  set goodsQnt(value) {
    quantityOfGoods = value;
  }

  set goodsUnit(unit) {
    goodsQuantityUnit = unit;
  }

}