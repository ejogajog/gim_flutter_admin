import 'package:flutter/material.dart';
import 'package:app/api/api_response.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/ui/template/create/create_template_bloc.dart';
import 'package:app/ui/template/widget/template_info_widget.dart';
import 'package:app/ui/trip/create/config/widget/instruction_widget.dart';
import 'package:app/ui/trip/create/model/customer_model.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/ui/widget/app_button.dart';
import 'package:app/ui/widget/base_widget.dart';
import 'package:app/ui/widget/goods/goods_unit_widget.dart';
import 'package:app/ui/widget/trip_address_widget.dart';
import 'package:app/ui/widget/trip_product_widget.dart';
import 'package:app/ui/widget/truck/truck_info_widget.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/string_util.dart';
import 'package:app/utils/ui_util.dart';
import 'package:provider/provider.dart';

class CreateTemplateScreen extends StatelessWidget {
  final CustomerInfo cstInfo;
  final CustomerPreferences cstPref;

  CreateTemplateScreen(this.cstInfo, this.cstPref);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<CreateTemplateBloc>(
            create: (_) => CreateTemplateBloc(this.cstInfo, this.cstPref)),
      ],
      child: _CreateTemplatePage(),
    );
  }
}

class _CreateTemplatePage extends StatefulWidget {
  @override
  _CreateTemplatePageState createState() => _CreateTemplatePageState();
}

class _CreateTemplatePageState
    extends BasePageWidgetState<_CreateTemplatePage, CreateTemplateBloc> {
  var _formKey = GlobalKey<FormState>();
  final _fieldTempNameKey = GlobalKey<FormFieldState>();

  @override
  List<Widget> getPageWidget() {
    var pad = 10.0;
    var curveRadius = Radius.circular(10.0);
    return [
      LayoutBuilder(
          builder: (context, constraint) => Column(
                children: [
                  Expanded(
                      child: SingleChildScrollView(
                          child: Form(
                    key: _formKey,
                    child: Center(
                      child: Container(
                          padding: EdgeInsets.fromLTRB(pad, pad, pad, 0.0),
                          width: isMobileBrowser(constraint)
                              ? double.infinity
                              : containerWidth,
                          decoration: BoxDecoration(
                              color: ColorResource.bluBg,
                              borderRadius: BorderRadius.only(
                                  topLeft: curveRadius, topRight: curveRadius)),
                          child: Column(
                            children: [
                              TemplateInfoWidget(_fieldTempNameKey,
                                  labelResId: bloc!.headerText,
                                  companyId: bloc!.cmpId,
                                  onTemplateNameChange:
                                      bloc!.onTemplateValidated,
                                  textEditingController: bloc!.txtEdtController),
                              SizedBox(height: 20.0),
                              Container(
                                padding: EdgeInsets.all(2 * pad),
                                child: AddressWidget(
                                    bloc!.notifyIns,
                                    bloc!.customerPreset,
                                    bloc!.loadPoint,
                                    bloc!.unloadPoints,
                                    isEditTemp: true),
                                decoration: BoxDecoration(
                                    color: ColorResource.colorWhite,
                                    borderRadius: BorderRadius.only(
                                        topLeft: curveRadius,
                                        topRight: curveRadius)),
                              ),
                              Container(
                                  padding: EdgeInsets.all(2 * pad),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      ProductTypeWidget(
                                          bloc!.customerPreset, bloc!.goodsFlag,
                                          callback: bloc!.onSelGoods,
                                          notifyIns: bloc!.notifyIns),
                                      GoodsUnitWidget(
                                          bloc!.goodsQnt, bloc!.goodsUnit,
                                          defQntValue: bloc!.goodsQty,
                                          defQntUnit:
                                              bloc!.customerPreset?.qntUnit,
                                          notifyIns: bloc!.notifyIns),
                                      TruckInfoWidget(bloc!.truckInf,
                                          callback: bloc!.truckInfo,
                                          onEdit: bloc!.notifyIns,
                                          isUsingTemp: bloc!.isTempEdit,
                                          presetInfo: bloc!.customerPreset),
                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                    color: ColorResource.lightBluBg,
                                  )),
                              Container(
                                  width: double.maxFinite,
                                  padding: EdgeInsets.only(
                                      left: 2 * pad, right: pad),
                                  child: TripInstructionsWidget(
                                      bloc!.cmpId, bloc!.instructions,
                                      onEdit: bloc!.notifyIns),
                                  decoration: BoxDecoration(
                                      color: ColorResource.off_white)),
                            ],
                          )),
                    ),
                  ))),
                  Container(
                    padding: EdgeInsets.fromLTRB(pad, 0.0, pad, pad),
                    width: isMobileBrowser(constraint)
                        ? double.infinity
                        : containerWidth,
                    decoration: BoxDecoration(
                        color: ColorResource.bluBg,
                        borderRadius: BorderRadius.only(
                            bottomLeft: curveRadius, bottomRight: curveRadius)),
                    child: Container(
                      decoration: BoxDecoration(
                          color: ColorResource.colorWhite,
                          borderRadius: BorderRadius.only(
                              bottomLeft: curveRadius,
                              bottomRight: curveRadius)),
                      child: Consumer<CreateTemplateBloc>(
                        builder: (context, bloc, child) => FilledColorButton(
                          fontSize: 16.0,
                          verticalMargin: 20.0,
                          horizonatalMargin: 20.0,
                          textColor: ColorResource.colorWhite,
                          buttonText: translate(context, bloc.buttonText),
                          onPressed: bloc.isActivate
                              ? () => bloc.isTempEdit??false
                                  ? _updateTemplate()
                                  : _saveTemplate()
                              : null,
                          eventName: AnalyticsEvents.CREATE_TEMP_BTN_CLICK,
                        ),
                      ),
                    ),
                  ),
                ],
              ))
    ];
  }

  _validate() {
    bool isValidated = _formKey.currentState?.validate()??false;
    var errorMsg = bloc!.validateFields();
    if (!isNullOrEmpty(errorMsg)) {
      showToast(localize(errorMsg));
      return false;
    } else if (!isValidated) return false;
    return true;
  }

  _saveTemplate() async {
    if (_validate()) {
      submitDialog(context, dismissible: false);
      ApiResponse response = await bloc!.saveTemplate();
      Navigator.pop(context);
      onApiResponse(response);
    }
  }

  _updateTemplate() {
    if (_validate()) {
      showCustomDialog(
          context, localize("btn_edit_temp"), '${bloc!.templateName}?',
          () async {
        submitDialog(context, dismissible: false);
        ApiResponse response = await bloc!.saveTemplate();
        Navigator.pop(context);
        onApiResponse(response);
      },
          eventName: AnalyticsEvents.TMP_CREATE_SUCCESS_DLG,
          heading: 'tmp_edit_msg',
          withNegBtn: true);
    }
  }

  onApiResponse(apiResponse) {
    if (apiResponse.status == Status.COMPLETED) {
      AnalyticsManager().logEvent(AnalyticsEvents.TMP_CREATE_SUCCESS);
      showCustomDialog(context, localize(bloc!.headerText), bloc!.templateName,
          () => Navigator.pop(context, true),
          eventName: AnalyticsEvents.TMP_CREATE_SUCCESS_DLG,
          heading: bloc!.successText);
    } else {
      showToast(apiResponse.message ?? "Something went wrong");
      AnalyticsManager().logEvent(AnalyticsEvents.TMP_CREATE_FAILED);
    }
  }

  @override
  isResizeToBottomInset() {
    return true;
  }

  @override
  void dispose() {
    bloc!.txtEdtController.dispose();
    super.dispose();
  }
}
