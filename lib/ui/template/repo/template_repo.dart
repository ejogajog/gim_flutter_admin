import 'package:app/api/api_response.dart';

class TemplateRepository {
  static var helper = ApiBaseHelper();

  static Future<ApiResponse> addTemplate(request) async {
    ApiResponse response = await helper.post("/ejogajogAdminAPI/api/v1/trip/templates", request);
    return response;
  }

  static Future<ApiResponse> validateTemplate(companyId,templateName) async {
    ApiResponse response = await helper.get("/ejogajogAdminAPI/api/v1/trip/templates/validate", queryParameters: {'companyId':companyId,'templateName':templateName});
    return response;
  }

  static Future<ApiResponse> fetchTemplates(queryParams) async {
    ApiResponse response = await helper.get("/ejogajogAdminAPI/api/v1/trip/templates/", queryParameters: queryParams);
    return response;
  }

  static Future<ApiResponse> editTemplate(request) async {
    ApiResponse response = await helper.post("/ejogajogAdminAPI/api/v1/trip/templates/update", request, isPut: true);
    return response;
  }
}
