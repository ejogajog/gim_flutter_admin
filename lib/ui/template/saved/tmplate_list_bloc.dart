import 'package:app/bloc/pagination_bloc.dart';
import 'package:app/model/base_response.dart';
import 'package:app/ui/template/repo/template_repo.dart';
import 'package:app/ui/trip/create/model/trip_model.dart';
import 'package:app/ui/trip/create/model/customer_model.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/utils/string_util.dart';

class TemplateListBloc extends PaginationBloc {
  CustomerInfo _customerInfo;
  CustomerPreferences _customerPreferences;

  TemplateListBloc(this._customerInfo, this._customerPreferences);

  get customer => _customerInfo;

  get customerPreference => _customerPreferences;

  get customerClone => customer?.clone();

  get preferenceClone => customerPreference?.clone();

  @override
  getListFromApi({callback}) async {
    isLoading = true;
    queryParam['companyId'] = _customerInfo.companyId.toString();
    var response = await TemplateRepository.fetchTemplates(getBaseQueryParam());
    checkResponse(response, successCallback: () async {
      var pagination = BasePagination<TripModel>.fromJson(response.data);
      if (!isNullOrEmptyList(pagination.contentList)) {
        pagination.numberOfResults = pagination.numberOfResults!+1;
        if (currentPage == 0) pagination.contentList!.insert(0, TripModel());
      }
      setPaginationItem(pagination);
      if (callback != null) callback();
    });
    takeDecisionShowingError(response);
  }

  preferenceWithTempData(tripModel){
    var preference = preferenceClone;
    preference.addTempToPref(tripModel);
    return preference;
  }
}
