import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/ui/trip/create/model/trip_model.dart';
import 'package:app/ui/widget/base_widget.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/string_util.dart';

class TemplateItem extends BaseItemView<TripModel> {

  @override
  Widget build(BuildContext context) {
    var pos = (position! + 1) % 2 == 0;
    var curveRadius = Radius.circular(10.0);
    var topItemPad = EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0);
    var midItemPad = EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0);
    var btmItemPad = EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0);
    return position == 0
        ? InkWell(
            onTap: () => onItemClick!(null,false),
            child: Container(
              margin: EdgeInsets.fromLTRB(
                  pos ? 10.0 : 0.0, 10.0, pos ? 0.0 : 10.0, 0.0),
              child: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(Icons.add_circle_outlined,
                        size: 60.0, color: ColorResource.bluBg),
                    Text(localize('btn_new_temp'),
                        style: TextStyle(
                            color: ColorResource.txtClr, fontSize: 14.0)),
                  ],
                ),
              ),
              decoration: BoxDecoration(
                  color: ColorResource.transBluBg,
                  borderRadius: BorderRadius.all(curveRadius)),
            ),
          )
        : Container(
            margin: EdgeInsets.fromLTRB(
                pos ? 10.0 : 0.0, 10.0, pos ? 0.0 : 10.0, 0.0),
            decoration: BoxDecoration(
                color: ColorResource.lightWhite,
                borderRadius: BorderRadius.all(curveRadius)),
            child: InkWell(
              onTap: () => onItemClick!(item,false),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: topItemPad,
                  decoration: BoxDecoration(
                      color: ColorResource.colorWhite,
                      borderRadius: BorderRadius.only(
                          topLeft: curveRadius, topRight: curveRadius)),
                    child: Row(
                      children: [
                        Expanded(child: textWidget(item?.name)),
                        ElevatedButton(
                          onPressed: () {
                            onItemClick!(item, true);
                          },
                          child: const Icon(Icons.edit, size: 18, color: ColorResource.btnColor),
                          style: ElevatedButton.styleFrom(
                            elevation: 0.0,
                            backgroundColor: ColorResource.white_gray,
                            minimumSize: Size(34,34),
                            maximumSize: Size(34,34),
                            padding: const EdgeInsets.all(4.0),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(2),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Container(
                        padding: midItemPad,
                        color: ColorResource.colorWhite,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: textWidget(
                                      '${item?.loadingAddress()} - ${item?.unloadingAddress()}',
                                      maxLine: 3,
                                      icon: Icons.fmd_good,
                                      textColor: ColorResource.lblBlueTxtClr),
                                ),
                              ],
                            ),
                            SizedBox(height: 5.0),
                            Visibility(
                                visible: item!.noOfUnloadPoints! > 1,
                                child: textWidget(
                                    '${item!.noOfUnloadPoints} ${item!.noOfUnloadPoints! <= 1 ? localize('lbl_unload_point') : localize('lbl_unload_points')}',
                                    icon: Icons.dashboard_sharp,
                                    iconClr: ColorResource.colorMarineBlueLight)),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: btmItemPad,
                    decoration: BoxDecoration(
                        color: ColorResource.lightWhite,
                        borderRadius: BorderRadius.only(
                            bottomLeft: curveRadius, bottomRight: curveRadius)),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        textWidget(
                            '${item?.truckTypeResponse}, ${item?.truckSizeResponse ?? '0.0'}Ton, ${item?.truckLength ?? '0.0'}ft',
                            icon: Icons.time_to_leave),
                        SizedBox(height: 5.0),
                        textWidget('${item?.goodType.toString()}${_goodsQty(item)}',
                            icon: Icons.breakfast_dining, maxLine: 2),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
  }

  Widget textWidget(text,
      {icon,
      maxLine= 1,
      overflow= TextOverflow.ellipsis,
      fontSize= 14.0,
      textColor= ColorResource.txtClr,
      fontWeight= FontWeight.w500,
      iconClr= ColorResource.iconClr}) {
    var sizeBox = icon == null ? SizedBox() : SizedBox(width: 5.0);
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        icon == null ? sizeBox : Icon(icon, size: 16.0, color: iconClr),
        sizeBox,
        Expanded(
          child: AutoSizeText(text,
              maxLines: maxLine,
              overflow: overflow,
              minFontSize: 10.0,
              style: TextStyle(
                  fontSize: fontSize,
                  color: textColor,
                  fontWeight: fontWeight)),
        ),
      ],
    );
  }

  // _goodsQty(item){
  //   return '${isNullOrEmpty(item.quantityOfGoods) ? isNullOrEmpty(item.goodsQuantityUnit) ? '': ', Unit: ${units[item.goodsQuantityUnit]?.name}' : isNullOrEmpty(item.goodsQuantityUnit) ? ', Qty: ${item.quantityOfGoods}' : ', ${item.quantityOfGoods}${units[item.goodsQuantityUnit]?.name}'}';
  // }
  String? _goodsQty(item) {
    if (item == null) return null;

    // Ensuring quantityOfGoods is converted to String safely
    String? quantity = item.quantityOfGoods != null ? item.quantityOfGoods.toString() : null;
    String? unit = item.goodsQuantityUnit != null ? units[item.goodsQuantityUnit]?.name : null;

    if (quantity != null && unit != null) {
      return ', $quantity $unit';
    } else if (quantity != null) {
      return ', Qty: $quantity';
    } else if (unit != null) {
      return ', Unit: $unit';
    } else {
      return '';
    }
  }



}
