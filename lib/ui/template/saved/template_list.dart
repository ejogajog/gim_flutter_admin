import 'package:flutter/material.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/ui/template/create/create_template.dart';
import 'package:app/ui/template/saved/template_item.dart';
import 'package:app/ui/widget/grid_adapter.dart';
import 'package:app/utils/string_util.dart';
import 'package:provider/provider.dart';
import 'package:app/ui/widget/app_button.dart';
import 'package:app/utils/navigation_util.dart';
import 'package:app/utils/ui_util.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/ui/widget/base_widget.dart';
import 'package:app/ui/trip/create/create_trip_page.dart';
import 'package:app/ui/trip/create/model/customer_model.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/ui/template/saved/tmplate_list_bloc.dart';

class TemplateListScreen extends StatelessWidget {
  final CustomerInfo cstInfo;
  final CustomerPreferences cstPref;

  TemplateListScreen(this.cstInfo, this.cstPref);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<TemplateListBloc>(
            create: (_) => TemplateListBloc(this.cstInfo, this.cstPref)),
      ],
      child: _TemplateListPage(),
    );
  }
}

class _TemplateListPage extends StatefulWidget {
  @override
  _TemplateListPageState createState() => _TemplateListPageState();
}

class _TemplateListPageState
    extends BasePageWidgetState<_TemplateListPage, TemplateListBloc> {
  @override
  List<Widget> getPageWidget() {
    var pad = 10.0;
    var curveRadius = Radius.circular(10.0);
    return [
      LayoutBuilder(
        builder: (context, constraint) => Center(
          child: Container(
              padding: EdgeInsets.all(pad),
              width: isMobileBrowser(constraint)
                  ? double.infinity
                  : containerWidth,
              decoration: BoxDecoration(
                  color: ColorResource.bluBg,
                  borderRadius: BorderRadius.all(curveRadius)),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(localize('shw_tmp_lbl'),
                                style: TextStyle(
                                    color: ColorResource.txtClr,
                                    fontSize: 14.0)),
                            Text(bloc!.customer.companyName,
                                style: TextStyle(
                                    color: ColorResource.txtClr,
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.w500)),
                          ],
                        ),
                      ),
                      FilledColorButton(
                        buttonText: localize('create_trip'),
                        isFullwidth: false,
                        textColor: ColorResource.colorWhite,
                        onPressed: () async {
                          var result = await navigateNextScreen(
                              context,
                              CreateTripPage(
                                  bloc!.customer, bloc!.preferenceClone));
                          if (result ?? false) bloc!.reloadList();
                        },
                      )
                    ],
                  ),
                  Consumer<TemplateListBloc>(
                    builder: (context, bloc, _) => Visibility(
                      visible: isNullOrEmptyList(bloc.itemList),
                      child: InkWell(
                        onTap: () {
                          _createTemp();
                        },
                        child: Container(
                          height: 120.0,
                          width: double.infinity,
                          margin: EdgeInsets.only(top: 20.0),
                          child: Center(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Icon(Icons.add_circle_outlined,
                                    size: 60.0, color: ColorResource.bluBg),
                                Text(localize('btn_new_temp'),
                                    style: TextStyle(
                                        color: ColorResource.txtClr,
                                        fontSize: 14.0)),
                              ],
                            ),
                          ),
                          decoration: BoxDecoration(
                              color: ColorResource.transBluBg,
                              borderRadius: BorderRadius.all(curveRadius)),
                        ),
                      ),
                    ),
                  ),
                  getGridList<TemplateListBloc>(bloc!, () => TemplateItem(),
                      apiCallback: onApiCallback, onItemClicked: (item,isEdit) {
                    item == null ? _createTemp() : isEdit ? _editTemp(item) : _createTrip(item);
                  }, mainAxisExtent: 220.0)
                ],
              )),
        ),
      ),
      showLoader<TemplateListBloc>(Provider.of<TemplateListBloc>(context)),
    ];
  }

  @override
  onBuildCompleted() {
    if (mounted) {
      bloc!.getListFromApi(callback: onApiCallback);
    }
  }

  _createTemp() async {
    var result = await navigateNextScreen(context,
        CreateTemplateScreen(bloc!.customerClone, bloc!.preferenceClone));
    if (result ?? false) bloc!.reloadList();
  }

  _editTemp(item) async {
    var result = await navigateNextScreen(context,
        CreateTemplateScreen(bloc!.customerClone, bloc!.preferenceWithTempData(item)));
    if (result ?? false) bloc!.reloadList();
  }

  _createTrip(item) async {
    navigateNextScreen(context,
        CreateTripPage(bloc!.customerClone, bloc!.preferenceWithTempData(item)));
  }

  onApiCallback() async => () {
        print('OnAPICallback');
      };
}
