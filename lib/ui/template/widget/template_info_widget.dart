import 'dart:async';

import 'package:flutter/material.dart';
import 'package:app/api/api_response.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/model/base_response.dart';
import 'package:app/ui/template/repo/template_repo.dart';
import 'package:app/ui/widget/editfield.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/string_util.dart';
import 'package:app/utils/validation_util.dart';

class TemplateInfoWidget extends StatefulWidget {
  final int? companyId;
  final String? labelResId;
  final bool? isShowFieldOnly;
  final Function? onTemplateNameChange;
  final GlobalKey<FormFieldState>? _formKey;
  final TextEditingController? textEditingController;

  TemplateInfoWidget(this._formKey,
      {this.labelResId = 'lbl_new_temp',
      this.companyId,
      this.onTemplateNameChange,
      this.isShowFieldOnly = false,
      this.textEditingController});

  @override
  _TemplateInfoWidgetState createState() => _TemplateInfoWidgetState();
}

class _TemplateInfoWidgetState extends State<TemplateInfoWidget> {
  bool verifyStatus = false;
  Timer? _debounce;
  String? defTmpName;

  @override
  void initState() {
    super.initState();
    defTmpName = widget.textEditingController?.text;
  }

  @override
  Widget build(BuildContext context) {
    return widget.isShowFieldOnly??false
        ? Container(
            margin: EdgeInsets.only(left: 5.0, right: 5.0),
            child: TextEditText(
              formkey: widget._formKey,
              labelText: localize('hnt_tmp_name'),
              behaveNormal: true,
              radius: 4.0,
              fontWeight: FontWeight.w500,
              textColor: ColorResource.txtClr,
              textAlign: TextAlign.center,
              enableBorderColor: ColorResource.light_grey,
              focusedBorderColor: ColorResource.light_grey,
              validationMessageRegexPair: {
                NOT_EMPTY_REGEX: translate(context, 'mne_val_msg')
              },
              onChanged: (val) async {
                if (_debounce?.isActive ?? false) _debounce?.cancel();
                _debounce = Timer(const Duration(milliseconds: 300), () async {
                  widget.onTemplateNameChange!(await _verifyTemplateName(val));
                });
              },
              errorText: verifyStatus ? localize('tmp_exist_error') : null,
            ),
          )
        : Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(localize(widget.labelResId),
                    style: TextStyle(
                        color: ColorResource.lblClr,
                        fontSize: 18.0,
                        fontWeight: FontWeight.w500)),
                Container(
                  padding: EdgeInsets.all(20.0),
                  margin: EdgeInsets.only(top: 20.0),
                  child: Column(
                    children: [
                      Text(localize('lbl_name_temp'),
                          style: TextStyle(
                              color: ColorResource.txtClr, fontSize: 14.0)),
                      TextEditText(
                        formkey: widget._formKey,
                        labelText: '',
                        fontSize: 16.0,
                        behaveNormal: true,
                        fontWeight: FontWeight.w500,
                        textColor: ColorResource.txtClr,
                        textAlign: TextAlign.center,
                        enableBorderColor: ColorResource.light_grey,
                        focusedBorderColor: ColorResource.light_grey,
                        textEditingController: widget.textEditingController,
                        validationMessageRegexPair: {
                          NOT_EMPTY_REGEX: translate(context, 'mne_val_msg')
                        },
                        onChanged: (val) async {
                          if (_debounce?.isActive ?? false) _debounce?.cancel();
                          _debounce = Timer(const Duration(milliseconds: 300),
                              () async {
                            widget.onTemplateNameChange!(
                                await _verifyTemplateName(val));
                          });
                        },
                        errorText:
                            verifyStatus ? localize('tmp_exist_error') : null,
                      )
                    ],
                  ),
                  decoration: BoxDecoration(
                      color: ColorResource.colorWhite,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                )
              ],
            ),
          );
  }

  _verifyTemplateName(templateName) async {
    verifyStatus = false;
    print('~~~~~~~Template name new $templateName');
    if (isNullOrEmpty(templateName) || defTmpName == templateName.trim()) {
      setState(() {
        verifyStatus = false;
      });
      return verifyStatus ? null : templateName;
    }
    ApiResponse response = await TemplateRepository.validateTemplate(
        widget.companyId, templateName);
    if (response.status == Status.COMPLETED) {
      var statusResponse = StatusResponse.fromJson(response.data);
      setState(() {
        verifyStatus = statusResponse.status??false;
      });
    }
    return verifyStatus ? null : templateName;
  }

  @override
  void dispose() {
    _debounce?.cancel();
    super.dispose();
  }
}
