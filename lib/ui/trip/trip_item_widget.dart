
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/styles.dart';
import 'package:app/ui/detail/trip_details.dart';
import 'package:app/ui/trip/trip_item.dart';
import 'package:app/ui/trip/trip_list_bloc.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/app_colors.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/date_time_util.dart';
import 'package:app/utils/navigation_util.dart';

class TripItemWidget extends StatefulWidget {
  final TripItem item;
  final TripListBloc _tripListBloc;

  TripItemWidget(this.item, this._tripListBloc);

  @override
  _TripItemWidgetState createState() => _TripItemWidgetState();
}

class _TripItemWidgetState extends State<TripItemWidget> {
  int tripRunningHours = 0;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        AnalyticsManager().logEventProp(AnalyticsEvents.TRIP_ITEM_CLICK,{AnalyticProperties.TRIP_ID: widget.item.tripId});
        navigateNextScreen(
            context, TripDetailScreen(widget.item.enterpriseName, widget.item, widget._tripListBloc, tripRunningHours: tripRunningHours), pageName: AnalyticsEvents.TRIP_DETAIL_SCREEN);
      },
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: 6),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: stillRunning()
                    ? ColorResource.trip_above_24hourBg
                    : _getBackgroundColor(widget.item),
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            padding: const EdgeInsets.fromLTRB(6.0,10.0,6.0,6.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('${translate(context, 'lbl_trip_no')}${widget.item.tripNumber}',
                    style: Styles.txtStyleWhiteNormal),
                SizedBox(
                  height: 1,
                ),
                Text('${translate(context, 'lbl_partner')}${widget.item.partnerName ?? ''}',
                    maxLines: 1, style: Styles.txtStyleWhiteNormal),
                SizedBox(
                  height: 1,
                ),
                Text('${translate(context,'lbl_driver')}${widget.item.driverName ?? ''}',
                    maxLines: 1, style: Styles.txtStyleWhiteNormal),
                SizedBox(
                  height: 1,
                ),
                Text('${translate(context, 'lbl_pickup')}${widget.item.tripPickupAddress}',
                    maxLines: 3, style: Styles.txtStyleWhiteNormal),
              ],
            ),
          ),
          widget.item.tripStatus == TripState.Live.statusValue() ? Positioned(
            top: 0.0,
            right: 0.0,
            child: Container(
              padding: EdgeInsets.all(2.0),
              decoration: BoxDecoration(
                  color: Colors.lightGreen,
                  borderRadius: BorderRadius.all(Radius.circular(4.0))),
              child: Text(
                '${tripRunningHours.toString()} hrs',
                maxLines: 1,
                style: TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 1.0),
                    fontSize: 10,
                    fontFamily: "notosans",
                    fontWeight: FontWeight.w300,
              ),
            ),
          ),
          ) : Container()
        ],
      ),
    );
  }

  bool stillRunning() {
    if (widget.item.tripStatus == TripState.Live.statusValue()) {
      tripRunningHours = int.parse(getTimeDifferenceInHours(widget.item.startDateTime));
      if(('Narayanganj' == widget.item.tripPickupDistrict && 'Chittagong' == widget.item.tripDropOffDistrict) || ('Chittagong' == widget.item.tripPickupDistrict &&  'Narayanganj' == widget.item.tripDropOffDistrict))
        return tripRunningHours >= 12;
      else if(('Dhaka' == widget.item.tripPickupDistrict && 'Chittagong' == widget.item.tripDropOffDistrict) || ('Chittagong' == widget.item.tripPickupDistrict &&  'Dhaka' == widget.item.tripDropOffDistrict))
        return tripRunningHours >= 14;
      else if(('Gazipur' == widget.item.tripPickupDistrict && 'Chittagong' == widget.item.tripDropOffDistrict) || ('Chittagong' == widget.item.tripPickupDistrict &&  'Gazipur' == widget.item.tripDropOffDistrict))
        return tripRunningHours >= 16;
      else if(('Mymansingh' == widget.item.tripPickupDistrict && 'Chittagong' == widget.item.tripDropOffDistrict) || ('Chittagong' == widget.item.tripPickupDistrict &&  'Mymansingh' == widget.item.tripDropOffDistrict))
        return tripRunningHours >= 18;
      else
        return tripRunningHours >= 24;
    }
    return false;
  }

  Color _getBackgroundColor(TripItem item) {
    if (item.tripStatus == TripState.Bidded.statusValue()) {
    return HexColor('FF053432');
    } else if(item.tripStatus == TripState.Booked.statusValue()) {
      return HexColor('FF1C0339');
    } else if (item.tripStatus == TripState.Live.statusValue()) {
      return AppColors.Primary;
    } else {
      return HexColor('4a4a4a');
    }
  }
}
