
import 'package:app/api/api_response.dart';

class TripRepository{

  static var  helper = ApiBaseHelper();

  @Deprecated('Use `getTripList(request)` instead')
  static Future<ApiResponse>  getLiveTripList(request) async{
    ApiResponse response=await helper.get("/ejogajogAdminAPI/api/v1/admin/tripmanagement/trips?tripFilter=LiveTrips",queryParameters: request);
    return response;
  }

  static Future<ApiResponse>  getTripList(request) async{
    ApiResponse response=await helper.get("/ejogajogAdminAPI/api/v1/admin/tripmanagement/trips", queryParameters: request);
    return response;
  }

  static Future<ApiResponse>  getPaymentTrips(request) async{
    ApiResponse response=await helper.get("/ejogajogAdminAPI/api/v1/trips/enterprise/trips", queryParameters: request);
    return response;
  }

  static Future<ApiResponse>  startTrip(tripId) async{
    ApiResponse response = await helper.post("/ejogajogAdminAPI/api/v1/admin/startTrip",'{"id":$tripId}');
    return response;
  }
}