import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:app/api/api_response.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/styles.dart';
import 'package:app/ui/template/widget/template_info_widget.dart';
import 'package:app/ui/trip/create/bloc/create_trip_bloc.dart';

import 'package:app/ui/trip/create/model/customer_model.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/ui/trip/create/model/no_of_truck.dart';
import 'package:app/ui/widget/CustomCheckBox.dart';
import 'package:app/ui/widget/app_button.dart';
import 'package:app/ui/widget/base_widget.dart';
import 'package:app/ui/widget/custom_dialog_box.dart';
import 'package:app/ui/widget/goods/goods_unit_widget.dart';
import 'package:app/ui/widget/no_of_truck_widget.dart';
import 'package:app/ui/widget/trip_address_widget.dart';
import 'package:app/ui/widget/kam/kam_user_widget.dart';
import 'package:app/ui/widget/trip_date_time_widget.dart';
import 'package:app/ui/widget/trip_instruction_widget.dart';
import 'package:app/ui/widget/trip_payment_widget.dart';
import 'package:app/ui/widget/trip_product_widget.dart';
import 'package:app/ui/widget/truck/truck_info_widget.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/string_util.dart';
import 'package:app/utils/ui_util.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:firebase_performance/firebase_performance.dart';

import '../../widget/editfield.dart';

class CreateTripPage extends StatelessWidget {
  final CustomerInfo? cstInfo;
  final CustomerPreferences? cstPref;

  CreateTripPage(this.cstInfo, this.cstPref);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<CreateTripBloc>(
            create: (_) => CreateTripBloc(this.cstInfo, this.cstPref)),
      ],
      child: _CreateTripScreen(),
    );
  }
}

class _CreateTripScreen extends StatefulWidget {
  @override
  _CreateTripScreenState createState() => _CreateTripScreenState();
}

class _CreateTripScreenState
    extends BasePageWidgetState<_CreateTripScreen, CreateTripBloc> {
  final TextEditingController insController = TextEditingController();
  var _formKey = GlobalKey<FormState>();
  var pad = 10.0;
  var curveRadius = Radius.circular(10.0);
  final Trace trace = FirebasePerformance.instance.newTrace('create_trip');
  final _fieldTempNameKey = GlobalKey<FormFieldState>();

  @override
  void initState() {
    trace.start();
    super.initState();
  }

  @override
  List<Widget> getPageWidget() {
    return [
      LayoutBuilder(
          builder: (context, constraint) => Column(children: [
            Expanded(
              child: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Center(
                      child: Container(
                        padding: EdgeInsets.only(top: pad),
                        width: isMobileBrowser(constraint)
                            ? double.infinity
                            : containerWidth,
                        decoration: BoxDecoration(
                            color: ColorResource.bluBg,
                            borderRadius: BorderRadius.only(
                                topLeft: curveRadius, topRight: curveRadius)),
                        child: _presetWidget(),
                      )),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(pad, 0.0, pad, pad),
              width: isMobileBrowser(constraint)
                  ? double.infinity
                  : containerWidth,
              decoration: BoxDecoration(
                  color: ColorResource.bluBg,
                  borderRadius: BorderRadius.only(
                      bottomLeft: curveRadius, bottomRight: curveRadius)),
              child: Container(
                decoration: BoxDecoration(
                    color: ColorResource.colorWhite,
                    borderRadius: BorderRadius.only(
                        bottomLeft: curveRadius, bottomRight: curveRadius)),
                child: FilledColorButton(
                  fontSize: 16.0,
                  verticalMargin: 20.0,
                  horizonatalMargin: 20.0,
                  textColor: ColorResource.colorWhite,
                  buttonText: translate(context, 'create_trip'),
                  onPressed: () => _createTrip(),
                  eventName: AnalyticsEvents.CREATE_TRIP_BTN_CLICK,
                ),
              ),
            ),
          ]))
    ];
  }

  Widget _presetWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(bloc!.client,
                style: GoogleFonts.roboto(
                  color: ColorResource.colorMarineBlue,
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                )),
            SizedBox(height: 20.0),
            Text(localize('create_new_trip'),
                style: GoogleFonts.roboto(
                  color: ColorResource.colorMarineBlue,
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                )),
            SizedBox(height: 4.0),
            bloc!.isUsingPref
                ? Text(localize('create_new_trip_dsc'),
                style: GoogleFonts.roboto(
                  color: ColorResource.colorBrownGrey,
                  fontSize: 12,
                  fontWeight: FontWeight.w500,
                ))
                : Container(
              padding: EdgeInsets.all(2 * pad),
              color: ColorResource.off_white,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.info_outline,
                          color: ColorResource.colorMariGoldTran),
                      SizedBox(width: 4.0),
                      Text(localize('using_temp_lbl'),
                          style: Styles.formLabel),
                    ],
                  ),
                  Text(bloc!.prefTemplateName,
                      style: Styles.blueMedTextBold),
                ],
              ),
            ),
            SizedBox(height: 20.0),
            Container(
              padding: EdgeInsets.all(2 * pad),
              margin: EdgeInsets.only(left: pad, right: pad),
              child: CustomCheckbox(
                localize('hide_from_partner'),
                onChange: (val) {
                  bloc!.hideFromPartner = val;
                },
              ),
              decoration: BoxDecoration(
                  color: ColorResource.lightGreen,
                  borderRadius: BorderRadius.only(
                      topLeft: curveRadius, topRight: curveRadius)),
            ),
            Container(
              padding: EdgeInsets.all(2 * pad),
              margin: EdgeInsets.only(left: pad, right: pad),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  bloc!.isEnterprise ? KamUserWidget(bloc!.kamFlag, callback: bloc!.kam) : SizedBox(),
                  SizedBox(height: 6.0),
                  Text("Customer Unique Number ", style: Styles.formLabel),
                  TextEditText(
                    labelText: '',
                    behaveNormal: true,
                    fontWeight: FontWeight.w300,
                    focusNode: FocusNode(), // You can manage focus if needed
                    onChanged: (value) {
                      bloc?.lcNumber = value;
                    },
                    textEditingController: bloc?.lcNumberController,
                  ),
                  AddressWidget(bloc!.notifyIns, bloc!.customerPreset,
                      bloc!.loadPoint, bloc!.unloadPoints,
                      isEditTemp: false, validDistrict: bloc!.districtFlag),
                ],
              ),
              decoration: BoxDecoration(color: ColorResource.colorWhite),
            ),
            Container(
                padding: EdgeInsets.all(2 * pad),
                margin: EdgeInsets.only(left: pad, right: pad),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TripDateTimeWidget(callback: bloc!.tripDateTime),
                    ProductTypeWidget(bloc!.customerPreset, bloc!.goodsFlag,
                        callback: bloc!.goodsType, notifyIns: bloc!.notifyIns),
                    GoodsUnitWidget(bloc!.goodsQnt, bloc!.goodsUnit,
                        notifyIns: bloc!.notifyIns,
                        defQntValue: bloc!.customerPreset?.qntOfGoods,
                        defQntUnit: bloc!.customerPreset?.qntUnit),
                    IncDecWidget(
                      'lbl_truck_count',
                      NoOfTruck().build(),
                      '',
                      callback: bloc!.noOfTrip,
                      defaultNo: bloc?.customerPreset?.tripDataSuggestionResponse
                          ?.noOfTrucks,
                    ),
                    TruckInfoWidget(bloc!.truckInf,
                        callback: bloc!.truckInfo,
                        presetInfo: bloc!.customerPreset,
                        isUsingTemp: !bloc!.isUsingPref),
                    TripPaymentWidget(bloc!.isEnterprise, bloc!.customerPreset,
                        bloc!.paymentMode)
                  ],
                ),
                decoration: BoxDecoration(
                  color: ColorResource.lightBluBg,
                )),
            Container(
                width: double.maxFinite,
                margin: EdgeInsets.only(left: pad, right: pad),
                color: ColorResource.off_white,
                padding: EdgeInsets.only(left: 2 * pad, right: pad),
                child: TripInstructionsWidget())
          ],
        ),
      ],
    );
  }

  createTripRes(apiResponse) {
    if (apiResponse.status == Status.COMPLETED) {
      AnalyticsManager().logEvent(AnalyticsEvents.TRIP_CREATE_SUCCESS);
      bloc!.isUsingPref
          ? showEtDialogWithImg(
          context,
          'Congratulations!',
          bloc!.isOPS
              ?  'Trip has been created for'// localize('ops_trip_success')
              : bloc!.isGroupTrip()
              ? 'Trips have been created for'//localize("success_group_trip") + localize('use_for_tmp')
              :'Trip has been created for' ,
          bloc!.isOPS
              ?  '${bloc?.cmpName }'// localize('ops_trip_success')
              : bloc!.isGroupTrip()
              ? '${bloc?.cmpName} '//localize("success_group_trip") + localize('use_for_tmp')
              :'${bloc?.cmpName}' ,
          bloc!.isOPS
              ?  'Trip #${bloc?.tripsNo}'// localize('ops_trip_success')
              : bloc!.isGroupTrip()
              ? 'Trip #${bloc?.tripsNo}'//localize("success_group_trip") + localize('use_for_tmp')
              :'Trip #${bloc?.tripsNo}' ,//localize("success_trip") + localize('use_for_tmp'),
          TemplateInfoWidget(_fieldTempNameKey,
              companyId: bloc!.cmpId,
              onTemplateNameChange: bloc!.onTemplateNameChange,
              isShowFieldOnly: true), () async {
        var isValid = bloc!.isValidTemplate();
        if (!isNullOrEmpty(isValid)) {
          showToast(localize(isValid));
          return;
        }
        submitDialog(context, dismissible: false);
        var apiResponse = await bloc!.createTemplate();
        Navigator.pop(context);
        if (apiResponse.status == Status.COMPLETED) {
          Navigator.pop(context);
          showCustomDialog(context, localize("new_template_suc_ttl"),
              bloc!.templateName, () => Navigator.pop(context, true),
              eventName: AnalyticsEvents.TMP_CREATE_SUCCESS_DLG,
              heading: 'new_template_suc_lbl');
        } else
          showToast(apiResponse.message ?? "Something went wrong");
      }, () => Navigator.pop(context),() => _copyToClipboard("${bloc?.tripsNo??""} ${bloc?.cmpName??""}"),
          eventName: AnalyticsEvents.TMP_CREATE_SUCCESS)
          :
      showCustomDialogWithImg(
          context,
          'Congratulations!',
          bloc!.isOPS
              ? 'Trip has been created for'
              : bloc!.isGroupTrip()
              ? 'Trip has been created for'
              : 'Trip has been created for',
          bloc!.isOPS
              ? '${bloc?.cmpName}'
              : bloc!.isGroupTrip()
              ? '${bloc?.cmpName}'
              : '${bloc?.cmpName}',
          bloc!.isOPS
              ? 'Trip #${bloc?.tripsNo}'
              : bloc!.isGroupTrip()
              ? 'Trip #${bloc?.tripsNo}'
              : 'Trip #${bloc?.tripsNo}',

              () => Navigator.pop(context, true),_copyToClipboard("${bloc?.tripsNo??""} ${bloc?.cmpName??""}"),
          eventName: AnalyticsEvents.TRIP_CREATE_SUCCESS_DLG);
    } else {
      showToast(apiResponse.message ?? "Something went wrong");
      AnalyticsManager().logEvent(AnalyticsEvents.TRIP_CREATE_FAILED);
    }
  }

  @override
  scaffoldBackgroundColor() {
    return ColorResource.light_white;
  }

  @override
  isResizeToBottomInset() {
    return true;
  }

  _createTrip() {
    bool isValidated = _formKey.currentState?.validate()??false;
    var errorMsg = bloc!.validateFields();
    if (!isNullOrEmpty(errorMsg)) {
      showToast(localize(errorMsg));
      return;
    } else if (!isValidated) return;
    if (bloc!.isOPS) {
      showCustomFormDialog(context, bloc?.projectId, bloc!.tripAmount,bloc!.comment, _reqCreateTrip);
    } else
      _reqCreateTrip();
  }

  _reqCreateTrip() async {
    bool isValidated = _formKey.currentState?.validate()??false;
    var errorMsg = bloc!.validateFields();
    if (!isNullOrEmpty(errorMsg)) {
      showToast(localize(errorMsg));
      return;
    }else if(!isValidated)return;
    if(!bloc!.isOPS && await bloc!.isOutsideCluster()){

      showDialog(
          context: context,
          barrierColor: Colors.black54,
          builder: (_) => CustomDialogBox(UniqueKey(),"images/dialog_error_icon.png",
              title: 'Cluster Mismatched!',
              descriptions: 'This district is not in your cluster . Are you sure you want to create this trip?',
              buttonText: 'Yes',
              negButtonText: 'No',
              ttlTextColor: Color.fromRGBO(255, 16, 0, 1), onFinishCallback: ()async{
                final error = await bloc!.validateDistrict();
                if(isNullOrEmpty(error)) _process();
                else  _districtMismatched(error);
              }));
      return;
    }
    final error = await bloc!.validateDistrict();
    if (!isNullOrEmpty(error)){
      _districtMismatched(error);
      return;
    }
    _process();
  }

  _districtMismatched(error){
    showDialog(
        context: context,
        barrierColor: Colors.black54,
        builder: (_) => CustomDialogBox(UniqueKey(),"images/dialog_error_icon.png",
            title: 'District mismatched!',
            descriptions: error,
            buttonText: 'Yes',
            negButtonText: 'No',
            ttlTextColor: Color.fromRGBO(255, 16, 0, 1), onFinishCallback: (){
              _process();
            }));
  }

  _process()async{
    submitDialog(context, dismissible: false);
    ApiResponse response = await bloc!.requestCreateTrip();
    trace.stop();
    Navigator.pop(context);
    createTripRes(response);
  }
}

_copyToClipboard(String text) {
  Clipboard.setData(ClipboardData(text: text));
  //showToast('Copied to clipboard');
}
