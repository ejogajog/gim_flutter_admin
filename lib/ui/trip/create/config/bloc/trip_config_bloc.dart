import 'package:app/api/api_response.dart';
import 'package:app/bloc/base_bloc.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/ui/trip/create/config/model/preset.dart';
import 'package:app/ui/trip/create/config/repo/customer_repo.dart';
import 'package:app/ui/trip/create/model/trip_model.dart';
import 'package:app/ui/trip/create/model/customer_model.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/ui/trip/create/model/preset_instruction.dart';
import 'package:app/ui/trip/create/model/truck_info.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/string_util.dart';
import 'package:app/model/master/goods_type.dart';

class TripConfigBloc extends BaseBloc {
  TruckInfo? _truckInfo;
  bool _validGood = true;
  Preset _preset = Preset();
  CustomerInfo? _customerInfo;
  CustomerPreferences? _customerPreferences;

  TripConfigBloc(this._customerInfo, this._customerPreferences) {
    if (_customerPreferences != null &&
        !isNullOrEmptyList(_customerPreferences?.instructionSuggestions)) {
      _preset.instructions = _customerPreferences?.instructionSuggestions;
    }
    _customerPreferences?.companyId = _customerInfo?.companyId;
  }

  selGoodsType(GoodsType? selGoods) {
    _preset.goodsId = selGoods?.id ?? 0;
  }

  setSrcAddress(address) {
    _preset.srcLat = address.lat;
    _preset.srcLng = address.lon;
    _preset.srcAddress = address.address;
    if (address.depot ?? false) {
      _preset.pickUpDepotId = address.id ?? 0;
      _preset.pickUpFactoryId = 0;
    } else {
      _preset.pickUpDepotId = 0;
      _preset.pickUpFactoryId = address.id ?? 0;
    }
  }

  setDstAddress(address) {
    _preset.dstLat = address.lat;
    _preset.dstLng = address.lon;
    _preset.dstAddress = address.address;
    if (address.depot ?? false) {
      _preset.dropOffDepotId = address.id ?? 0;
      _preset.dropOffFactoryId = 0;
    } else {
      _preset.dropOffDepotId = 0;
      _preset.dropOffFactoryId = address.id ?? 0;
    }
  }

  dropOffLocation(index) {
    if (index > -1) {
      _preset.tripDropoffList.removeAt(index);
      return _preset.tripDropoffList;
    }
    _preset.tripDropoffList.add(TripLocation(
        _preset.dropOffAddress,
        _preset.dropOfflat,
        _preset.dropOfflon,
        _preset.dropOffName,
        _preset.dropOffFactoryId,
        _preset.dropOffDepotId,
        _preset.dropOffDistrictId));
    return _preset.tripDropoffList;
  }

  truckInfo(truckDtl) {
    _truckInfo = truckDtl;
    _preset.truckType = _truckInfo?.type?.id;
    _preset.truckSize = _truckInfo?.size?.id;
    _preset.truckLen = _truckInfo?.length == null
        ? 0
        : double.tryParse(_truckInfo?.length.value)??0;
  }

  updateIns(index, content) {
    _preset.instructions?.elementAt(index).instruction = content;
  }

  goodsFlag(value) {
    _validGood = value;
  }

  goodsQnt(value) {
    _preset.qnt = int.tryParse(value)??0;
  }

  goodsUnit(unit) {
    _preset.unt = unit;
  }

  get instructions => _preset.instructions;

  get truckInf => _truckInfo ?? TruckInfo();

  get isEnterprise => !isNullOrEmptyInt(_customerInfo?.companyId);

  get customerPreset => _customerPreferences;

  get cmpId => _customerInfo?.companyId;

  get client =>
      '${_customerInfo?.userName}-${_customerInfo?.mobileNumber} (${_customerInfo?.companyName ?? localize('regular')})';

  updatePreset() async {
    validInstructions();
    _preset.companyId = cmpId;
    print(_preset.toJson());
    ApiResponse apiResponse = await CustomerRepository.setPresets(_preset);
    return apiResponse;
  }

  validInstructions() {
    _preset.instructions
        ?.retainWhere((element) => element.instruction?.trim().length != 0);
  }

  validateFields() {
    if (_preset.goodsTypeId == null ||
        _preset.goodsTypeId == 0 ||
        !_validGood) {
      return 'goods_emp_error';
    } else if (isNullOrEmpty(_preset.pickUpAddress)) {
      return 'src_emp_error';
    } else if (isNullOrEmpty(_preset.dropOffAddress)) {
      return 'dst_emp_error';
    } else if (truckInf.type == null) {
      return 'truck_type_error';
    } else if (truckInf.size == null) {
      return 'truck_size_error';
    } else if (_saveIns()) {
      return 'ins_save_error';
    } else if (!validInstLength()) {
      return 'ins_len_error';
    }
  }

  validInstLength() {
    int length = 0;
    _preset.instructions?.forEach((element) {
      length += element.toString().trim().length;
    });
    return length <= insMaxCharCount;
  }

  _saveIns() {
    for (int index = 0; index < _preset.instructions!.length; index++) {
      PresetInstruction instruction = _preset.instructions![index];
      if (!(instruction.isSaved ?? true)) return true;
    }
    return false;
  }
}
