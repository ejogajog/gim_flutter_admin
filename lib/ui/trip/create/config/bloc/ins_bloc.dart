import 'package:app/bloc/base_bloc.dart';
import 'package:app/ui/trip/create/model/preset_instruction.dart';

class InstructionBloc extends BaseBloc {
  int? companyId;
  Function? onEdit;
  List<PresetInstruction>? editInst;

  InstructionBloc(this.companyId, instructions,{this.onEdit}) {
    editInst = instructions ?? [];
  }

  addNewIns() {
    editInst?.add(PresetInstruction('', id: 0, companyId: companyId, isSaved: false));
    notifyListeners();
  }

  onDelete(int index) {
    editInst?.removeAt(index);
    notifyListeners();
  }

  onSave(insInd,content) {
    var isDuplicate = false;
    for (int index = 0; index < (editInst?.length ?? [].length); index++) {
      if (index != insInd && content.length > 0 && editInst?[index].toString().toLowerCase() == content.toLowerCase()) return true;
    }
    return isDuplicate;
  }
}
