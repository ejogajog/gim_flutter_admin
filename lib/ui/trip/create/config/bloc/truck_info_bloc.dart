import 'package:app/bloc/base_bloc.dart';
import 'package:app/model/master/truck_dimension.dart';
import 'package:app/model/master/truck_size.dart';
import 'package:app/model/master/truck_type.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/ui/trip/create/model/truck_info.dart';
import 'package:app/utils/prefs.dart';

class TruckInfoBloc extends BaseBloc {
  Function? callback;
  bool _showCustom = true;
  bool _addOthOpt = false;
  TruckType? _defTruckType;
  TruckSize? _defTruckSize;
  TruckInfo? _truckInfo;
  List<TruckSize>? _truckSizes;
  List<TruckType>? _truckTypes;
  TruckDimensionLength? _defTruckLen;
  List<TruckDimensionLength>? _truckLengths;

  CustomerPreferences? presetInfo;

  get truckTypes => _truckTypes ?? [];

  get truckSizes => _truckSizes ?? [];

  get truckLengths => _truckLengths ?? [];

  TruckInfoBloc(this._truckInfo, this.presetInfo, {this.callback}) {
    _truckTypes = Prefs.getTrucksType();
    _truckSizes = Prefs.getTrucksSizes();
    _truckLengths = Prefs.getTrucksLength();
    _initData();
  }

  set setCustom(val) {
    _showCustom = val;
    notifyListeners();
  }

  get custom => _showCustom;

  set _truckType(item) {
    setCustom = true;
    _truckInfo?.type = item;
    callback!(_truckInfo);
  }

  truckSize(item) {
    _truckInfo?.size = item;
    if (callback != null) callback!(_truckInfo);
  }

  truckLength(item) {
    _truckInfo?.length = item;
    if (callback != null) callback!(_truckInfo);
  }

  set truckDimen(index) {
    _truckInfo?.size = _truckSizes?[index];
    _truckInfo?.length = _truckLengths?[index];
    callback!(_truckInfo);
  }

  get addOthOptFlag => _addOthOpt;

  set othOptFlag(flag) {
    _addOthOpt = flag;
    notifyListeners();
  }

  get truckInfo => _truckInfo;

  get defTruck => _defTruckType;

  get defSize => _defTruckSize;

  get defLen => _defTruckLen;

  get truckLengthTxt =>
      _truckInfo?.length == null ? '' : _truckInfo?.length.toString();

  set addTruck(item) {
    _defTruckType = item;
    othOptFlag = !_addOthOpt;
    _truckType = _defTruckType;
    setCustom = false;
  }

  _initData() {
    if (presetInfo?.tripDataSuggestionResponse == null ||
        presetInfo?.tripDataSuggestionResponse?.truckTypeId == null) {
      othOptFlag = true;
      return;
    }
    TruckDataSuggestions? tds = presetInfo?.tripDataSuggestionResponse?.truckDataSuggestions;
    tds?.build();
    _defTruckType = TruckType(tds?.truckTypeId, tds?.truckType, tds?.truckTypeBn, null, 0,null, isDefault: true);
    _defTruckSize = tds?.sizeList[0];
    _defTruckLen = tds?.lengthList[0];
    truckSize(_defTruckSize);
    truckLength(_defTruckLen);
    _truckType = _defTruckType;
  }
}
