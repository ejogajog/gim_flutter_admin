import 'package:app/api/api_response.dart';

class CustomerRepository {
  static var helper = ApiBaseHelper();

  static Future<ApiResponse> setPresets(request) async {
    ApiResponse response = await helper.post("/ejogajogAdminAPI/api/v1/enterprises/trip/config", request);
    return response;
  }

}
