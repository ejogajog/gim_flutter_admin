// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'preset.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Preset _$PresetFromJson(Map<String, dynamic> json) => Preset()
  ..companyId = (json['companyId'] as num?)?.toInt()
  ..goodsTypeId = (json['goodsTypeId'] as num?)?.toInt()
  ..truckSizeId = (json['truckSizeId'] as num?)?.toInt()
  ..truckTypeId = (json['truckTypeId'] as num?)?.toInt()
  ..truckLength = (json['truckLength'] as num?)?.toDouble()
  ..pickUpFactoryId = (json['pickUpFactoryId'] as num?)?.toInt()
  ..pickUpDepotId = (json['pickUpDepotId'] as num?)?.toInt()
  ..pickUplat = (json['pickUplat'] as num?)?.toDouble()
  ..pickUplon = (json['pickUplon'] as num?)?.toDouble()
  ..dropOfflat = (json['dropOfflat'] as num?)?.toDouble()
  ..dropOfflon = (json['dropOfflon'] as num?)?.toDouble()
  ..pickUpAddress = json['pickUpAddress'] as String?
  ..pickUpDistrictId = (json['pickUpDistrictId'] as num?)?.toInt()
  ..dropOffDistrictId = (json['dropOffDistrictId'] as num?)?.toInt()
  ..dropOffAddress = json['dropOffAddress'] as String?
  ..dropOffFactoryId = (json['dropOffFactoryId'] as num?)?.toInt()
  ..dropOffDepotId = (json['dropOffDepotId'] as num?)?.toInt()
  ..instructions = (json['instructions'] as List<dynamic>?)
      ?.map((e) => PresetInstruction.fromJson(e as Map<String, dynamic>))
      .toList()
  ..quantityOfGoods = (json['quantityOfGoods'] as num?)?.toInt()
  ..goodsQuantityUnit = json['goodsQuantityUnit'] as String?;

Map<String, dynamic> _$PresetToJson(Preset instance) => <String, dynamic>{
      'companyId': instance.companyId,
      'goodsTypeId': instance.goodsTypeId,
      'truckSizeId': instance.truckSizeId,
      'truckTypeId': instance.truckTypeId,
      'truckLength': instance.truckLength,
      'pickUpFactoryId': instance.pickUpFactoryId,
      'pickUpDepotId': instance.pickUpDepotId,
      'pickUplat': instance.pickUplat,
      'pickUplon': instance.pickUplon,
      'dropOfflat': instance.dropOfflat,
      'dropOfflon': instance.dropOfflon,
      'pickUpAddress': instance.pickUpAddress,
      'pickUpDistrictId': instance.pickUpDistrictId,
      'dropOffDistrictId': instance.dropOffDistrictId,
      'dropOffAddress': instance.dropOffAddress,
      'dropOffFactoryId': instance.dropOffFactoryId,
      'dropOffDepotId': instance.dropOffDepotId,
      'instructions': instance.instructions,
      'quantityOfGoods': instance.quantityOfGoods,
      'goodsQuantityUnit': instance.goodsQuantityUnit,
    };
