import 'package:app/ui/trip/create/model/preset_instruction.dart';
import 'package:app/ui/trip/create/model/trip_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'preset.g.dart';

@JsonSerializable()
class Preset {
  int? companyId;
  int? goodsTypeId;
  int? truckSizeId;
  int? truckTypeId;
  double? truckLength;
  int? pickUpFactoryId;
  int? pickUpDepotId;
  double? pickUplat;
  double? pickUplon;
  double? dropOfflat;
  double? dropOfflon;
  String? pickUpAddress;
  int? pickUpDistrictId;
  int? dropOffDistrictId;
  String? dropOffAddress;
  int? dropOffFactoryId;
  int? dropOffDepotId;
  @JsonKey(ignore: true)
  String? pickUpName;
  @JsonKey(ignore: true)
  String? dropOffName;
  List<PresetInstruction>? instructions = [];
  @JsonKey(ignore: true)
  List<TripLocation> tripDropoffList = [];
  int? quantityOfGoods;
  String? goodsQuantityUnit;

  Preset();

  factory Preset.fromJson(Map<String, dynamic> json) => _$PresetFromJson(json);

  Map<String, dynamic> toJson() => _$PresetToJson(this);

  set goodsId(int? selGoodsId) {
    goodsTypeId = selGoodsId;
  }

  set truckType(int? selTruckType) {
    truckTypeId = selTruckType;
  }

  set truckSize(int? selTruckSize) {
    truckSizeId = selTruckSize;
  }

  set truckLen(double? selTruckLen) {
    truckLength = selTruckLen;
  }

  set srcAddress(String? selSrcAdr) {
    pickUpAddress = selSrcAdr;
  }

  set dstAddress(String? selDstAdr) {
    dropOffAddress = selDstAdr;
  }

  set srcLat(double? selSrcLat) {
    pickUplat = selSrcLat;
  }

  set srcLng(double? selSrcLng) {
    pickUplon = selSrcLng;
  }

  set dstLat(double? selDstLat) {
    dropOfflat = selDstLat;
  }

  set dstLng(double? selDstLng) {
    dropOfflon = selDstLng;
  }

  set qnt(int? quantity) {
    this.quantityOfGoods = quantity;
  }

  set unt(String? unit) {
    this.goodsQuantityUnit = unit;
  }
}
