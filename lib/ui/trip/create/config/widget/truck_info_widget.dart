import 'package:flutter/material.dart';
import 'package:app/model/master/truck_dimension.dart';
import 'package:app/model/master/truck_size.dart';
import 'package:app/ui/trip/create/config/bloc/truck_info_bloc.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:provider/provider.dart';
import 'package:app/styles.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/ui/widget/editfield.dart';
import 'package:app/utils/validation_util.dart';
import 'package:app/model/master/truck_type.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/ui/trip/create/model/truck_info.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';

class TruckTypeWidget extends StatelessWidget {
  final Function? callback;
  final TruckInfo? truckInfo;
  final CustomerPreferences? presetInfo;

  TruckTypeWidget(this.truckInfo, {this.callback, this.presetInfo});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<TruckInfoBloc>(
          create: (_) =>
              TruckInfoBloc(truckInfo, this.presetInfo, callback: callback),
        ),
      ],
      child: _TruckTypeWidget(),
    );
  }
}

class _TruckTypeWidget extends StatefulWidget {
  @override
  _TripTruckTypeWidgetState createState() => _TripTruckTypeWidgetState();
}

class _TripTruckTypeWidgetState extends State<_TruckTypeWidget> {
  TruckInfoBloc? bloc;
  double spaceWidth = 5;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<TruckInfoBloc>(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(height: 20.0),
        Text(localize('sel_truck_type'), style: Styles.formLabel),
        SizedBox(height: 5.0),
        Consumer<TruckInfoBloc>(builder: (context, bloc, _) => _buildUi()),
      ],
    );
  }

  Widget _buildUi() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: TextEditText(
            labelText: '',
            fontSize: 12,
            hintText: localize('trk_type'),
            isDropDown: true,
            selValue: bloc!.truckInfo.type,
            borderColor: ColorResource.brownish_grey,
            suggestionDropDownItemList: bloc!.truckTypes,
            searchDrodownCallback: (item) {
              bloc!.addTruck = item;
              AnalyticsManager().logEventProp(
                  AnalyticsEvents.TRUCK_TYPE_ITM_CLK,
                  {AnalyticProperties.TRUCK_TYPE: item.toString()});
            },
            validationMessageRegexPair: {
              NOT_EMPTY_REGEX: translate(context, 'mne_val_msg')
            },
          ),
        ),
        SizedBox(width: spaceWidth),
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: TextEditText(
                  labelText: '',
                  fontSize: 12,
                  isDropDown: true,
                  hintText: localize('unit_ton'),
                  selValue: bloc!.truckInfo.size,
                  borderColor: ColorResource.brownish_grey,
                  suggestionDropDownItemList: bloc!.truckSizes,
                  searchDrodownCallback: (item) {
                    bloc!.truckSize(item);
                    AnalyticsManager().logEventProp(
                        AnalyticsEvents.TRUCK_Size_BTN_CLK,
                        {AnalyticProperties.TRUCK_SIZE: item.toString()});
                  },
                  validationMessageRegexPair: {
                    NOT_EMPTY_REGEX: translate(context, 'mne_val_msg')
                  },
                ),
              ),
              SizedBox(width: spaceWidth),
              Expanded(
                child: TextEditText(
                  labelText: '',
                  fontSize: 10,
                  isDropDown: true,
                  hintText: localize('unit_feet'),
                  selValue: bloc!.truckInfo.length,
                  borderColor: ColorResource.brownish_grey,
                  suggestionDropDownItemList: bloc!.truckLengths,
                  searchDrodownCallback: (item) {
                    bloc!.truckLength(item);
                    AnalyticsManager().logEventProp(
                        AnalyticsEvents.TRUCK_Length_BTN_CLK,
                        {AnalyticProperties.TRUCK_Length: item.toString()});
                  },
                  validationMessageRegexPair: {
                    NOT_EMPTY_REGEX: translate(context, 'mne_val_msg')
                  },
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
