import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/styles.dart';
import 'package:app/ui/trip/create/config/bloc/trip_address_bloc.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/ui/widget/auto_search_location.dart';
import 'package:app/ui/widget/editfield.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/ui_util.dart';
import 'package:app/utils/validation_util.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

class LocationWidget extends StatelessWidget {
  final bool? isMultiple;
  final Function? address;
  final Function? locations;
  final CustomerPreferences? presetInfo;

  LocationWidget(this.address, this.presetInfo,
      {this.isMultiple = false, this.locations});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<TripAddressBloc>(
          create: (_) => TripAddressBloc(address, isMultiple, presetInfo,
              locations: locations),
        ),
      ],
      child: _SetLocationWidget(),
    );
  }
}

class _SetLocationWidget extends StatefulWidget {
  @override
  _SetTripLocationWidgetState createState() => _SetTripLocationWidgetState();
}

class _SetTripLocationWidgetState extends State<_SetLocationWidget> {
  double mapHeight = 240;
  double iconSize = 24;
  TripAddressBloc? bloc;
  bool isShown = false;
  String showHide = "Show Map";

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of(context, listen: false);
    return Consumer<TripAddressBloc>(
        builder: (context, bloc, _) => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                    localize((bloc.isMultiple??false) ? 'set_drop_loc' : 'set_pick_loc'),
                    style: Styles.formLabel),
                SizedBox(height: 5.0),
                Text(bloc.selLblAddress, style: Styles.selMedTextBold),
                SizedBox(height: 5.0),
                Wrap(
                  children: [
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Expanded(
                              child: GooglePlacesSearchWidget(
                                isSrc: true,
                                companyId: bloc?.companyId,
                                focusNode: bloc.focusNode,
                                callBack: _onFocus,
                                immediateSuggestion: false,
                                hintText: translate(
                                    context,
                                    (bloc.isMultiple??false)
                                        ? 'eda_or_dp'
                                        : 'epa_or_dp'),
                                typeAheadController: bloc.addressController,
                                onSuggestionItemSelected: (dynamic value) {
                                  bloc.getPosUsingPlaceId(value);
                                  AnalyticsManager().logEventProp(AnalyticsEvents.LOADING_POINT_ITEM_CLK,{AnalyticProperties.ADDRESS:value.toString()});
                                },
                                validationMessageRegexPair: {
                                  NOT_EMPTY_REGEX:
                                      translate(context, 'mne_val_msg')
                                },
                                onChange: (data, {isEditing = true}){
                                  print('Onchange Get Called======>>>>>>>>>>>>');
                                  bloc.onChange(data, isEditing: isEditing);
                                },
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 2.0),
                        Visibility(
                          visible: isShown,
                          child: LayoutBuilder(
                              builder: (context, constraint) => Stack(
                                    children: [
                                      GestureDetector(
                                        onHorizontalDragStart: (start) {
                                          bloc.flagUpdateOnDrag = true;
                                        },
                                        onVerticalDragStart: (start) {
                                          bloc.flagUpdateOnDrag = true;
                                        },
                                        child: Container(
                                          height: mapHeight,
                                          child: GoogleMap(
                                            onMapCreated: bloc.onCreated,
                                            initialCameraPosition: CameraPosition(
                                              target: bloc.markerPosition,
                                              zoom: 12.0,
                                            ),
                                            onCameraMove: bloc.onCameraMove,
                                            onCameraIdle: bloc.onCameraIdle,
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                        top: (mapHeight - iconSize) / 2,
                                        right: isMobileBrowser(constraint)
                                            ? (constraint.maxWidth - iconSize) / 2
                                            : (containerWidth -
                                                    (iconSize +
                                                        (2 *
                                                            horizontalPadding))) /
                                                2,
                                        child: Image.asset(
                                          (bloc.isMultiple??false)
                                              ? "images/dropoff_pin.png"
                                              : "images/pickup_pin.png",
                                          height: 28,
                                          width: 24,
                                        ),
                                      )
                                    ],
                                  )),
                        ),
                        RichText(
                          text: new TextSpan(
                            children: [TextSpan(
                              text: showHide,
                              style: TextStyle(color: Colors.blue, decoration: TextDecoration.underline),
                              recognizer: new TapGestureRecognizer()
                                ..onTap = () {
                                  setState(() {
                                    isShown = !isShown;
                                    showHide = isShown ? "Hide Map" : "Show Map";
                                  });
                                },
                            ),
                            ],
                          ),
                        ),
                        (bloc.isMultiple??false)
                            ? ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: bloc.listOfLocations.length,
                                itemBuilder: (context, index) => TextEditText(
                                  labelText: '',
                                  readOnly: true,
                                  suffixIcon: InkWell(
                                      child: Icon(Icons.clear),
                                      onTap: () =>
                                          bloc.handleMultipleLocation(index)),
                                  textEditingController: TextEditingController(
                                      text: bloc.listOfLocations[index]
                                          .dropoffAddress),
                                  behaveNormal: true,
                                ),
                              )
                            : SizedBox()
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 3.0),
              ],
            ));
  }

  _onFocus(FocusNode focusNode) {
    bloc?.modAddress(focusNode.hasFocus);
  }
}
