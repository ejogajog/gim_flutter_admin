import 'package:flutter/material.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/ui/trip/create/model/preset_instruction.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/string_util.dart';
import 'package:app/utils/ui_util.dart';

class InstructionItemWidget extends StatefulWidget {
  final int? index;
  final Function? onSave;
  final Function? onDelete;
  final PresetInstruction? instruction;

  InstructionItemWidget(this.index, this.onSave, this.onDelete, this.instruction)
      : super(key: ValueKey(instruction));

  @override
  _InstructionItemWidgetState createState() => _InstructionItemWidgetState();
}

class _InstructionItemWidgetState extends State<InstructionItemWidget> {
  bool? _isReadOnly;
  String? _lastValue;
  double iconSize = 24.0;
  TextEditingController? tc;

  @override
  void initState() {
    super.initState();
    tc = TextEditingController(text: widget.instruction.toString());
    _lastValue = widget.instruction.toString();
    _isReadOnly =
        (_lastValue?.trim().length != 0 && (widget.instruction?.isSaved ?? true));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 5.0),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              minLines: 1,
              maxLines: 5,
              controller: tc,
              readOnly: _isReadOnly!,
              keyboardType: TextInputType.multiline,
              style: TextStyle(color: ColorResource.colorMarineBlue),
              decoration: InputDecoration(
                filled: true,
                fillColor: ColorResource.colorWhite,
                focusColor: ColorResource.colorWhite,
                contentPadding: const EdgeInsets.all(10.0),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: _isReadOnly!
                      ? BorderSide(color: ColorResource.transBluBg)
                      : BorderSide(color: ColorResource.colorLightBlueGrey),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: _isReadOnly!
                      ? BorderSide(color: ColorResource.transBluBg)
                      : BorderSide(color: ColorResource.colorLightBlueGrey),
                ),
              ),
              onChanged: (data) {
                if (widget.instruction?.id == 0)
                  widget.instruction?.instruction = data;
              },
            ),
          ),
          SizedBox(width: 10.0),
          _isReadOnly!
              ? GestureDetector(
                  onTap: () {
                    setState(() {
                      _isReadOnly = false;
                      widget.instruction?.isSaved = false;
                      tc?.value = tc!.value.copyWith(text: tc!.text);
                      AnalyticsManager().logEvent(AnalyticsEvents.INST_EDIT_BTN_CLK);
                    });
                  },
                  child: Icon(
                    Icons.edit,
                    size: iconSize,
                    color: ColorResource.colorMarineBlue,
                  ))
              : Row(mainAxisSize: MainAxisSize.min, children: [
                  GestureDetector(
                      onTap: () {
                        if (tc!.text.trim().length == 0) {
                          showToast(localize('ins_emp_error'));
                          return;
                        } else if (widget.onSave!(widget.index, tc!.text)) {
                          showToast(localize('dup_ins_sel'));
                          return;
                        }
                        setState(() {
                          _isReadOnly = true;
                          _lastValue = tc?.text;
                          widget.instruction?.isSaved = true;
                          widget.instruction?.instruction = _lastValue;
                        });
                        AnalyticsManager().logEvent(AnalyticsEvents.INST_SAVE_BTN_CLK);
                      },
                      child: Icon(Icons.done_outlined,
                          size: iconSize,
                          color: ColorResource.colorMarineBlue)),
                  SizedBox(width: 10.0),
                  GestureDetector(
                      onTap: () {
                        if (isNullOrEmpty(_lastValue)) {
                          widget.onDelete!(widget.index);
                          return;
                        }
                        setState(() {
                          _isReadOnly = true;
                          widget.instruction?.isSaved = true;
                          tc?.value = tc!.value.copyWith(text: _lastValue);
                        });
                      },
                      child: Icon(Icons.close_outlined,
                          size: iconSize,
                          color: ColorResource.trip_above_24hourBg)),
                ]),
          (_isReadOnly??false)
              ? Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: GestureDetector(
                      onTap: () {
                        widget.onDelete!(widget.index);
                        AnalyticsManager().logEvent(AnalyticsEvents.INST_DEL_BTN_CLK);
                      },
                      child: Icon(
                        Icons.delete,
                        size: iconSize,
                        color: ColorResource.trip_above_24hourBg,
                      )),
                )
              : SizedBox()
        ],
      ),
    );
  }

  @override
  void dispose() {
    tc?.dispose();
    super.dispose();
  }
}
