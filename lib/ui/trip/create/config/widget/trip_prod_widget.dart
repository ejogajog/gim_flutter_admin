import 'package:flutter/material.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/model/master/goods_type.dart';
import 'package:app/styles.dart';
import 'package:app/ui/trip/create/bloc/product_type_bloc.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/ui/widget/editfield.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/validation_util.dart';
import 'package:provider/provider.dart';

class ProductTypeWidget extends StatelessWidget {
  final Function? callback;
  final Function? validGoods;
  final CustomerPreferences? presetInfo;

  ProductTypeWidget(this.presetInfo, this.validGoods,{this.callback});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ProductTypeBloc>(
          create: (_) => ProductTypeBloc(this.presetInfo,this.validGoods,callback: callback),
        ),
      ],
      child: _TripProductWidget(),
    );
  }
}

class _TripProductWidget extends StatefulWidget {
  @override
  _TripProductWidgetState createState() => _TripProductWidgetState();
}

class _TripProductWidgetState extends State<_TripProductWidget> {
  ProductTypeBloc? bloc;
  final FocusNode focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<ProductTypeBloc>(context);
    return Consumer<ProductTypeBloc>(
        builder: (context, bloc, _) => Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 20.0),
                Text(localize('sel_prod_type'), style: Styles.formLabel),
                SizedBox(height: 6.0),
                Text(bloc.selGoods, style: Styles.selMedTextBold),
                SizedBox(height: 3.0),
                productWidget()
              ],
            ));
  }

  Widget productWidget() {
    return TextEditText<GoodsType>(
      labelText: '',
      hintText: localize('sel_prod_type'),
      behaveNormal: true,
      fontWeight: FontWeight.w300,
      isFilterWidget: true,
      isSearchableItemWidget: true,
      suggestionDropDownItemList: bloc!.masterGoods,
      onChanged: (data) {
        bloc!.isGoodsValid = false;
      },
      searchDrodownCallback: (item) {
        bloc!.prodEditController.text = item!.text!;
        bloc!.productType = item;
        AnalyticsManager().logEventProp(AnalyticsEvents.GOODS_TYPE_ITEM_CLK,{AnalyticProperties.GOODS_ID: item.id});
      },
      focusNode: focusNode,
      onFocusChange: (FocusNode focusNode) {
        if (!focusNode.hasFocus && !bloc!.isGoodsValid){
          bloc!.prodEditController.clear();
        }
        bloc!.removeGoodsType(bloc!.prodEditController.text);
      },
      validationMessageRegexPair: {
        NOT_EMPTY_REGEX: translate(context, 'mne_val_msg')
      },
      textEditingController: bloc!.prodEditController,
    );
  }

  @override
  void dispose() {
    focusNode.dispose();
    bloc?.prodEditController.dispose();
    super.dispose();
  }
}
