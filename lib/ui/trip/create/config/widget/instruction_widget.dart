import 'package:flutter/material.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/ui/trip/create/config/bloc/ins_bloc.dart';
import 'package:app/ui/trip/create/config/widget/instruction_item_widget.dart';
import 'package:app/ui/trip/create/model/preset_instruction.dart';
import 'package:app/ui/widget/app_button.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/color_const.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class TripInstructionsWidget extends StatelessWidget {

  final int? companyId;
  final Function? onEdit;
  final List<PresetInstruction>? instructions;

  TripInstructionsWidget(this.companyId, this.instructions,{this.onEdit});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<InstructionBloc>(
          create: (_) => InstructionBloc(companyId, instructions, onEdit: this.onEdit),
        ),
      ],
      child: InstructionWidget(),
    );
  }
}

class InstructionWidget extends StatefulWidget {
  @override
  _TripInstructionsWidgetState createState() => _TripInstructionsWidgetState();
}

class _TripInstructionsWidgetState extends State<InstructionWidget> {
  InstructionBloc? bloc;

  @override
  void initState() {
    super.initState();
    bloc = Provider.of<InstructionBloc>(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<InstructionBloc>(
        builder: (context, bloc, _) => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(height: 20.0),
                  Text(localize('sel_ur_inst'),
                      style: GoogleFonts.roboto(
                        color: Color.fromRGBO(100, 100, 100, 1.0),
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                      )),
                  SizedBox(height: 5.0),
                  ..._editableInst(),
                  FilledColorButton(
                    fontSize: 14.0,
                    isFullwidth: false,
                    textColor: ColorResource.colorWhite,
                    verticalMargin: 20.0,
                    buttonText: translate(context, 'ins_new_btn_txt'),
                    onPressed: () {
                      bloc.addNewIns();
                    },
                    eventName: AnalyticsEvents.ADD_NEW_INST_BTN_CLK,
                  )
                ]));
  }

  List<Widget> _editableInst() {
    return List.generate(
        bloc!.editInst!.length,
        (index) => InstructionItemWidget(index, _onSave, _onDelete, bloc!.editInst?[index]));
  }

  _onDelete(index) {
    bloc!.onDelete(index);
    _onEdit();
  }

  _onSave(index,content) {
    _onEdit();
    return bloc!.onSave(index,content);
  }

  _onEdit(){
    if(bloc!.onEdit != null)bloc!.onEdit!();
  }
}
