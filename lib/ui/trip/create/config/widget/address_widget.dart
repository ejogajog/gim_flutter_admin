import 'package:flutter/material.dart';
import 'package:app/ui/trip/create/config/widget/trip_address_widget.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';

class TripAddressWidget extends StatefulWidget {
  final Function pickUpAddress;
  final Function dropOffAddress;
  final Function dropOffLocations;
  final CustomerPreferences presetInfo;

  TripAddressWidget(this.presetInfo, this.pickUpAddress, this.dropOffAddress,
      this.dropOffLocations);

  @override
  _TripAddressWidgetState createState() => _TripAddressWidgetState();
}

class _TripAddressWidgetState extends State<TripAddressWidget> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 20.0),
        LocationWidget(widget.pickUpAddress, widget.presetInfo),
        SizedBox(height: 20.0),
        LocationWidget(widget.dropOffAddress, widget.presetInfo,locations: widget.dropOffLocations, isMultiple: true),
      ],
    );
  }
}
