import 'package:flutter/material.dart';
import 'package:app/api/api_response.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/ui/trip/create/config/bloc/trip_config_bloc.dart';
import 'package:app/ui/trip/create/config/widget/address_widget.dart';
import 'package:app/ui/trip/create/config/widget/instruction_widget.dart';
import 'package:app/ui/trip/create/config/widget/trip_prod_widget.dart';
import 'package:app/ui/trip/create/config/widget/truck_info_widget.dart';
import 'package:app/ui/trip/create/model/customer_model.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/ui/widget/app_button.dart';
import 'package:app/ui/widget/base_widget.dart';
import 'package:app/ui/widget/goods/goods_unit_widget.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/string_util.dart';
import 'package:app/utils/ui_util.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class CustomerTripConfigPage extends StatelessWidget {
  final CustomerInfo? cstInfo;
  final CustomerPreferences? cstPref;

  CustomerTripConfigPage(this.cstInfo, this.cstPref);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<TripConfigBloc>(
            create: (_) => TripConfigBloc(this.cstInfo, this.cstPref)),
      ],
      child: _CustomerTripConfigScreen(),
    );
  }
}

class _CustomerTripConfigScreen extends StatefulWidget {
  @override
  _CustomerTripConfigScreenState createState() =>
      _CustomerTripConfigScreenState();
}

class _CustomerTripConfigScreenState
    extends BasePageWidgetState<_CustomerTripConfigScreen, TripConfigBloc> {
  var _formKey = GlobalKey<FormState>();

  @override
  List<Widget> getPageWidget() {
    var pad = 10.0;
    var curveRadius = Radius.circular(10.0);
    return [
      LayoutBuilder(
          builder: (context, constraint) => Column(
                children: [
                  Expanded(
                      child: SingleChildScrollView(
                          child: Form(
                    key: _formKey,
                    child: Center(
                      child: Container(
                          padding: EdgeInsets.fromLTRB(pad, pad, pad, 0.0),
                          width: isMobileBrowser(constraint)
                              ? double.infinity
                              : containerWidth,
                          decoration: BoxDecoration(
                              color: ColorResource.bluBg,
                              borderRadius: BorderRadius.only(
                                  topLeft: curveRadius, topRight: curveRadius)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(bloc!.client,
                                  style: GoogleFonts.roboto(
                                    color: ColorResource.colorMarineBlue,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500,
                                  )),
                              SizedBox(height: 20.0),
                              Text(localize('cst_preset_cfg_ttl'),
                                  style: GoogleFonts.roboto(
                                    color: ColorResource.colorMarineBlue,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w500,
                                  )),
                              SizedBox(height: 4.0),
                              Text(localize('cst_preset_cfg_dsc'),
                                  style: GoogleFonts.roboto(
                                    color: ColorResource.colorBrownGrey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                  )),
                              SizedBox(height: 20.0),
                              Container(
                                padding: EdgeInsets.all(2 * pad),
                                child: TripAddressWidget(
                                    bloc!.customerPreset,
                                    bloc!.setSrcAddress,
                                    bloc!.setDstAddress,
                                    bloc!.dropOffLocation),
                                decoration: BoxDecoration(
                                    color: ColorResource.colorWhite,
                                    borderRadius: BorderRadius.only(
                                        topLeft: curveRadius,
                                        topRight: curveRadius)),
                              ),
                              Container(
                                  padding: EdgeInsets.all(2 * pad),
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      ProductTypeWidget(bloc!.customerPreset, bloc!.goodsFlag,
                                          callback: bloc!.selGoodsType),
                                      GoodsUnitWidget(bloc!.goodsQnt,bloc!.goodsUnit, defQntUnit: bloc!.customerPreset.qntUnit, showGoodsQty: false),
                                      TruckTypeWidget(bloc!.truckInf,
                                          callback: bloc!.truckInfo, presetInfo: bloc!.customerPreset),
                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                    color: ColorResource.lightBluBg,
                                  )),
                              Container(
                                  width: double.maxFinite,
                                  padding: const EdgeInsets.only(
                                      left: horizontalPadding,
                                      right: horizontalPadding),
                                  child: TripInstructionsWidget(bloc!.cmpId, bloc!.instructions),
                                  decoration: BoxDecoration(
                                      color: ColorResource.off_white))
                            ],
                          )),
                    ),
                  ))),
                  Container(
                    padding: EdgeInsets.fromLTRB(pad, 0.0, pad, pad),
                    width: isMobileBrowser(constraint)
                        ? double.infinity
                        : containerWidth,
                    decoration: BoxDecoration(
                        color: ColorResource.bluBg,
                        borderRadius: BorderRadius.only(
                            bottomLeft: curveRadius, bottomRight: curveRadius)),
                    child: Container(
                      decoration: BoxDecoration(
                          color: ColorResource.colorWhite,
                          borderRadius: BorderRadius.only(
                              bottomLeft: curveRadius,
                              bottomRight: curveRadius)),
                      child: FilledColorButton(
                        isFullwidth: false,
                        textColor: ColorResource.colorWhite,
                        horizonatalMargin: horizontalPadding,
                        buttonText: translate(context, 'btn_sav'),
                        onPressed: () => _reqSetPreset(),
                        eventName: AnalyticsEvents.SAVE_CONFIG_BTN_CLICK,
                      ),
                    ),
                  ),
                ],
              )),
    ];
  }

  _reqSetPreset() async {
    bool isValidated = _formKey.currentState?.validate()??false;
    var errorMsg = bloc!.validateFields();
    if (!isNullOrEmpty(errorMsg)) {
      showToast(localize(errorMsg));
      return;
    } else if (!isValidated) return;
    submitDialog(context, dismissible: false);
    ApiResponse response = await bloc!.updatePreset();
    Navigator.pop(context);
    _setPresetRes(response);
  }

  _setPresetRes(apiResponse) {
    if (apiResponse.status == Status.COMPLETED) {
      showToast(apiResponse.message ?? "Preset updated successfully.");
      Navigator.pop(context, true);
      AnalyticsManager().logEvent(AnalyticsEvents.CONFIG_SAVE_SUCCESS);
    } else {
      showToast('Preset update failed please try again.');
      AnalyticsManager().logEvent(AnalyticsEvents.CONFIG_SAVE_FAILED);
    }
  }

  @override
  scaffoldBackgroundColor() {
    return ColorResource.light_white;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  isResizeToBottomInset() {
    return true;
  }
}
