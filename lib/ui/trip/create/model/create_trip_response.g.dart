// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_trip_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateTripResponse _$CreateTripResponseFromJson(Map<String, dynamic> json) =>
    CreateTripResponse()
      ..data = (json['data'] as List<dynamic>?)
          ?.map((e) => TripResponse.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$CreateTripResponseToJson(CreateTripResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
    };

TripResponse _$TripResponseFromJson(Map<String, dynamic> json) => TripResponse()
  ..tripId = (json['tripId'] as num?)?.toInt()
  ..tripNumber = (json['tripNumber'] as num?)?.toInt();

Map<String, dynamic> _$TripResponseToJson(TripResponse instance) =>
    <String, dynamic>{
      'tripId': instance.tripId,
      'tripNumber': instance.tripNumber,
    };
