import 'package:app/ui/trip/create/model/base_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'truck_dimen.g.dart';

@JsonSerializable()
class TruckDimen {
  TruckLength? Length;

  TruckDimen();

  factory TruckDimen.fromJson(Map<String, dynamic> json) =>
      _$TruckDimenFromJson(json);

  Map<String, dynamic> toJson() => _$TruckDimenToJson(this);
}

@JsonSerializable()
class TruckLength extends DataInfo{
  int? id;
  int? value;

  TruckLength({this.id, this.value});

  List<TruckLength> build() {
    int val = 5;
    List<TruckLength> length = [];
    for (int index = 1; index < 72; index++) {
      length.add(TruckLength(id: index == 1 ? index : ++index, value: val++));
    }
    return length;
  }

  factory TruckLength.fromJson(Map<String, dynamic> json) =>
      _$TruckLengthFromJson(json);

  Map<String, dynamic> toJson() => _$TruckLengthToJson(this);

  @override
  String toString() {
    return value.toString();
  }

  @override
  get iD => value;
}
