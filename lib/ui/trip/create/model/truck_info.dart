import 'package:app/model/master/truck_dimension.dart';
import 'package:app/model/master/truck_size.dart';
import 'package:app/model/master/truck_type.dart';

class TruckInfo {
  TruckType? _truckType;
  TruckSize? _truckSize;
  TruckDimensionLength? _truckLength;

  set type(trkType) {
    _truckType = trkType;
  }

  set size(trkSiz) {
    _truckSize = trkSiz;
  }

  set length(trkLen) {
    _truckLength = trkLen;
  }

  get type => _truckType;

  get size => _truckSize;

  get length => _truckLength;
}
