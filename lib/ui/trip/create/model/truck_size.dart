import 'package:app/ui/trip/create/model/base_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'truck_size.g.dart';

@JsonSerializable()
class TruckSize extends DataInfo{
  int? id;
  int? size;

  TruckSize({this.id, this.size});

  List<TruckSize> build() {
    int val = 1;
    List<TruckSize> truckSizes = [];
    for (int index = 1; index < 40; index++) {
      truckSizes.add(TruckSize(id: (index == 1 ? index : ++index), size: val++));
    }
    return truckSizes;
  }

  factory TruckSize.fromJson(Map<String, dynamic> json) =>
      _$TruckSizeFromJson(json);

  Map<String, dynamic> toJson() => _$TruckSizeToJson(this);

  @override
  String toString() {
    return size.toString();
  }

  @override
  get iD => id;
}
