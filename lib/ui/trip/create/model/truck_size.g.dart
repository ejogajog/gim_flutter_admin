// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'truck_size.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TruckSize _$TruckSizeFromJson(Map<String, dynamic> json) => TruckSize(
      id: (json['id'] as num?)?.toInt(),
      size: (json['size'] as num?)?.toInt(),
    );

Map<String, dynamic> _$TruckSizeToJson(TruckSize instance) => <String, dynamic>{
      'id': instance.id,
      'size': instance.size,
    };
