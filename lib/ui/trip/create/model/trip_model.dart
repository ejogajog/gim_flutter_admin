import 'dart:core';

import 'package:flutter/foundation.dart';
import 'package:app/model/master/goods_type.dart';
import 'package:app/model/master/truck_size.dart';
import 'package:app/model/master/truck_type.dart';
import 'package:app/ui/template/create/template_model.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/ui/trip/create/model/preset_instruction.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/string_util.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:app/model/master/districts.dart';
part 'trip_model.g.dart';

@JsonSerializable()
class TripModel {

  TripModel();

  String? name;
  int? templateId;
  int? companyId;
  int? tripCount;
  String? datetimeLocal;
  String? datetimeUtc;
  String? datetimeUtcStr;
  String? dropOffAddress;
  double? dropOfflat;
  double? dropOfflon;
  int? goodsType;
  String? goodsTypeStr;
  String? otherGoodType = "";
  String? image;
  String? paymentType = "Cash";
  String? pickUpAddress;
  double? pickUplat;
  double? pickUplon;
  String? specialInsturctions;
  String? truckHeight;
  double? truckLength;
  double? truckSize;
  int? truckType;
  int? productId;
  int? projectId;
  String? distributorStr;
  String? comment;
  int? expectedTripPrice = 0;
  String? customerMobileNumber;
  String? creationReason;
  bool? pending = true;
  bool? fixBid = false;
  int? pickUpFactoryId;
  int? pickUpDepotId;
  int? dropOffFactoryId;
  int? dropOffDepotId;
  String? dropOffName;
  String? pickUpName;
  int? noOfUnloadPoints;
  GoodsType? goodType;
  TruckType? truckTypeResponse;
  TruckSize? truckSizeResponse;
  List<TripLocation>? tripDropoffList = [];
  List<PresetInstruction>? instructions = [];
  @JsonKey(ignore: true)
  String? tempPickUpAddress;
  double?  quantityOfGoods;
  String? goodsQuantityUnit;
  int? pickupDistrictId;
  String? platformType = kIsWeb ? "GIM_ADMIN_APP_WEB_VERSION" : "GIM_ADMIN_APP";
  @JsonKey(ignore: true)
  List<BaseAddressesSuggestion?> loadPoint = [null];
  @JsonKey(ignore: true)
  List<BaseAddressesSuggestion?> unloadPoints = [null];
  int? kamUserId;
  bool? hiddenFromPartner = false;
  String? lcNumber;

  factory TripModel.fromJson(Map<String, dynamic> json) =>
      _$TripModelFromJson(json);

  Map<String, dynamic> toJson() => _$TripModelToJson(this);

  clearPickAddress() {
    pickUplat = null;
    pickUplon = null;
    pickUpDepotId = 0;
    pickUpFactoryId = 0;
    pickUpName = null;
    pickUpAddress = null;
  }

  clearDropAddress() {
    dropOfflat = null;
    dropOfflon = null;
    dropOffDepotId = 0;
    dropOffFactoryId = 0;
    dropOffName = null;
    dropOffAddress = null;
  }

  set unloadingPoint(address){
    dropOfflat = address.lat;
    dropOfflon = address.lon;
    dropOffName = address.name;
    dropOffAddress = address.address;
    if (address.depot ?? false) {
      dropOffDepotId = address.id ?? 0;
      dropOffFactoryId = 0;
    } else {
      dropOffDepotId = 0;
      dropOffFactoryId = address.id ?? 0;
    }
  }

  loadingAddress() => pickUpName ?? pickUpAddress;

  unloadingAddress() => dropOffName ?? dropOffAddress;

  loadingPoint(){
    return BaseAddressesSuggestion(lat: pickUplat,lon: pickUplon,name: pickUpName, address: pickUpAddress, id: pickUpFactoryId ?? pickUpDepotId, depot: pickUpDepotId != null, isDefault: true);
  }

  unLoadingPoint(){
    return BaseAddressesSuggestion(name: dropOffName, address: dropOffAddress, lat: dropOfflat, lon: dropOfflon, id: dropOffFactoryId ?? dropOffDepotId, depot: dropOffDepotId != null, isDefault: true);
  }

  set tripLocation(upAddress){
    tripDropoffList = upAddress;
  }

  buildDefUnloadPoints(){
    unloadPoints = [];
    tripDropoffList?.forEach((element) {
      unloadPoints.add(element.baseAddress);
    });
    return unloadPoints;
  }

  buildAddressPoints(){
    pickUplat = loadPoint[0]?.lat;
    pickUplon = loadPoint[0]?.lon;
    pickUpDepotId = loadPoint[0]?.depot ?? false ? loadPoint[0]?.id : 0;
    pickUpFactoryId = loadPoint[0]?.depot ?? true ? 0 : loadPoint[0]?.id;
    pickUpName = loadPoint[0]?.name;
    pickUpAddress = loadPoint[0]?.address;
    pickupDistrictId = loadPoint[0]?.district?.id;
    tripDropoffList = [];
    unloadPoints.forEach((element) {
      tripDropoffList?.add(element?.tripAddress);
    });
  }

  set tempName(tempName){
    name = tempName;
  }

  buildInstruction(inst){
    if(!isNullOrEmptyList(inst))
      instructions = inst[0].type == UNLOAD_POINT_INS ? inst[inst.length-1].type == GOOD_QTY_UNT_INS ? inst.sublist(1,inst.length-1) : inst.sublist(1,inst.length) : inst;
  }

  TemplateModel get template => TemplateModel.withTrip(name,companyId,goodsType,truckType,truckSize?.toInt(), truckLength,pickUplat,pickUplon,pickUpAddress,pickUpFactoryId,pickUpDepotId,dropOfflat,dropOfflon,dropOffAddress,dropOffFactoryId,dropOffDepotId,instructions,tripDropoffList,quantityOfGoods,goodsQuantityUnit);
}

@JsonSerializable()
class TripLocation {
  double? dropoffLat;
  double? dropoffLon;
  String? dropOffName;
  String? dropoffAddress;
  int? dropOffFactoryId = 0;
  int? dropOffDepotId = 0;
  int? dropoffDistrictId;
  @JsonKey(ignore: true)
  Districts? district;
  TripLocation(this.dropoffAddress, this.dropoffLat, this.dropoffLon,this.dropOffName,this.dropOffFactoryId,this.dropOffDepotId, this.dropoffDistrictId, {this.district}){
    dropoffDistrictId = district?.id;
  }

  factory TripLocation.fromJson(Map<String, dynamic> json) =>
      _$TripLocationFromJson(json);

  Map<String, dynamic> toJson() => _$TripLocationToJson(this);

  get name => dropOffName ?? dropoffAddress;

  @override
  String toString() {
    return dropoffAddress ?? '';
  }

  get baseAddress => BaseAddressesSuggestion(name: dropOffName, address: dropoffAddress, lat: dropoffLat, lon: dropoffLon, id: dropOffFactoryId ?? dropOffDepotId, depot: dropOffDepotId != null, isDefault: true, dropoffDistrictId: dropoffDistrictId);
}
