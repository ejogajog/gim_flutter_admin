import 'package:json_annotation/json_annotation.dart';

part 'preset_instruction.g.dart';

@JsonSerializable()
class PresetInstruction {
  @JsonKey(ignore: true)
  bool isSelected = true;

  @JsonKey(ignore: true)
  bool isSaved = true;

  @JsonKey(ignore: true)
  int type = 0;

  int? id;
  int? companyId;
  String? instruction;

  PresetInstruction(this.instruction,{this.id,this.companyId, this.isSaved = true, this.type = 0});

  factory PresetInstruction.fromJson(Map<String, dynamic> json) =>
      _$PresetInstructionFromJson(json);

  Map<String, dynamic> toJson() => _$PresetInstructionToJson(this);

  @override
  String toString() {
    return '${instruction?.trim()}';
  }

  set inst(content){
    instruction = content;
  }

  toggle() {
    isSelected = !isSelected;
  }
}
