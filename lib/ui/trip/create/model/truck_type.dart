import 'package:app/localization/app_translation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'truck_type.g.dart';

@JsonSerializable()
class TruckType {
  int? id;
  String? text;
  String? textBn;
  String? image;
  int? sequence;

  TruckType({this.id, this.text, this.textBn});

  List<TruckType> build() {
    return [TruckType(id:1, text: 'Open Truck', textBn: 'খোলা ট্রাক'), TruckType(id:3, text: 'Covered Truck', textBn: 'কভার ট্রাক'), TruckType(id:4, text: 'Trailer', textBn: 'ট্রেলার'), TruckType(id:5, text: 'Dump Truck/Tipper', textBn: 'Dump Truck/Tipper')];
  }

  factory TruckType.fromJson(Map<String, dynamic> json) =>
      _$TruckTypeFromJson(json);

  Map<String, dynamic> toJson() => _$TruckTypeToJson(this);

  @override
  String toString() {
    return '${isBangla() ? textBn  : text}';
  }
}
