import 'dart:convert';

import 'package:app/utils/string_util.dart';
import 'package:json_annotation/json_annotation.dart';

part 'customer_model.g.dart';

@JsonSerializable()
class CustomerInfo {
  int? companyId;
  String? userName;
  String? companyName;
  String? mobileNumber;

  CustomerInfo(
      this.companyId, this.userName, this.companyName, this.mobileNumber);

  factory CustomerInfo.fromJson(Map<String, dynamic> json) =>
      _$CustomerInfoFromJson(json);

  Map<String, dynamic> toJson() => _$CustomerInfoToJson(this);

  CustomerInfo clone() {
    final String jsonString = json.encode(this);
    final jsonResponse = json.decode(jsonString);
    return CustomerInfo.fromJson(jsonResponse as Map<String, dynamic>);
  }

  @override
  String toString() {
    return '${isNullOrEmpty(userName) ? '' : '$userName -'} $mobileNumber(${companyName ?? 'Regular'})';
  }
}
