// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'truck_dimen.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TruckDimen _$TruckDimenFromJson(Map<String, dynamic> json) => TruckDimen()
  ..Length = json['Length'] == null
      ? null
      : TruckLength.fromJson(json['Length'] as Map<String, dynamic>);

Map<String, dynamic> _$TruckDimenToJson(TruckDimen instance) =>
    <String, dynamic>{
      'Length': instance.Length,
    };

TruckLength _$TruckLengthFromJson(Map<String, dynamic> json) => TruckLength(
      id: (json['id'] as num?)?.toInt(),
      value: (json['value'] as num?)?.toInt(),
    );

Map<String, dynamic> _$TruckLengthToJson(TruckLength instance) =>
    <String, dynamic>{
      'id': instance.id,
      'value': instance.value,
    };
