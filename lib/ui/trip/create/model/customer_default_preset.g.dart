// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_default_preset.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DefaultPresets _$DefaultPresetsFromJson(Map<String, dynamic> json) =>
    DefaultPresets()
      ..goodsTypeId = (json['goodsTypeId'] as num?)?.toInt()
      ..goodsType = json['goodsType'] as String?
      ..goodsTypeBn = json['goodsTypeBn'] as String?
      ..pickUpName = json['pickUpName'] as String?
      ..pickUpAddress = json['pickUpAddress'] as String?
      ..pickUpLat = (json['pickUpLat'] as num?)?.toDouble()
      ..pickUpLon = (json['pickUpLon'] as num?)?.toDouble()
      ..truckTypeId = (json['truckTypeId'] as num?)?.toInt()
      ..truckType = json['truckType'] as String?
      ..truckTypeBn = json['truckTypeBn'] as String?
      ..truckSizeId = (json['truckSizeId'] as num?)?.toInt()
      ..truckSize = (json['truckSize'] as num?)?.toDouble()
      ..truckLength = (json['truckLength'] as num?)?.toDouble()
      ..noOfTrucks = (json['noOfTrucks'] as num?)?.toInt()
      ..fixBid = json['fixBid'] as bool?
      ..paymentType = json['paymentType'] as String?
      ..pickUpDistrictId = (json['pickUpDistrictId'] as num?)?.toInt()
      ..dropOffDistrictId = (json['dropOffDistrictId'] as num?)?.toInt()
      ..pickUpFactoryId = (json['pickUpFactoryId'] as num?)?.toInt()
      ..pickUpDepotId = (json['pickUpDepotId'] as num?)?.toInt()
      ..quantityOfGoods = (json['quantityOfGoods'] as num?)?.toInt()
      ..goodsQuantityUnit = json['goodsQuantityUnit'] as String?
      ..dropOffResponses = (json['dropOffResponses'] as List<dynamic>?)
          ?.map((e) => DropOffAddress.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$DefaultPresetsToJson(DefaultPresets instance) =>
    <String, dynamic>{
      'goodsTypeId': instance.goodsTypeId,
      'goodsType': instance.goodsType,
      'goodsTypeBn': instance.goodsTypeBn,
      'pickUpName': instance.pickUpName,
      'pickUpAddress': instance.pickUpAddress,
      'pickUpLat': instance.pickUpLat,
      'pickUpLon': instance.pickUpLon,
      'truckTypeId': instance.truckTypeId,
      'truckType': instance.truckType,
      'truckTypeBn': instance.truckTypeBn,
      'truckSizeId': instance.truckSizeId,
      'truckSize': instance.truckSize,
      'truckLength': instance.truckLength,
      'noOfTrucks': instance.noOfTrucks,
      'fixBid': instance.fixBid,
      'paymentType': instance.paymentType,
      'pickUpDistrictId': instance.pickUpDistrictId,
      'dropOffDistrictId': instance.dropOffDistrictId,
      'pickUpFactoryId': instance.pickUpFactoryId,
      'pickUpDepotId': instance.pickUpDepotId,
      'quantityOfGoods': instance.quantityOfGoods,
      'goodsQuantityUnit': instance.goodsQuantityUnit,
      'dropOffResponses': instance.dropOffResponses,
    };

DropOffAddress _$DropOffAddressFromJson(Map<String, dynamic> json) =>
    DropOffAddress()
      ..id = (json['id'] as num?)?.toInt()
      ..dropoffLat = (json['dropoffLat'] as num?)?.toDouble()
      ..dropoffLon = (json['dropoffLon'] as num?)?.toDouble()
      ..dropOffName = json['dropOffName'] as String?
      ..dropoffAddress = json['dropoffAddress'] as String?
      ..dropOffDepotId = (json['dropOffDepotId'] as num?)?.toInt()
      ..dropOffFactoryId = (json['dropOffFactoryId'] as num?)?.toInt()
      ..depot = json['depot'] as bool?;

Map<String, dynamic> _$DropOffAddressToJson(DropOffAddress instance) =>
    <String, dynamic>{
      'id': instance.id,
      'dropoffLat': instance.dropoffLat,
      'dropoffLon': instance.dropoffLon,
      'dropOffName': instance.dropOffName,
      'dropoffAddress': instance.dropoffAddress,
      'dropOffDepotId': instance.dropOffDepotId,
      'dropOffFactoryId': instance.dropOffFactoryId,
      'depot': instance.depot,
    };
