import 'dart:core';

import 'package:json_annotation/json_annotation.dart';

part 'create_trip_response.g.dart';

@JsonSerializable()
class CreateTripResponse {
  CreateTripResponse();

  List<TripResponse>? data;

  factory CreateTripResponse.fromJson(Map<String, dynamic> json) =>
      _$CreateTripResponseFromJson(json);
}

@JsonSerializable()
class TripResponse {
  TripResponse();

  int? tripId;
  int? tripNumber;

  factory TripResponse.fromJson(Map<String, dynamic> json) =>
      _$TripResponseFromJson(json);

  @override
  String toString() {
    return tripNumber!.toString();
  }
}
