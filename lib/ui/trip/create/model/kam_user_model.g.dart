// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'kam_user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

KamUserModel _$KamUserModelFromJson(Map<String, dynamic> json) => KamUserModel()
  ..kamUsers = (json['kamUsers'] as List<dynamic>?)
      ?.map((e) => KamUser.fromJson(e as Map<String, dynamic>))
      .toList();

Map<String, dynamic> _$KamUserModelToJson(KamUserModel instance) =>
    <String, dynamic>{
      'kamUsers': instance.kamUsers,
    };

KamUser _$KamUserFromJson(Map<String, dynamic> json) => KamUser()
  ..userId = (json['userId'] as num?)?.toInt()
  ..firstName = json['firstName'] as String?
  ..email = json['email'] as String?
  ..mobileNumber = json['mobileNumber'] as String?;

Map<String, dynamic> _$KamUserToJson(KamUser instance) => <String, dynamic>{
      'userId': instance.userId,
      'firstName': instance.firstName,
      'email': instance.email,
      'mobileNumber': instance.mobileNumber,
    };
