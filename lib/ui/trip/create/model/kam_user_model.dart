import 'package:json_annotation/json_annotation.dart';

part 'kam_user_model.g.dart';

@JsonSerializable()
class KamUserModel{
  List<KamUser>? kamUsers;
  KamUserModel();
  factory KamUserModel.fromJson(Map<String, dynamic> json) => _$KamUserModelFromJson(json);
  Map<String, dynamic> toJson() => _$KamUserModelToJson(this);
}

@JsonSerializable()
class KamUser{
  int? userId;
  String? firstName;
  String? email;
  String? mobileNumber;
  KamUser();
  factory KamUser.fromJson(Map<String, dynamic> json) => _$KamUserFromJson(json);
  Map<String, dynamic> toJson() => _$KamUserToJson(this);

  @override
  String toString() {
    return '${firstName == null && mobileNumber == null ? email : '${firstName??''}-${mobileNumber??''}\n$email'}';
  }
}