import 'dart:convert';

import 'package:app/model/master/goods_type.dart';
import 'package:app/model/master/truck_dimension.dart';
import 'package:app/model/master/truck_size.dart';
import 'package:app/ui/trip/create/model/customer_default_preset.dart';
import 'package:app/ui/trip/create/model/preset_instruction.dart';
import 'package:app/ui/trip/create/model/trip_model.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/string_util.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:app/model/master/districts.dart';
part 'customer_preference.g.dart';

@JsonSerializable()
class CustomerPreferences {
  @JsonKey(ignore: true)
  int? templateId;
  @JsonKey(ignore: true)
  String? templateName;
  @JsonKey(ignore: true)
  int selInstIndex = -1;
  @JsonKey(ignore: true)
  List<GoodsType> _goods = [];

  @JsonKey(ignore: true)
  TruckDataSuggestions? _truckDataSuggestion;

  @JsonKey(ignore: true)
  List<BaseAddressesSuggestion> _listOfPickUp = [];
  @JsonKey(ignore: true)
  List<BaseAddressesSuggestion> _listOfDropOf = [];

  @JsonKey(ignore: true)
  List<TripLocation> listOfMultiDropOf = [];
  @JsonKey(ignore: true)
  double? qntOfGoods;
  @JsonKey(ignore: true)
  String? _goodsQntUnit;

  @JsonKey(ignore: true)
  int? companyId;

  List<GoodsType>? goodsTypeSuggestions;
  List<DepotSuggestions>? depotSuggestions;
  List<FactorySuggestions>? factorySuggestions;
  List<TruckDataSuggestions>? truckDataSuggestions;
  List<UsedPickUpAddressesSuggestions>? usedPickUpAddressesSuggestions;
  List<UsedDropOffAddressesSuggestions>? usedDropOffAddressesSuggestions;

  List<PresetInstruction>? instructionSuggestions;

  DefaultPresets? tripDataSuggestionResponse;

  @JsonKey(ignore: true)
  List<BaseAddressesSuggestion?> defaultUpList = [];

  CustomerPreferences();

  factory CustomerPreferences.fromJson(Map<String, dynamic> json) =>
      _$CustomerPreferencesFromJson(json);

  Map<String, dynamic> toJson() => _$CustomerPreferencesToJson(this);

  CustomerPreferences clone() {
    final String jsonString = json.encode(this);
    final jsonResponse = json.decode(jsonString);
    var pref =
        CustomerPreferences.fromJson(jsonResponse as Map<String, dynamic>);
    pref.buildDataSet();
    return pref;
  }

  _goodsTypeList() {
    _goods.addAll(goodsTypeSuggestions ?? []);
    if (tripDataSuggestionResponse?.goods != null) {
      _goods.remove(tripDataSuggestionResponse?.goods);
      _goods.insert(0, tripDataSuggestionResponse?.goods);
    }
  }

  _pickUpAddresses() {
    _listOfPickUp
      ..addAll(usedPickUpAddressesSuggestions!)
      ..addAll(factorySuggestions!)
      ..addAll(depotSuggestions!);
    _listOfPickUp = _listOfPickUp.sublist(
        0, _listOfPickUp.length > NO_OF_REC ? NO_OF_REC : _listOfPickUp.length);
    if (tripDataSuggestionResponse?.srcAddress != null) {
      _listOfPickUp.remove(tripDataSuggestionResponse?.srcAddress);
      _listOfPickUp.insert(0, tripDataSuggestionResponse?.srcAddress);
    }
  }

  _dropOffAddresses() {
    _listOfDropOf
      ..addAll(usedDropOffAddressesSuggestions!)
      ..addAll(factorySuggestions!)
      ..addAll(depotSuggestions!);
    _listOfDropOf = _listOfDropOf.sublist(
        0, _listOfDropOf.length > NO_OF_REC ? NO_OF_REC : _listOfDropOf.length);
    if (tripDataSuggestionResponse?.dstAddress != null) {
      _listOfDropOf.remove(tripDataSuggestionResponse?.dstAddress);
      _listOfDropOf.insert(0, tripDataSuggestionResponse?.dstAddress);
    }
  }

  buildDataSet() {
    _goodsTypeList();
    _pickUpAddresses();
    _dropOffAddresses();
    instructionSuggestions = instructionSuggestions ?? [];
  }

  get goodsType =>
      _goods.length > NO_OF_REC ? _goods.sublist(0, NO_OF_REC) : _goods;

  get pickUpList => _listOfPickUp;

  get dropOffList => _listOfDropOf;

  get pickUpSearchableList => usedPickUpAddressesSuggestions ?? [];

  get dropOfSearchableList => usedDropOffAddressesSuggestions ?? [];

  get qntUnit =>
      units[_goodsQntUnit ?? tripDataSuggestionResponse?.goodsQuantityUnit];

  get tempTds => _truckDataSuggestion;

  addTempToPref(tripModel) {
    _addGoodsRemDup(tripModel);
    _loadPointRemDup(tripModel);
    templateName = tripModel.name;
    templateId = tripModel.templateId;
    tripModel.truckTypeResponse.isDefault = true;
    tripModel.truckSizeResponse.isDefault = true;
    defaultUpList = tripModel.buildDefUnloadPoints();
    _truckDataSuggestion = TruckDataSuggestions(
        truckTypeId: tripModel.truckTypeResponse.id,
        truckType: tripModel.truckTypeResponse.text,
        lengthAndSizeResponses: [
          TruckProp(
              truckSizeId: tripModel.truckSizeResponse.id,
              truckSize: tripModel.truckSizeResponse.size,
              truckLength: tripModel.truckLength ?? 0.0)
        ]);
    instructionSuggestions = List.from(tripModel.instructions);
  }

  _addGoodsRemDup(tripModel) {
    _goods.remove(tripModel.goodType);
    tripModel.goodType.isDefault = true;
    _goods.insert(0, tripModel.goodType);
    qntOfGoods = tripModel.quantityOfGoods;
    _goodsQntUnit = tripModel.goodsQuantityUnit;
  }

  _loadPointRemDup(tripModel) {
    var loadPoint = tripModel.loadingPoint();
    if (!_listOfPickUp.remove(loadPoint) && !isNullOrEmptyList(_listOfPickUp)){
      _listOfPickUp.removeLast();
    }
    loadPoint.isDefault = true;
    _listOfPickUp.insert(0, loadPoint);
  }
}

@JsonSerializable()
class FactorySuggestions extends BaseAddressesSuggestion {
  FactorySuggestions();

  factory FactorySuggestions.fromJson(Map<String, dynamic> json) =>
      _$FactorySuggestionsFromJson(json);

  Map<String, dynamic> toJson() => _$FactorySuggestionsToJson(this);
}

@JsonSerializable()
class DepotSuggestions extends BaseAddressesSuggestion {
  DepotSuggestions();

  factory DepotSuggestions.fromJson(Map<String, dynamic> json) =>
      _$DepotSuggestionsFromJson(json);

  Map<String, dynamic> toJson() => _$DepotSuggestionsToJson(this);
}

@JsonSerializable()
class GoodsTypeSuggestions {
  int goodsTypeId;
  String type;
  String typeBn;
  bool isDefault;

  GoodsTypeSuggestions(this.goodsTypeId, this.type, this.typeBn,
      {this.isDefault = false});

  factory GoodsTypeSuggestions.fromJson(Map<String, dynamic> json) =>
      _$GoodsTypeSuggestionsFromJson(json);

  Map<String, dynamic> toJson() => _$GoodsTypeSuggestionsToJson(this);
}

@JsonSerializable()
class UsedPickUpAddressesSuggestions extends BaseAddressesSuggestion {
  UsedPickUpAddressesSuggestions();

  factory UsedPickUpAddressesSuggestions.fromJson(Map<String, dynamic> json) =>
      _$UsedPickUpAddressesSuggestionsFromJson(json);

  Map<String, dynamic> toJson() => _$UsedPickUpAddressesSuggestionsToJson(this);
}

@JsonSerializable()
class UsedDropOffAddressesSuggestions extends BaseAddressesSuggestion {
  UsedDropOffAddressesSuggestions();

  factory UsedDropOffAddressesSuggestions.fromJson(Map<String, dynamic> json) =>
      _$UsedDropOffAddressesSuggestionsFromJson(json);

  Map<String, dynamic> toJson() =>
      _$UsedDropOffAddressesSuggestionsToJson(this);
}

@JsonSerializable()
class TruckDataSuggestions {
  int? truckTypeId;
  String? truckType;
  String? truckTypeBn;
  List<TruckProp>? lengthAndSizeResponses;
  @JsonKey(ignore: true)
  List<TruckSize>? _truckSize;
  @JsonKey(ignore: true)
  List<TruckDimensionLength>? _truckLength;

  TruckDataSuggestions(
      {this.truckTypeId, this.truckType, this.lengthAndSizeResponses});

  factory TruckDataSuggestions.fromJson(Map<String, dynamic> json) =>
      _$TruckDataSuggestionsFromJson(json);

  Map<String, dynamic> toJson() => _$TruckDataSuggestionsToJson(this);

  build() {
    if (isNullOrEmptyList(_truckSize)) {
      _truckSize = [];
      _truckLength = [];
      lengthAndSizeResponses?.forEach((e) {
        _truckSize?.add(TruckSize(e.truckSizeId, e.truckSize));
      });
      lengthAndSizeResponses?.forEach((e) {
        _truckLength?.add(TruckDimensionLength(
            isNullOrEmptyDouble(e.truckLength) ? 0 : e.truckLength?.toInt(),
            isNullOrEmptyDouble(e.truckLength)
                ? '0.0'
                : '${e.truckLength.toString().contains('.') ? e.truckLength.toString() : '${e.truckLength.toString()}.0'}'));
      });
    }
  }

  reBuild() {
    _truckSize = [];
    _truckLength = [];
    lengthAndSizeResponses?.forEach((e) {
      _truckSize?.add(TruckSize(e.truckSizeId, e.truckSize));
    });
    lengthAndSizeResponses?.forEach((e) {
      _truckLength?.add(TruckDimensionLength(
          isNullOrEmptyDouble(e.truckLength) ? 0 : e.truckLength?.toInt(),
          isNullOrEmptyDouble(e.truckLength)
              ? '0.0'
              : '${e.truckLength.toString().contains('.') ? e.truckLength.toString() : '${e.truckLength.toString()}.0'}'));
    });
  }

  get sizeList => _truckSize;

  get lengthList => _truckLength;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TruckDataSuggestions &&
          runtimeType == other.runtimeType &&
          truckTypeId == other.truckTypeId;

  @override
  int get hashCode => truckTypeId.hashCode;
}

@JsonSerializable()
class TruckProp {
  int? truckSizeId;
  double? truckLength;
  double? truckSize;

  TruckProp({this.truckSizeId, this.truckSize, this.truckLength});

  factory TruckProp.fromJson(Map<String, dynamic> json) =>
      _$TruckPropFromJson(json);

  Map<String, dynamic> toJson() => _$TruckPropToJson(this);
}

@JsonSerializable()
class BaseAddressesSuggestion {
  double? lat;
  double? lon;
  int? id;
  String? name;
  String? address;
  String? placeId;
  bool? depot;
  @JsonKey(ignore: true)
  bool isDefault;
  int? dropoffDistrictId;
  @JsonKey(ignore: true)
  Districts? district;
  List<TruckDataSuggestions>? truckDataSuggestions;

  BaseAddressesSuggestion(
      {this.name,
      this.id,
      this.placeId,
      this.address,
      this.lat,
      this.lon,
      this.depot,
      this.isDefault = false, this.dropoffDistrictId, this.district}){
    dropoffDistrictId = district?.id;
  }

  @override
  String toString() {
    return '${name ?? address}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BaseAddressesSuggestion && address == other.address;

  @override
  int get hashCode => address.hashCode;

  get tripAddress => TripLocation(address, lat, lon, name,
      (depot ?? false) ? 0 : id, (depot ?? true) ? id : 0, dropoffDistrictId, district: district);
}
