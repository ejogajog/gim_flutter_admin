// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_preference.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerPreferences _$CustomerPreferencesFromJson(Map<String, dynamic> json) =>
    CustomerPreferences()
      ..goodsTypeSuggestions = (json['goodsTypeSuggestions'] as List<dynamic>?)
          ?.map((e) => GoodsType.fromJson(e as Map<String, dynamic>))
          .toList()
      ..depotSuggestions = (json['depotSuggestions'] as List<dynamic>?)
          ?.map((e) => DepotSuggestions.fromJson(e as Map<String, dynamic>))
          .toList()
      ..factorySuggestions = (json['factorySuggestions'] as List<dynamic>?)
          ?.map((e) => FactorySuggestions.fromJson(e as Map<String, dynamic>))
          .toList()
      ..truckDataSuggestions = (json['truckDataSuggestions'] as List<dynamic>?)
          ?.map((e) => TruckDataSuggestions.fromJson(e as Map<String, dynamic>))
          .toList()
      ..usedPickUpAddressesSuggestions =
          (json['usedPickUpAddressesSuggestions'] as List<dynamic>?)
              ?.map((e) => UsedPickUpAddressesSuggestions.fromJson(
                  e as Map<String, dynamic>))
              .toList()
      ..usedDropOffAddressesSuggestions =
          (json['usedDropOffAddressesSuggestions'] as List<dynamic>?)
              ?.map((e) => UsedDropOffAddressesSuggestions.fromJson(
                  e as Map<String, dynamic>))
              .toList()
      ..instructionSuggestions = (json['instructionSuggestions']
              as List<dynamic>?)
          ?.map((e) => PresetInstruction.fromJson(e as Map<String, dynamic>))
          .toList()
      ..tripDataSuggestionResponse = json['tripDataSuggestionResponse'] == null
          ? null
          : DefaultPresets.fromJson(
              json['tripDataSuggestionResponse'] as Map<String, dynamic>);

Map<String, dynamic> _$CustomerPreferencesToJson(
        CustomerPreferences instance) =>
    <String, dynamic>{
      'goodsTypeSuggestions': instance.goodsTypeSuggestions,
      'depotSuggestions': instance.depotSuggestions,
      'factorySuggestions': instance.factorySuggestions,
      'truckDataSuggestions': instance.truckDataSuggestions,
      'usedPickUpAddressesSuggestions': instance.usedPickUpAddressesSuggestions,
      'usedDropOffAddressesSuggestions':
          instance.usedDropOffAddressesSuggestions,
      'instructionSuggestions': instance.instructionSuggestions,
      'tripDataSuggestionResponse': instance.tripDataSuggestionResponse,
    };

FactorySuggestions _$FactorySuggestionsFromJson(Map<String, dynamic> json) =>
    FactorySuggestions()
      ..lat = (json['lat'] as num?)?.toDouble()
      ..lon = (json['lon'] as num?)?.toDouble()
      ..id = (json['id'] as num?)?.toInt()
      ..name = json['name'] as String?
      ..address = json['address'] as String?
      ..placeId = json['placeId'] as String?
      ..depot = json['depot'] as bool?
      ..dropoffDistrictId = (json['dropoffDistrictId'] as num?)?.toInt()
      ..truckDataSuggestions = (json['truckDataSuggestions'] as List<dynamic>?)
          ?.map((e) => TruckDataSuggestions.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$FactorySuggestionsToJson(FactorySuggestions instance) =>
    <String, dynamic>{
      'lat': instance.lat,
      'lon': instance.lon,
      'id': instance.id,
      'name': instance.name,
      'address': instance.address,
      'placeId': instance.placeId,
      'depot': instance.depot,
      'dropoffDistrictId': instance.dropoffDistrictId,
      'truckDataSuggestions': instance.truckDataSuggestions,
    };

DepotSuggestions _$DepotSuggestionsFromJson(Map<String, dynamic> json) =>
    DepotSuggestions()
      ..lat = (json['lat'] as num?)?.toDouble()
      ..lon = (json['lon'] as num?)?.toDouble()
      ..id = (json['id'] as num?)?.toInt()
      ..name = json['name'] as String?
      ..address = json['address'] as String?
      ..placeId = json['placeId'] as String?
      ..depot = json['depot'] as bool?
      ..dropoffDistrictId = (json['dropoffDistrictId'] as num?)?.toInt()
      ..truckDataSuggestions = (json['truckDataSuggestions'] as List<dynamic>?)
          ?.map((e) => TruckDataSuggestions.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$DepotSuggestionsToJson(DepotSuggestions instance) =>
    <String, dynamic>{
      'lat': instance.lat,
      'lon': instance.lon,
      'id': instance.id,
      'name': instance.name,
      'address': instance.address,
      'placeId': instance.placeId,
      'depot': instance.depot,
      'dropoffDistrictId': instance.dropoffDistrictId,
      'truckDataSuggestions': instance.truckDataSuggestions,
    };

GoodsTypeSuggestions _$GoodsTypeSuggestionsFromJson(
        Map<String, dynamic> json) =>
    GoodsTypeSuggestions(
      (json['goodsTypeId'] as num).toInt(),
      json['type'] as String,
      json['typeBn'] as String,
      isDefault: json['isDefault'] as bool? ?? false,
    );

Map<String, dynamic> _$GoodsTypeSuggestionsToJson(
        GoodsTypeSuggestions instance) =>
    <String, dynamic>{
      'goodsTypeId': instance.goodsTypeId,
      'type': instance.type,
      'typeBn': instance.typeBn,
      'isDefault': instance.isDefault,
    };

UsedPickUpAddressesSuggestions _$UsedPickUpAddressesSuggestionsFromJson(
        Map<String, dynamic> json) =>
    UsedPickUpAddressesSuggestions()
      ..lat = (json['lat'] as num?)?.toDouble()
      ..lon = (json['lon'] as num?)?.toDouble()
      ..id = (json['id'] as num?)?.toInt()
      ..name = json['name'] as String?
      ..address = json['address'] as String?
      ..placeId = json['placeId'] as String?
      ..depot = json['depot'] as bool?
      ..dropoffDistrictId = (json['dropoffDistrictId'] as num?)?.toInt()
      ..truckDataSuggestions = (json['truckDataSuggestions'] as List<dynamic>?)
          ?.map((e) => TruckDataSuggestions.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$UsedPickUpAddressesSuggestionsToJson(
        UsedPickUpAddressesSuggestions instance) =>
    <String, dynamic>{
      'lat': instance.lat,
      'lon': instance.lon,
      'id': instance.id,
      'name': instance.name,
      'address': instance.address,
      'placeId': instance.placeId,
      'depot': instance.depot,
      'dropoffDistrictId': instance.dropoffDistrictId,
      'truckDataSuggestions': instance.truckDataSuggestions,
    };

UsedDropOffAddressesSuggestions _$UsedDropOffAddressesSuggestionsFromJson(
        Map<String, dynamic> json) =>
    UsedDropOffAddressesSuggestions()
      ..lat = (json['lat'] as num?)?.toDouble()
      ..lon = (json['lon'] as num?)?.toDouble()
      ..id = (json['id'] as num?)?.toInt()
      ..name = json['name'] as String?
      ..address = json['address'] as String?
      ..placeId = json['placeId'] as String?
      ..depot = json['depot'] as bool?
      ..dropoffDistrictId = (json['dropoffDistrictId'] as num?)?.toInt()
      ..truckDataSuggestions = (json['truckDataSuggestions'] as List<dynamic>?)
          ?.map((e) => TruckDataSuggestions.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$UsedDropOffAddressesSuggestionsToJson(
        UsedDropOffAddressesSuggestions instance) =>
    <String, dynamic>{
      'lat': instance.lat,
      'lon': instance.lon,
      'id': instance.id,
      'name': instance.name,
      'address': instance.address,
      'placeId': instance.placeId,
      'depot': instance.depot,
      'dropoffDistrictId': instance.dropoffDistrictId,
      'truckDataSuggestions': instance.truckDataSuggestions,
    };

TruckDataSuggestions _$TruckDataSuggestionsFromJson(
        Map<String, dynamic> json) =>
    TruckDataSuggestions(
      truckTypeId: (json['truckTypeId'] as num?)?.toInt(),
      truckType: json['truckType'] as String?,
      lengthAndSizeResponses: (json['lengthAndSizeResponses'] as List<dynamic>?)
          ?.map((e) => TruckProp.fromJson(e as Map<String, dynamic>))
          .toList(),
    )..truckTypeBn = json['truckTypeBn'] as String?;

Map<String, dynamic> _$TruckDataSuggestionsToJson(
        TruckDataSuggestions instance) =>
    <String, dynamic>{
      'truckTypeId': instance.truckTypeId,
      'truckType': instance.truckType,
      'truckTypeBn': instance.truckTypeBn,
      'lengthAndSizeResponses': instance.lengthAndSizeResponses,
    };

TruckProp _$TruckPropFromJson(Map<String, dynamic> json) => TruckProp(
      truckSizeId: (json['truckSizeId'] as num?)?.toInt(),
      truckSize: (json['truckSize'] as num?)?.toDouble(),
      truckLength: (json['truckLength'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$TruckPropToJson(TruckProp instance) => <String, dynamic>{
      'truckSizeId': instance.truckSizeId,
      'truckLength': instance.truckLength,
      'truckSize': instance.truckSize,
    };

BaseAddressesSuggestion _$BaseAddressesSuggestionFromJson(
        Map<String, dynamic> json) =>
    BaseAddressesSuggestion(
      name: json['name'] as String?,
      id: (json['id'] as num?)?.toInt(),
      placeId: json['placeId'] as String?,
      address: json['address'] as String?,
      lat: (json['lat'] as num?)?.toDouble(),
      lon: (json['lon'] as num?)?.toDouble(),
      depot: json['depot'] as bool?,
      dropoffDistrictId: (json['dropoffDistrictId'] as num?)?.toInt(),
    )..truckDataSuggestions = (json['truckDataSuggestions'] as List<dynamic>?)
        ?.map((e) => TruckDataSuggestions.fromJson(e as Map<String, dynamic>))
        .toList();

Map<String, dynamic> _$BaseAddressesSuggestionToJson(
        BaseAddressesSuggestion instance) =>
    <String, dynamic>{
      'lat': instance.lat,
      'lon': instance.lon,
      'id': instance.id,
      'name': instance.name,
      'address': instance.address,
      'placeId': instance.placeId,
      'depot': instance.depot,
      'dropoffDistrictId': instance.dropoffDistrictId,
      'truckDataSuggestions': instance.truckDataSuggestions,
    };
