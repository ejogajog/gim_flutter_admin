// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'truck_type.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TruckType _$TruckTypeFromJson(Map<String, dynamic> json) => TruckType(
      id: (json['id'] as num?)?.toInt(),
      text: json['text'] as String?,
      textBn: json['textBn'] as String?,
    )
      ..image = json['image'] as String?
      ..sequence = (json['sequence'] as num?)?.toInt();

Map<String, dynamic> _$TruckTypeToJson(TruckType instance) => <String, dynamic>{
      'id': instance.id,
      'text': instance.text,
      'textBn': instance.textBn,
      'image': instance.image,
      'sequence': instance.sequence,
    };
