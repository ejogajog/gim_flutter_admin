// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'preset_instruction.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PresetInstruction _$PresetInstructionFromJson(Map<String, dynamic> json) =>
    PresetInstruction(
      json['instruction'] as String?,
      id: (json['id'] as num?)?.toInt(),
      companyId: (json['companyId'] as num?)?.toInt(),
    );

Map<String, dynamic> _$PresetInstructionToJson(PresetInstruction instance) =>
    <String, dynamic>{
      'id': instance.id,
      'companyId': instance.companyId,
      'instruction': instance.instruction,
    };
