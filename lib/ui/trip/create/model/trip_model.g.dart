// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'trip_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TripModel _$TripModelFromJson(Map<String, dynamic> json) => TripModel()
  ..name = json['name'] as String?
  ..templateId = (json['templateId'] as num?)?.toInt()
  ..companyId = (json['companyId'] as num?)?.toInt()
  ..tripCount = (json['tripCount'] as num?)?.toInt()
  ..datetimeLocal = json['datetimeLocal'] as String?
  ..datetimeUtc = json['datetimeUtc'] as String?
  ..datetimeUtcStr = json['datetimeUtcStr'] as String?
  ..dropOffAddress = json['dropOffAddress'] as String?
  ..dropOfflat = (json['dropOfflat'] as num?)?.toDouble()
  ..dropOfflon = (json['dropOfflon'] as num?)?.toDouble()
  ..goodsType = (json['goodsType'] as num?)?.toInt()
  ..goodsTypeStr = json['goodsTypeStr'] as String?
  ..otherGoodType = json['otherGoodType'] as String?
  ..image = json['image'] as String?
  ..paymentType = json['paymentType'] as String?
  ..pickUpAddress = json['pickUpAddress'] as String?
  ..pickUplat = (json['pickUplat'] as num?)?.toDouble()
  ..pickUplon = (json['pickUplon'] as num?)?.toDouble()
  ..specialInsturctions = json['specialInsturctions'] as String?
  ..truckHeight = json['truckHeight'] as String?
  ..truckLength = (json['truckLength'] as num?)?.toDouble()
  ..truckSize = (json['truckSize'] as num?)?.toDouble()
  ..truckType = (json['truckType'] as num?)?.toInt()
  ..productId = (json['productId'] as num?)?.toInt()
  ..projectId = (json['projectId'] as num?)?.toInt()
  ..distributorStr = json['distributorStr'] as String?
  ..comment = json['comment'] as String?
  ..expectedTripPrice = (json['expectedTripPrice'] as num?)?.toInt()
  ..customerMobileNumber = json['customerMobileNumber'] as String?
  ..creationReason = json['creationReason'] as String?
  ..pending = json['pending'] as bool?
  ..fixBid = json['fixBid'] as bool?
  ..pickUpFactoryId = (json['pickUpFactoryId'] as num?)?.toInt()
  ..pickUpDepotId = (json['pickUpDepotId'] as num?)?.toInt()
  ..dropOffFactoryId = (json['dropOffFactoryId'] as num?)?.toInt()
  ..dropOffDepotId = (json['dropOffDepotId'] as num?)?.toInt()
  ..dropOffName = json['dropOffName'] as String?
  ..pickUpName = json['pickUpName'] as String?
  ..noOfUnloadPoints = (json['noOfUnloadPoints'] as num?)?.toInt()
  ..goodType = json['goodType'] == null
      ? null
      : GoodsType.fromJson(json['goodType'] as Map<String, dynamic>)
  ..truckTypeResponse = json['truckTypeResponse'] == null
      ? null
      : TruckType.fromJson(json['truckTypeResponse'] as Map<String, dynamic>)
  ..truckSizeResponse = json['truckSizeResponse'] == null
      ? null
      : TruckSize.fromJson(json['truckSizeResponse'] as Map<String, dynamic>)
  ..tripDropoffList = (json['tripDropoffList'] as List<dynamic>?)
      ?.map((e) => TripLocation.fromJson(e as Map<String, dynamic>))
      .toList()
  ..instructions = (json['instructions'] as List<dynamic>?)
      ?.map((e) => PresetInstruction.fromJson(e as Map<String, dynamic>))
      .toList()
  ..quantityOfGoods = (json['quantityOfGoods'] as num?)?.toDouble()
  ..goodsQuantityUnit = json['goodsQuantityUnit'] as String?
  ..pickupDistrictId = (json['pickupDistrictId'] as num?)?.toInt()
  ..platformType = json['platformType'] as String?
  ..kamUserId = (json['kamUserId'] as num?)?.toInt()
  ..hiddenFromPartner = json['hiddenFromPartner'] as bool?
  ..lcNumber = json['lcNumber'] as String?;

Map<String, dynamic> _$TripModelToJson(TripModel instance) => <String, dynamic>{
      'name': instance.name,
      'templateId': instance.templateId,
      'companyId': instance.companyId,
      'tripCount': instance.tripCount,
      'datetimeLocal': instance.datetimeLocal,
      'datetimeUtc': instance.datetimeUtc,
      'datetimeUtcStr': instance.datetimeUtcStr,
      'dropOffAddress': instance.dropOffAddress,
      'dropOfflat': instance.dropOfflat,
      'dropOfflon': instance.dropOfflon,
      'goodsType': instance.goodsType,
      'goodsTypeStr': instance.goodsTypeStr,
      'otherGoodType': instance.otherGoodType,
      'image': instance.image,
      'paymentType': instance.paymentType,
      'pickUpAddress': instance.pickUpAddress,
      'pickUplat': instance.pickUplat,
      'pickUplon': instance.pickUplon,
      'specialInsturctions': instance.specialInsturctions,
      'truckHeight': instance.truckHeight,
      'truckLength': instance.truckLength,
      'truckSize': instance.truckSize,
      'truckType': instance.truckType,
      'productId': instance.productId,
      'projectId': instance.projectId,
      'distributorStr': instance.distributorStr,
      'comment': instance.comment,
      'expectedTripPrice': instance.expectedTripPrice,
      'customerMobileNumber': instance.customerMobileNumber,
      'creationReason': instance.creationReason,
      'pending': instance.pending,
      'fixBid': instance.fixBid,
      'pickUpFactoryId': instance.pickUpFactoryId,
      'pickUpDepotId': instance.pickUpDepotId,
      'dropOffFactoryId': instance.dropOffFactoryId,
      'dropOffDepotId': instance.dropOffDepotId,
      'dropOffName': instance.dropOffName,
      'pickUpName': instance.pickUpName,
      'noOfUnloadPoints': instance.noOfUnloadPoints,
      'goodType': instance.goodType,
      'truckTypeResponse': instance.truckTypeResponse,
      'truckSizeResponse': instance.truckSizeResponse,
      'tripDropoffList': instance.tripDropoffList,
      'instructions': instance.instructions,
      'quantityOfGoods': instance.quantityOfGoods,
      'goodsQuantityUnit': instance.goodsQuantityUnit,
      'pickupDistrictId': instance.pickupDistrictId,
      'platformType': instance.platformType,
      'kamUserId': instance.kamUserId,
      'hiddenFromPartner': instance.hiddenFromPartner,
      'lcNumber': instance.lcNumber,
    };

TripLocation _$TripLocationFromJson(Map<String, dynamic> json) => TripLocation(
      json['dropoffAddress'] as String?,
      (json['dropoffLat'] as num?)?.toDouble(),
      (json['dropoffLon'] as num?)?.toDouble(),
      json['dropOffName'] as String?,
      (json['dropOffFactoryId'] as num?)?.toInt(),
      (json['dropOffDepotId'] as num?)?.toInt(),
      (json['dropoffDistrictId'] as num?)?.toInt(),
    );

Map<String, dynamic> _$TripLocationToJson(TripLocation instance) =>
    <String, dynamic>{
      'dropoffLat': instance.dropoffLat,
      'dropoffLon': instance.dropoffLon,
      'dropOffName': instance.dropOffName,
      'dropoffAddress': instance.dropoffAddress,
      'dropOffFactoryId': instance.dropOffFactoryId,
      'dropOffDepotId': instance.dropOffDepotId,
      'dropoffDistrictId': instance.dropoffDistrictId,
    };
