import 'package:app/model/master/goods_type.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/utils/string_util.dart';
import 'package:json_annotation/json_annotation.dart';

part 'customer_default_preset.g.dart';

@JsonSerializable()
class DefaultPresets {
  int? goodsTypeId;
  String? goodsType;
  String? goodsTypeBn;
  String? pickUpName;
  String? pickUpAddress;
  double? pickUpLat;
  double? pickUpLon;
  int? truckTypeId;
  String? truckType;
  String? truckTypeBn;
  int? truckSizeId;
  double? truckSize;
  double? truckLength;
  int? noOfTrucks;
  bool? fixBid;
  String? paymentType;
  int? pickUpDistrictId;
  int? dropOffDistrictId;
  int? pickUpFactoryId;
  int? pickUpDepotId;
  int? quantityOfGoods;
  String? goodsQuantityUnit;
  List<DropOffAddress>? dropOffResponses;

  DefaultPresets();

  factory DefaultPresets.fromJson(Map<String, dynamic> json) =>
      _$DefaultPresetsFromJson(json);

  Map<String, dynamic> toJson() => _$DefaultPresetsToJson(this);

  get goods => isNullOrEmpty(goodsType)
      ? null
      : GoodsType(goodsTypeId, goodsType, null, goodsTypeBn, isDefault: true);

  get srcAddress => isNullOrEmpty(pickUpAddress)
      ? null
      : BaseAddressesSuggestion(
          lat: pickUpLat,
          lon: pickUpLon,
          name: pickUpName,
          address: pickUpAddress,
          id: pickUpFactoryId ?? pickUpDepotId,
          depot: pickUpDepotId != null,
          isDefault: true);

  get dstAddress => _dropOffAddress();

  TruckDataSuggestions get  truckDataSuggestions => TruckDataSuggestions(truckTypeId: truckTypeId,truckType: truckType, lengthAndSizeResponses: [TruckProp(truckSizeId: truckSizeId, truckSize: truckSize, truckLength: truckLength ?? 0.0)]);

  _dropOffAddress() {
    if (isNullOrEmptyList(dropOffResponses) ||
        isNullOrEmpty(
            dropOffResponses?[dropOffResponses!.length - 1].dropoffAddress))
      return null;
    var location = dropOffResponses?[dropOffResponses!.length - 1];
    return BaseAddressesSuggestion(
        name: location?.dropOffName,
        address: location?.dropoffAddress,
        lat: location?.dropoffLat,
        lon: location?.dropoffLon,
        id: location?.dropOffFactoryId ?? location?.dropOffDepotId,
        depot: location?.dropOffDepotId != null,
        isDefault: true);
  }
}

@JsonSerializable()
class DropOffAddress {
  int? id;
  double? dropoffLat;
  double? dropoffLon;
  String? dropOffName;
  String? dropoffAddress;
  int? dropOffDepotId = 0;
  int? dropOffFactoryId = 0;
  bool? depot;

  DropOffAddress();

  factory DropOffAddress.fromJson(Map<String, dynamic> json) =>
      _$DropOffAddressFromJson(json);

  Map<String, dynamic> toJson() => _$DropOffAddressToJson(this);
}
