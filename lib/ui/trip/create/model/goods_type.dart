import 'dart:core';

import 'package:app/localization/app_translation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'goods_type.g.dart';

@JsonSerializable()
class GoodsType  {
  int id;
  String text;
  String image = '';
  String textBn;
  GoodsType(this.id,this.text,this.image,this.textBn);
  factory GoodsType.fromJson(Map<String, dynamic> json) => _$GoodsTypeFromJson(json);
  Map<String, dynamic> toJson() => _$GoodsTypeToJson(this);
  @override
  String toString() {
    return isBangla() ? textBn ?? text ?? '' : text ?? '';
    //return text;
  }
}