// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerInfo _$CustomerInfoFromJson(Map<String, dynamic> json) => CustomerInfo(
      (json['companyId'] as num?)?.toInt(),
      json['userName'] as String?,
      json['companyName'] as String?,
      json['mobileNumber'] as String?,
    );

Map<String, dynamic> _$CustomerInfoToJson(CustomerInfo instance) =>
    <String, dynamic>{
      'companyId': instance.companyId,
      'userName': instance.userName,
      'companyName': instance.companyName,
      'mobileNumber': instance.mobileNumber,
    };
