import 'package:app/ui/trip/create/model/base_model.dart';
import 'package:app/utils/app_constants.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class NoOfTruck extends DataInfo{
  int? id;
  NoOfTruck({this.id});

  List<NoOfTruck> build() {
    List<NoOfTruck> noOfTruck = [];
    int length = MAX_TRIP+1;
    for (int index = 1; index < length; index++) {
      noOfTruck.add(NoOfTruck(id:index));
    }
    return noOfTruck;
  }

  @override
  String toString() {
    return id.toString();
  }

  @override
  get iD => id;
}
