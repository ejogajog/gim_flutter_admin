import 'package:app/api/api_response.dart';

class TripRepository {
  static var helper = ApiBaseHelper();

  static Future<ApiResponse> createTrip(request) async {
    ApiResponse response = await helper.post("/ejogajogAdminAPI/api/v1/admin/trips", request);
    return response;
  }

  static Future<ApiResponse> customerPref(companyId) async {
    ApiResponse response = await helper.get("/ejogajogAdminAPI/api/v1/enterprises/$companyId/preference/data");
    return response;
  }

}
