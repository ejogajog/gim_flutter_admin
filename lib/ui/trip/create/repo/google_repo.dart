import 'package:app/api/api_response.dart';
import 'package:app/ui/trip/create/model/address_component.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';

class GoogleRepository {
  static var helper = ApiBaseHelper();

  static Future<List> getPlaceSearchResult(pattern, companyId) async {
    List<Future> futures = [];
    futures.add(_dbAutoCompleteApi(pattern, companyId));
    futures.add(_gAutoCompleteApi(pattern));
    var response = await Future.wait(futures);
    var suggestionList = List.from(response[0])..addAll(response[1]);

    return suggestionList;
  }

  static Future<List> _dbAutoCompleteApi(pattern, companyId) async {
    List pds = [];
    ApiResponse response = await helper.get(
        "/ejogajogAdminAPI/api/v1/admin/enterprise/depotfactory/locations",
        queryParameters: {'searchQuery': '$pattern'});
    if (response.status == Status.COMPLETED) {
      List values = response.data;
      pds.addAll(values
          .map((prediction) => BaseAddressesSuggestion(
              id: prediction['id'],
              name: prediction['name'],
              address: prediction['address'],
              lat: prediction['lat'],
              lon: prediction['lon'],
              depot: prediction['depot']))
          .toList());
    }
    return pds;
  }

  static Future<List> _gAutoCompleteApi(pattern) async {
    List pds = [];
    ApiResponse response = await helper.get(
        "/ejogajogAdminAPI/api/v1/admin/gplaceautocomplete",
        queryParameters: {
          'components': 'country:bd',
          'region': 'bd',
          'types': 'establishment',
          'input': '$pattern'
        });
    if (response.status == Status.COMPLETED) {
      List values = response.data["predictions"];
      pds.addAll(values
          .map((prediction) => BaseAddressesSuggestion(
                name: prediction['description'],
                address: prediction['description'],
                id: prediction['id'],
                placeId: prediction['place_id'],
              ))
          .toList());
    }
    return pds;
  }

  static Future<Map> gPlaceDetailApi(placeId) async {
    Map map = Map();
    ApiResponse response = await helper.get(
        "/ejogajogAdminAPI/api/v1/admin/gplacedetails",
        queryParameters: {'placeid': '$placeId'});
    if (response.status == Status.COMPLETED) {
      map = response.data['result']['geometry']['location'];
    }
    return map;
  }

  static Future<AddressComponent?> gGeocode(latitude, longitude) async {
    AddressComponent? address;
    ApiResponse response = await helper.get(
        "/ejogajogAdminAPI/api/v1/admin/geocode",
        queryParameters: {'latlng': '$latitude,$longitude'});
    if (response.status == Status.COMPLETED && response.data != null) {
      address = AddressComponent.fromJson(response.data);
    }
    return address;
  }
}
