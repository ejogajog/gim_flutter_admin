import 'package:app/api/api_response.dart';

class AdminRepository {

  static var helper = ApiBaseHelper();

  static Future<ApiResponse> districtId(latitude,longitude) async {
    ApiResponse response = await helper.get("/ejogajogAdminAPI/api/v1/admin/location/district?latitude=$latitude&longitude=$longitude");
    return response;
  }

  static Future<ApiResponse> getKamList() async {
    ApiResponse response = await helper.get("/ejogajogAdminAPI/api/v1/trips/kam-pam");
    return response;
  }

}
