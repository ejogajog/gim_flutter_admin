import 'package:app/api/api_response.dart';
import 'package:app/ui/trip/create/model/customer_model.dart';
import 'package:app/utils/string_util.dart';

class FindCustomerRepository {
  static var helper = ApiBaseHelper();

  static Future<List> searchCustomer(pattern, isEnterprise) async {
    List info = [];
    if (isNullOrEmpty(pattern)) return info;
    ApiResponse response = await helper
        .get("/ejogajogAdminAPI/api/v1/customers", queryParameters: {
      'page': '0',
      'size': '20',
      'ASC': 'ASC',
      'customerSortBy': 'Name',
      'isEnterprise': isEnterprise,
      'applicationStatus': 'APPROVED',
      'isCallFromFlutterAdmin': true,
      'searchKey': '$pattern'
    });
    if (response.status == Status.COMPLETED) {
      List values = response.data["contentList"];
      info.addAll(values
          .map((cstInfo) => CustomerInfo(
              cstInfo['companyId'],
              cstInfo['userName'],
              cstInfo['companyName'],
              cstInfo['mobileNumber']))
          .toList());
    }
    return info;
  }
}
