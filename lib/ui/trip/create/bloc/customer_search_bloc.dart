import 'package:app/api/api_response.dart';
import 'package:app/bloc/base_bloc.dart';
import 'package:app/ui/trip/create/model/customer_model.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/ui/trip/create/repo/create_trip_repo.dart';
import 'package:app/utils/string_util.dart';

class SearchCustomerBloc extends BaseBloc {
  final bool? goToConfig;
  CustomerInfo? _customerInfo;
  CustomerPreferences? _customerPreferences;

  SearchCustomerBloc({this.goToConfig = false});

  customer(CustomerInfo customerInfo) {
    _customerInfo = customerInfo;
  }

  get customerInfo => _customerInfo;

  get customerPreset => _customerPreferences;

  requestCustomerPref() async {
    if (isNullOrEmptyInt(customerInfo.companyId)) {
      _customerPreferences = null;
      return;
    }
    ApiResponse apiResponse =
        await TripRepository.customerPref(customerInfo.companyId);
    if (apiResponse.status == Status.COMPLETED) {
      _customerPreferences = CustomerPreferences.fromJson(apiResponse.data);
      customerPreset.buildDataSet();
      notifyListeners();
    }
  }
}
