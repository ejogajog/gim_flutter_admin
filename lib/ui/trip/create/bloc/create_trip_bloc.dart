import 'package:flutter/cupertino.dart';
import 'package:app/api/api_response.dart';
import 'package:app/bloc/base_bloc.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/model/master/goods_type.dart';
import 'package:app/ui/template/repo/template_repo.dart';
import 'package:app/ui/trip/create/model/kam_user_model.dart';
import 'package:app/ui/trip/create/model/trip_model.dart';
import 'package:app/ui/trip/create/model/customer_model.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/ui/trip/create/model/preset_instruction.dart';
import 'package:app/ui/trip/create/model/truck_info.dart';
import 'package:app/ui/trip/create/repo/create_trip_repo.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/string_util.dart';
import 'package:translator/translator.dart';
import '../../../../model/base_response.dart';
import '../../../../utils/prefs.dart';
import '../model/create_trip_response.dart';

class CreateTripBloc extends BaseBloc {
  GoodsType? _goodsType;
  TruckInfo? _truckInfo;
  bool _validKam = true;
  bool _validGood = true;
  bool _validDistrict = true;
  CustomerInfo? _customerInfo;
  TripModel? _tripRequest;
  bool _inValidTemp = true;
  List<PresetInstruction> instructions = [];
  CustomerPreferences? _customerPreferences;
  GoogleTranslator? translator;
  List<TripResponse>? listOfTrips;

  CreateTripBloc(this._customerInfo, this._customerPreferences) {
    translator = GoogleTranslator();
    _tripRequest = TripModel();
    if (customerPreset != null) {
      customerPreset?.companyId = _customerInfo?.companyId;
      _tripRequest?.quantityOfGoods = customerPreset?.qntOfGoods as double?;
      if (!isNullOrEmptyList(customerPreset?.defaultUpList)) {
        _tripRequest?.unloadPoints = customerPreset!.defaultUpList;
      }
    }
    _tripRequest?.pending = !isOPS;
  }

  CustomerPreferences? get customerPreset => _customerPreferences;

  get prefTemplateName => customerPreset?.templateName;

  get templateName => _tripRequest?.name;

  get isUsingPref => isNullOrEmpty(prefTemplateName);

  get loadPoint => _tripRequest?.loadPoint;

  get unloadPoints => _tripRequest?.unloadPoints;

  onTemplateNameChange(templateName) {
    _inValidTemp = isNullOrEmpty(templateName);
    _tripRequest?.tempName = templateName;
  }

  tripDateTime(String dateTime, String dateTimeUtc, String dateTimeLocal) {
    _tripRequest?.datetimeUtcStr = dateTime;
    _tripRequest?.datetimeUtc = dateTimeUtc;
    _tripRequest?.datetimeLocal = dateTimeLocal;
  }

  comment(String cmt) {
    _tripRequest?.creationReason = cmt;
  }

  tripAmount(String? tripPrice) {
    _tripRequest?.expectedTripPrice = int.tryParse(tripPrice!);
  }

  goodsType(GoodsType? goodsType) {
    _goodsType = goodsType;
  }

  kam(KamUser? kamUser) {
    _tripRequest?.kamUserId = kamUser?.userId;
  }

  truckInfo(truckDtl) {
    _truckInfo = truckDtl;
    _tripRequest?.truckType = _truckInfo?.type?.id;
    _tripRequest?.truckSize = _truckInfo?.size?.id?.toDouble();
    _tripRequest?.truckLength = _truckInfo?.length == null
        ? 0
        : double.tryParse(_truckInfo?.length.value);
  }

  noOfTrip(noOfTruck) {
    _tripRequest?.tripCount = noOfTruck;
  }

  notifyIns() {
    notifyListeners();
  }

  paymentMode(mode) {
    _tripRequest?.paymentType = mode;
  }

  ins(insList) {
    instructions = insList;
  }

  goodsFlag(value) {
    _validGood = value;
  }

  kamFlag(value) {
    _validKam = value;
  }

  districtFlag(value) {
    _validDistrict = value;
  }

  goodsQnt(value) {
    _tripRequest?.quantityOfGoods = double.tryParse(value);
  }

  goodsUnit(unit) {
    _tripRequest?.goodsQuantityUnit = unit;
  }

  projectId(int? id){
    _tripRequest?.projectId = id;
  }

  get reason => _tripRequest?.creationReason;

  get tripAmt => _tripRequest?.expectedTripPrice.toString();

  get inst => _tripRequest?.specialInsturctions;

  get contNo => _tripRequest?.customerMobileNumber;

  get cmpId => _customerInfo?.companyId;
  get cmpName => _customerInfo?.companyName;
  get unloadAddress => _tripRequest?.dropOffAddress;

  get qty => _tripRequest?.quantityOfGoods;

  get unit => _tripRequest?.goodsQuantityUnit;

  get client =>
      '${isNullOrEmpty(_customerInfo?.userName) ? '' : '${_customerInfo?.userName}-'}${_customerInfo?.mobileNumber} (${_customerInfo?.companyName ?? localize('regular')})';

  get product => _goodsType;

  get truckInf => _truckInfo ?? TruckInfo();

  requestCreateTrip() async {
    _buildIns();
    _tripRequest?.goodsType = product?.id;
    _tripRequest?.customerMobileNumber = _customerInfo?.mobileNumber;
    _tripRequest?.buildAddressPoints();
    print(_tripRequest?.toJson());
    ApiResponse apiResponse = await TripRepository.createTrip(_tripRequest);
    if (apiResponse.status == Status.COMPLETED) {
      listOfTrips = convertDynamicListToStaticList<TripResponse>(apiResponse.data);
    }
    return apiResponse;
  }

  createTemplate() async {
    _tripRequest?.companyId = cmpId;
    _tripRequest?.buildInstruction(instructions);
    _tripRequest?.tempName = _tripRequest?.name?.trim();
    var template = _tripRequest?.template;
    print(template?.toJson());
    ApiResponse apiResponse = await TemplateRepository.addTemplate(template);
    return apiResponse;
  }

  set fixBid(fixBid) {
    _tripRequest?.fixBid = fixBid ?? false;
  }

  set hideFromPartner(hideOrShow) {
    _tripRequest?.hiddenFromPartner = hideOrShow ?? false;
  }

  get isEnterprise => !isNullOrEmptyInt(_customerInfo?.companyId);

  _buildIns() {
    _tripRequest?.specialInsturctions = '';
    instructions.forEach((element) {
      _tripRequest?.specialInsturctions = _tripRequest!.specialInsturctions!+'$element\n';
    });
    if (_tripRequest!.specialInsturctions!.length - 1 > 0)
      _tripRequest?.specialInsturctions = _tripRequest?.specialInsturctions
      !.substring(0, _tripRequest!.specialInsturctions!.length - 1);
  }

  validateFields() {
    if (isEnterprise && (isNullOrEmptyInt(_tripRequest!.kamUserId) || !_validKam)) {
      return 'kam_emp_error';
    }else if (isNullOrEmpty(_tripRequest!.loadPoint[0]?.address)) {
      return 'src_emp_error';
    } else if (_isInvalidUnloadPoint()) {
      return 'dst_emp_error';
    } else if (_goodsType?.id == null || _goodsType?.id == 0 || !_validGood) {
      return 'goods_emp_error';
    } else if (!_validDistrict) {
      return 'district_emp_error';
    } else if (truckInf.type == null) {
      return 'truck_type_error';
    } else if (truckInf.size == null) {
      return 'truck_size_error';
    } else if (_tripRequest!.tripCount == 0) {
      return 'no_of_truck_error';
    } else if (_saveIns()) {
      return 'ins_save_error';
    } else if (!validInstLength()) {
      return 'ins_len_error';
    }
  }

  validInstLength() {
    _tripRequest?.specialInsturctions = '';
    instructions.forEach((element) {
      _tripRequest?.specialInsturctions = _tripRequest!.specialInsturctions!+'$element\n';
    });
    return _tripRequest!.specialInsturctions!.length <= insMaxCharCount;
  }

  isValidTemplate() {
    if (isNullOrEmpty(_tripRequest!.name) || _inValidTemp) {
      return isNullOrEmpty(_tripRequest!.name)
          ? 'tmp_name_error'
          : 'tmp_exist_error';
    }
  }

  _isInvalidUnloadPoint() {
    int size = _tripRequest!.unloadPoints.length;
    for (int loop = 0; loop < size; loop++) {
      _validDistrict = _tripRequest!.unloadPoints[loop]?.district != null;
      if (isNullOrEmpty(_tripRequest!.unloadPoints[loop]?.address)) return true;
    }
    return false;
  }

  _saveIns() {
    for (int index = 0; index < instructions.length; index++) {
      PresetInstruction instruction = instructions[index];
      if (!(instruction.isSaved ?? true)) return true;
    }
    return false;
  }

  bool isGroupTrip() {
    return _tripRequest!.tripCount! > 1;
  }

  validateDistrict()async{
    var loadDstMsg;
    String uLoadDstMsg = '';
    int lastIndex = _tripRequest!.loadPoint[0]!.address!.lastIndexOf(',');
    final token = lastIndex == -1 ? _tripRequest!.loadPoint[0]!.address : _tripRequest!.loadPoint[0]!.address?.substring(lastIndex+1).trim();
    loadDstMsg = (token!.contains(_tripRequest!.loadPoint[0]!.district!.textBn!) || token.toLowerCase().contains(_tripRequest!.loadPoint[0]!.district!.text!.toLowerCase())) ?'' : _tripRequest!.loadPoint[0]?.district?.text;

    for(int index = 0 ; index < _tripRequest!.unloadPoints.length ; index++){
      final element = _tripRequest!.unloadPoints[index];
      int lastIndex = element!.address!.lastIndexOf(',');
      final token = lastIndex == -1 ? element.address : element.address?.substring(lastIndex+1).trim();
      uLoadDstMsg += ((token!.contains(element.district!.textBn!) || token.toLowerCase().contains(element.district!.text!.toLowerCase()))|| element.district?.text == uLoadDstMsg ? '' : isNullOrEmpty(uLoadDstMsg) ? element.district?.text : index < (_tripRequest!.unloadPoints.length -1) ? ', ${element.district?.text}' : ' & ${element.district?.text}')!;
    }
    return isNullOrEmpty(loadDstMsg) && isNullOrEmpty(uLoadDstMsg) ? null : 'Are you sure you want to select ${isNullOrEmpty(loadDstMsg) ? '$uLoadDstMsg as unloading district?' : isNullOrEmpty(uLoadDstMsg) ? '$loadDstMsg as loading district ?' : '$loadDstMsg as loading district and $uLoadDstMsg as unloading district?'}';
  }

  isOutsideCluster()async{
    return !(await clusterId).contains(_tripRequest?.loadPoint[0]?.district?.id);
  }

  get isOPS => Prefs.getString(Prefs.ADMIN_TYPE) == OPS;
  get projects => Prefs.getProjects();
  get clusterId async => await Prefs.getIntegerList(Prefs.CLS_DIST_ID);



  List<String> get listOfTripsNo => listOfTrips!.length == 1 ? [listOfTrips![0].toString()] : [listOfTrips!.length.toString(),listOfTrips![0].toString(),listOfTrips![listOfTrips!.length-1].toString()];

  String get tripsNo => listOfTrips!.length == 1 ? '${listOfTrips![0]}' : '${listOfTrips![0]} - ${listOfTrips![listOfTrips!.length-1]}';
  set lcNumber(String? value) {
    _tripRequest?.lcNumber = value;
    notifyListeners(); // Notify listeners if needed
  }
  TextEditingController lcNumberController = TextEditingController();
}
