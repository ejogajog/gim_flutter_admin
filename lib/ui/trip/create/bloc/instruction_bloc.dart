import 'package:flutter/foundation.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/ui/trip/create/bloc/create_trip_bloc.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/string_util.dart';
import 'package:app/ui/trip/create/model/preset_instruction.dart';

class InstructionBloc extends ChangeNotifier{
  CreateTripBloc? bloc;
  bool invalidNotify = true;
  bool isNewInsAdded = false;
  int lastSelIndex = negIndex;
  List<PresetInstruction>? editInst = [];
  List<PresetInstruction> nonEditInst = [];

  InstructionBloc(this.bloc) {
    if (bloc?.customerPreset != null &&
        !isNullOrEmptyList(bloc?.customerPreset?.instructionSuggestions)) {
      editInst = bloc!.customerPreset?.instructionSuggestions;
    }
    editInst?.insert(0, PresetInstruction('',isSaved:true, type: UNLOAD_POINT_INS));
  }

  nonEditInsLen() {
    int length = 0;
    nonEditInst.forEach((element) {
      length += element.instruction!.length;
    });
    return length;
  }

  noEditInsList(){
    nonEditInst = List.from(editInst!);
    nonEditInst.retainWhere((element) => element.isSelected && !isNullOrEmpty(element.instruction));
    bloc?.ins(nonEditInst);
  }

  onChecked() {
    notifyListeners();
  }

  onSave(insInd,content) {
    var isDuplicate = false;
    for (int index = 0; index < editInst!.length; index++) {
      if (index != insInd && content.length > 0 && editInst?[index].toString().toLowerCase() == content.toLowerCase()) return true;
    }
    notifyListeners();
    return isDuplicate;
  }

  addNewIns() {
    isNewInsAdded = true;
    editInst?.insert(1, PresetInstruction('',isSaved: false));
    notifyListeners();
  }

  _qtyUnitIns(qty,unit){
    var goodQtyIns = isNullOrEmptyInt(qty) ? unit == null ? null : '${unit?.toString()??''} ${bloc?.product?.textBn ?? bloc?.product?.text} যাবে।' : '${localize('number_count', dynamicValue: qty.toString())}${unit?.toString()??''} ${bloc?.product?.textBn ?? bloc?.product?.text} যাবে।';
    if(!isNullOrEmpty(goodQtyIns)){
      if(editInst?.last.type == GOOD_QTY_UNT_INS)
        editInst?[editInst!.length-1].inst = goodQtyIns;
      else editInst?.add(PresetInstruction(goodQtyIns,isSaved: true, type: GOOD_QTY_UNT_INS));
    }
  }

  unloadPointUpdate(){
    String unloadPoints = '';
    for(int index = 0 ; index < bloc?.unloadPoints.length ; index++){
      unloadPoints += '${index+1} ${bloc?.unloadPoints[index].toString()}, ';
    }
    int length = isNullOrEmpty(bloc?.unloadAddress) ? bloc?.unloadPoints.length : bloc?.unloadPoints.length+1;
    unloadPoints += isNullOrEmpty(bloc?.unloadAddress) ?'': '$length ${bloc?.unloadAddress ?? ''}, ';
    int lastSepInd = unloadPoints.lastIndexOf(',');
    editInst?[0].instruction = lastSepInd != -1 ? '${'$length আনলোডিং পয়েন্ট: '}${unloadPoints.substring(0,lastSepInd)}' : '0 আনলোডিং পয়েন্ট';
    _qtyUnitIns(bloc?.qty,units[bloc?.unit]);
    notifyListeners();
  }
}
