import 'package:flutter/cupertino.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/ui/trip/create/model/base_model.dart';
import 'package:app/utils/string_util.dart';

class IncDecBloc extends ChangeNotifier {
  int? _pos = 0;
  DataInfo? _value;
  List<DataInfo>? _list;
  String? unit;
  Function? callback;
  var noOfValController = TextEditingController();

  IncDecBloc(this._list, defaultPos,{this.unit, this.callback}) {
    _pos = isNullOrEmptyInt(defaultPos) ? 0 :  defaultPos - 1;
    _value = _list![_pos!];
    noOfValController.text = _value.toString();
    if (callback != null) callback!(_value?.iD);
  }

  plusDataVal() {
    if (_pos! < (_list!.length - 1)) {
      _value = _list?[_pos = _pos!+1];
      noOfValController.text = _value.toString();
      noOfValController.selection = TextSelection.fromPosition(TextPosition(offset: noOfValController.text.length));
      callback!(_value?.iD);
    }
  }

  minusDataVal() {
    if (_pos! > 0) {
      _value = _list![_pos = _pos!-1];
      noOfValController.text = _value.toString();
      noOfValController.selection = TextSelection.fromPosition(TextPosition(offset: noOfValController.text.length));
      callback!(_value?.iD);
    }
  }

  set value(text){
    if(!isNullOrEmpty(text)){
      _pos = int.tryParse(text!)??0;
      _value = _list![_pos = _pos!-1];
      callback!(_value?.iD);
    }
  }

  String get value =>
      '${localize('number_count', dynamicValue: _value.toString())} ${isNullOrEmpty(unit) ? '' : localize(unit)}';
}
