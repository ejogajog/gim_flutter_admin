import 'package:app/bloc/base_bloc.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/utils/string_util.dart';

class TripAddressBloc extends BaseBloc {
  int? companyId;
  bool? isEditTemp;
  int noUnloadPoints = 1;
  Function? notifyParent;
  Function? validDistrict;
  CustomerPreferences? preference;
  List<BaseAddressesSuggestion?> loadPoint;
  List<BaseAddressesSuggestion?> unloadPoints;
  List<BaseAddressesSuggestion>? _lpAdrHistory;
  List<BaseAddressesSuggestion>? _upAdrHistory;
  List<BaseAddressesSuggestion>? _lpSearchAdrHistory;
  List<BaseAddressesSuggestion>? _upSearchAdrHistory;


  TripAddressBloc(this.notifyParent, this.preference,  this.loadPoint, this.unloadPoints,{this.isEditTemp, this.validDistrict}){
    if(preference != null){
      _buildLoadAdr();
      _buildUnLoadAdr();
      companyId = preference?.companyId;
    }
    notifyListeners();
  }

  get lpAdrHistory => isNullOrEmptyList(_lpAdrHistory) ? <BaseAddressesSuggestion>[] : _lpAdrHistory;
  get _unlPntAdrHistory => isNullOrEmptyList(_upAdrHistory) ? <BaseAddressesSuggestion>[] : _upAdrHistory;

  get lpSearchAdr => isNullOrEmptyList(_lpSearchAdrHistory) ? <BaseAddressesSuggestion>[] : _lpSearchAdrHistory;
  get upSearchAdr => isNullOrEmptyList(_upSearchAdrHistory) ? <BaseAddressesSuggestion>[] : _upSearchAdrHistory;

  _buildLoadAdr(){
    _lpAdrHistory = preference?.pickUpList;
    _lpSearchAdrHistory = preference?.pickUpSearchableList;
    if (!isNullOrEmptyList(_lpAdrHistory) && _lpAdrHistory![0].isDefault) {
      loadPoint[0] = _lpAdrHistory![0];
    }
  }

  _buildUnLoadAdr(){
    _upAdrHistory = preference?.dropOffList;
    _upSearchAdrHistory = preference?.dropOfSearchableList;
    noUnloadPoints = unloadPoints.length;
  }

  unlPntHistory(index){
    if(isNullOrEmptyList(_upAdrHistory)){
      return <BaseAddressesSuggestion>[];
    }
    if(unloadPoints[0] == null && _upAdrHistory![0].isDefault){
        unloadPoints[0] = _upAdrHistory![0];
    }else if(unloadPoints[index] != null){
      List list = List<BaseAddressesSuggestion>.from(_upAdrHistory!, growable: true);
      bool res = list.remove(unloadPoints[index]);
      if(res){
        list.insert(0, unloadPoints[index]);
      }
      return list;
    }
    return _unlPntAdrHistory;
  }

  addRemUnloadPoint({index}){
    if(index != null){
      unloadPoints.removeAt(index);
    }else
    unloadPoints.add(null);
    noUnloadPoints = unloadPoints.length;
    notifyListeners();
  }
}
