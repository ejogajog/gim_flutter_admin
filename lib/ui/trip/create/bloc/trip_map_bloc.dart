import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/ui/trip/create/model/address_component.dart';
import 'package:app/ui/trip/create/model/trip_model.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/ui/trip/create/repo/google_repo.dart';
import 'package:app/utils/string_util.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class TripMapBloc extends ChangeNotifier {
  bool? isMultiple;
  int?  companyId;
  bool isEditing = false;
  int selAddressIndex = -1;
  Function? locations;
  bool? _invalidNotify = true;
  bool? flagUpdateOnDrag = true;
  LatLng markerPosition = LatLng(23.8103, 90.4125);
  LatLng defaultPosition = LatLng(23.8103, 90.4125);
  List<TripLocation> listOfLocations = [];
  List<BaseAddressesSuggestion>? _addresses;
  List<BaseAddressesSuggestion>? _searchableList;
  BaseAddressesSuggestion? selAddress;

  get addressList => isNullOrEmptyList(_addresses) ? [] : _addresses;

  get searchableAddress => _searchableList;

  final FocusNode focusNode = FocusNode();

  final Function? addressCallback;

  TextEditingController _addressController = TextEditingController();

  TripMapBloc(this.addressCallback, this.isMultiple, presetInfo,
      {this.locations}) {
    companyId = presetInfo?.companyId;
    _setUpData(presetInfo);
  }

  _setUpData(presetInfo) {
    if (presetInfo != null) {
      if (isMultiple??false) {
        _addresses = presetInfo.dropOffList;
        _searchableList = presetInfo.dropOfSearchableList;
        if (!isNullOrEmptyList(presetInfo.listOfMultiDropOf))
          listOfLocations = presetInfo.listOfMultiDropOf;
        if (!isNullOrEmptyList(_addresses) && _addresses![0].isDefault!) {
          selPresetAddress = selAddressIndex = 0;
        }
        return;
      }
      _addresses = presetInfo.pickUpList;
      _searchableList = presetInfo.pickUpSearchableList;
      if (!isNullOrEmptyList(_addresses) && _addresses![0].isDefault!) {
        selPresetAddress = selAddressIndex = 0;
      }
    }
  }

  set adrInd(int index) {
    if (selAddressIndex != -1) {
      selAddressIndex = index;
      notifyListeners();
    }
  }

  set selPresetAddress(index) {
    flagUpdateOnDrag = false;
    selAddress = addressList[index];
    _updateAddress();
  }

  getPosUsingPlaceId(suggestion) async {
    isEditing = false;
    flagUpdateOnDrag = false;
    selAddressIndex = -1;
    selAddress = suggestion;
    if (isNullOrEmpty(suggestion.placeId)) {
      _updateAddress();
      return;
    }
    GoogleRepository.gPlaceDetailApi(suggestion.placeId).then((map) {
      selAddress?.lat = map['lat'];
      selAddress?.lon = map['lng'];
      selAddress?.address = selAddress?.name;
      _updateAddress();
      _updatePosition(LatLng(map['lat'], map['lng']));
    });
  }

  _updateAddress() {
    addressController.text = selAddress?.address;
    addressController.selection = TextSelection.fromPosition(
        TextPosition(offset: addressController.text.length));
    _updatePosition(LatLng(selAddress!.lat!, selAddress!.lon!));
    addressCallback!(selAddress, notify: !_invalidNotify!);
    _invalidNotify = false;
  }

  void _updatePosition(LatLng? position) {
    if (position != null) {
      markerPosition = LatLng(position.latitude, position.longitude);
      Future.delayed(Duration(milliseconds: 500), () async {
        GoogleMapController controller = await _mapController.future;
        controller.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(
              target: LatLng(position.latitude, position.longitude),
              zoom: 14.0,
            ),
          ),
        );
      });
      notifyListeners();
    }
  }

  modAddress(hasFocus) {
    if (!hasFocus && selAddress != null) {
      selAddress?.address = _addressController.text;
    }
  }

  get addressController => _addressController;

  int _markerIdCounter = 0;
  GoogleMapController? _controller;
  Map<MarkerId, Marker> _markers = {};
  Completer<GoogleMapController> _mapController = Completer();

  onCreated(GoogleMapController controller) {
    _controller = controller;
    _onMapCreated();
    if (!_mapController.isCompleted) _mapController.complete(_controller);
  }

  _onMapCreated() async {
    MarkerId markerId = MarkerId(_markerIdVal());
    Marker marker =
        Marker(markerId: markerId, position: markerPosition, draggable: false);
    _markers[markerId] = marker;
  }

  String _markerIdVal({bool increment = false}) {
    String val = 'marker_id_$_markerIdCounter';
    if (increment) _markerIdCounter++;
    return val;
  }

  onCameraMove(CameraPosition position) {
    markerPosition = position.target;
  }

  onCameraIdle() async {
    if (flagUpdateOnDrag!) {
      isEditing = false;
      Map? addresses =
          await getAddress(markerPosition.latitude, markerPosition.longitude);
      if (addresses != null) {
        String isoCode = addresses["countryCode"];
        if (isoCode == 'BD' || isoCode == 'BGD') {
          _addressController.text = addresses["address"];
          selAddress = BaseAddressesSuggestion(
              name: _addressController.text,
              address: _addressController.text,
              lat: markerPosition.latitude,
              lon: markerPosition.longitude);
          addressCallback!(selAddress, notify: (isMultiple??false) ? true : false);
          notifyListeners();
        } else {
          _updatePosition(
              LatLng(defaultPosition.latitude, defaultPosition.longitude));
        }
      }
    }
    if (!kIsWeb) flagUpdateOnDrag = true;
  }

  Future<Map> getAddress(latitude, longitude) async {
    Map map = Map();
    AddressComponent? address = await GoogleRepository.gGeocode(latitude, longitude);
    List listOfAddress = [];
    if (address != null && address.address_components != null) {
      listOfAddress = address.address_components!;
      map["address"] = address.formatted_address?.replaceAll(", Bangladesh", "");
    }
    for (var ads in listOfAddress) {
      for (var type in ads.types) {
        if (type == 'country')
          map["countryCode"] = ads.short_name;
        else if (type == 'postal_code') {
          map["address"] = map["address"].replaceAll(ads.short_name, "").trim();
        }
      }
    }
    return map;
  }

  handleMultipleLocation(index) {
    var dropOffLocations = locations!(index);
    if (index == -1 || dropOffLocations == null) {
      _addressController.clear();
      selAddress = null;
      adrInd = -1;
      if (dropOffLocations == null) {
        notifyListeners();
        return null;
      }
    }
    listOfLocations = dropOffLocations;
    notifyListeners();
    return dropOffLocations;
  }

  isDuplicateUnloadPoint(value) {
    for (int index = 0; index < listOfLocations.length; index++) {
      if (listOfLocations[index].toString() == value.address) {
        _addressController.clear();
        selAddress = null;
        flagUpdateOnDrag = false;
        notifyListeners();
        return true;
      }
    }
    return false;
  }

  onChange(data, {isEditing = true}) {
    this.isEditing = isEditing;
    if (selAddress != null) {
      flagUpdateOnDrag = true;
      adrInd = -1;
      selAddress = BaseAddressesSuggestion(
          name: data,
          address: data,
          lat: selAddress?.lat,
          lon: selAddress?.lon);
      addressCallback!(selAddress, notify: (isMultiple??false) ? true : false);
    }else
      selAddress = BaseAddressesSuggestion(lat: defaultPosition.latitude,
          lon: defaultPosition.longitude);
    notifyListeners();
  }

  get canAddAnoUnlPnt => isEditing || isNullOrEmpty(addressController.text);

  get selLblAddress => isNullOrEmpty(addressController.text)
      ? localize('lbl_not_sel')
      : isNullOrEmptyInt(selAddress?.id)
          ? addressController.text
          : selAddress?.name;
}
