import 'package:flutter/material.dart';
import 'package:app/bloc/base_bloc.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/model/master/truck_dimension.dart';
import 'package:app/model/master/truck_size.dart';
import 'package:app/model/master/truck_type.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/ui/trip/create/model/truck_info.dart';
import 'package:app/utils/prefs.dart';
import 'package:app/utils/string_util.dart';

class TruckInfoBloc extends BaseBloc {
  bool? isUsingTemp;
  Function? callback;
  TruckInfo? _truckInfo;
  List<TruckSize> _presetTruckSizes = [];
  List<TruckSize>? _masterTruckSizes;
  List<TruckType>? _masterTruckTypes;
  List<TruckDimensionLength> _presetTruckLengths = [];
  List<TruckDimensionLength>? _masterTruckLengths;
  final Map<int, TruckDataSuggestions> _truckProp = {};

  int selectedType = -1;
  int selectedDimen = -1;
  CustomerPreferences? presetInfo;

  var sizeFillColor = Colors.white;
  var lengthFillColor = Colors.white;

  get truckTypes => _masterTruckTypes ?? [];

  get truckSizes => _presetTruckSizes;

  get masterTruckSizes => _masterTruckSizes ?? [];

  get truckLengths => _presetTruckLengths;

  get masterTruckLengths => _masterTruckLengths ;

  TruckInfoBloc(this._truckInfo, this.presetInfo,
      {this.callback, this.isUsingTemp}) {
    _masterTruckTypes = Prefs.getTrucksType();
    _masterTruckSizes = Prefs.getTrucksSizes();
    _masterTruckLengths = Prefs.getTrucksLength();
    _buildDataSet();
  }

  _buildDataSet() {
    if (presetInfo != null) {
      var tds = _defaultOrTempTds();
      if (tds == null) return;
      selectedType = _masterTruckTypes
          !.indexWhere((element) => element.id == tds.truckTypeId);
      if (selectedType != -1) {
        tds.build();
        _truckProp[tds.truckTypeId] = tds;
        _presetTruckSizes = tds.sizeList;
        _presetTruckLengths = tds.lengthList;
        truckType = truckTypes[selectedType];
        truckSize(truckSizes[selectedDimen = 0]);
        truckLength(truckLengths[selectedDimen]);
      }

      if (!isNullOrEmptyList(presetInfo!.truckDataSuggestions)) {
        presetInfo!.truckDataSuggestions!.forEach((element) {
          element.build();
          if (_truckProp.containsKey(element.truckTypeId)) {
            var trkSize = _truckProp[element.truckTypeId]?.sizeList[0];
            var trkLength = _truckProp[element.truckTypeId]?.lengthList[0];
            _truckProp[element.truckTypeId]?.lengthAndSizeResponses?.addAll(element.lengthAndSizeResponses!);
            _truckProp[element.truckTypeId]?.reBuild();
            _presetTruckSizes = element.sizeList;
            _presetTruckLengths = element.lengthList;
            for (int loop = 0; loop < _presetTruckSizes.length; loop++) {
              if (_presetTruckSizes[loop] == trkSize &&
                  _presetTruckLengths[loop] == trkLength) {
                _presetTruckSizes.removeAt(loop);
                _presetTruckLengths.removeAt(loop);
                _truckProp[element.truckTypeId]?.lengthAndSizeResponses?.removeAt(loop);
                _truckProp[element.truckTypeId]?.reBuild();
              }
            }
            _presetTruckSizes.insert(0, trkSize);
            _presetTruckLengths.insert(0, trkLength);
            return;
          }
          _truckProp[element.truckTypeId!] = element;
        });
      }
    }
  }

  _defaultOrTempTds() {
    var tds;
    if (isUsingTemp??false) {
      tds = presetInfo?.tempTds;
      var defTds = presetInfo?.tripDataSuggestionResponse?.truckDataSuggestions;
      if (defTds != null) {
        int td = presetInfo!.truckDataSuggestions!.indexOf(defTds);
        if (td != -1) {
          var tdsObj = presetInfo!.truckDataSuggestions!.elementAt(td);
          if (tdsObj.lengthAndSizeResponses != null) {
            tdsObj.build();
            int length = tdsObj.sizeList.length;
            for (int loop = 0; loop < length; loop++) {
              if (tdsObj.sizeList[loop] == defTds.lengthAndSizeResponses?[0].truckSize &&
                  tdsObj.lengthList[loop] == defTds.lengthAndSizeResponses?[0].truckLength) {
                tdsObj.sizeList.removeAt(loop);
                tdsObj.lengthList.removeAt(loop);
              }
            }
          }
          if (tdsObj.lengthAndSizeResponses == null) tdsObj.lengthAndSizeResponses = [];
          tdsObj.lengthAndSizeResponses?.insert(0,TruckProp(
              truckSizeId: defTds.lengthAndSizeResponses?[0].truckSizeId,
              truckSize: defTds.lengthAndSizeResponses?[0].truckSize,
              truckLength: defTds.lengthAndSizeResponses?[0].truckLength));
          tdsObj.reBuild();
        } else
          presetInfo?.truckDataSuggestions?.add(defTds);
      }
    } else
      tds = presetInfo?.tripDataSuggestionResponse?.truckDataSuggestions;
    return tds;
  }

  set truckType(item) {
    _truckInfo?.type = item;
    callback!(_truckInfo);
    var tds = _truckProp[item.id];
    if (tds == null || tds.lengthAndSizeResponses == null) {
      _presetTruckSizes = [];
      _presetTruckLengths = [];
    } else {
      _presetTruckSizes = tds.sizeList;
      _presetTruckLengths = tds.lengthList;
    }
  }

  truckSize(item, {sync = false}) {
    _truckInfo?.size = item;
    callback!(_truckInfo);
    if (sync) {
      sizeFillColor = Colors.blue;
      notifyListeners();
    }
  }

  truckLength(item, {sync = false}) {
    _truckInfo?.length = item;
    callback!(_truckInfo);
    if (sync) {
      lengthFillColor = Colors.blue;
      notifyListeners();
    }
  }

  onCustomTap(index) {
    selectedDimen = index;
    sizeFillColor = Colors.white;
    lengthFillColor = Colors.white;
    _truckInfo?.size = null;
    _truckInfo?.length = null;
  }

  resetDimen() {
    selectedDimen = 0 - 1;
    _truckInfo?.size = null;
    _truckInfo?.length = null;
  }

  set truckDimen(index) {
    _truckInfo?.size = _presetTruckSizes[index];
    _truckInfo?.length = _presetTruckLengths[index];
    callback!(_truckInfo);
  }

  truckInfo() {
    var truckInfo = TruckInfo();
    truckInfo.type = _truckInfo?.type;
    return truckInfo;
  }

  get truckDetail => (_truckInfo?.type == null &&
          _truckInfo?.size == null &&
          _truckInfo?.length == null)
      ? localize('lbl_not_sel')
      : '${_truckInfo?.type == null ? '' : _truckInfo?.type.toString()} ${_truckInfo?.size == null ? '' : _truckInfo?.size.toString()}  ${_truckInfo?.length == null ? '' : _truckInfo?.length.toString()}';

  bool get emptyPresets => presetInfo == null;
}
