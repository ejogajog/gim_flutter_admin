import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:app/api/api_response.dart';
import 'package:app/model/master/districts.dart';
import 'package:app/ui/trip/create/repo/admin_repo.dart';
import 'package:app/utils/prefs.dart';
import 'package:app/utils/string_util.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/ui/trip/create/repo/google_repo.dart';
import 'package:app/ui/trip/create/model/address_component.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';

class AddressMapBloc extends ChangeNotifier {
  int? index;
  int? companyId;
  bool? isEditTemp;
  bool? isEditing = false;
  bool? isLoadPoint = true;
  bool? isValidDistrict = true;
  String? hintTextKey;
  int? selAddressIndex = -1;
  Function? notifyParent;
  Function? validDistrict;
  List<Districts>? defDistricts;
  bool? flagUpdateOnDrag = false;
  Function? addRemUnloadPoints;
  bool? isShowAddNewUnlPoint = false;
  FocusNode? focusNode = FocusNode();
  LatLng? markerPosition = LatLng(23.8103, 90.4125);
  LatLng? defaultPosition = LatLng(23.8103, 90.4125);
  List<BaseAddressesSuggestion>? _addressHistory;
  List<BaseAddressesSuggestion>? _searchAddressHistory;
  List<BaseAddressesSuggestion?>? addressPoints;
  TextEditingController _addressController = TextEditingController();
  TextEditingController districtEditController = TextEditingController();

  AddressMapBloc(this.hintTextKey, this.index, this.companyId,
      this._addressHistory, this._searchAddressHistory, this.addressPoints,
      {this.notifyParent, this.addRemUnloadPoints,this.validDistrict, this.isEditTemp}) {
    defDistricts = Prefs.getDistricts();
    if (addRemUnloadPoints != null) {
      isLoadPoint = false;
      isShowAddNewUnlPoint = (index == (addressPoints?.length??0) - 1);
    }
    if (addressPoints?[index!] != null) {
      selAddressIndex = _addressHistory?.indexOf(addressPoints![index!]!);
      selPresetAddress = selAddressIndex;
    }
  }

  get addressHistory => _addressHistory;

  get searchableAddress => _searchAddressHistory;

  set adrInd(int index) {
    if (selAddressIndex != -1) {
      selAddressIndex = index;
      notifyListeners();
    }
  }

  set selPresetAddress(pos) {
    flagUpdateOnDrag = false;
    if (pos > -1) addressPoints?[index!] = addressHistory[pos];
    _updateAddress();
  }

  getPosUsingPlaceId(suggestion) async {
    isEditing = false;
    flagUpdateOnDrag = false;
    selAddressIndex = -1;
    addressPoints?[index!] = suggestion;
    if (isNullOrEmpty(suggestion.placeId)) {
      _updateAddress();
      return;
    }
    GoogleRepository.gPlaceDetailApi(suggestion.placeId).then((map) {
      addressPoints?[index!]?.lat = map['lat'];
      addressPoints?[index!]?.lon = map['lng'];
      addressPoints?[index!]?.address = addressPoints![index!]?.name;
      _updateAddress();
      _updatePosition(LatLng(map['lat'], map['lng']));
    });
  }

  _updateAddress() {
    addressController.text = addressPoints![index!]?.address;
    addressController.selection = TextSelection.fromPosition(
        TextPosition(offset: addressController.text.length));
    _updatePosition(LatLng(addressPoints![index!]!.lat!, addressPoints![index!]!.lon!));
  }

  void _updatePosition(LatLng? position) {
    if (position != null) {
      markerPosition = LatLng(position.latitude, position.longitude);
      Future.delayed(Duration(milliseconds: 500), () async {
        GoogleMapController controller = await _mapController.future;
        controller.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(
              target: LatLng(position.latitude, position.longitude),
              zoom: 14.0,
            ),
          ),
        );
      });
      notifyListeners();
    }
    _fetchDistrict(position?.latitude, position?.longitude);
  }

  modAddress(hasFocus) {
    if (!hasFocus && addressPoints?[index!] != null) {
      addressPoints?[index!]?.address = _addressController.text;
      updateInstruction();
    }
  }

  updateInstruction() {
    if (notifyParent != null) notifyParent!();
  }

  get addressController => _addressController;

  int _markerIdCounter = 0;
  GoogleMapController? _controller;
  Map<MarkerId, Marker> _markers = {};
  Completer<GoogleMapController> _mapController = Completer();

  onCreated(GoogleMapController controller) {
    _controller = controller;
    _onMapCreated();
    if (!_mapController.isCompleted) _mapController.complete(_controller);
  }

  _onMapCreated() async {
    MarkerId markerId = MarkerId(_markerIdVal());
    Marker marker =
        Marker(markerId: markerId, position: markerPosition!, draggable: false);
    _markers[markerId] = marker;
  }

  String _markerIdVal({bool increment = false}) {
    String val = 'marker_id_$_markerIdCounter';
    if (increment) _markerIdCounter++;
    return val;
  }

  onCameraMove(CameraPosition position) {
    markerPosition = position.target;
  }

  onCameraIdle() async {
    if (flagUpdateOnDrag!) {
      adrInd = -1;
      isEditing = false;
      Map? addresses =
      await getAddress(markerPosition?.latitude, markerPosition?.longitude);
      if (addresses != null) {
        String isoCode = addresses["countryCode"];
        if (isoCode == 'BD' || isoCode == 'BGD') {
          int plusInd = addresses["address"].indexOf('+');
          _addressController.text = plusInd == -1 ? addresses["address"] : addresses["address"].replaceRange(0, addresses["address"].indexOf(RegExp('[, ]'), plusInd),'Unnamed Road');
          addressPoints?[index!] = BaseAddressesSuggestion(
              name: _addressController.text,
              address: _addressController.text,
              lat: markerPosition?.latitude,
              lon: markerPosition?.longitude);
          notifyListeners();
          updateInstruction();
          _fetchDistrict(markerPosition?.latitude, markerPosition?.longitude);
        } else {
          _updatePosition(
              LatLng(defaultPosition!.latitude, defaultPosition!.longitude));
        }
      }
      flagUpdateOnDrag = false;
    }
    if (!kIsWeb){
      flagUpdateOnDrag = true;
    }
  }

  Future<Map> getAddress(latitude, longitude) async {
    Map map = Map();
    AddressComponent? address =
        await GoogleRepository.gGeocode(latitude, longitude);
    List listOfAddress = [];
    if (address != null && address.address_components != null) {
      listOfAddress = address.address_components!;
      map["address"] = address.formatted_address?.replaceAll(", Bangladesh", "");
    }
    for (var ads in listOfAddress) {
      for (var type in ads.types) {
        if (type == 'country')
          map["countryCode"] = ads.short_name;
        else if (type == 'postal_code') {
          map["address"] = map["address"].replaceAll(ads.short_name, "").trim();
        }
      }
    }
    return map;
  }

  addNewUnloadPoint() {
    addRemUnloadPoints!();
  }

  removeUnlPoint() {
    addRemUnloadPoints!(index: index);
    updateInstruction();
  }

  isDuplicateUnloadPoint(value) {
    if(notifyParent == null)return false;
    for (int counter = 0; counter < addressPoints!.length; counter++) {
      if (addressPoints?[counter]?.address == value.address) {
        _addressController.clear();
        addressPoints?[index!] = BaseAddressesSuggestion(lat: defaultPosition?.latitude, lon: defaultPosition?.longitude);
        notifyListeners();
        return true;
      }
    }
    return false;
  }

  isDuplicateUp(hisInd) {
    if(notifyParent == null)return false;
    if (hisInd != selAddressIndex) {
      for (int index = 0; index < addressPoints!.length; index++) {
        if (addressPoints?[index].toString() ==
            addressHistory[hisInd].toString()) {
          return true;
        }
      }
    }
    return false;
  }

  onChange(data, {isEditing = true}) {
    this.isEditing = isEditing;
    if (addressPoints?[index!] != null) {
      flagUpdateOnDrag = true;
      adrInd = -1;
      addressPoints?[index!] = BaseAddressesSuggestion(
          name: data,
          address: data,
          district: addressPoints?[index!]?.district,
          lat: addressPoints?[index!]?.lat,
          lon: addressPoints?[index!]?.lon);
    } else
      addressPoints?[index!] = BaseAddressesSuggestion(
          district: addressPoints?[index!]?.district,
          dropoffDistrictId: addressPoints?[index!]?.dropoffDistrictId,
          lat: defaultPosition?.latitude, lon: defaultPosition?.longitude);
    notifyListeners();
  }

  set district(item){
      isDistrictValid = true;
      addressPoints?[index!]?.district = item;
  }

  set isDistrictValid(val){
    isValidDistrict = val;
    if(validDistrict != null)
      validDistrict!(val);
  }

  removeDistrict(districtTxt) {
    if(isNullOrEmpty(districtTxt) || !isValidDistrict!) {
      districtEditController.clear();
      addressPoints?[index!]?.district = null;
    }
  }

  _fetchDistrict(latitude,longitude)async{
    ApiResponse apiResponse = await AdminRepository.districtId(latitude, longitude);
    if(apiResponse.status == Status.COMPLETED){
      if(apiResponse.data != null){
        int? index = defDistricts?.indexOf(Districts(apiResponse.data));
        if(index!=-1){
          Districts? dist = defDistricts?.elementAt(defDistricts!.indexOf(Districts(apiResponse.data)));
          districtEditController.text = dist?.text??'';
          district = dist;
        }
      }
    }
  }

  get isShowDelete => (addressPoints?.length??0) > 1;

  get isShowHistory => ((addressPoints?.length??0) - 1 == index) || (isEditing??false);

  get canAddAnoUnlPnt => (isEditing??false) || isNullOrEmpty(addressController.text);

  get selLblAddress => isNullOrEmpty(addressController.text)
      ? localize('lbl_not_sel')
      : isNullOrEmptyInt(addressPoints?[index!]?.id)
          ? addressController.text
          : addressPoints?[index!]?.name;
}
