import 'package:flutter/material.dart';
import 'package:app/bloc/base_bloc.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/utils/date_time_util.dart';
import 'package:intl/intl.dart';

class TripDateTimeBloc extends BaseBloc {
  bool isCustom = false;
  Function? callback;
  String? targetUtcDateTime;
  String? targetLocalDateTime;
  DateTime? selectedDate, selectedTime;
  DateTime? _lastSelectedDate, _lastSelectedTime;
  bool isDefaultView = true;
  TripDateTimeBloc({this.callback}) {
    selectedDate = getTimeSlab();
  }

  set custom(showCustom) {
    isCustom = showCustom;
    notifyListeners();
  }

  _buildApiTripTimeStamp(String tripDateTime) {
    if (tripDateTime.isNotEmpty) {
      try {
        var formatter = DateFormat("yyyy-MM-dd hh:mm a");
        var convertedFormatter = DateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        targetLocalDateTime =
            convertedFormatter.format(formatter.parse(tripDateTime).toLocal());
        targetUtcDateTime =
            convertedFormatter.format(formatter.parse(tripDateTime).toUtc());
        convertedFormatter = DateFormat(AppDateTimeFormat);
        if (callback != null)
          callback!(convertedFormatter.format(formatter.parse(tripDateTime)),
              targetUtcDateTime, targetLocalDateTime);
      } catch (e) {
        print(e.toString());
      }
    }
    return "";
  }

  set selDate(DateTime selDate) {
    selectedDate = selDate;
    selTime(selectedTime);
    notifyListeners();
  }

  get tripDate => DateFormat(TripDateViewFormat).format(selectedDate!);

  get tripTime =>
      DateFormat(TripTimeViewFormat).format(selectedTime ?? getViewTime());

  getSelectedTime() {
    if (selectedTime == null) {
      selectedTime = getTimeSlab();
      _buildApiTripTimeStamp(DateFormat("yyyy-MM-dd").format(selectedTime!) +
          ' ' +
          DateFormat.jm().format(selectedTime!));
    }
    return TimeOfDay.fromDateTime(selectedTime!);
  }

  getViewTime() {
    if (selectedTime == null) {
      selectedTime = getTimeSlab();
      _buildApiTripTimeStamp(DateFormat("yyyy-MM-dd").format(selectedTime!) +
          ' ' +
          DateFormat.jm().format(selectedTime!));
    }
    return selectedTime;
  }

  selTime(time) {
    if (selectedTime == null || time == null) return;
    selectedTime = DateTime(
        selectedDate!.year,
        selectedDate!.month,
        selectedDate!.day,
        (time == null ? selectedDate!.hour : time.hour),
        (time == null ? selectedDate!.minute : time.minute));
    _buildApiTripTimeStamp(DateFormat("yyyy-MM-dd").format(selectedTime!) +
        ' ' +
        DateFormat.jm().format(selectedTime!));
    notifyListeners();
  }

  hourLoop() {
    List hours = [];
    int r = DateTime.now().hour;
    r = getTimeSlab().hour;
    for (int index = r, hour = 0; index < 24; index++, hour++) {
      hours.add(DateFormat('hh a').format(getTimeSlab().add(Duration(hours: hour))));
    }
    defaultDateTime(hours[0], markRebuild: false);
    return hours;
  }

  defaultDateTime(String timeInHour, {bool markRebuild = true}) {
    DateTime  time = DateFormat("hh a").parse(timeInHour);
    selectedTime = DateTime(selectedDate!.year, selectedDate!.month, selectedDate!.day,time.hour);
    _buildApiTripTimeStamp(DateFormat("yyyy-MM-dd").format(selectedTime!) +
        ' ' +
        DateFormat.jm().format(selectedTime!));
    if (markRebuild) notifyListeners();
  }

  todayTomorrow() {
    return (DateTime.now().hour >= 20 || DateTime.now().hour == 0) ? localize('tomorrow') : localize('today');
  }

  getTimeSlab(){
    int r = DateTime.now().hour;
    return r >= 20
        ? DateTime.now().add(Duration(hours: ((24 - r) + 10)))
        : r < 10
        ? DateTime.now().add(Duration(hours: (11 - r)))
        : DateTime.now().add(Duration(hours: 2));
  }

  defaultView(isDef,{hour}){
    isDefaultView = isDef;
    if(isDef){
      selectedDate = getTimeSlab();
      defaultDateTime(hour, markRebuild: true);
    }
    custom = false;
  }
}
