import 'package:flutter/cupertino.dart';
import 'package:app/bloc/base_bloc.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/model/master/goods_type.dart';
import 'package:app/utils/prefs.dart';
import 'package:app/utils/string_util.dart';

class ProductTypeBloc extends BaseBloc {
  int _selGoodIndex = -1;
  Function? callback;
  GoodsType? _selGoodsType;
  bool _isValidGood = true;
  Function? notifyIns;
  List<GoodsType>? _goodsType;
  List<GoodsType>? _presetGoods;
  Function? validGoodFlag;
  TextEditingController prodEditController = TextEditingController();

  ProductTypeBloc(customerPreset, this.validGoodFlag, {this.callback, this.notifyIns}) {
    _goodsType = Prefs.getGoodsType();
    if (customerPreset == null || isNullOrEmptyList(customerPreset.goodsType))
      return;
    _presetGoods = customerPreset.goodsType;
    if (!isNullOrEmptyList(_presetGoods) && _presetGoods![0].isDefault) {
      goodsType = 0;
    }
  }

  get goods => isNullOrEmptyList(_presetGoods) ? [] : _presetGoods;

  get masterGoods => _goodsType ?? [];

  set productType(item) {
    _selGoodIndex = -1;
    isGoodsValid = true;
    _selGoodsType = item;
    notifyListeners();
    callback!(item);
    if(notifyIns != null)notifyIns!();
  }

  set goodsType(index) {
    isGoodsValid = true;
    _selGoodIndex = index;
    _selGoodsType = goods[_selGoodIndex];
    callback!(_selGoodsType);
    _updateController(_selGoodsType.toString());
    notifyListeners();
  }

  _updateController(text){
    prodEditController.text = text;
    prodEditController.selection = TextSelection.fromPosition(TextPosition(offset: prodEditController.text.length));
  }

  removeGoodsType(goodsTxt) {
    if(isNullOrEmpty(goodsTxt) || !_isValidGood) {
      _updateController('');
      _selGoodsType = null;
      callback!(null);
      notifyListeners();
    }
  }

  set isGoodsValid(val){
    _selGoodIndex = -1;
    _isValidGood = val;
    validGoodFlag!(_isValidGood);
    notifyListeners();
  }

  get isGoodsValid => _isValidGood;

  get goodsIndex => _selGoodIndex;

  get selGoods => isNullOrEmpty(prodEditController.text) ? localize('lbl_not_sel') : prodEditController.text;
}
