import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/ui/template/saved/template_list.dart';
import 'package:app/ui/trip/create/bloc/customer_search_bloc.dart';
import 'package:app/ui/trip/create/config/trip_config_page.dart';
import 'package:app/ui/trip/create/create_trip_page.dart';
import 'package:app/ui/trip/create/model/customer_model.dart';
import 'package:app/ui/widget/auto_searh_customer.dart';
import 'package:app/ui/widget/base_widget.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/navigation_util.dart';
import 'package:app/utils/ui_util.dart';
import 'package:app/utils/validation_util.dart';
import 'package:provider/provider.dart';

class CustomerSearchPage extends StatelessWidget {
  final bool launchConfig;

  CustomerSearchPage({this.launchConfig = false});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<SearchCustomerBloc>(
            create: (_) => SearchCustomerBloc(goToConfig: launchConfig)),
      ],
      child: FindTripCustomerWidget(),
    );
  }
}

class FindTripCustomerWidget extends StatefulWidget {
  @override
  _FindTripCustomerWidgetState createState() => _FindTripCustomerWidgetState();
}

class _FindTripCustomerWidgetState
    extends BasePageWidgetState<FindTripCustomerWidget, SearchCustomerBloc> {
  TextEditingController _customerEditController = TextEditingController();

  @override
  List<Widget> getPageWidget() {
    return [
      LayoutBuilder(
        builder: (context, constraint) =>
            isMobileBrowser(constraint) ? _phoneUi() : _webUi(),
      ),
    ];
  }

  _phoneUi() {
    return Padding(
      padding: const EdgeInsets.only(left: 20.0, top: 40.0, right: 20.0),
      child: Column(
        children: [
          Text(localize('cst_find_lbl').toUpperCase(),
              style: TextStyle(
                color: ColorResource.colorMarineBlue,
                fontSize: 14,
                fontFamily: "roboto",
                fontWeight: FontWeight.w700,
              )),
          SizedBox(height: 10.0),
          CustomerSearchWidget<CustomerInfo>(
            immediateSuggestion: false,
            onlyForEnterprise: bloc!.goToConfig,
            hintText: translate(context, 'name_or_mob'),
            typeAheadController: _customerEditController,
            onSuggestionItemSelected: (dynamic suggestion) {
              _reqTripPref(suggestion);
            },
            validationMessageRegexPair: {
              NOT_EMPTY_REGEX: translate(context, 'mne_val_msg')
            },
          ),
        ],
      ),
    );
  }

  _webUi() {
    return Container(
      color: ColorResource.light_white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: containerWidth / 1.3,
                padding: EdgeInsets.fromLTRB(60.0, 100.0, 60.0, 100.0),
                decoration: BoxDecoration(
                  color: ColorResource.colorWhite,
                  borderRadius:
                      const BorderRadius.all(const Radius.circular(16.0)),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(localize('cst_find_lbl').toUpperCase(),
                        style: TextStyle(
                          color: ColorResource.colorMarineBlue,
                          fontSize: 15,
                          fontFamily: "roboto",
                          letterSpacing: .8,
                          fontWeight: FontWeight.w900,
                        )),
                    CustomerSearchWidget<CustomerInfo>(
                      immediateSuggestion: false,
                      onlyForEnterprise: bloc!.goToConfig,
                      hintText: translate(context, 'name_or_mob'),
                      typeAheadController: _customerEditController,
                      onSuggestionItemSelected: (dynamic suggestion) {
                        _reqTripPref(suggestion);
                      },
                      validationMessageRegexPair: {
                        NOT_EMPTY_REGEX: translate(context, 'mne_val_msg')
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  _reqTripPref(suggestion) async {
    bloc!.customer(suggestion);
    submitDialog(context);
    await bloc!.requestCustomerPref();
    Navigator.pop(context);
    var page, pageName;
    if (bloc!.goToConfig??false) {
      pageName = AnalyticsEvents.CUSTOMER_CONFIG_SCREEN;
      page = CustomerTripConfigPage(bloc!.customerInfo, bloc!.customerPreset);
    } else if (bloc!.customerPreset == null) {
      pageName = AnalyticsEvents.CREATE_TRIP_SCREEN;
      page = CreateTripPage(bloc!.customerInfo, bloc!.customerPreset);
    } else {
      pageName = AnalyticsEvents.TEMPLATE_LIST_SCREEN;
      page = TemplateListScreen(bloc!.customerInfo, bloc!.customerPreset);
    }
    var response = await navigateNextScreen(context, page, pageName: pageName);
    if (response ?? false) {
      Navigator.pop(context);
    } else {
      _customerEditController.text = "";
    }
  }
}
