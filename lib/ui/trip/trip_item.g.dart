// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'trip_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TripItem _$TripItemFromJson(Map<String, dynamic> json) => TripItem()
  ..trackerAdded = json['trackerAdded'] as bool?
  ..trackerActivated = json['trackerActivated'] as bool?
  ..tripId = (json['tripId'] as num?)?.toInt()
  ..tripNumber = (json['tripNumber'] as num?)?.toInt()
  ..tripPickupDateTimeLocal = (json['tripPickupDateTimeLocal'] as num?)?.toInt()
  ..tripPickupDateTimeUtc = (json['tripPickupDateTimeUtc'] as num?)?.toInt()
  ..tripPickupAddress = json['tripPickupAddress'] as String?
  ..tripPickupCity = json['tripPickupCity'] as String?
  ..tripPickupDistrict = json['tripPickupDistrict'] as String?
  ..tripPickupLat = json['tripPickupLat'] as String?
  ..tripPickupLong = json['tripPickupLong'] as String?
  ..tripDropOffAddress = json['tripDropOffAddress'] as String?
  ..tripDropOffCity = json['tripDropOffCity'] as String?
  ..tripDropOffDistrict = json['tripDropOffDistrict'] as String?
  ..tripDropoffLat = json['tripDropoffLat'] as String?
  ..tripDropoffLong = json['tripDropoffLong'] as String?
  ..tripStatus = json['tripStatus'] as String?
  ..tripPaymentType = json['tripPaymentType'] as String?
  ..tripInstruction = json['tripInstruction'] as String?
  ..tripGoodsImage = json['tripGoodsImage'] as String?
  ..tripCreatedAt = (json['tripCreatedAt'] as num?)?.toInt()
  ..goodsType = (json['goodsType'] as num?)?.toInt()
  ..truckType = (json['truckType'] as num?)?.toInt()
  ..truckRegistrationNumber = json['truckRegistrationNumber'] as String?
  ..truckSize = (json['truckSize'] as num?)?.toInt()
  ..truckHeight = (json['truckHeight'] as num?)?.toDouble()
  ..truckWidth = (json['truckWidth'] as num?)?.toDouble()
  ..truckLength = (json['truckLength'] as num?)?.toDouble()
  ..customerName = json['customerName'] as String?
  ..customerImage = json['customerImage'] as String?
  ..customerMobileNo = json['customerMobileNo'] as String?
  ..customerNidNo = json['customerNidNo'] as String?
  ..customerDob = (json['customerDob'] as num?)?.toInt()
  ..receiverNumber = json['receiverNumber'] as String?
  ..receiverName = json['receiverName'] as String?
  ..totalBid = (json['totalBid'] as num?)?.toInt()
  ..tripOverviewComment = json['tripOverviewComment'] as String?
  ..tripMetaCustomerType = json['tripMetaCustomerType'] as String?
  ..comment = json['comment'] as String?
  ..creationReason = json['creationReason'] as String?
  ..enterprise = json['enterprise'] as bool?
  ..enterpriseName = json['enterpriseName'] as String?
  ..distributor = json['distributor'] as bool?
  ..project = json['project'] as bool?
  ..projectName = json['projectName'] as String?
  ..expectedTripPrice = (json['expectedTripPrice'] as num?)?.toDouble()
  ..numberOfUnloadPoint = (json['numberOfUnloadPoint'] as num?)?.toInt()
  ..completedDateTime = json['completedDateTime'] as String?
  ..partnerName = json['partnerName'] as String?
  ..partnerMobileNo = json['partnerMobileNo'] as String?
  ..driverName = json['driverName'] as String?
  ..driverMobileNo = json['driverMobileNo'] as String?
  ..startDateTime = (json['startDateTime'] as num?)?.toInt()
  ..goodsTypeName = json['goodsTypeName'] as String?
  ..distributorCompanyName = json['distributorCompanyName'] as String?
  ..distributorMobileNumber = json['distributorMobileNumber'] as String?
  ..distributorUserId = (json['distributorUserId'] as num?)?.toInt();

Map<String, dynamic> _$TripItemToJson(TripItem instance) => <String, dynamic>{
      'trackerAdded': instance.trackerAdded,
      'trackerActivated': instance.trackerActivated,
      'tripId': instance.tripId,
      'tripNumber': instance.tripNumber,
      'tripPickupDateTimeLocal': instance.tripPickupDateTimeLocal,
      'tripPickupDateTimeUtc': instance.tripPickupDateTimeUtc,
      'tripPickupAddress': instance.tripPickupAddress,
      'tripPickupCity': instance.tripPickupCity,
      'tripPickupDistrict': instance.tripPickupDistrict,
      'tripPickupLat': instance.tripPickupLat,
      'tripPickupLong': instance.tripPickupLong,
      'tripDropOffAddress': instance.tripDropOffAddress,
      'tripDropOffCity': instance.tripDropOffCity,
      'tripDropOffDistrict': instance.tripDropOffDistrict,
      'tripDropoffLat': instance.tripDropoffLat,
      'tripDropoffLong': instance.tripDropoffLong,
      'tripStatus': instance.tripStatus,
      'tripPaymentType': instance.tripPaymentType,
      'tripInstruction': instance.tripInstruction,
      'tripGoodsImage': instance.tripGoodsImage,
      'tripCreatedAt': instance.tripCreatedAt,
      'goodsType': instance.goodsType,
      'truckType': instance.truckType,
      'truckRegistrationNumber': instance.truckRegistrationNumber,
      'truckSize': instance.truckSize,
      'truckHeight': instance.truckHeight,
      'truckWidth': instance.truckWidth,
      'truckLength': instance.truckLength,
      'customerName': instance.customerName,
      'customerImage': instance.customerImage,
      'customerMobileNo': instance.customerMobileNo,
      'customerNidNo': instance.customerNidNo,
      'customerDob': instance.customerDob,
      'receiverNumber': instance.receiverNumber,
      'receiverName': instance.receiverName,
      'totalBid': instance.totalBid,
      'tripOverviewComment': instance.tripOverviewComment,
      'tripMetaCustomerType': instance.tripMetaCustomerType,
      'comment': instance.comment,
      'creationReason': instance.creationReason,
      'enterprise': instance.enterprise,
      'enterpriseName': instance.enterpriseName,
      'distributor': instance.distributor,
      'project': instance.project,
      'projectName': instance.projectName,
      'expectedTripPrice': instance.expectedTripPrice,
      'numberOfUnloadPoint': instance.numberOfUnloadPoint,
      'completedDateTime': instance.completedDateTime,
      'partnerName': instance.partnerName,
      'partnerMobileNo': instance.partnerMobileNo,
      'driverName': instance.driverName,
      'driverMobileNo': instance.driverMobileNo,
      'startDateTime': instance.startDateTime,
      'goodsTypeName': instance.goodsTypeName,
      'distributorCompanyName': instance.distributorCompanyName,
      'distributorMobileNumber': instance.distributorMobileNumber,
      'distributorUserId': instance.distributorUserId,
    };
