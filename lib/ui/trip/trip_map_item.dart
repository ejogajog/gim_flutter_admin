import 'package:app/ui/trip/trip_item.dart';

class TripMapItem{

  String enterpriseName;
  List<TripItem> tripItem = List.empty(growable: true);

  TripMapItem(this.enterpriseName);
}