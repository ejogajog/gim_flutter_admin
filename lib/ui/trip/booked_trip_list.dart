import 'dart:async';

import 'package:flutter/material.dart';
import 'package:app/api/api_response.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/styles.dart';
import 'package:app/ui/trip/booked_trip_item.dart';
import 'package:app/ui/trip/trip_list_bloc.dart';
import 'package:app/ui/widget/base_widget.dart';
import 'package:app/ui/widget/list_adapter.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/ui_util.dart';
import 'package:provider/provider.dart';

class BookedTripListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<TripListBloc>(
        create: (context) => TripListBloc(), child: BookedTripsScreen());
  }
}

class BookedTripsScreen extends StatefulWidget {
  @override
  _BookedTripsScreenState createState() => _BookedTripsScreenState();
}

class _BookedTripsScreenState extends BasePageWidgetState<BookedTripsScreen, TripListBloc> {
  Timer? timer;

  Icon actionIcon = new Icon(
    Icons.search,
    color: Colors.blue,
  );

  Widget appBarTitle = new Text(
    '',
    style: new TextStyle(color: Colors.white),
  );

  final TextEditingController _searchQuery = new TextEditingController();
  bool _IsSearching = false;
  String _searchText = "";

  @override
  onBuildCompleted() {
    if (mounted) {
      bloc!.headerTitle = 'Booked Trips';
      appBarTitle = new Text(
        translate(context, 'lbl_booked_trip'),
        style: new TextStyle(color: Colors.black),
      );
      bloc!.setTripState(TripState.Booked);
      bloc!.getListFromApi(callback: onApiCallback);
      _searchQuery.addListener(() {
        if (_searchQuery.text.isEmpty && !_IsSearching) {
          _IsSearching = false;
          _searchText = "";
          onSearchTrip(_searchText);
        } else {
          _IsSearching = true;
          _searchText = _searchQuery.text;
          onSearchTrip(_searchText);
        }
      });
    }
    setUpTimer();
  }

  @override
  onRetryClick() {
    super.onRetryClick();
    bloc!.reloadList(callback: onApiCallback);
  }

  onApiCallback() async => print("Trip Count: ${bloc!.trip}");

  onSearchTrip(String text) {
    bloc!.searchTrip(text);
  }

  onFilterTrip() {
    bloc!.reloadList(callback: onApiCallback);
  }

  @override
  List<Widget> getPageWidget() {
    return [
      showEmptyWidget<TripListBloc>(warning_msg: 'no_booked_trip'),
      Container(
          width: double.infinity,
          child: Consumer<TripListBloc>(
              builder: (context, bloc, _) => Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                Container(
                      width: 160,
                      height: 40,
                      margin: const EdgeInsets.only(top: 5.0, right: 5.0, bottom: 5.0),
                      child: TextField(
                        autofocus: true,
                        controller: _searchQuery,
                        style: Styles.txtStyleBlackBangla,
                        decoration: new InputDecoration(
                            prefixIcon: new Icon(Icons.search, color: Colors.grey),
                            hintText: translate(context, 'hnt_search'),
                            hintStyle: new TextStyle(color: Colors.grey),
                            focusedBorder: const OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: ColorResource.colorMarineBlue, width: 1.0),
                            ),
                            border: OutlineInputBorder(),
                            filled: true,
                            fillColor: Colors.white70,
                            contentPadding: const EdgeInsets.all(10.0)),
                      ),
                    ),
                getList<TripListBloc>(
                  bloc,
                      () => BookedTripItem(
                    bloc,
                    onStartTrip: _startTrip,
                  ),
                  apiCallback: onApiCallback,
                  shouldKeepDefaultDivider: true,
                  onItemClicked: (item) => onNavigateToDetailPage(
                    item,
                  ),
                )
              ]))),
      showLoader<TripListBloc>(Provider.of<TripListBloc>(context))
    ];
  }

  @override
  getAppbar() => PreferredSize(
    preferredSize: Size.fromHeight(56.0),
    child: AppBar(
      centerTitle: true,
      title: Text(
        bloc!.headerTitle,
        style: new TextStyle(color: Colors.black),
      ),
    ),
  );

  onNavigateToDetailPage(item) async {
    print('Navigate to detail page');
  }

  setUpTimer() {
    timer = Timer.periodic(const Duration(minutes: 5), (Timer t) {
      if (!_IsSearching && bloc!.tripState() == TripState.Live) {
        onRetryClick();
      }
    });
  }

  _startTrip(tripId) async{
    hideSoftKeyboard(context);
    showCustomDialog(
        context, 'Start Trip', 'Are you sure you want to Start trip?',
            () async {
          ApiResponse response = await bloc!.startTrip(tripId);
          if(response.status == Status.COMPLETED){
            showCustomDialog(context, 'Success', response.message, (){
              _IsSearching = false;
              _searchQuery.clear();
              onRetryClick();
            });
            return;
          }
          showCustomDialog(context, 'Start Trip', response.message ?? "Something went wrong", (){});
          AnalyticsManager().logEvent(AnalyticsEvents.START_TRIP_ERROR);
        },
        eventName: AnalyticsEvents.START_TRIP_SUCCESS_DLG,
        withNegBtn: true);
  }
}
