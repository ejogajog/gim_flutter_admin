import 'package:app/model/base_response.dart';
import 'package:json_annotation/json_annotation.dart';

part 'trip_item.g.dart';

@JsonSerializable()
class TripItem extends BaseTrackerInfo {
  TripItem();

  int? tripId;
  int? tripNumber;
  int? tripPickupDateTimeLocal;
  int? tripPickupDateTimeUtc;
  String? tripPickupAddress;
  String? tripPickupCity;
  String? tripPickupDistrict;
  String? tripPickupLat;
  String? tripPickupLong;
  String? tripDropOffAddress;
  String? tripDropOffCity;
  String? tripDropOffDistrict;
  String? tripDropoffLat;
  String? tripDropoffLong;
  String? tripStatus;
  String? tripPaymentType;
  String? tripInstruction;
  String? tripGoodsImage;
  int? tripCreatedAt;
  int? goodsType;
  int? truckType;
  String? truckRegistrationNumber;
  int? truckSize;
  double? truckHeight;
  double? truckWidth;
  double? truckLength;
  String? customerName;
  String? customerImage;
  String? customerMobileNo;
  String? customerNidNo;
  int? customerDob;
  String? receiverNumber;
  String? receiverName;
  int? totalBid;
  String? tripOverviewComment;
  String? tripMetaCustomerType;
  String? comment;
  String? creationReason;
  bool? enterprise;
  String? enterpriseName;
  bool? distributor;
  bool? project;
  String? projectName;
  double? expectedTripPrice;
  int? numberOfUnloadPoint;
  String? completedDateTime;
  String? partnerName;
  String? partnerMobileNo;
  String? driverName;
  String? driverMobileNo;
  int? startDateTime;
  String? goodsTypeName;

  String? distributorCompanyName;
  String? distributorMobileNumber;
  int? distributorUserId;

  factory TripItem.fromJson(Map<String, dynamic> json) =>
      _$TripItemFromJson(json);

  Map<String, dynamic> toJson() => _$TripItemToJson(this);

  getTruckTypeWithSize() {
    return '$truckType, $truckSize TON';
  }
}
