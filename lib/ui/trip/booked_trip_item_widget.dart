import 'package:flutter/material.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/styles.dart';
import 'package:app/ui/detail/trip_details.dart';
import 'package:app/ui/trip/trip_item.dart';
import 'package:app/ui/trip/trip_list_bloc.dart';
import 'package:app/ui/widget/app_button.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/navigation_util.dart';

class BookedTripItemWidget extends StatefulWidget {
  final TripItem? item;
  final Function? onStartTrip;
  final TripListBloc? _tripListBloc;

  BookedTripItemWidget(this.item, this._tripListBloc, {this.onStartTrip});

  @override
  _TripItemWidgetState createState() => _TripItemWidgetState();
}

class _TripItemWidgetState extends State<BookedTripItemWidget> {
  int? tripRunningHours;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        AnalyticsManager().logEventProp(AnalyticsEvents.TRIP_ITEM_CLICK,
            {AnalyticProperties.TRIP_ID: widget.item?.tripId});
        navigateNextScreen(
            context,
            TripDetailScreen(
                widget.item?.enterpriseName, widget.item, widget._tripListBloc),
            pageName: AnalyticsEvents.TRIP_DETAIL_SCREEN);
      },
      child: Column(
        children: [
          Expanded(
            child: Container(
              margin: EdgeInsets.only(top: 6),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: HexColor('FF1C0339'),
                  borderRadius: BorderRadius.only(topLeft:Radius.circular(10.0), topRight: Radius.circular(10.0))),
              padding: const EdgeInsets.fromLTRB(6.0, 10.0, 6.0, 6.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                      '${translate(context, 'lbl_trip_no')}${widget.item?.tripNumber}',
                      style: Styles.txtStyleWhiteNormal),
                  SizedBox(
                    height: 1,
                  ),
                  Text(
                      '${translate(context, 'lbl_partner')}${widget.item?.partnerName ?? ''}',
                      maxLines: 1,
                      style: Styles.txtStyleWhiteNormal),
                  SizedBox(
                    height: 1,
                  ),
                  Text(
                      '${translate(context, 'lbl_driver')}${widget.item?.driverName ?? ''}',
                      maxLines: 1,
                      style: Styles.txtStyleWhiteNormal),
                  SizedBox(
                    height: 1,
                  ),
                  Expanded(
                    child: Text(
                        '${translate(context, 'lbl_pickup')}${widget.item?.tripPickupAddress}',
                        maxLines: 2,
                        style: Styles.txtStyleWhiteNormal),
                  ),
                ],
              ),
            ),
          ),
          FilledColorButton(backGroundColor: ColorResource.colorWhite, textColor: ColorResource.lblClr, fontSize: 14.0, onPressed: ()=>widget.onStartTrip!(widget.item?.tripId), buttonText: 'Start Trip', isOnlyTopRadius: true, innerPadding: 16.0, verticalMargin: 0.0)
        ],
      ),
    );
  }
}
