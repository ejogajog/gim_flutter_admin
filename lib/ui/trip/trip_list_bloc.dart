import 'dart:collection';

import 'package:app/api/api_response.dart';
import 'package:app/bloc/base_trip_list_bloc.dart';
import 'package:app/model/master/districts.dart';
import 'package:app/model/master/thanas.dart';
import 'package:app/ui/auth/auth_repository.dart';
import 'package:app/ui/auth/logout_response.dart';
import 'package:app/ui/trip/trip_item.dart';
import 'package:app/ui/trip/trip_map_item.dart';
import 'package:app/ui/trip/trip_repository.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/prefs.dart';
import 'package:app/utils/string_util.dart';

class TripListBloc extends BaseTripListBloc {
  String headerTitle = 'Live Trips';

  List<Thanas>? thanas;
  List<Districts>? districts;

  ApiResponse? _logoutResponse;
  ApiResponse get logoutResponse => _logoutResponse!;
  set logoutResponse(ApiResponse value) {
    _logoutResponse = value;
  }

  TripState? _selectedTripState;
  TripState? tripState() => _selectedTripState;
  setTripState(TripState? state) {
    _selectedTripState = state;

    queryParam.remove('tripFilter');
    queryParam.remove('availableTripFilter');

    if (_selectedTripState == null) {
      return;
    }

    if (_selectedTripState == TripState.Bidded) {
      queryParam['tripFilter'] = TripState.Available.filterValue()!;
      queryParam['availableTripFilter'] = _selectedTripState!.filterValue()!;
    } else {
      queryParam['tripFilter'] = _selectedTripState!.filterValue()!;
    }
  }

  List<TripItem>? orgListOfTripItems;
  List<TripMapItem>? listOfTripMapItem;
  String searchText = '';

  @override
  getListFromApi({callback}) async {
    isLoading = true;
    ApiResponse response =
        await TripRepository.getTripList(getBaseQueryParam());
    onTripListResponse(response, callback: _buildMap);
    takeDecisionShowingError(response);
    if (response.status == Status.COMPLETED) {
      int index = headerTitle.indexOf('(');
      headerTitle =
          '${index == -1 ? '$headerTitle(${trip?.tripListForSelectedTripFilter?.numberOfResults})' : headerTitle.substring(0, index) + '(${trip?.tripListForSelectedTripFilter?.numberOfResults})'}';
    }
  }

  bool isBiddedTrip() => this._selectedTripState == TripState.Bidded;

  bool isBookedTrip() => this._selectedTripState == TripState.Booked;

  bool isLiveTrip() => this._selectedTripState == TripState.Live;

  searchTrip(String text) {
    if (trip == null) return;
    searchText = text;
    clearItems();
    setPaginationItem(
        trip?.tripListForSelectedTripFilter,
        isNullOrEmpty(searchText)
            ? _buildMap(orgListOfTripItems)
            : _buildMap(trip?.tripListForSelectedTripFilter?.contentList));
  }

  _buildMap(tripItems) {
    LinkedHashMap<String, TripMapItem> map = LinkedHashMap();

    if (!isNullOrEmptyList(tripItems)) {
      orgListOfTripItems = List.empty(growable: true);
      orgListOfTripItems?.addAll(tripItems);

      for (int index = 0; index < tripItems.length; index++) {
        var element = tripItems.elementAt(index);
        var tripNumber = element.tripNumber.toString();
        if (tripNumber.contains(searchText) ||
            (!isNullOrEmpty(element.partnerName) &&
                element.partnerName.toLowerCase().contains(searchText)) ||
            (!isNullOrEmpty(element.driverName) &&
                element.driverName.toLowerCase().contains(searchText)) ||
            (!isNullOrEmpty(element.tripPickupAddress) &&
                element.tripPickupAddress.toLowerCase().contains(searchText))) {
          element.enterpriseName = element.enterpriseName ?? 'Regular';

          if (map.containsKey(element.enterpriseName)) {
            var item = map[element.enterpriseName];
            item?.tripItem.add(element);
          } else {
            var tripMapItem = TripMapItem(element.enterpriseName);
            tripMapItem.tripItem.add(element);
            map[element.enterpriseName] = tripMapItem;
          }
        }
      }
    }

    List<TripMapItem> list = List.empty(growable: true);

    for (var v in map.values) {
      list.add(v);
      //print(v);
    }

    return listOfTripMapItem = list;
  }

  signOut() async {
    print("Inside logout");
    isLoading = true;
    var response = await AuthRepository.signOut();
    logoutResponse = response;
    isLoading = false;
    switch (response.status) {
      case Status.LOADING:
        break;
      case Status.COMPLETED:
        print("Logout success");
        LogoutResponse? logoutResponse = LogoutResponse.fromJson(response.data);
        if (logoutResponse.status??false) {
          Prefs.setBoolean(Prefs.IS_LOGGED_IN, false);
          Prefs.setBoolean(Prefs.IS_DISTRIBUTOR, false);
          Prefs.setBoolean(Prefs.PREF_IS_ENTERPRISE_USER, false);
        }
        break;
      case Status.ERROR:
        print("Logout ERROR");
        break;
    }
    return response;
  }

  startTrip(tripId) async {
    isLoading = true;
    ApiResponse response = await TripRepository.startTrip(tripId);
    isLoading = false;
    return response;
  }
}
