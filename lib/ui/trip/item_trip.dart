import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app/styles.dart';
import 'package:app/ui/trip/trip_item_widget.dart';
import 'package:app/ui/trip/trip_list_bloc.dart';
import 'package:app/ui/trip/trip_map_item.dart';
import 'package:app/ui/widget/base_widget.dart';
import 'package:app/ui/widget/listview_shape.dart';
import 'package:app/utils/app_colors.dart';
import 'package:app/utils/color_const.dart';

class ItemTrip extends BaseItemView<TripMapItem> {

  final textColor;
  final backgroundColor;
  final TripListBloc _tripListBloc;

  ItemTrip(this._tripListBloc,{
    this.textColor = AppColors.TextOnPrimary,
    this.backgroundColor = AppColors.Primary,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: textColor,
      child: InkWell(
        onTap: () => onItemClick!(item),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Divider(
              color: ColorResource.divider_color,
              height: 1,
            ),
            Stack(
              children: <Widget>[
                getLeftSideShape(),
                getRightSideShape(),
                Padding(
                  padding:
                      EdgeInsets.only(left: 18, right: 18, top: 10, bottom: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Column(
                        children: [
                          ListTile(title: Text('${item!.enterpriseName}', textAlign: TextAlign.start, style: Styles.blueBigTextBold)),
                          GridView.extent(
                              maxCrossAxisExtent: 170.0,
                              crossAxisSpacing: 20.0,
                              mainAxisSpacing: 20.0,
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              children: item!.tripItem.map((el) => TripItemWidget(el,_tripListBloc)).toList()
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
