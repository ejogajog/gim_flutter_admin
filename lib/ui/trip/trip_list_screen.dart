import 'dart:async';

import 'package:flutter/material.dart';
import 'package:app/api/api_response.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/styles.dart';
import 'package:app/ui/auth/login_screen.dart';
import 'package:app/ui/trip/item_trip.dart';
import 'package:app/ui/trip/search_customer.dart';
import 'package:app/ui/trip/trip_list_bloc.dart';
import 'package:app/ui/widget/base_widget.dart';
import 'package:app/ui/widget/list_adapter.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/app_colors.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/navigation_util.dart';
import 'package:app/utils/prefs.dart';
import 'package:app/utils/ui_util.dart';
import 'package:provider/provider.dart';

class TripListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<TripListBloc>(
        create: (context) => TripListBloc(), child: LiveTripScreen());
  }
}

class LiveTripScreen extends StatefulWidget {
  @override
  _LiveTripScreenState createState() => _LiveTripScreenState();
}

class _LiveTripScreenState extends BasePageWidgetState<LiveTripScreen, TripListBloc> {
  Timer? timer;

  Icon actionIcon = new Icon(
    Icons.search,
    color: Colors.blue,
  );

  Widget appBarTitle = new Text(
    '',
    style: new TextStyle(color: Colors.white),
  );

  final TextEditingController _searchQuery = new TextEditingController();
  bool _IsSearching = false;
  String _searchText = "";

  @override
  void initState() {
    super.initState();
  }

  @override
  onBuildCompleted() {
    if (mounted) {
      appBarTitle = new Text(
        translate(context, 'lbl_live_trip'),
        style: new TextStyle(color: Colors.white),
      );
      bloc!.setTripState(TripState.Live);
      bloc!.getListFromApi(callback: onApiCallback);
      _searchQuery.addListener(() {
        if (_searchQuery.text.isEmpty && !_IsSearching) {
          _IsSearching = false;
          _searchText = "";
          onSearchTrip(_searchText);
        } else {
          _IsSearching = true;
          _searchText = _searchQuery.text;
          print(_searchText);
          onSearchTrip(_searchText);
        }
      });
    }
    setUpTimer();
  }

  @override
  onRetryClick() {
    super.onRetryClick();
    bloc?.reloadList(callback: onApiCallback);
  }

  onApiCallback() async => print("Trip Count: ${bloc?.trip}");

  onSearchTrip(String text) {
    bloc!.searchTrip(text);
  }

  onFilterTrip() {
    bloc!.reloadList(callback: onApiCallback);
  }

  @override
  List<Widget> getPageWidget() {
    return [
      showEmptyWidget<TripListBloc>(warning_msg: 'no_live_trip'),
      Container(
          width: double.infinity,
          child: Consumer<TripListBloc>(
              builder: (context, bloc, _) => Column(children: [
                    Container(
                        padding: EdgeInsets.symmetric(
                          vertical: 8,
                        ),
                        child: mobTabWidget()),
                    getList<TripListBloc>(
                      bloc,
                      () => ItemTrip(
                        bloc,
                        textColor: _getTextColor(bloc.tripState()),
                        backgroundColor: _getBackgroundColor(bloc.tripState()),
                      ),
                      apiCallback: onApiCallback,
                      shouldKeepDefaultDivider: true,
                      onItemClicked: (item) => onNavigateToDetailPage(
                        item,
                      ),
                    )
                  ]))),
      showLoader<TripListBloc>(Provider.of<TripListBloc>(context))
    ];
  }

  @override
  getAppbar() => PreferredSize(
        preferredSize: Size.fromHeight(56.0),
        child: AppBar(
          centerTitle: true,
          title: Text(
            bloc!.headerTitle,
            style: new TextStyle(color: Colors.white),
          ),
          leading: InkWell(
            child: Padding(
              padding: EdgeInsets.only(right: 20.0, left: 10.0),
              child: Icon(
                Icons.logout,
                color: Colors.white,
                size: 20.0,
              ),
            ),
            onTap: () => _onLogoutButtonPressed(),
          ),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: ActionChip(
                onPressed: () =>
                    navigateNextScreen(context, CustomerSearchPage()),
                avatar: Icon(Icons.add_circle,
                    color: ColorResource.colorPrimary, size: 18),
                label: Text('Create New Trip',
                    style: TextStyle(
                        color: ColorResource.colorPrimary,
                        fontWeight: FontWeight.bold,
                        fontSize: 10.0)),
              ),
            )
            /*InkWell(
              child: Padding(
                padding: EdgeInsets.only(right: 20.0, left: 10.0),
                child: Icon(
                  Icons.refresh,
                  color: Colors.white,
                  size: 20.0,
                ),
              ),
              onTap: () => onRetryClick(),
            ),*/
            /*InkWell(
              child: Padding(
                padding: EdgeInsets.only(left: 10.0, right: 20.0, top: 8.0),
                child: Text("৳",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0),
                    textAlign: TextAlign.center),
              ),
              onTap: () => navigateNextScreen(context, PaymentApprovalScreen()),
            ),*/
          ],
        ),
      );

  Widget mobTabWidget() {
    return Column(
      children: [
        Wrap(
          spacing: 2.0,
          children: [
            AppFilterChip(
              text: tripTypeConst[3],
              selected: bloc!.isLiveTrip(),
              textColor: _getBackgroundColor(TripState.Live),
              selectedTextColor: _getTextColor(TripState.Live),
              onSelected: (selected) {
                AnalyticsManager().logEvent(AnalyticsEvents.LIVE_TRIP_BTN_CLK);
                this.setState(() {
                  bloc!.headerTitle = translate(context, 'lbl_live_trip');
                  bloc!.setTripState(TripState.Live);
                  onFilterTrip();
                });
              },
            ),
            AppFilterChip(
              text: tripTypeConst[2],
              selected: bloc!.isBookedTrip(),
              textColor: _getBackgroundColor(TripState.Booked),
              selectedTextColor: _getTextColor(TripState.Booked),
              onSelected: (selected) {
                AnalyticsManager()
                    .logEvent(AnalyticsEvents.BOOKED_TRIP_BTN_CLK);
                this.setState(() {
                  bloc!.headerTitle = translate(context, 'lbl_booked_trip');
                  bloc!.setTripState(TripState.Booked);
                  onFilterTrip();
                });
              },
            ),
            AppFilterChip(
              text: tripTypeConst[1],
              selected: bloc!.isBiddedTrip(),
              textColor: _getBackgroundColor(TripState.Bidded),
              selectedTextColor: _getTextColor(TripState.Bidded),
              onSelected: (selected) {
                AnalyticsManager().logEvent(AnalyticsEvents.BID_TRIP_BTN_CLK);
                this.setState(() {
                  bloc!.headerTitle = translate(context, 'lbl_bidded_trip');
                  bloc!.setTripState(TripState.Bidded);
                  onFilterTrip();
                });
              },
            ),
            AppFilterChip(
              text: tripTypeConst[0],
              selected: !bloc!.isBiddedTrip() &&
                  !bloc!.isBookedTrip() &&
                  !bloc!.isLiveTrip(),
              textColor: _getBackgroundColor(null),
              selectedTextColor: _getTextColor(null),
              onSelected: (selected) {
                AnalyticsManager().logEvent(AnalyticsEvents.ALL_TRIP_BTN_CLK);
                this.setState(() {
                  bloc?.headerTitle = translate(context, 'lbl_all_trip');
                  bloc?.setTripState(null);
                  onFilterTrip();
                });
              },
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              width: 180,
              height: 40,
              child: TextField(
                autofocus: true,
                controller: _searchQuery,
                style: Styles.txtStyleBlackBangla,
                decoration: new InputDecoration(
                    prefixIcon: new Icon(Icons.search, color: Colors.grey),
                    hintText: translate(context, 'hnt_search'),
                    hintStyle: new TextStyle(color: Colors.grey),
                    focusedBorder: const OutlineInputBorder(
                      borderSide: const BorderSide(
                          color: ColorResource.colorMarineBlue, width: 1.0),
                    ),
                    border: OutlineInputBorder(),
                    filled: true,
                    fillColor: Colors.white70,
                    contentPadding: const EdgeInsets.all(10.0)),
              ),
            ),
            SizedBox(
              width: 5.0,
            ),
          ],
        ),
      ],
    );
  }

  Color _getTextColor(TripState? tripState) {
    if (tripState == null) {
      return HexColor('ffffffff');
    } else if (tripState == TripState.Bidded) {
      return AppColors.TextOnSecondary;
    } else if (tripState == TripState.Booked) {
      return HexColor('ffffffff');
    } else if (tripState == TripState.Live) {
      return AppColors.TextOnPrimary;
    } else {
      return HexColor('ff000000');
    }
  }

  Color _getBackgroundColor(TripState? tripState) {
    if (tripState == null) {
      return HexColor('4a4a4a');
    } else if (tripState == TripState.Bidded) {
      return HexColor('FF053432');
    } else if (tripState == TripState.Booked) {
      return HexColor('FF1C0339');
    } else if (tripState == TripState.Live) {
      return AppColors.Primary;
    } else {
      return HexColor('ff000000');
    }
  }

  onNavigateToDetailPage(item) async {
    print('Navigate to detail page');
  }

  _onLogoutButtonPressed() async {
    submitDialog(context, dismissible: false);
    AnalyticsManager().logEventProp(AnalyticsEvents.LOGOUT_BTN_CLK, {
      UserProperties.USER_EMAIL:
          await Prefs.getStringAsync(Prefs.PREF_USER_EMAIL)
    });
    var response =
        await Provider.of<TripListBloc>(context, listen: false).signOut();
    Navigator.pop(context);
    manipulateResponse(response);
  }

  manipulateResponse(ApiResponse response) async {
    switch (response.status) {
      case Status.LOADING:
        break;
      case Status.COMPLETED:
        AnalyticsManager().logEventProp(AnalyticsEvents.LOGOUT_SUCCESS, {
          UserProperties.USER_EMAIL:
              await Prefs.getStringAsync(Prefs.PREF_USER_EMAIL)
        });
        navigate(response);
        break;
      case Status.ERROR:
        showCustomSnackbar(response.message);
        AnalyticsManager().logEventProp(AnalyticsEvents.LOGOUT_FAILED, {
          UserProperties.USER_EMAIL:
              await Prefs.getStringAsync(Prefs.PREF_USER_EMAIL)
        });
        break;
    }
  }

  setUpTimer() {
    timer = Timer.periodic(const Duration(minutes: 5), (Timer t) {
      if (!_IsSearching && bloc!.tripState() == TripState.Live) {
        onRetryClick();
      }
    });
  }

  navigate(response) async {
    navigateNextScreen(context, LoginScreenContainer(),
        shouldFinishPreviousPages: true, pageName: AnalyticsEvents.ADMIN_LOGIN_SCREEN);
  }
}
