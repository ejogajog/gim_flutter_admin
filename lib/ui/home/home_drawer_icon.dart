import 'package:flutter/material.dart';
import 'package:app/ui/home/home_bloc.dart';
import 'package:app/utils/color_const.dart';
import 'package:provider/provider.dart';

Widget getDrawerIcon(ScaffoldState state){
  /*
  * this method is used to provide drawer in every pages
  * which share same bottom navigation menu and drawer

  * We will call appStatus api on opening drawer every time to check
  * whether customer is approved or not
   */
  return GestureDetector(
      onTap: () async{
        state?.openDrawer();
        var bloc=Provider.of<HomeBloc>(state.context,listen: false);
      },
      child: Padding(
        padding: const EdgeInsets.only(left:8.0),
        child: Icon(
          Icons.dehaze,
          color: ColorResource.colorMarineBlue,
          size: 24,
        ),
      ));
}