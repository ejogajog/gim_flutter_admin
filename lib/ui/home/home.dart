import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app/api/api_response.dart';
import 'package:app/ui/auth/login_screen.dart';
import 'package:app/ui/home/home_bloc.dart';
import 'package:app/ui/trip/trip_list_screen.dart';
import 'package:app/ui/widget/base_widget.dart';
import 'package:app/utils/navigation_util.dart';
import 'package:app/utils/ui_util.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(providers: [
      ChangeNotifierProvider<HomeBloc>(create: (_) => HomeBloc()),
    ], child: Home());
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends BasePageWidgetState<Home, HomeBloc> {

  @override
  List<Widget> getPageWidget() {
    return [
      Consumer<HomeBloc>(builder: (context, bloc, _) => TripListScreen())
    ];
  }

  manipulateResponse(ApiResponse response) {
    switch (response.status) {
      case Status.LOADING:
        break;
      case Status.COMPLETED:
        navigate(response);
        break;
      case Status.ERROR:
        showCustomSnackbar(response.message);
        break;
    }
  }

  navigate(response) async {
    navigateNextScreen(context, LoginScreenContainer(), shouldFinishPreviousPages: true);
  }
}
