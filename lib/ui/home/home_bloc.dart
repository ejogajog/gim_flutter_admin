import 'package:app/api/api_response.dart';
import 'package:app/bloc/base_bloc.dart';
import 'package:app/ui/auth/auth_repository.dart';
import 'package:app/ui/auth/logout_response.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/prefs.dart';

class HomeBloc extends BaseBloc {
  ApiResponse? _logoutResponse;

  ApiResponse? get logoutResponse => _logoutResponse;

  set logoutResponse(ApiResponse? value) {
    _logoutResponse = value;
  }

  signOut() async {
    print("Inside logout");
    isLoading = true;
    var response = await AuthRepository.signOut();
    logoutResponse = response;
    isLoading = false;
    switch (response.status) {
      case Status.LOADING:
        break;
      case Status.COMPLETED:
      print("Logout success");
        var logoutResponse = LogoutResponse.fromJson(response.data);
        if (logoutResponse.status??false) {
          Prefs.setBoolean(Prefs.IS_LOGGED_IN, false);
          Prefs.setBoolean(Prefs.IS_DISTRIBUTOR, false);
          Prefs.setBoolean(Prefs.PREF_IS_ENTERPRISE_USER, false);
          AnalyticsManager().logEventProp(AnalyticsEvents.LOGOUT_SUCCESS,{UserProperties.USER_EMAIL: await Prefs.getStringAsync(Prefs.PREF_USER_EMAIL)});
        }
        break;
      case Status.ERROR:
      print("Logout ERROR");
        break;
    }
    return response;
  }
}
