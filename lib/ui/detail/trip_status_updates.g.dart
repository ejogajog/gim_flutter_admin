// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'trip_status_updates.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TripStatusUpdates _$TripStatusUpdatesFromJson(Map<String, dynamic> json) =>
    TripStatusUpdates()
      ..data = (json['data'] as List<dynamic>?)
          ?.map((e) => TripStatus.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$TripStatusUpdatesToJson(TripStatusUpdates instance) =>
    <String, dynamic>{
      'data': instance.data,
    };

TripStatus _$TripStatusFromJson(Map<String, dynamic> json) => TripStatus()
  ..address = json['address'] as String?
  ..createdAt = (json['createdAt'] as num?)?.toInt()
  ..isFollowup = json['isFollowup'] as bool?
  ..district = json['district'] as String?
  ..modifiedAt = (json['modifiedAt'] as num?)?.toInt()
  ..otherStatusDetails = json['otherStatusDetails'] as String?
  ..quantity = (json['quantity'] as num?)?.toDouble()
  ..status = json['status'] as String?
  ..thana = json['thana'] as String?
  ..tripId = (json['tripId'] as num?)?.toInt()
  ..tripNumber = (json['tripNumber'] as num?)?.toInt()
  ..unit = json['unit'] as String?
  ..comment = json['comment'] as String?
  ..images =
      (json['images'] as List<dynamic>?)?.map((e) => e as String).toList()
  ..actionTime = (json['actionTime'] as num?)?.toInt();

Map<String, dynamic> _$TripStatusToJson(TripStatus instance) =>
    <String, dynamic>{
      'address': instance.address,
      'createdAt': instance.createdAt,
      'isFollowup': instance.isFollowup,
      'district': instance.district,
      'modifiedAt': instance.modifiedAt,
      'otherStatusDetails': instance.otherStatusDetails,
      'quantity': instance.quantity,
      'status': instance.status,
      'thana': instance.thana,
      'tripId': instance.tripId,
      'tripNumber': instance.tripNumber,
      'unit': instance.unit,
      'comment': instance.comment,
      'images': instance.images,
      'actionTime': instance.actionTime,
    };
