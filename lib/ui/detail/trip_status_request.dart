import 'package:json_annotation/json_annotation.dart';

part 'trip_status_request.g.dart';

@JsonSerializable()
class TripStatusUpdateRequest {
  String? address;
  int? districtId;
  bool? isFollowup;
  double? locationLat;
  double? locationLon;
  String? otherStatusDetails;
  int? quantity;
  int? thanaId;
  int? tripId;
  String? tripUpdateStatus;
  String? unit;
  String? actionTime;
  String? comment;
  TripStatusUpdateRequest(
      {this.address,
      this.districtId,
      this.isFollowup,
      this.locationLat,
      this.locationLon,
      this.otherStatusDetails,
      this.quantity,
      this.thanaId,
      this.tripId,
      this.tripUpdateStatus,
      this.unit,
      this.actionTime,
      this.comment});
  TripStatusUpdateRequest.internal();
  factory TripStatusUpdateRequest.fromJson(Map<String, dynamic> json) =>
      _$TripStatusUpdateRequestFromJson(json);
  Map<String, dynamic> toJson() => _$TripStatusUpdateRequestToJson(this);

  actionTimeStamp(String actionTimeStamp) {
    actionTime = actionTimeStamp;
  }
}
