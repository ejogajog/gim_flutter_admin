import 'package:json_annotation/json_annotation.dart';

part 'trip_status_updates.g.dart';

@JsonSerializable()
class TripStatusUpdates {
  List<TripStatus>? data;

  TripStatusUpdates();

  factory TripStatusUpdates.fromJson(Map<String, dynamic> json) =>
      _$TripStatusUpdatesFromJson(json);

  Map<String, dynamic> toJson() => _$TripStatusUpdatesToJson(this);
}

@JsonSerializable()
class TripStatus {
  String? address;
  int? createdAt;
  bool? isFollowup;
  String? district;
  int? modifiedAt;
  String? otherStatusDetails;
  double? quantity;
  String? status;
  String? thana;
  int? tripId;
  int? tripNumber;
  String? unit;
  String? comment;
  List<String>? images;
  int? actionTime;
  TripStatus();

  TripStatus.status(this.status);

  factory TripStatus.fromJson(Map<String, dynamic> json) =>
      _$TripStatusFromJson(json);

  Map<String, dynamic> toJson() => _$TripStatusToJson(this);

  @override
  int get hashCode => createdAt.hashCode ^ status.hashCode;

  bool operator ==(o) => status == (o as TripStatus).status;

  @override
  toString() {
    return "$status";
  }
}
