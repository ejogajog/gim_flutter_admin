import 'package:app/api/api_response.dart';
import 'package:app/ui/detail/model/driver_truck_update_request.dart';

class TripDetailRepository {
  static var helper = ApiBaseHelper();

  static Future<ApiResponse> updateTripStatus(request) async {
    ApiResponse response = await helper.post
      ("/ejogajogAdminAPI/api/v1/trips/statusupdate", request, isPut: true);
    return response;
  }

  static Future<ApiResponse> tripStatusUpdates(tripId) async {
    ApiResponse response = await helper.get("/ejogajogAdminAPI/api/v1/trips/$tripId/statusupdate");
    return response;
  }

static Future<ApiResponse> thanas() async {
    ApiResponse response = await helper.get("/ejogajog/api/v1/master/thanas");
    return response;
  }

  static Future<ApiResponse> districts() async {
    ApiResponse response = await helper.get("/ejogajog/api/v1/master/districts");
    return response;
  }

  static Future<ApiResponse> fetchBids(int? tripId) async {
    ApiResponse response = await helper.get("/ejogajogAdminAPI/api/v1/admin/trip/$tripId/bids?page=1&size=1000");
    return response;
  }

  static Future<ApiResponse> fetchDriversAndTrucksOfPartner(int? partnerId) async {
    ApiResponse response = await helper.get("/ejogajogAdminAPI/api/v1/admin/partner/driverandtruck?partnerId=$partnerId");
    return response;
  }
  
  static Future<ApiResponse> updateDriverAndTruck(int? tripId, DriverTruckUpdateRequest request) async {
    ApiResponse response = await helper.post("/ejogajogAdminAPI/api/v1/admin/trips/$tripId/updateTruckOrDriver", request.toJson(), isPut: true);
    return response;
  }
}
