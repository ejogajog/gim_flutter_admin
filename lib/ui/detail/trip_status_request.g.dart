// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'trip_status_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TripStatusUpdateRequest _$TripStatusUpdateRequestFromJson(
        Map<String, dynamic> json) =>
    TripStatusUpdateRequest(
      address: json['address'] as String?,
      districtId: (json['districtId'] as num?)?.toInt(),
      isFollowup: json['isFollowup'] as bool?,
      locationLat: (json['locationLat'] as num?)?.toDouble(),
      locationLon: (json['locationLon'] as num?)?.toDouble(),
      otherStatusDetails: json['otherStatusDetails'] as String?,
      quantity: (json['quantity'] as num?)?.toInt(),
      thanaId: (json['thanaId'] as num?)?.toInt(),
      tripId: (json['tripId'] as num?)?.toInt(),
      tripUpdateStatus: json['tripUpdateStatus'] as String?,
      unit: json['unit'] as String?,
      actionTime: json['actionTime'] as String?,
      comment: json['comment'] as String?,
    );

Map<String, dynamic> _$TripStatusUpdateRequestToJson(
        TripStatusUpdateRequest instance) =>
    <String, dynamic>{
      'address': instance.address,
      'districtId': instance.districtId,
      'isFollowup': instance.isFollowup,
      'locationLat': instance.locationLat,
      'locationLon': instance.locationLon,
      'otherStatusDetails': instance.otherStatusDetails,
      'quantity': instance.quantity,
      'thanaId': instance.thanaId,
      'tripId': instance.tripId,
      'tripUpdateStatus': instance.tripUpdateStatus,
      'unit': instance.unit,
      'actionTime': instance.actionTime,
      'comment': instance.comment,
    };
