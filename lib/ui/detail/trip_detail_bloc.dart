import 'dart:async';

import 'package:app/api/api_response.dart';
import 'package:app/bloc/base_bloc.dart';
import 'package:app/model/base_response.dart';
import 'package:app/model/master/districts.dart';
import 'package:app/model/master/thanas.dart';
import 'package:app/ui/detail/model/bid.dart';
import 'package:app/ui/detail/model/content_list.dart';
import 'package:app/ui/detail/model/driver_truck_data.dart';
import 'package:app/ui/detail/model/driver_truck_update_request.dart';
import 'package:app/ui/detail/trip_detail_repo.dart';
import 'package:app/ui/detail/trip_status_updates.dart';
import 'package:app/ui/trip/trip_item.dart';
import 'package:app/ui/trip/trip_list_bloc.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/date_time_util.dart';
import 'package:app/utils/string_util.dart';

class TripDetailBloc extends BaseBloc {
  TripListBloc? tripListBloc;
  int? tripId;
  String? address;
  int? tripRunningHours;
  Map<String, List<Thanas>>? map;
  Districts? selectedDistrict;
  Thanas? selectedThana;
  List<Districts>? districts;
  List<Thanas>? thanas;
  List<TripStatus>? tripStatusUpdates;
  List<String>? loadingImages;
  List<String>? unLoadingImages;
  List<Bid>? bids;
  bool? isBidLoading = true;
  BookedTripStatus? _bookedTripStatus = BookedTripStatus.None;
  String? actionTime;
  var _waitingForLoadingRemarks = "";
  set waitingLoadingRemarks(String waitingForLoadingRemarks) {
    _waitingForLoadingRemarks = waitingForLoadingRemarks;
  }

  get waitingForLoadingRemarks => _waitingForLoadingRemarks;

  String _waitingForUnLoadingRemarks = "";
  set waitingForUnLoadingRemarks(String waitingForUnLoadingRemarks) {
    _waitingForUnLoadingRemarks = waitingForUnLoadingRemarks;
  }

  get waitingForUnLoadingRemark => _waitingForUnLoadingRemarks;

  TripDetailBloc(this.tripId, this.tripListBloc, {this.tripRunningHours}) {
    thanas = tripListBloc?.thanas;
    districts = tripListBloc?.districts;
  }

  get bookedTripUpdate => _bookedTripStatus;

  set bookedStatus(BookedTripStatus bookedTripStatus) {
    _bookedTripStatus = bookedTripStatus;
  }

  updateTripStatus(tripStatusUpdateRequest) async {
    isLoading = true;
    if (!isNullOrEmpty(actionTime))
      tripStatusUpdateRequest.actionTimeStamp(getFormattedDate(actionTime,
          originalFormat: AppDateTimeFormat, presentationFormat: ISOFormat));
    var response =
        await TripDetailRepository.updateTripStatus(tripStatusUpdateRequest);
    isLoading = false;
    switch (response.status) {
      case Status.LOADING:
        break;
      case Status.COMPLETED:
        getTripStatus(tripStatusUpdateRequest.tripId);
        break;
      case Status.ERROR:
        break;
    }
    return response;
  }

  getTripStatus(tripId) async {
    var response = await TripDetailRepository.tripStatusUpdates(tripId);
    tripStatusUpdates =
        convertDynamicListToStaticList<TripStatus>(response.data);
    setLoadingUnloadingImg();
    notifyListeners();
  }

  getDistricts() async {
    if (!isNullOrEmptyList(districts)) {
      await _getThanas();
      return;
    }
    isLoading = true;
    var response = await TripDetailRepository.districts();
    if (response.status == Status.COMPLETED) {
      districts = convertDynamicListToStaticList<Districts>(response.data);
      tripListBloc?.districts = districts!;
    }
    await _getThanas();
    isLoading = false;
  }

  _getThanas() async {
    if (!isNullOrEmptyList(thanas)) {
      _buildThanas();
      return;
    }
    var response = await TripDetailRepository.thanas();
    if (response.status == Status.COMPLETED) {
      print("Districts API success");
      thanas = convertDynamicListToStaticList<Thanas>(response.data);
      tripListBloc?.thanas = thanas!;
    }
    _buildThanas();
  }

  _buildThanas() {
    map = Map();
    thanas?.forEach((element) {
      if (map!.containsKey(element.districtId.toString())) {
        map![element.districtId.toString()]?.add(element);
      } else {
        List<Thanas> newThanas = List.empty(growable: true);
        newThanas.add(element);
        map![element.districtId.toString()] = newThanas;
      }
    });
  }

  setDistrict(selDist) {
    selectedDistrict = selDist;
    thanas = map![selDist.id.toString()];
    selectedThana = null;
  }

  setThana(selThana) {
    selectedThana = selThana;
  }

  fetchBids() async {
    isBidLoading = true;
    notifyListeners();

    var response = await TripDetailRepository.fetchBids(tripId);
    if (response.status == Status.COMPLETED) {
      bids = ContentList.fromJson(response.data, (json) => Bid.fromJson(json as Map<String, dynamic>?))
          .contentList;
      bids = bids?.where((element) => element.bidStatus == 'PENDING').toList();
    }

    isBidLoading = false;
    notifyListeners();
  }

  Future<DriverTruckData>? getDriversAndTrucks(int? partnerId) async {
    isLoading = true;
    var response =
        await TripDetailRepository.fetchDriversAndTrucksOfPartner(partnerId);
    isLoading = false;
    return DriverTruckData.fromJson(response.data);
  }

  updateDriverAndTruck(int? partnerId, int? driverId, int? truckId) async {
    isLoading = true;
    DriverTruckUpdateRequest updateRequest = DriverTruckUpdateRequest(
      partnerId: partnerId,
      truckId: truckId,
      driverId: driverId,
    );
    var response =
        await TripDetailRepository.updateDriverAndTruck(tripId, updateRequest);
    return response;
  }

  clearLocationSelection() {
    selectedDistrict = null;
    selectedThana = null;
    address = '';
  }

  tripStatusRowData(index) {
    TripStatus? tripStatus = tripStatusUpdates![index];
    Followup? followup = tripStatus.isFollowup ?? false
        ? Followup.values
            .firstWhere((element) => element.value == tripStatus.status)
        : null;

    return '${convertTimestampToDateTime(dateFormat: AppDateTimeFormat, timestamp: tripStatus.createdAt)} -> '
        '${tripStatus.isFollowup ?? false ? "FOLLOWUP UPDATE at ${convertTimestampToDateTime(dateFormat: AppDateTimeViewFormat, timestamp: tripStatus.actionTime)}: ${followup?.text}" : '${tripStatus.status?.replaceAll('_', ' ')}' + '${tripStatus.comment == null ? '' : tripStatus.comment == "" ? '' : '(${tripStatus.comment})'}' + ' at ${convertTimestampToDateTime(dateFormat: AppDateTimeViewFormat, timestamp: tripStatus.actionTime)}'}'
        '${!isNullOrEmpty(tripStatus.otherStatusDetails) ? '(${tripStatus.otherStatusDetails})' : ''}'
        '${isNullOrEmpty(tripStatus.district) ? '' : ' District: ' + tripStatus.district!}'
        '${isNullOrEmpty(tripStatus.thana) ? '' : (isNullOrEmpty(tripStatus.district) ? ' Thana: ' : ', Thana: ') + tripStatus.thana!}'
        '${tripStatus.address == null ? '' : ' ' + tripStatus.address!}';
  }

  tripStatus(tripUpdateStatus) {
    switch (tripUpdateStatus) {
      case TripUpdateStatus.ETAUpdate:
        return 'ETA_UPDATE';
      case TripUpdateStatus.OnTheWayToLoadPoint:
        return 'ON_THE_WAY_TO_LOAD_POINT';
      case TripUpdateStatus.TruckReachedAtLoadingPoint:
        return 'TRUCK_REACHED_AT_LOADING_POINT';
      case TripUpdateStatus.LoadingOnProcess:
        return 'LOADING_ON_PROCESS';
      case TripUpdateStatus.LoadingDone:
        return 'LOADING_DONE';
      case TripUpdateStatus.TruckReachedAtUnloadingPoint:
        return 'TRUCK_REACHED_AT_UNLOADING_POINT';
      case TripUpdateStatus.StartedForDestination:
        return 'STARTED_FOR_DESTINATION';
      case TripUpdateStatus.LocationUpdate:
        return 'LOCATION_UPDATE';
      case TripUpdateStatus.DriverHelperNotReachable:
        return 'NOT_AVAILABLE_CURRENTLY';
      case TripUpdateStatus.UnloadingStarted:
        return 'UNLOADING_STARTED';
      case TripUpdateStatus.UnloadingDoneTripInTransit:
        return 'UNLOADING_DONE_AND_IN_TRANSIT';
      case TripUpdateStatus.UnloadingCompleted:
        return 'UNLOADING_COMPLETED';
      case TripUpdateStatus.WaitingForUnloading:
        return 'WAITING_FOR_UNLOADING';
      case TripUpdateStatus.WaitingForLoading:
        return 'WAITING_FOR_LOADING';
      case TripUpdateStatus.None:
        return 'NONE';
    }
  }

  bookedTripStatus() {
    switch (bookedTripUpdate) {
      case BookedTripStatus.OnTheWayToLoadPoint:
        return bookedStatusConst[0];
      case BookedTripStatus.TruckReachedAtLoadingPoint:
        return bookedStatusConst[1];
      case BookedTripStatus.WaitingForLoading:
        return bookedStatusConst[2];
      case BookedTripStatus.LoadingOnProcess:
        return bookedStatusConst[3];
      case BookedTripStatus.LoadingDone:
        return bookedStatusConst[4];
      case BookedTripStatus.StartedForDestination:
        return bookedStatusConst[5];
      case BookedTripStatus.None:
        return 'NONE';
    }
  }

  setLoadingUnloadingImg() {
    int index;
    index = tripStatusUpdates!.indexOf(TripStatus.status(bookedStatusConst[2]));
    if (index != -1) {
      loadingImages = tripStatusUpdates![index].images;
    }
    index = tripStatusUpdates!.indexOf(TripStatus.status(liveStatusConst[0]));
    if (index != -1) {
      unLoadingImages = tripStatusUpdates![index].images;
    }
  }

  maxStatusUpdateIndex() {
    int index = bookedStatusConst.length - 1;
    for (; index >= 0; index--) {
      if (tripStatusUpdates!.indexOf(TripStatus.status(bookedStatusConst[index])) != -1) break;
    }
    return index;
  }

  clipBoardData(TripItem tripItem, TripStatus tripStatus) {
    String status = tripStatus.status!.replaceAll('_', " ");
    return '${tripItem.enterpriseName} TRIP #${tripItem.tripNumber} (${tripItem.tripDropOffAddress})\n'
        'Follow up time: ${convertTimestampToDateTime(dateFormat: AppDateTimeFormat, timestamp: tripStatus.createdAt)}\n'
        'Driver: ${tripItem.driverName} ${isNullOrEmpty(tripItem.driverMobileNo) ? '' : '(${tripItem.driverMobileNo})'}\n'
        'Status: $status ${_remarks(tripStatus)} ${_address(tripStatus)} at ${!isNullOrEmpty(actionTime) ? getDateFormatted(actionTime, originalFormat: AppDateTimeFormat, presentationFormat: AppDateTimeViewFormat) : convertTimestampToDateTime(dateFormat: AppDateTimeViewFormat, timestamp: tripStatus.actionTime ?? tripStatus.createdAt)} ${(((tripStatus.isFollowup ?? false) && !isNullOrEmpty(tripStatus.otherStatusDetails)) ? '(${tripStatus.otherStatusDetails})' : '')}\n'
        'Truck: ${tripItem.truckRegistrationNumber}\n'
        '${_isLiveTrip(tripItem) ? 'Run time : $tripRunningHours Hours' : ''}';
  }

  _address(TripStatus tripStatus) {
    return isNullOrEmpty(tripStatus.address)
        ? isNullOrEmpty(tripStatus.district)
            ? ''
            : 'District: ${tripStatus.district} ${isNullOrEmpty(tripStatus.district) ? ' Thana: ${tripStatus.thana}' : ', Thana: ${tripStatus.thana}'}'
        : tripStatus.address;
  }

  _remarks(TripStatus tripStatus) {
    return tripStatus.comment == null
        ? ''
        : (tripStatus.comment == '' ? '' : '(${tripStatus.comment})');
  }

  bool _isLiveTrip(tripItem) {
    return tripItem.tripStatus == TripState.Live.statusValue();
  }
}
