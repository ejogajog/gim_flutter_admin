import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:app/api/api_response.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/styles.dart';
import 'package:app/ui/detail/bid_list_widget.dart';
import 'package:app/ui/detail/location_widget.dart';
import 'package:app/ui/detail/model/driver.dart';
import 'package:app/ui/detail/model/truck.dart';
import 'package:app/ui/detail/trip_detail_bloc.dart';
import 'package:app/ui/detail/trip_status_request.dart';
import 'package:app/ui/detail/trip_status_updates.dart';
import 'package:app/ui/trip/trip_item.dart';
import 'package:app/ui/trip/trip_list_bloc.dart';
import 'package:app/ui/widget/app_bar.dart';
import 'package:app/ui/widget/app_button.dart';
import 'package:app/ui/widget/base_widget.dart';
import 'package:app/ui/widget/booked_status_button.dart';
import 'package:app/ui/widget/editfield.dart';
import 'package:app/ui/widget/loading_unloading_image.dart';
import 'package:app/ui/widget/remark_dailog_widget.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/analytics_util.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/date_time_util.dart';
import 'package:app/utils/string_util.dart';
import 'package:app/utils/ui_util.dart';
import 'package:app/utils/validation_util.dart';
import 'package:app/utils/widget_util.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class TripDetailScreen extends StatelessWidget {
  final String? title;
  final TripItem? tripItem;
  final int? tripRunningHours;
  final TripListBloc? _tripListBloc;

  TripDetailScreen(this.title, this.tripItem, this._tripListBloc,
      {this.tripRunningHours});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(providers: [
      ChangeNotifierProvider<TripDetailBloc>(
          create: (_) => TripDetailBloc(tripItem?.tripId, _tripListBloc,
              tripRunningHours: this.tripRunningHours)),
    ], child: TripDetailsPage(title, tripItem));
  }
}

class TripDetailsPage extends StatefulWidget {
  final String? title;
  final TripItem? tripItem;

  TripDetailsPage(this.title, this.tripItem);

  @override
  _TripDetailsScreenState createState() => _TripDetailsScreenState();
}

class _TripDetailsScreenState
    extends BasePageWidgetState<TripDetailsPage, TripDetailBloc> {
  bool locationUpdateFlag = false;
  TripUpdateStatus tripUpdateStatus = TripUpdateStatus.None;
  String? codeDialog;
  String? valueText;

  @override
  PreferredSizeWidget getAppbar() {
    return AppBarWidget(
      title: widget.title,
      shouldShowBackButton: true,
      shouldShowDivider: false,
    );
  }

  @override
  onBuildCompleted() {
    if (mounted) {
      bloc!.getTripStatus(widget.tripItem?.tripId);
      if (_isBiddedTrip()) {
        bloc!.fetchBids();
      }
      bloc!.getDistricts();
    }
  }

  @override
  List<Widget> getPageWidget() {
    return [
      Column(
        children: [
          Expanded(
              child: SingleChildScrollView(
            child: Container(
              color: ColorResource.light_grey,
              padding: EdgeInsets.all(16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            translate(context, 'lbl_trip_no').toUpperCase(),
                            style: Styles.detailLabel,
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            widget.tripItem?.tripNumber?.toString() ?? '',
                            style: Styles.detailValue,
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      SizedBox(height: 16),
                      Row(
                        children: [
                          Text(
                            translate(context, 'lbl_pickup').toUpperCase(),
                            style: Styles.detailLabel,
                            textAlign: TextAlign.center,
                          ),
                          Expanded(
                            child: Text(
                              widget.tripItem?.tripPickupAddress ?? '',
                              style: Styles.detailValue,
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 16),
                      Row(
                        children: [
                          Text(
                            translate(context, 'lbl_drop_off').toUpperCase(),
                            style: Styles.detailLabel,
                            textAlign: TextAlign.center,
                          ),
                          Expanded(
                            child: Text(
                              widget.tripItem?.tripDropOffAddress ?? '',
                              style: Styles.detailValue,
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 16),
                      Row(
                        children: [
                          Text(
                            translate(context, 'lbl_partner').toUpperCase(),
                            style: Styles.detailLabel,
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            widget.tripItem?.partnerName ?? '',
                            style: Styles.detailValue,
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      SizedBox(height: 16),
                      Row(
                        children: [
                          Text(
                            translate(context, 'lbl_driver').toUpperCase(),
                            style: Styles.detailLabel,
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            widget.tripItem?.driverName ?? '',
                            style: Styles.detailValue,
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      SizedBox(height: 16),
                      if (_isBiddedTrip()) ...[
                        Text(
                          translate(context, 'lbl_bids').toUpperCase(),
                          style: TextStyle(
                              color: ColorResource.colorBlack,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 16),
                        Consumer<TripDetailBloc>(
                          builder: (context, bloc, _) {
                            return BidListWidget(
                              bloc.bids,
                              bloc.isBidLoading,
                              (partnerId, truckRegNo, driverId) {
                                _showDriverAndTruckUpdateDialog(
                                    context, partnerId, truckRegNo, driverId);
                              },
                            );
                          },
                        ),
                      ],
                      SizedBox(height: 16),
                      Text(
                        translate(context, 'lbl_trip_update').toUpperCase(),
                        style: TextStyle(
                            color: ColorResource.colorBlack,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Consumer<TripDetailBloc>(
                        builder: (context, bloc, _) {
                          return Wrap(
                            spacing: 10.0,
                            runSpacing: 20.0,
                            children: [
                              if (_isBiddedTrip() && _isAnyBidUpdated()) ...[
                                StatusButton(
                                  prefixIcon: SizedBox(
                                    width: 20.0,
                                  ),
                                  text: translate(context, 'lbl_eta'),
                                  onPressed: () {
                                    tripUpdateStatus =
                                        TripUpdateStatus.ETAUpdate;
                                    _showTimePickerDialog(
                                      context,
                                      (hour, minute) =>
                                          onETAUpdate(hour, minute),
                                    );
                                  },
                                ),
                              ],
                              if (_isBookedTrip()) ...[
                                BookedStatusButton(bloc)
                              ],
                              if (_isLiveTrip()) ...[
                                StatusButton(
                                  prefixIcon: SizedBox(
                                    width: 20.0,
                                  ),
                                  text: translate(context, 'lbl_loc_update'),
                                  onPressed: () {
                                    tripUpdateStatus =
                                        TripUpdateStatus.LocationUpdate;
                                    _displayLocationInputDialog(true);
                                  },
                                ),
                                StatusButton(
                                  text: translate(context, 'lbl_tr_up'),
                                  prefixIcon: tripUpdateStatus ==
                                          TripUpdateStatus
                                              .TruckReachedAtUnloadingPoint
                                      ? Icon(
                                          Icons.check,
                                          color: Colors.green,
                                          size: 24,
                                        )
                                      : Icon(
                                          Icons.cancel,
                                          color: Colors.grey,
                                          size: 24,
                                        ),
                                  onPressed: () {
                                    setState(() {
                                      tripUpdateStatus = TripUpdateStatus
                                          .TruckReachedAtUnloadingPoint;
                                    });
                                    selectDateTime(
                                        context, _onSelectedDateTime);
                                  },
                                ),
                                StatusButton(
                                  text: translate(context, 'lbl_wait_unload'),
                                  prefixIcon: tripUpdateStatus ==
                                          TripUpdateStatus.WaitingForUnloading
                                      ? Icon(
                                          Icons.check,
                                          color: Colors.green,
                                          size: 24,
                                        )
                                      : Icon(
                                          Icons.cancel,
                                          color: Colors.grey,
                                          size: 24,
                                        ),
                                  onPressed: () {
                                    setState(() {
                                      tripUpdateStatus =
                                          TripUpdateStatus.WaitingForUnloading;
                                    });
                                    _showRemarkDialog(context);
                                    // _showWFUnloadRemDialog(context);
                                  },
                                ),
                                StatusButton(
                                  text: translate(context, 'lbl_unload'),
                                  prefixIcon: tripUpdateStatus ==
                                          TripUpdateStatus.UnloadingStarted
                                      ? Icon(
                                          Icons.check,
                                          color: Colors.green,
                                          size: 24,
                                        )
                                      : Icon(
                                          Icons.cancel,
                                          color: Colors.grey,
                                          size: 24,
                                        ),
                                  onPressed: () {
                                    setState(() {
                                      tripUpdateStatus =
                                          TripUpdateStatus.UnloadingStarted;
                                    });
                                    selectDateTime(
                                        context, _onSelectedDateTime);
                                    AnalyticsManager().logEventProp(
                                        AnalyticsEvents.STATUS_UPDATE_BTN_CLK, {
                                      AnalyticProperties.TRIP_STATUS:
                                          translate(context, 'lbl_unload')
                                    });
                                  },
                                ),
                                StatusButton(
                                  text: translate(context, 'lbl_ud_tt'),
                                  prefixIcon: SizedBox(
                                    width: 20.0,
                                  ),
                                  onPressed: () {
                                    tripUpdateStatus = TripUpdateStatus
                                        .UnloadingDoneTripInTransit;
                                    _displayLocationInputDialog(false);
                                    AnalyticsManager().logEventProp(
                                        AnalyticsEvents.STATUS_UPDATE_BTN_CLK, {
                                      AnalyticProperties.TRIP_STATUS: translate(
                                          context,
                                          translate(context, 'lbl_ud_tt'))
                                    });
                                  },
                                ),
                                StatusButton(
                                  text: translate(context, 'lbl_uc'),
                                  prefixIcon: tripUpdateStatus ==
                                          TripUpdateStatus.UnloadingCompleted
                                      ? Icon(
                                          Icons.check,
                                          color: Colors.green,
                                          size: 24.0,
                                        )
                                      : Icon(
                                          Icons.cancel,
                                          color: Colors.grey,
                                          size: 24,
                                        ),
                                  onPressed: () {
                                    setState(() {
                                      tripUpdateStatus =
                                          TripUpdateStatus.UnloadingCompleted;
                                    });
                                    selectDateTime(
                                        context, _onSelectedDateTime);
                                    AnalyticsManager().logEventProp(
                                        AnalyticsEvents.STATUS_UPDATE_BTN_CLK, {
                                      AnalyticProperties.TRIP_STATUS:
                                          translate(context, 'lbl_uc')
                                    });
                                  },
                                ),
                              ],
                              if (_isBookedTrip() || _isLiveTrip()) ...[
                                StatusButton(
                                  text: translate(context, 'lbl_fwu'),
                                  prefixIcon: SizedBox(
                                    width: 20.0,
                                  ),
                                  onPressed: () {
                                    _displayFollowupDialog(
                                        _isBookedTrip()
                                            ? Followup.DriverTakingRest
                                            : Followup
                                                .TruckReachedAtUnloadingPoint,
                                        (followup, otherStatusText) {
                                      tripUpdateStatus =
                                          TripUpdateStatus.FollowupUpdate;
                                      onFollowupUpdate(
                                          followup, otherStatusText);
                                    });
                                    AnalyticsManager().logEventProp(
                                        AnalyticsEvents.STATUS_UPDATE_BTN_CLK, {
                                      AnalyticProperties.TRIP_STATUS: translate(
                                          context,
                                          translate(context, 'lbl_fwu'))
                                    });
                                  },
                                ),
                              ],
                            ],
                          );
                        },
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Consumer<TripDetailBloc>(builder: (context, bloc, _) {
                        return LoadingUnloadingImages(bloc);
                      }),
                      SizedBox(
                        height: 10,
                      ),
                      Consumer<TripDetailBloc>(
                        builder: (context, bloc, _) {
                          return isNullOrEmptyList(bloc.tripStatusUpdates)
                              ? Container()
                              : ListView.separated(
                                  separatorBuilder: (context, index) => Divider(
                                        color: Colors.white,
                                        height: 0,
                                        thickness: 0.0,
                                      ),
                                  scrollDirection: Axis.vertical,
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: bloc.tripStatusUpdates!.length,
                                  itemBuilder: (context, index) {
                                    var text = bloc.tripStatusRowData(index);
                                    return Row(
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.all(4.0),
                                            child: RichText(
                                              text: TextSpan(
                                                children: <InlineSpan>[
                                                  TextSpan(
                                                      text: text,
                                                      style: Styles
                                                          .txtStyleBlackBangla),
                                                  WidgetSpan(
                                                    alignment:
                                                        PlaceholderAlignment
                                                            .middle,
                                                    child: IconButton(
                                                      icon: Icon(Icons.copy,
                                                          color: Colors.grey),
                                                      iconSize: 24.0,
                                                      onPressed: () => copyTo(
                                                          widget.tripItem,
                                                          bloc.tripStatusUpdates![
                                                              index]),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    );
                                  });
                        },
                      )
                    ],
                  ),
                ],
              ),
            ),
          )),
          FilledColorButton(
            horizonatalMargin: 20.0,
            buttonText: translate(context, 'lbl_unc'),
            onPressed: () {
              _isBookedTrip() ? updateBookedTripStatus() : onStatusUpdate();
              AnalyticsManager()
                  .logEventProp(AnalyticsEvents.STATUS_UPDATE_BTN_CLK, {
                AnalyticProperties.TRIP_STATUS:
                    translate(context, translate(context, 'lbl_unc'))
              });
            },
          ),
        ],
      )
    ];
  }

  _onSelectedDateTime(dateTime) {
    bloc!.actionTime = dateTime;
  }

  _showRemarkDialog(BuildContext context) async {
    dynamic remark = await showDialog(
      context: context,
      builder: (BuildContext context) => CommentDialog("Remarks"),
    );

    if (remark != null) {
      setState(() {
        bloc!.waitingForUnLoadingRemarks = remark;
      });
    }
  }

  bool _isBiddedTrip() {
    return widget.tripItem?.tripStatus == TripState.Bidded.statusValue();
  }

  bool _isBookedTrip() {
    return widget.tripItem?.tripStatus == TripState.Booked.statusValue();
  }

  bool _isLiveTrip() {
    return widget.tripItem?.tripStatus == TripState.Live.statusValue();
  }

  bool _isAnyBidUpdated() {
    if (bloc!.bids == null || bloc!.bids!.isEmpty) {
      return false;
    }
    return bloc!.bids
        !.any((e) => e.driverId != null && e.truckRegistrationNumber != null);
  }

  onStatusUpdate() async {
    var status = bloc!.tripStatus(tripUpdateStatus);
    print('Booked trip update status is : $status');
    if (tripUpdateStatus == TripUpdateStatus.None ||
        tripUpdateStatus == TripUpdateStatus.LocationUpdate ||
        tripUpdateStatus == TripUpdateStatus.UnloadingDoneTripInTransit ||
        tripUpdateStatus == TripUpdateStatus.FollowupUpdate) {
      showToast(translate(context, 'msg_pso'));
      return;
    } else if (tripUpdateStatus == TripUpdateStatus.WaitingForUnloading) {
      onWaitingForUnloadingStatusUpdate(bloc!.waitingForUnLoadingRemark);
      return;
    }
    TripStatus tripStatus = TripStatus();
    tripStatus.status = status;
    tripStatus.createdAt = DateTime.now().millisecondsSinceEpoch;
    copyTo(widget.tripItem, tripStatus);

    TripStatusUpdateRequest request = TripStatusUpdateRequest(
        tripId: widget.tripItem?.tripId, tripUpdateStatus: status);

    var response = await bloc!.updateTripStatus(request);
    manipulateResponse(response, status);
  }

  updateBookedTripStatus() async {
    var status = bloc!.bookedTripStatus();
    if (bloc!.bookedTripUpdate == BookedTripStatus.None) {
      showToast(translate(context, 'msg_pst_uop'));
      return;
    } else if (bloc!.bookedTripUpdate == BookedTripStatus.WaitingForLoading) {
      onWaitingForLoadingStatusUpdate(bloc!.waitingForLoadingRemarks);
      return;
    }

    TripStatus tripStatus = TripStatus();
    tripStatus.status = status;
    tripStatus.createdAt = DateTime.now().millisecondsSinceEpoch;
    copyTo(widget.tripItem, tripStatus);

    TripStatusUpdateRequest request = TripStatusUpdateRequest(
        tripId: widget.tripItem?.tripId, tripUpdateStatus: status);

    var response = await bloc!.updateTripStatus(request);
    manipulateResponse(response, status);
  }

  onLocationUpdate() async {
    var status = bloc!.tripStatus(tripUpdateStatus);
    var districtId = bloc!.selectedDistrict?.id;
    var thanaId = bloc!.selectedThana?.id;
    print("ThanaId: $thanaId");
    print("DistrictId: $districtId");

    TripStatusUpdateRequest request = TripStatusUpdateRequest(
        tripId: bloc!.tripId,
        tripUpdateStatus: status,
        districtId: isNullOrEmpty(bloc!.address) ? districtId : 0,
        thanaId: isNullOrEmpty(bloc!.address) ? thanaId : 0,
        address: bloc!.address);
    var response = await bloc!.updateTripStatus(request);

    // status  = status + ' ' +bloc.selectedDistrict?.text + ' ' + bloc.address;
    status = '$status '
        ' ${(!isNullOrEmptyInt(request.districtId)) ? !isNullOrEmptyInt(request.thanaId) ? 'District : ${bloc?.selectedDistrict?.text}, Thana: ${bloc?.selectedThana?.name}' : 'District : ${bloc?.selectedDistrict?.text}' : isNullOrEmptyInt(request.thanaId) ? request.address : 'Thana: ${bloc?.selectedThana?.name}'}';

    TripStatus tripStatus = TripStatus();
    tripStatus.status = status;
    tripStatus.createdAt = DateTime.now().millisecondsSinceEpoch;
    copyTo(widget.tripItem, tripStatus);

    manipulateResponse(response, status);
  }

  onFollowupUpdate(Followup followup, String? otherStatusText) async {
    TripStatusUpdateRequest request = TripStatusUpdateRequest(
        tripId: bloc!.tripId,
        tripUpdateStatus: followup.value,
        isFollowup: true,
        otherStatusDetails: otherStatusText);
    var response = await bloc!.updateTripStatus(request);
    TripStatus tripStatus = TripStatus();
    tripStatus.status = followup.value;
    tripStatus.isFollowup = true;
    tripStatus.otherStatusDetails = otherStatusText;
    tripStatus.createdAt = DateTime.now().millisecondsSinceEpoch;
    copyTo(widget.tripItem, tripStatus);
    manipulateResponse(response, followup.value);
  }

  onETAUpdate(int hour, int minute) async {
    var status = bloc!.tripStatus(tripUpdateStatus);
    TripStatusUpdateRequest request = TripStatusUpdateRequest(
        tripId: bloc!.tripId,
        tripUpdateStatus: status,
        otherStatusDetails: '$hour hours $minute mins');
    var response = await bloc!.updateTripStatus(request);

    TripStatus tripStatus = TripStatus();
    tripStatus.status = status;
    tripStatus.createdAt = DateTime.now().millisecondsSinceEpoch;
    copyTo(widget.tripItem, tripStatus);
    manipulateResponse(response, status);
  }

  //todo: on waiting for unloading status update code start
  onWaitingForUnloadingStatusUpdate(String remark) async {
    var status = bloc!.tripStatus(tripUpdateStatus);
    print(
        "trip ID ${bloc!.tripId}  trip update status ${status} comment ${remark}");
    TripStatusUpdateRequest request = TripStatusUpdateRequest(
        tripId: bloc!.tripId, tripUpdateStatus: status, comment: '$remark');
    var response = await bloc!.updateTripStatus(request);
    TripStatus tripStatus = TripStatus();
    tripStatus.status = status;
    tripStatus.comment = remark;
    tripStatus.createdAt = DateTime.now().millisecondsSinceEpoch;

    copyTo(widget.tripItem, tripStatus);
    manipulateResponse(response, status);
    bloc!.waitingForUnLoadingRemarks = "";
  }

  //todo code end
  onWaitingForLoadingStatusUpdate(String remark) async {
    var status = bloc!.bookedTripStatus();
    print(
        "trip ID ${bloc!.tripId}  trip update status ${status} comment ${remark}");
    TripStatusUpdateRequest request = TripStatusUpdateRequest(
        tripId: bloc!.tripId, tripUpdateStatus: status, comment: '$remark');
    var response = await bloc!.updateTripStatus(request);
    TripStatus tripStatus = TripStatus();
    tripStatus.status = status;
    tripStatus.comment = remark;
    tripStatus.createdAt = DateTime.now().millisecondsSinceEpoch;

    copyTo(widget.tripItem, tripStatus);
    manipulateResponse(response, status);
    bloc!.waitingLoadingRemarks = "";
  }

  manipulateResponse(ApiResponse response, status) {
    bloc!.actionTime = null;
    switch (response.status) {
      case Status.LOADING:
        break;
      case Status.COMPLETED:
        showToast(translate(context, 'msg_tsu'));
        AnalyticsManager().logEventProp(AnalyticsEvents.TRIP_STATUS_CLICK,
            {AnalyticProperties.TRIP_STATUS: status});
        bloc!.bookedStatus = BookedTripStatus.None;
        break;
      case Status.ERROR:
        showCustomSnackbar(response.message);
        break;
    }
  }

  copyTo(trip, TripStatus tripStatus) {
    print("copy to called");
    Clipboard.setData(
        new ClipboardData(text: bloc!.clipBoardData(trip, tripStatus)));
    showToast(translate(context, 'msg_cop'));
  }

  Future<void> _displayLocationInputDialog(bool required) async {
    bloc!.clearLocationSelection();

    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: SingleChildScrollView(child: LocationWidget(bloc!)),
            actions: <Widget>[
              TextButton(
                style: ButtonStyle(
                  padding: MaterialStateProperty.all(
                      EdgeInsets.symmetric(horizontal: 16)),
                  backgroundColor: MaterialStateProperty.all(Colors.red),
                ),
                child: Text(
                  translate(context, 'cancel'),
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  bloc!.clearLocationSelection();
                  setState(() {
                    Navigator.pop(context);
                  });
                },
              ),
              TextButton(
                style: ButtonStyle(
                  padding: MaterialStateProperty.all(
                      EdgeInsets.symmetric(horizontal: 16)),
                  backgroundColor: MaterialStateProperty.all(Colors.green),
                ),
                child: Text(
                  translate(context, 'lbl_unc'),
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  setState(() {
                    if (required &&
                        isNullOrEmpty(bloc!.address) &&
                        isNullOrEmptyInt(bloc!.selectedDistrict?.id) &&
                        isNullOrEmptyInt(bloc!.selectedThana?.id)) {
                      showCustomSnackbar(translate(context, 'msg_ppv_inp'));
                      return;
                    }
                    Navigator.pop(context);
                    onLocationUpdate();
                  });
                  AnalyticsManager()
                      .logEventProp(AnalyticsEvents.STATUS_UPDATE_BTN_CLK, {
                    AnalyticProperties.TRIP_STATUS:
                        translate(context, translate(context, 'lbl_unc'))
                  });
                },
              ),
            ],
          );
        });
  }

  Future<void> _displayFollowupDialog(
      Followup selected, Function callback) async {
    return showDialog(
        context: context,
        builder: (context) {
          Followup? selectedFollowup = selected;
          String? otherStatusText;
          List<DropdownMenuItem<Followup>> optionsItems = Followup.values.map((followUp) {
            return DropdownMenuItem(
              value: followUp,
              child: Text(followUp.text),
            );
          }).toList();
          return StatefulBuilder(
            builder: (context, setState) {
              final TextEditingController _tFDateTimeController =
                  TextEditingController(
                      text:
                          DateFormat(AppDateTimeFormat).format(DateTime.now()));
              _onSelectedDateTime(dateTime) {
                bloc!.actionTime = dateTime;
                _tFDateTimeController.text = dateTime;
              }

              return AlertDialog(
                content: Container(
                  width: MediaQuery.of(context).size.width * .3,
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        TextEditText(
                          labelText: 'DATE & TIME',
                          behaveNormal: true,
                          textEditingController: _tFDateTimeController,
                          readOnly: true,
                          onTap: () {
                            selectDateTime(context, _onSelectedDateTime);
                          },
                        ),
                        DropdownButton(
                          isExpanded: true,
                          value: selectedFollowup,
                          style: TextStyle(
                              color: ColorResource.colorMarineBlue,
                              fontSize: 16.0),
                          underline: Container(
                            height: 2,
                            color: Colors.redAccent,
                          ),
                          onChanged: (Followup? newValue) {
                            setState(() {
                              selectedFollowup = newValue;
                              otherStatusText = null;
                            });
                          },
                          items: _isBookedTrip()
                              ? optionsItems.sublist(1, optionsItems.length)
                              : optionsItems,
                        ),
                        ((selectedFollowup == Followup.Other)
                            ? TextField(
                                style: Styles.txtStyleBlackBangla,
                                maxLength: 250,
                                maxLengthEnforcement:
                                    MaxLengthEnforcement.enforced,
                                keyboardType: TextInputType.multiline,
                                maxLines: null,
                                decoration: InputDecoration(
                                    focusedBorder: const OutlineInputBorder(
                                      // width: 0.0 produces a thin "hairline" border
                                      borderSide: const BorderSide(
                                          color: ColorResource.colorMarineBlue,
                                          width: 0.0),
                                    ),
                                    border: OutlineInputBorder(),
                                    hintText: translate(context, 'msg_wae'),
                                    counterStyle: Styles.txtStyleBlackBangla),
                                onChanged: (value) {
                                  setState(() {
                                    otherStatusText = value;
                                  });
                                },
                              )
                            : Container())
                      ],
                    ),
                  ),
                ),
                actions: [
                  TextButton(
                    style: ButtonStyle(
                      padding: MaterialStateProperty.all(
                          EdgeInsets.symmetric(horizontal: 16)),
                      backgroundColor: MaterialStateProperty.all(Colors.red),
                    ),
                    child: Text(
                      translate(context, 'cancel'),
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      setState(() {
                        Navigator.pop(context);
                      });
                    },
                  ),
                  TextButton(
                    style: ButtonStyle(
                      padding: MaterialStateProperty.all(
                          EdgeInsets.symmetric(horizontal: 16)),
                      backgroundColor: MaterialStateProperty.all(Colors.green),
                    ),
                    child: Text(
                      translate(context, 'lbl_unc'),
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                      callback(selectedFollowup, otherStatusText);
                      AnalyticsManager()
                          .logEventProp(AnalyticsEvents.STATUS_UPDATE_BTN_CLK, {
                        AnalyticProperties.TRIP_STATUS:
                            translate(context, translate(context, 'lbl_unc'))
                      });
                    },
                  ),
                ],
              );
            },
          );
        });
  }

  Future<void> _showDriverAndTruckUpdateDialog(
      BuildContext context, int? partnerId, String? truckRegNo, int? driverId) {
    return bloc!.getDriversAndTrucks(partnerId)!.then((driverTruckData) {
      return showDialog(
        context: context,
        builder: (context) {
          final trucks =driverTruckData.truckRegistrationModelList ?? List.empty();
          final drivers = driverTruckData.driverNameModelList ?? List.empty();
          Truck? selectedTruck;
          if(truckRegNo != null)
              selectedTruck = trucks.firstWhere((truck) => truck.registrationNumber == truckRegNo);
          Driver? selectedDriver;
          if(driverId != null)
            selectedDriver = drivers.firstWhere((driver) => driver.driverId == driverId);

          return StatefulBuilder(
            builder: (context, setState) {
              return AlertDialog(
                content: Container(
                  width: MediaQuery.of(context).size.width * .3,
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          translate(context, 'msg_sat'),
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        DropdownButton<Truck>(
                          isExpanded: true,
                          itemHeight: 54.0,
                          style: TextStyle(
                              color: ColorResource.colorMarineBlue,
                              fontSize: 16.0),
                          value: selectedTruck,
                          underline: Container(
                            height: 2,
                            color: Colors.redAccent,
                          ),
                          onChanged: (newValue) {
                            setState(() {
                              selectedTruck = newValue;
                            });
                          },
                          items: trucks.map((truck) {
                            return DropdownMenuItem(
                              value: truck,
                              child: Text(truck.text()),
                            );
                          }).toList(),
                        ),
                        SizedBox(height: 24),
                        Text(
                          translate(context, 'msg_sel_driver'),
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        DropdownButton<Driver>(
                          isExpanded: true,
                          value: selectedDriver,
                          itemHeight: 54.0,
                          style: TextStyle(
                              color: ColorResource.colorMarineBlue,
                              fontSize: 16.0),
                          underline: Container(
                            height: 2,
                            color: Colors.redAccent,
                          ),
                          onChanged: (newValue) {
                            setState(() {
                              selectedDriver = newValue;
                            });
                          },
                          items: drivers.map((driver) {
                            return DropdownMenuItem(
                              value: driver,
                              child: Text(driver.text()),
                            );
                          }).toList(),
                        ),
                      ],
                    ),
                  ),
                ),
                actions: [
                  TextButton(
                    style: ButtonStyle(
                      padding: MaterialStateProperty.all(
                          EdgeInsets.symmetric(horizontal: 16)),
                      backgroundColor: MaterialStateProperty.all(Colors.red),
                    ),
                    child: Text(
                      'Cancel',
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  TextButton(
                    style: ButtonStyle(
                      padding: MaterialStateProperty.all(
                          EdgeInsets.symmetric(horizontal: 16)),
                      backgroundColor: MaterialStateProperty.all(Colors.green),
                    ),
                    child: Text(
                      translate(context, 'lbl_unc'),
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () async {
                      print('On Update & Copy...');
                      if (selectedTruck == null || selectedDriver == null) {
                        showCustomSnackbar(translate(context, 'msg_tod_not_sel'));
                        return;
                      }
                      var response = await bloc!.updateDriverAndTruck(
                        partnerId,
                        selectedDriver?.driverId,
                        selectedTruck?.truckId,
                      );
                      Navigator.pop(context);
                      onResponse(response);
                      AnalyticsManager().logEventProp(
                          AnalyticsEvents.STATUS_UPDATE_BTN_CLK, {
                        AnalyticProperties.TRIP_STATUS:
                            AnalyticsEvents.TRUCK_DRIVER_UPDATE_BTN_CLK
                      });
                    },
                  ),
                ],
              );
            },
          );
        },
      );
    });
  }

  Future<void> _showTimePickerDialog(BuildContext context, Function callback) {
    return showDialog(
        context: context,
        builder: (context) {
          var minuteController = TextEditingController();
          var hour = -1;
          var minute = -1;
          return StatefulBuilder(
            builder: (context, setState) {
              return AlertDialog(
                content: Container(
                  width: MediaQuery.of(context).size.width * .3,
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              width: 128,
                              child: TextField(
                                style: Styles.txtStyleBlackBangla,
                                inputFormatters: [
                                  NumberRangeInputFormatter(0, 24),
                                  FilteringTextInputFormatter.digitsOnly,
                                ],
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  labelStyle: new TextStyle(
                                      color: ColorResource.colorMarineBlue),
                                  focusedBorder: const OutlineInputBorder(
                                    // width: 0.0 produces a thin "hairline" border
                                    borderSide: const BorderSide(
                                        color: ColorResource.colorMarineBlue,
                                        width: 0.0),
                                  ),
                                  border: OutlineInputBorder(),
                                  hintText: translate(context, 'hour'),
                                  labelText: translate(context, 'hour'),
                                ),
                                onChanged: (value) {
                                  setState(() {
                                    if (value == '') {
                                      hour = -1;
                                    } else {
                                      hour = int.parse(value);
                                      if (hour == 24) {
                                        minute = -1;
                                        minuteController.clear();
                                      }
                                    }
                                  });
                                },
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                left: 8.0,
                                right: 8.0,
                              ),
                              child: Text(
                                ':',
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 128,
                              child: TextField(
                                style: Styles.txtStyleBlackBangla,
                                controller: minuteController,
                                inputFormatters: [
                                  NumberRangeInputFormatter(
                                      0, hour == 24 ? 0 : 59),
                                  FilteringTextInputFormatter.digitsOnly,
                                ],
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  labelStyle: new TextStyle(
                                      color: ColorResource.colorMarineBlue),
                                  focusedBorder: const OutlineInputBorder(
                                    // width: 0.0 produces a thin "hairline" border
                                    borderSide: const BorderSide(
                                        color: ColorResource.colorMarineBlue,
                                        width: 0.0),
                                  ),
                                  border: OutlineInputBorder(),
                                  hintText: translate(context, 'minute'),
                                  labelText: translate(context, 'minute'),
                                ),
                                onChanged: (value) {
                                  setState(() {
                                    if (value == '') {
                                      minute = -1;
                                    } else {
                                      minute = int.parse(value);
                                    }
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 8),
                        Text(
                          translate(context, 'not_max_24'),
                          style: TextStyle(
                              fontSize: 12,
                              color: Colors.black,
                              fontWeight: FontWeight.w500),
                        ),
                        SizedBox(height: 32),
                        TextButton(
                          style: ButtonStyle(
                            padding: MaterialStateProperty.all(
                                EdgeInsets.symmetric(horizontal: 16)),
                            backgroundColor: MaterialStateProperty.all(
                              hour >= 0 || minute >= 0
                                  ? ColorResource.colorMarineBlue
                                  : ColorResource.colorMarineBlueAlpha,
                            ),
                          ),
                          child: Text(
                            translate(context, 'submit'),
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () {
                            if (hour >= 0 || minute >= 0) {
                              Navigator.pop(context);
                              callback(
                                  hour < 0 ? 0 : hour, minute < 0 ? 0 : minute);
                            }
                            AnalyticsManager().logEventProp(
                                AnalyticsEvents.STATUS_UPDATE_BTN_CLK, {
                              AnalyticProperties.TRIP_STATUS:
                                  translate(context, 'lbl_eta')
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          );
        });
  }

  onResponse(response) {
    print('Inside on response');
    switch (response.status) {
      case Status.LOADING:
        break;
      case Status.COMPLETED:
        showCustomSnackbar(response.message ?? translate(context, 'msg_dtu'));
        onRetryClick();
        break;
      case Status.ERROR:
        print('Inside onError');
        showCustomSnackbar(response.message ?? translate(context, 'msg_dtf'));
        break;
    }
  }

  @override
  onRetryClick() {
    bloc!.getTripStatus(widget.tripItem?.tripId);
    if (_isBiddedTrip()) {
      bloc!.fetchBids();
    }
  }
}
