
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app/model/master/districts.dart';
import 'package:app/model/master/thanas.dart';
import 'package:app/ui/detail/trip_detail_bloc.dart';
import 'package:app/ui/widget/editfield.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/date_time_util.dart';
import 'package:app/utils/string_util.dart';
import 'package:app/utils/ui_util.dart';
import 'package:app/utils/widget_util.dart';
import 'package:intl/intl.dart';

class LocationWidget extends StatefulWidget{
  final TripDetailBloc bloc;

  LocationWidget(this.bloc);

  @override
  _LocationWidgetState createState() => _LocationWidgetState();
}

class _LocationWidgetState extends State<LocationWidget>{
  final TextEditingController _tFDateTimeController = TextEditingController();
  final TextEditingController _textFieldController = TextEditingController();
  final TextEditingController _districtFieldController = TextEditingController();
  final TextEditingController _thanaFieldController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _tFDateTimeController.text = DateFormat(AppDateTimeFormat).format(DateTime.now());
  }

  _onSelectedDateTime(dateTime){
    widget.bloc.actionTime = dateTime;
    _tFDateTimeController.text = dateTime;
  }

  @override
  Widget build(BuildContext context){
    print('Rebuilding Location Widget');
    return Column(
      children: [
        TextEditText(
          labelText: 'DATE & TIME',
          behaveNormal: true,
          textEditingController: _tFDateTimeController,
          readOnly: true,
          onTap: (){
            selectDateTime(context,_onSelectedDateTime);
          },
        ),
        TextEditText(
            labelText: 'DISTRICT',
            enableBorderColor: ColorResource.colorWhite,
            textColor: ColorResource.colorWhite,
            labelTextColor: Colors.white,
            focusedBorderColor: ColorResource.colorWhite,
            behaveNormal: true,
            textEditingController: _districtFieldController,
            isSearchableItemWidget: true,
            suggestionDropDownItemList: widget.bloc.districts,
            searchDrodownCallback: (item) {
              widget.bloc.address = '';
              widget.bloc.setDistrict(item);
              setState(() {
                _thanaFieldController.text = '';
              });
            }),
        TextEditText(
            labelText: 'THANA',
            textEditingController: _thanaFieldController,
            enableBorderColor: ColorResource.colorWhite,
            textColor: ColorResource.colorWhite,
            labelTextColor: Colors.white,
            focusedBorderColor: ColorResource.colorWhite,
            behaveNormal: true,
            isSearchableItemWidget: true,
            suggestionDropDownItemList: widget.bloc.thanas,
            searchDrodownCallback: (item) {
              if(isNullOrEmpty(_districtFieldController.text.trim())) {
                _thanaFieldController.text = '';
                widget.bloc.setThana(null);
                showToast("Please select district first.");
                return;
              }
               widget.bloc.address = '';
               widget.bloc.setThana(item);
            }),
        SizedBox(
          height: 10.0,
        ),
        Text("OR"),
        SizedBox(
          height: 10.0,
        ),
        TextEditText(
            labelText: 'ADDRESS',
            enableBorderColor: ColorResource.colorBrownGrey,
            textColor: ColorResource.colorBlack,
            labelTextColor: ColorResource.colorMarineBlue,
            focusedBorderColor: ColorResource.colorBrownGrey,
            keyboardType: TextInputType.emailAddress,
            behaveNormal: true,
            textEditingController: _textFieldController,
            onChanged: (text) => widget.bloc.address = text)
      ],
    );
  }
}