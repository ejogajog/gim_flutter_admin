import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:app/ui/detail/model/bid.dart';
import 'package:app/ui/widget/empty_data_widget.dart';
import 'package:app/utils/app_colors.dart';
import 'package:app/utils/string_util.dart';
import 'package:app/utils/ui_util.dart';

class BidListWidget extends StatelessWidget {
  List<Bid>? items;
  final isLoading;
  Function? updateCallback;

  BidListWidget(this.items, this.isLoading, this.updateCallback);

  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return EmptyDataWidget(
        message: 'Fetching bids of this trip ...',
      );
    }

    if (isNullOrEmptyList(items)) {
      return EmptyDataWidget(
        message: 'No Bids found for this trip',
      );
    }

    return Container(
      width: double.infinity,
      child: Table(
        border: TableBorder.all(),
        children: items!.map((bid) {
          return TableRow(children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                    text: TextSpan(
                      style: TextStyle(color: Colors.black),
                      text: 'Partner: ${bid.fleetOwnerName}',
                      children: [
                        TextSpan(text: "\nMobile: "),
                        TextSpan(
                          text: bid.fleetOwnerContact,
                          style: TextStyle(
                            color: AppColors.Primary,
                            decoration: TextDecoration.underline,
                          ),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              copyToClipboard(context, bid.fleetOwnerContact);
                            },
                        ),
                        TextSpan(
                            text:
                                '\n\nDriver: ${bid.driverName ?? "Not Provided"}'),
                        TextSpan(text: '\nMobile:'),
                        TextSpan(
                          text: bid.driverMobileNumber ?? "Not Provided",
                          style: TextStyle(
                            color: AppColors.Primary,
                            decoration: TextDecoration.underline,
                          ),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              copyToClipboard(context, bid.driverMobileNumber);
                            },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                    text: TextSpan(
                      text: 'Bid Amount: ${bid.bidAmount}',
                      style: TextStyle(color: Colors.black),
                      children: [
                        TextSpan(text: "\nService Fee: ${bid.serviceFee}"),
                        TextSpan(text: "\nTrip Amount: ${bid.tripAmount}"),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                    text: TextSpan(
                      style: TextStyle(color: Colors.black),
                      text:
                          'Truck No: ${bid.truckRegistrationNumber ?? "Not Provided"}',
                      children: [
                        TextSpan(
                            text:
                                '\nTruck Type: ${bid.truckType ?? "Not Provided"}'),
                        TextSpan(
                            text:
                                '\nTruck Size: ${bid.truckSize ?? "Not Provided"}'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child:
                  (bid.driverId == null || bid.truckRegistrationNumber == null)
                      ? StatefulBuilder(
                          builder: (context, setState) {
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                ElevatedButton(
                                  child: Text('Update Info'),
                                  style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            AppColors.Primary),
                                  ),
                                  onPressed: () {
                                    this.updateCallback!(
                                        bid.partnerId,
                                        bid.truckRegistrationNumber,
                                        bid.driverId);
                                  },
                                ),
                              ],
                            );
                          },
                        )
                      : Container(),
            ),
          ]);
        }).toList(),
      ),
    );
  }
}
