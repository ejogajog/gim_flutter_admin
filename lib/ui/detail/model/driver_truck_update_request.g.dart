// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'driver_truck_update_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DriverTruckUpdateRequest _$DriverTruckUpdateRequestFromJson(
        Map<String, dynamic> json) =>
    DriverTruckUpdateRequest(
      partnerId: (json['partnerId'] as num?)?.toInt(),
      driverId: (json['driverId'] as num?)?.toInt(),
      truckId: (json['truckId'] as num?)?.toInt(),
    );

Map<String, dynamic> _$DriverTruckUpdateRequestToJson(
        DriverTruckUpdateRequest instance) =>
    <String, dynamic>{
      'partnerId': instance.partnerId,
      'driverId': instance.driverId,
      'truckId': instance.truckId,
    };
