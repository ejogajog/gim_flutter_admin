// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'driver_truck_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DriverTruckData _$DriverTruckDataFromJson(Map<String, dynamic> json) =>
    DriverTruckData(
      driverNameModelList: (json['driverNameModelList'] as List<dynamic>?)
          ?.map((e) => Driver.fromJson(e as Map<String, dynamic>))
          .toList(),
      truckRegistrationModelList:
          (json['truckRegistrationModelList'] as List<dynamic>?)
              ?.map((e) => Truck.fromJson(e as Map<String, dynamic>))
              .toList(),
    );

Map<String, dynamic> _$DriverTruckDataToJson(DriverTruckData instance) =>
    <String, dynamic>{
      'driverNameModelList': instance.driverNameModelList,
      'truckRegistrationModelList': instance.truckRegistrationModelList,
    };
