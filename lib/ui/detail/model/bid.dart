
import 'package:json_annotation/json_annotation.dart';

part 'bid.g.dart';

@JsonSerializable()
class Bid {
  int? bidId;
  double? bidAmount;
  String? bidStatus;
  double? advanceAmount;
  double? serviceFee;
  int? bidTime;
  int? driverId;
  String? driverName;
  String? driverMobileNumber;
  int? partnerId;
  String? fleetOwnerName;
  String? fleetOwnerContact;
  String? truckRegistrationNumber;
  String? truckType;
  double? truckSize;
  double? tripAmount;

  Bid({
    this.bidId,
    this.bidAmount,
    this.bidStatus,
    this.advanceAmount,
    this.serviceFee,
    this.bidTime,
    this.driverId,
    this.driverName,
    this.driverMobileNumber,
    this.partnerId,
    this.fleetOwnerName,
    this.fleetOwnerContact,
    this.truckRegistrationNumber,
    this.truckType,
    this.truckSize,
    this.tripAmount
  });

  Map<String, dynamic> toJson() => _$BidToJson(this);

  factory Bid.fromJson(Map<String, dynamic>? json) => _$BidFromJson(json!);
}
