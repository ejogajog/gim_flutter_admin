import 'package:json_annotation/json_annotation.dart';

part 'driver.g.dart';

@JsonSerializable()
class Driver {
  final int? driverId;
  final String? mobileNumber;
  final String? name;

  Driver({this.driverId, this.mobileNumber, this.name});

  factory Driver.fromJson(Map<String, dynamic> json) => _$DriverFromJson(json);

  Map<String, dynamic> toJson() => _$DriverToJson(this);

  String text() {
    return '$name\n$mobileNumber';
  }

}