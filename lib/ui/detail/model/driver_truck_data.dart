import 'package:app/ui/detail/model/driver.dart';
import 'package:app/ui/detail/model/truck.dart';
import 'package:json_annotation/json_annotation.dart';

part 'driver_truck_data.g.dart';

@JsonSerializable()
class DriverTruckData {
  final List<Driver>? driverNameModelList;
  final List<Truck>? truckRegistrationModelList;

  DriverTruckData({this.driverNameModelList, this.truckRegistrationModelList});

  factory DriverTruckData.fromJson(Map<String, dynamic> json) => _$DriverTruckDataFromJson(json);

  Map<String, dynamic> toJson() => _$DriverTruckDataToJson(this);
}