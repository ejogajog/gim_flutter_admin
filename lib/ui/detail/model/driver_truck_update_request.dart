
import 'package:json_annotation/json_annotation.dart';

part 'driver_truck_update_request.g.dart';

@JsonSerializable()
class DriverTruckUpdateRequest {
  final int? partnerId;
  final int? driverId;
  final int? truckId;

  DriverTruckUpdateRequest({this.partnerId, this.driverId, this.truckId});

  Map<String, dynamic> toJson() => _$DriverTruckUpdateRequestToJson(this);

  factory DriverTruckUpdateRequest.fromJson(Map<String, dynamic> json) => _$DriverTruckUpdateRequestFromJson(json);
}