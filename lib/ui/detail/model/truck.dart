import 'package:json_annotation/json_annotation.dart';

part 'truck.g.dart';

@JsonSerializable()
class Truck {
  String? registrationNumber;
  int? truckId;
  double? truckSize;
  String? truckType;

  Truck({
    this.registrationNumber,
    this.truckId,
    this.truckSize,
    this.truckType,
  });

  factory Truck.fromJson(Map<String, dynamic> json) => _$TruckFromJson(json);

  Map<String, dynamic> toJson() => _$TruckToJson(this);

  String text() {
    return '$registrationNumber\n$truckType($truckSize Ton)';
  }
}
