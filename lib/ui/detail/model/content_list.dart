import 'package:json_annotation/json_annotation.dart';

part 'content_list.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class ContentList<T> {
  List<T>? contentList;

  ContentList({this.contentList});

  factory ContentList.fromJson(Map<String, dynamic> json, T Function(Object? json) fromJsonT) => _$ContentListFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object Function(T value) toJsonT) => _$ContentListToJson(this, toJsonT);
}
