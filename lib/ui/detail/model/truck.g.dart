// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'truck.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Truck _$TruckFromJson(Map<String, dynamic> json) => Truck(
      registrationNumber: json['registrationNumber'] as String?,
      truckId: (json['truckId'] as num?)?.toInt(),
      truckSize: (json['truckSize'] as num?)?.toDouble(),
      truckType: json['truckType'] as String?,
    );

Map<String, dynamic> _$TruckToJson(Truck instance) => <String, dynamic>{
      'registrationNumber': instance.registrationNumber,
      'truckId': instance.truckId,
      'truckSize': instance.truckSize,
      'truckType': instance.truckType,
    };
