// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'content_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContentList<T> _$ContentListFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    ContentList<T>(
      contentList:
          (json['contentList'] as List<dynamic>?)?.map(fromJsonT).toList(),
    );

Map<String, dynamic> _$ContentListToJson<T>(
  ContentList<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'contentList': instance.contentList?.map(toJsonT).toList(),
    };
