// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bid.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Bid _$BidFromJson(Map<String, dynamic> json) => Bid(
      bidId: (json['bidId'] as num?)?.toInt(),
      bidAmount: (json['bidAmount'] as num?)?.toDouble(),
      bidStatus: json['bidStatus'] as String?,
      advanceAmount: (json['advanceAmount'] as num?)?.toDouble(),
      serviceFee: (json['serviceFee'] as num?)?.toDouble(),
      bidTime: (json['bidTime'] as num?)?.toInt(),
      driverId: (json['driverId'] as num?)?.toInt(),
      driverName: json['driverName'] as String?,
      driverMobileNumber: json['driverMobileNumber'] as String?,
      partnerId: (json['partnerId'] as num?)?.toInt(),
      fleetOwnerName: json['fleetOwnerName'] as String?,
      fleetOwnerContact: json['fleetOwnerContact'] as String?,
      truckRegistrationNumber: json['truckRegistrationNumber'] as String?,
      truckType: json['truckType'] as String?,
      truckSize: (json['truckSize'] as num?)?.toDouble(),
      tripAmount: (json['tripAmount'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$BidToJson(Bid instance) => <String, dynamic>{
      'bidId': instance.bidId,
      'bidAmount': instance.bidAmount,
      'bidStatus': instance.bidStatus,
      'advanceAmount': instance.advanceAmount,
      'serviceFee': instance.serviceFee,
      'bidTime': instance.bidTime,
      'driverId': instance.driverId,
      'driverName': instance.driverName,
      'driverMobileNumber': instance.driverMobileNumber,
      'partnerId': instance.partnerId,
      'fleetOwnerName': instance.fleetOwnerName,
      'fleetOwnerContact': instance.fleetOwnerContact,
      'truckRegistrationNumber': instance.truckRegistrationNumber,
      'truckType': instance.truckType,
      'truckSize': instance.truckSize,
      'tripAmount': instance.tripAmount,
    };
