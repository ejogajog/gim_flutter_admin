// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'driver.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Driver _$DriverFromJson(Map<String, dynamic> json) => Driver(
      driverId: (json['driverId'] as num?)?.toInt(),
      mobileNumber: json['mobileNumber'] as String?,
      name: json['name'] as String?,
    );

Map<String, dynamic> _$DriverToJson(Driver instance) => <String, dynamic>{
      'driverId': instance.driverId,
      'mobileNumber': instance.mobileNumber,
      'name': instance.name,
    };
