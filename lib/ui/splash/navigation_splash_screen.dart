import 'package:flutter/cupertino.dart';
import 'package:app/ui/home/home.dart';
import 'package:app/utils/navigation_util.dart';

class NavigationSplashScreen extends StatefulWidget{
  @override
  _NavigationSplashScreenState createState() => _NavigationSplashScreenState();
}

class _NavigationSplashScreenState extends State<NavigationSplashScreen> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      navigateNextScreen(context, HomeScreen(),shouldReplaceCurrentPage: true);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}