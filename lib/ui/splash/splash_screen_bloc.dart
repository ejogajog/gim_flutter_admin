import 'package:app/api/api_response.dart';
import 'package:app/bloc/base_bloc.dart';
import 'package:app/ui/auth/auth_repository.dart';
import 'package:app/ui/options/features_model.dart';

class SplashScreenBloc extends BaseBloc {
  FeaturesModel? _featuresModel;

  set featureModel(model) {
    _featuresModel = model;
  }

  get accessibleFeatures => _featuresModel;

  fetchAccessibleFeatures() async {
    if(accessibleFeatures == null){
      var response = await AuthRepository.accessibleFeatures();
      if (response.status == Status.COMPLETED) {
        featureModel = FeaturesModel.fromJson(response.data);
      }
    }
  }
}
