import 'dart:async';

import 'package:dio/dio.dart';
import 'package:app/api/http_util.dart';
import 'package:app/model/master/districts.dart';
import 'package:app/model/master/goods_type.dart';
import 'package:app/model/master/master.dart';
import 'package:app/model/master/truck_dimension.dart';
import 'package:app/model/master/truck_size.dart';
import 'package:app/model/master/truck_type.dart';
import 'package:app/utils/prefs.dart';


typedef Future<T> FutureGenerator<T>();
int masterDataVersion=0;
String? deviceToken;
Future<T?> retryProcess<T>(int retries, FutureGenerator aFuture) async {
  try {
    return await (aFuture() as FutureOr<T>);
  } catch (e) {
    print("retries"+retries.toString());
    if (retries > 0) {
      return retryProcess(retries - 1, aFuture);
    }else if(retries==0)return null;
    rethrow;
  }
}

Future doSomething() async {
  await Future.delayed(Duration(milliseconds: 500));
  return "Something";
}

Future<dynamic> prepareMasterData() async {
  var client=NetworkCommon();
  var response= await client.dio.get('/ejogajog/api/v1/master/all');
  if(response.statusCode!=200)
    throw Exception;
  else{
    processMasterData(response);
    return response.data;

  }

}

processMasterData(Response response)async{
  var data = MasterDataResponse.fromJson(response.data['data']);
  Prefs.setString(Prefs.DISTRICTS, Districts.encode(data.districts!));
  Prefs.setString(Prefs.GOODS_TYPE, GoodsType.encode(data.goodTypes!));
  Prefs.setString(Prefs.TRUCK_TYPE, TruckType.encode(data.truckTypes!));
  Prefs.setString(Prefs.TRUCK_SIZE, TruckSize.encode(data.truckSizes!));
  Prefs.setString(Prefs.TRUCK_LENGTH, TruckDimensionLength.encode(data.truckDimensions!.length!));
  Prefs.setBoolean(Prefs.MASTER_DATA_SYNCED, true);
}

getMasterDataUsingNativeProcess() async {
  retryProcess(15, prepareMasterData);
}

