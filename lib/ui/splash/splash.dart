import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:app/ui/auth/login_screen.dart';
import 'package:app/ui/options/activities_options_page.dart';
import 'package:app/ui/options/features_model.dart';
import 'package:app/ui/splash/master_data_handler.dart';
import 'package:app/ui/splash/splash_screen_bloc.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/navigation_util.dart';
import 'package:app/utils/prefs.dart';
import 'package:app/utils/ui_util.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<SplashScreenBloc>(create: (_) => SplashScreenBloc()),
      ],
      child: SplashScreenPage(),
    );
  }
}

class SplashScreenPage extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreenPage> {
  var bloc;
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    try {
      bloc = Provider.of<SplashScreenBloc>(context, listen: false);
      bloc.fetchAccessibleFeatures();
      checkMasterDataVersion();
    } catch (exception) {
      startTime();
    }
  }

  checkMasterDataVersion() async {
    if (!Prefs.getBoolean(Prefs.MASTER_DATA_SYNCED, defaultValue: false)) getMasterDataUsingNativeProcess();
    startTime();
  }

  startTime() async {
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() async {
    var widget;
    var pageName;
    bool isLoggedIn = await Prefs.getBool(Prefs.IS_LOGGED_IN, defaultValue: false);
    if (isLoggedIn) {
      await bloc.fetchAccessibleFeatures();
      List<FeatureItem>? items = bloc.accessibleFeatures?.features();
      if (items?.length == 1) {
        navigateToScreen(context, items?.first.pageUrl, pageName: items?.first.eventName);
        return;
      } else {
        pageName = AnalyticsEvents.ACC_FEATURE_SCREEN;
        widget = ActivityOptionPage(bloc.accessibleFeatures);
      }
    } else {
      pageName = AnalyticsEvents.ADMIN_LOGIN_SCREEN;
      widget = LoginScreenContainer();
    }
    if (Prefs.getString(Prefs.token) == null || Prefs.getString(Prefs.token)!.isEmpty) {
      pageName = AnalyticsEvents.ADMIN_LOGIN_SCREEN;
      widget = LoginScreenContainer();
    }
    navigateNextScreen(context, widget, shouldReplaceCurrentPage: true, pageName: pageName);
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(getGlobalContext(), designSize: Size(360, 640));
    return Scaffold(
      backgroundColor: Colors.white,
      key: scaffoldState,
      body: Container(
        color: ColorResource.colorMarineBlue,
        child: Center(
          child: GestureDetector(
            child: SizedBox(
              height: 100,
              child: Image.asset(
                'images/gim_logo.png',
              ),
            ),
            onTap: () => navigationPage(),
          ),
        ),
      ),
    );
  }
}
