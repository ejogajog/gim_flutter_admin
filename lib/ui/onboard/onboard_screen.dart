import 'package:flutter/material.dart';
import 'package:app/ui/auth/login_screen.dart';
import 'package:app/ui/detail/trip_details.dart';
import 'package:app/ui/onboard/onboard_bloc.dart';
import 'package:app/ui/widget/app_button.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/navigation_util.dart';
import 'package:app/utils/ui_util.dart';
import 'package:provider/provider.dart';

class OnboardScreen extends StatefulWidget{

  @override
  _OnboardScreenState createState() => _OnboardScreenState();
}

class _OnboardScreenState extends State<OnboardScreen> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<OnboardBloc>(
      create: (context)=>OnboardBloc(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          body: GestureDetector(
            onTap: ()=>hideSoftKeyboard(context),
            child: Container(
              color: ColorResource.colorMarineBlue,
              child: Padding(
                padding:  EdgeInsets.all(15),
                child: Stack(
                  children: <Widget>[
                    Positioned.fill(top: 60,child: Align(child: Image.asset('images/ic_gim_logo_2.webp',
                      width: 140,
                      height: 140,),
                      alignment: Alignment.topCenter,)),
                    Positioned.fill(top:60,child: Align(child: Text('THE PLATFORM WHERE CARRIER MEETS SHIPPER'.toUpperCase(),
                      style: TextStyle(color: ColorResource.colorWhite,fontWeight: FontWeight.bold,fontSize: 18.0),
                      textAlign: TextAlign.center,),alignment: Alignment.center,)),
                    Positioned.fill(top:15,child: Align(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            // FilledColorButton(buttonText: 'CREATE MY ACCOUNT'.toUpperCase(),isFilled: true,textColor: ColorResource.colorMarineBlue,
                            //   backGroundColor: ColorResource.colorMariGold,onPressed: (){
                            //     // showAlertDialog(context,"Do you want to withdraw its bid?","Yes, Cancel",(pressed)=>"");
                            //     // navigateNextScreen(context, OtpVerificationScreen(otpGenerationResponse: OtpGenerationResponse(),));
                            //     //navigateNextScreen(context, TripDetailsScreen());

                            //   },),
                            FilledColorButton(buttonText: 'Log In',isFilled: false,textColor:ColorResource.colorWhite,
                              backGroundColor:Colors.transparent,borderColor: ColorResource.colorWhite,onPressed: (){
                                navigateNextScreen(context, LoginScreenContainer());
                              },),

                          ]),alignment: Alignment.bottomCenter,)),
                    //01230000000
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

