import 'package:app/api/api_response.dart';
import 'package:app/bloc/base_trip_list_bloc.dart';
import 'package:app/model/base_response.dart';
import 'package:app/ui/payment/model/payment_model.dart';
import 'package:app/ui/payment/model/trip_item.dart';
import 'package:app/ui/payment/model/trip_pay_info.dart';
import 'package:app/ui/trip/trip_repository.dart';
import 'package:app/utils/string_util.dart';
import 'package:app/utils/validation_util.dart';

class PaymentApprovalBloc extends BaseTripListBloc {
  PaymentInfo _paymentInfo = PaymentInfo();

  bool? isSelAmt;
  int? bidAmount;
  bool? isValidAmt = true;
  Map? enterpriseMap;

  get valMaxValue => isSelAmt! ? bidAmount! - (partialAmt.length == 0 ? 0 : int.parse(partialAmt)) : bidAmount! - (advanceAmount.length == 0 ? 0 : int.parse(advanceAmount));

  get enterprises => enterpriseMap?.keys.toList();

  get enterprise => _paymentInfo.enterpriseName ?? '';

  selEnterprise(value) {
    _paymentInfo.enterpriseName = value;
    _paymentInfo.partnerName = '';
    _paymentInfo.selectedTrips = '';
    notifyListeners();
  }

  get partners => isNullOrEmpty(enterprise) ? List<String>.empty() : enterpriseMap![enterprise].keys.toList();

  get partner => _paymentInfo.partnerName ?? '';

  selPartner(value) {
    _paymentInfo.partnerName = value;
    notifyListeners();
  }

  get trips => isNullOrEmpty(partner) ? List<String>.empty() : enterpriseMap![enterprise][partner].listOfTrips;

  get tripItem => _paymentInfo.selectedTrips ?? '';

  selTripList(value) {
    _paymentInfo.selectedTrips = value;
    bidAmount = enterpriseMap![enterprise][partner]
        .bidAmounts[trips.indexOf(tripItem)]
        .toInt();
    _paymentInfo.dueAmount = bidAmount.toString();
    notifyListeners();
  }

  List<String> paymentTypes = ['Advance', 'Cash Upon Unloading'];

  get paymentType => _paymentInfo.paymentType ?? '';

  selPaymentType(value) {
    _paymentInfo.advance = '';
    _paymentInfo.partial = '';
    amount = '';
    _paymentInfo.paymentType = value;
    if (paymentType == paymentTypes[1]) {
      amount = bidAmount.toString();
      return;
    }
    notifyListeners();
  }

  List<String> paymentMethods = ['Cash', 'bKash', 'Bank'];

  get paymentMethod => _paymentInfo.paymentMethod ?? '';

  selPaymentMethod(value) {
    _paymentInfo.paymentMethod = value;
    _paymentInfo.accountType = '';
    _paymentInfo.selectedCharge = '';
    _paymentInfo.bCashMobNo = null;
    notifyListeners();
  }

  List<String> accountTypes = ['Personal', 'Agent'];

  get accountType => _paymentInfo.accountType ?? '';

  selAccountType(value) {
    _paymentInfo.accountType = value;
    notifyListeners();
  }

  List<String> charges = [
    '100% by GIM',
    '100% by Partner',
    '50-50 by GIM & Partner'
  ];

  get charge => _paymentInfo.selectedCharge ?? '';

  selCharge(value) {
    _paymentInfo.selectedCharge = value;
    notifyListeners();
  }

  set bCashMobNo(value) {
    _paymentInfo.bCashMobNo = value;
    notifyListeners();
  }

  get amount => _paymentInfo.amount ?? '';

  set amount(value) {
    isSelAmt = true;
    _paymentInfo.amount = value;
    if (paymentType == paymentTypes[0]) {
      _paymentInfo.advance = value;
      _paymentInfo.dueAmount = (bidAmount! -
          int.parse(advanceAmount.length == 0 ? '0' : advanceAmount) -
          int.parse(partialAmt.length == 0 ? '0' : partialAmt))
          .toString();
    }
    notifyListeners();
  }

  get advanceAmount => _paymentInfo.advance ?? '';

  set advance(value) {
    _paymentInfo.advance = value;
    notifyListeners();
  }

  get partialAmt => _paymentInfo.partial ?? '';

  set partial(value) {
    isSelAmt = false;
    _paymentInfo.partial = value;
    _paymentInfo.dueAmount = (bidAmount! -
        (advanceAmount.length == 0 ? 0 : int.parse(advanceAmount)) -
        (value.length == 0 ? 0 : (int.parse(value))))
        .toString();
    notifyListeners();
  }

  get dueAmt => _paymentInfo.dueAmount ?? bidAmount.toString();

  set dueAmount(value) {
    _paymentInfo.dueAmount = value;
    notifyListeners();
  }

  isValid() {
    return isNullOrEmpty(enterprise) ||
        isNullOrEmpty(partner) ||
        isNullOrEmpty(tripItem) ||
        isNullOrEmpty(paymentType) ||
        isNullOrEmpty(paymentMethod) ||
        (paymentMethod == paymentMethods[1] &&
            (isNullOrEmpty(_paymentInfo.bCashMobNo) ||
                !RegExp(MOBILE_NUMBER_REGEX_0)
                    .hasMatch(_paymentInfo.bCashMobNo!) ||
                isNullOrEmpty(accountType) ||
                isNullOrEmpty(charge))) ||
        (paymentType == paymentTypes[0] &&
            isNullOrEmpty(amount) &&
            isNullOrEmpty(partialAmt) ||
            !isValidAmt!);
  }

  mobileMail() {
    return 'Dear Tarik Bhai,'
        '\n\n Please acknowledge for the below payment to a $partner for ${1} trip for $enterprise company '
        '\n\n Trip Number - $tripItem '
        '\n Payment Type - $paymentType '
        '\n Payment Method - $paymentMethod, ${(paymentMethod ==
        paymentMethods[1]) ? '${_paymentInfo.bCashMobNo}, ${_paymentInfo
        .accountType}, ${_paymentInfo.selectedCharge}' : ''}'
        '\n Amount - $amount tk'
        '\n ${(paymentType == paymentTypes[0])
        ? '\n\n Payment term for the trips: \n Advance - $advanceAmount tk \n Partial - $partialAmt tk \n Final - $dueAmt tk'
        : ''}';
  }

  webMail() {
    return 'Dear Tarik Bhai, '
        '\n\n Please acknowledge for the below payment to a $partner for ${1} trip for $enterprise company '
        '\n\n Trip Number - $tripItem '
        '\n Payment Type - $paymentType '
        '\n Payment Method - $paymentMethod, ${(paymentMethod ==
        paymentMethods[1]) ? '${_paymentInfo.bCashMobNo}, ${_paymentInfo
        .accountType}, ${_paymentInfo.selectedCharge}' : ''}'
        '\n Amount - $amount tk'
        '\n ${(paymentType == paymentTypes[0])
        ? '\n\n Payment term for the trips: \n Advance - $advanceAmount tk \n Partial - $partialAmt tk \n Final - $dueAmt tk'
        : ''}';
  }

  @override
  getListFromApi({callback}) async {
    isLoading = true;
    queryParam['tripStatusParam'] = 'ALL';
    ApiResponse response = await TripRepository.getPaymentTrips(queryParam);
    if (response.status == Status.COMPLETED) {
      _buildData(convertDynamicListToStaticList<TripItem>(response.data));
    }
    isLoading = false;
    takeDecisionShowingError(response);
  }

  _buildData(listData) {
    enterpriseMap = Map<String, Map>();
    var partnerMap = Map<String, TripInfo>();
    listData.forEach((element) {
      if (enterpriseMap!.containsKey(element.companyName)) {
        partnerMap = enterpriseMap![element.companyName];
        if (partnerMap.containsKey(
            '${element.partnerName}(${element.partnerMobileNumber})')) {
          TripInfo? tripInfo = partnerMap[
          '${element.partnerName}(${element.partnerMobileNumber})'];
          tripInfo?.bidAmounts.add(element.bidAmount);
          tripInfo?.listOfTrips.add((element.tripNumber).toString());
        } else {
          TripInfo tripInfo = TripInfo();
          tripInfo.bidAmounts.add(element.bidAmount);
          tripInfo.listOfTrips.add((element.tripNumber).toString());
          partnerMap['${element.partnerName}(${element.partnerMobileNumber})'] =
              tripInfo;
        }
      } else {
        var tripInfo = TripInfo();
        partnerMap = Map<String, TripInfo>();
        tripInfo.bidAmounts.add(element.bidAmount);
        tripInfo.listOfTrips.add((element.tripNumber).toString());
        partnerMap['${element.partnerName}(${element.partnerMobileNumber})'] =
            tripInfo;
        enterpriseMap![element.companyName] = partnerMap;
      }
    });
  }
}
