import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app/styles.dart';
import 'package:app/ui/payment/bloc/payment_bloc.dart';
import 'package:app/ui/widget/app_bar.dart';
import 'package:app/ui/widget/app_button.dart';
import 'package:app/ui/widget/base_widget.dart';
import 'package:app/ui/widget/editfield.dart';
import 'package:app/utils/app_util.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/ui_util.dart';
import 'package:app/utils/validation_util.dart';
import 'package:provider/provider.dart';
import 'package:flutter/foundation.dart' show kIsWeb;


class PaymentApprovalScreen extends StatelessWidget {
  PaymentApprovalScreen();

  @override
  Widget build(BuildContext context) {
    return MultiProvider(providers: [
      ChangeNotifierProvider<PaymentApprovalBloc>(
          create: (_) => PaymentApprovalBloc()),
    ], child: PaymentApprovalPage());
  }
}

class PaymentApprovalPage extends StatefulWidget {
  @override
  _PaymentApprovalPage createState() => _PaymentApprovalPage();
}

class _PaymentApprovalPage
    extends BasePageWidgetState<PaymentApprovalPage, PaymentApprovalBloc> {
  TextEditingController amountController = TextEditingController();
  TextEditingController advanceController = TextEditingController();
  TextEditingController finalAmtController = TextEditingController();
  TextEditingController partialAmtController = TextEditingController();

  @override
  onBuildCompleted() {
    if (mounted) bloc!.getListFromApi();
  }

  @override
  isResizeToBottomInset() => true;

  @override
  getAppbar() => AppBarWidget(title: 'Payment Approval');

  @override
  List<Widget> getPageWidget() {
    return [
      Consumer<PaymentApprovalBloc>(
        builder: (context, bloc, _) => bloc.enterpriseMap == null
            ? showLoader<PaymentApprovalBloc>(
                Provider.of<PaymentApprovalBloc>(context))
            : SingleChildScrollView(
                child: Center(
                  child: Container(
                    width: kIsWeb ? 400 : double.infinity,
                    padding: EdgeInsets.all(20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Text('Select Enterprise'.toUpperCase(),
                            style: Styles.hintLabel),
                        TextEditText(
                            labelText: '',
                            isDropDown: true,
                            selValue: bloc.enterprise,
                            suggestionDropDownItemList: bloc.enterprises,
                            searchDrodownCallback: bloc.selEnterprise),
                        SizedBox(height: 10.0),
                        Text('Select Partner'.toUpperCase(),
                            style: Styles.hintLabel),
                        TextEditText(
                            labelText: '',
                            isDropDown: true,
                            selValue: bloc.partner,
                            searchDrodownCallback: bloc.selPartner,
                            suggestionDropDownItemList: bloc.partners),
                        SizedBox(height: 10.0),
                        Text('Select Trip'.toUpperCase(),
                            style: Styles.hintLabel),
                        TextEditText(
                            labelText: '',
                            isDropDown: true,
                            selValue: bloc.tripItem,
                            suggestionDropDownItemList: bloc.trips,
                            searchDrodownCallback: bloc.selTripList),
                        SizedBox(height: 10.0),
                        Text('Payment Type'.toUpperCase(),
                            style: Styles.hintLabel),
                        TextEditText(
                            labelText: '',
                            isDropDown: true,
                            selValue: bloc.paymentType,
                            suggestionDropDownItemList: bloc.paymentTypes,
                            searchDrodownCallback: bloc.selPaymentType),
                        SizedBox(height: 10.0),
                        Text('Payment Method'.toUpperCase(),
                            style: Styles.hintLabel),
                        TextEditText(
                            labelText: '',
                            isDropDown: true,
                            selValue: bloc.paymentMethod,
                            suggestionDropDownItemList: bloc.paymentMethods,
                            searchDrodownCallback: bloc.selPaymentMethod),
                        SizedBox(height: 10.0),
                        bloc.paymentMethod == bloc.paymentMethods[1]
                            ? Column(
                                mainAxisSize: MainAxisSize.max,
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  SizedBox(height: 10.0),
                                  Text('bKash No'.toUpperCase(),
                                      style: Styles.hintLabel),
                                  TextEditText(
                                      labelText: '',
                                      maxlength: 11,
                                      behaveNormal: true,
                                      validationMessageRegexPair: {
                                        NOT_EMPTY_REGEX: 'Must not be empty',
                                        MOBILE_NUMBER_REGEX_0:
                                            'Invalid Mobile Number'
                                      },
                                      onChanged: (text) =>
                                          bloc.bCashMobNo = text),
                                  SizedBox(height: 10.0),
                                  Text('Account Type'.toUpperCase(),
                                      style: Styles.hintLabel),
                                  TextEditText(
                                      labelText: '',
                                      isDropDown: true,
                                      selValue: bloc.accountType,
                                      suggestionDropDownItemList:
                                          bloc.accountTypes,
                                      searchDrodownCallback:
                                          bloc.selAccountType),
                                  SizedBox(height: 10.0),
                                  Text('Charge Settlement'.toUpperCase(),
                                      style: Styles.hintLabel),
                                  TextEditText(
                                      labelText: '',
                                      isDropDown: true,
                                      selValue: bloc.charge,
                                      suggestionDropDownItemList: bloc.charges,
                                      searchDrodownCallback: bloc.selCharge),
                                ],
                              )
                            : Container(),
                        Text('Amount'.toUpperCase(), style: Styles.hintLabel),
                        TextEditText(
                            labelText: '',
                            isValid: (isOk) => bloc.isValidAmt = isOk,
                            readOnly: bloc.paymentType == bloc.paymentTypes[1],
                            behaveNormal: true,
                            keyboardType: TextInputType.number,
                            onChanged: (text) => bloc.amount = text,
                            textEditingController: amountController
                              ..text = bloc.amount),
                        Visibility(
                          visible: bloc.paymentType == bloc.paymentTypes[0],
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              SizedBox(height: 10.0),
                              Text(
                                  'Payment Terms(Bid Amount: ${bloc.bidAmount})'
                                      .toUpperCase(),
                                  style: Styles.detailLabel),
                              SizedBox(height: 10.0),
                              Text('Advance'.toUpperCase(),
                                  style: Styles.hintLabel),
                              TextEditText(
                                labelText: '',
                                readOnly: true,
                                selValue: bloc.advanceAmount,
                                behaveNormal: true,
                                onChanged: (text) => bloc.advance = text,
                                textEditingController: advanceController
                                  ..text = bloc.advanceAmount,
                              ),
                              SizedBox(height: 10.0),
                              Text('Partial'.toUpperCase(),
                                  style: Styles.hintLabel),
                              TextEditText(
                                  labelText: '',
                                  isValid: (isOk) => bloc.isValidAmt = isOk,
                                  behaveNormal: true,
                                  keyboardType: TextInputType.number,
                                  onChanged: (text) => bloc.partial = text,
                                  textEditingController: partialAmtController
                                    ..text = bloc.partialAmt),
                              SizedBox(height: 10.0),
                              Text('Final'.toUpperCase(),
                                  style: Styles.hintLabel),
                              TextEditText(
                                  labelText: '',
                                  readOnly: true,
                                  behaveNormal: true,
                                  onChanged: (text) => bloc.dueAmount = text,
                                  textEditingController: finalAmtController
                                    ..text = bloc.dueAmt),
                            ],
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: FilledColorButton(
                                buttonText: 'Request'.toUpperCase(),
                                isFullwidth: false,
                                onPressed: bloc.isValid() ? null : () => launchWebEmail(isWebMobile ? bloc.mobileMail() : bloc.webMail()),
                              ),
                            ),
                            SizedBox(width: 5.0),
                            Expanded(
                              child: FilledColorButton(
                                buttonText: 'Cancel'.toUpperCase(),
                                isFullwidth: false,
                                textColor: ColorResource.colorMarineBlue,
                                backGroundColor: ColorResource.colorWhite,
                                onPressed: () => Navigator.canPop(context),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
      )
    ];
  }

  @override
  onRetryClick() {
    bloc!.getListFromApi();
  }
}
