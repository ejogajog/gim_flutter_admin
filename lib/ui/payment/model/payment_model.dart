class PaymentInfo{
  String? amount;
  String? advance;
  String? partial;
  String? dueAmount;
  String? bCashMobNo;
  String? paymentType;
  String? partnerName;
  String? accountType;
  String? selectedTrips;
  String? paymentMethod;
  String? enterpriseName;
  String? selectedCharge;
}