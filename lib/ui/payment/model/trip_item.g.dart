// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'trip_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TripItem _$TripItemFromJson(Map<String, dynamic> json) => TripItem()
  ..bidAmount = (json['bidAmount'] as num?)?.toDouble()
  ..tripNumber = (json['tripNumber'] as num?)?.toInt()
  ..companyName = json['companyName'] as String?
  ..partnerName = json['partnerName'] as String?
  ..partnerMobileNumber = json['partnerMobileNumber'] as String?;

Map<String, dynamic> _$TripItemToJson(TripItem instance) => <String, dynamic>{
      'bidAmount': instance.bidAmount,
      'tripNumber': instance.tripNumber,
      'companyName': instance.companyName,
      'partnerName': instance.partnerName,
      'partnerMobileNumber': instance.partnerMobileNumber,
    };
