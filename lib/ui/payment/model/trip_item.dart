import 'package:json_annotation/json_annotation.dart';

part 'trip_item.g.dart';

@JsonSerializable()
class TripItem{
  double? bidAmount;
  int? tripNumber;
  String? companyName;
  String? partnerName;
  String? partnerMobileNumber;

  TripItem();

  factory TripItem.fromJson(Map<String,dynamic>json)=>_$TripItemFromJson(json);
  Map<String, dynamic> toJson() => _$TripItemToJson(this);

  @override
  String toString() {
    return '$companyName';
  }
}