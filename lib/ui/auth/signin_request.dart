import 'package:json_annotation/json_annotation.dart';

part 'signin_request.g.dart';

@JsonSerializable()
class SignInRequest{
  String? email;
  String? password;
  SignInRequest({this.email,this.password});
  SignInRequest.internal();
  factory SignInRequest.fromJson(Map<String,dynamic>json)=>_$SignInRequestFromJson(json);
  Map<String, dynamic> toJson() => _$SignInRequestToJson(this);

}