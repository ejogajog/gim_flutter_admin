import 'package:app/api/api_response.dart';
import 'package:app/api/http_util.dart';
import 'package:app/bloc/base_bloc.dart';
import 'package:app/ui/auth/auth_repository.dart';
import 'package:app/ui/auth/login_response.dart';
import 'package:app/ui/auth/signin_request.dart';
import 'package:app/ui/options/features_model.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/app_util.dart';
import 'package:app/utils/prefs.dart';
import 'package:app/utils/string_util.dart';

class LoginScreenBloc extends BaseBloc {
  FeaturesModel? _featuresModel;
  late ApiResponse _loginResponse;

  ApiResponse get loginResponse => _loginResponse;

  set loginResponse(ApiResponse value) {
    _loginResponse = value;
  }

  var email = "";
  var password = "";

  set featureModel(model) {
    _featuresModel = model;
  }

  get accessibleFeatures => _featuresModel;

  fetchCredentials() async {
    isLoading = true;
    email = await Prefs.getStringAsync(Prefs.PREF_USER_EMAIL);
    password = await Prefs.getStringAsync(Prefs.PREF_USER_PWD);
    isLoading = false;
  }

  signIn() async {
    var response = await AuthRepository.signIn(
        SignInRequest(email: email, password: password));
    loginResponse = response;
    switch (response.status) {
      case Status.LOADING:
        break;
      case Status.COMPLETED:
        var loginResponse = LoginResponse.fromJson(response.data);
        print(loginResponse.name);
        await Prefs.setString(Prefs.PREF_USER_PWD, password);
        await Prefs.setString(Prefs.token, loginResponse.token);
        await Prefs.setString(Prefs.PREF_USER_EMAIL, email);
        await Prefs.setInt(Prefs.ROLE_ID, loginResponse.roleId);
        await Prefs.setInt(Prefs.USER_ID, loginResponse.id);
        await Prefs.setString(Prefs.ADMIN_TYPE, loginResponse.userType());
        await Prefs.setBoolean(Prefs.IS_APPROVED_USER,isNullOrEmpty(loginResponse.getUserStatus()) || loginResponse.isApprovedRoles());
        await Prefs.setBoolean(Prefs.PREF_IS_ENTERPRISE_USER,isEnterprise(loginResponse.userRoles!));
        await Prefs.setBoolean(Prefs.IS_DISTRIBUTOR, loginResponse.approvedDistributor);
        await Prefs.addIntegerList(Prefs.CLS_DIST_ID, loginResponse.clusterDistricts);
        NetworkCommon.instance.initConfig();
        loginResponse.email = email;
        AnalyticsManager().logUserProp(AnalyticsEvents.LOGIN_SUCCESS,loginResponse);
        await _fetchAccessibleFeatures();
        break;
      case Status.ERROR:
        break;
    }
    return response;
  }

  _fetchAccessibleFeatures() async {
    var response = await AuthRepository.accessibleFeatures();
    if (response.status == Status.COMPLETED) {
      featureModel = FeaturesModel.fromJson(response.data);
    }
    return response;
  }
}
