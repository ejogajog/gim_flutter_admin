import 'dart:async';

import 'package:dio/dio.dart';
import 'package:app/api/http_util.dart';
import 'package:app/model/project/project.dart';
import 'package:app/model/project/projects.dart';
import 'package:app/utils/prefs.dart';
import 'package:app/utils/string_util.dart';


typedef Future<T> FutureGenerator<T>();
int masterDataVersion=0;
String? deviceToken;
Future<T?> retryProcess<T>(int retries, FutureGenerator aFuture) async {
  try {
    return await (aFuture() as FutureOr<T>);
  } catch (e) {
    print("retries"+retries.toString());
    if (retries > 0) {
      return retryProcess(retries - 1, aFuture);
    }else if(retries==0)return null;
    rethrow;
  }
}

Future doSomething() async {
  await Future.delayed(Duration(milliseconds: 500));
  return "Something";
}

Future<dynamic> _fetchProjectList() async {
  var client = NetworkCommon();
  var response= await client.dio.get('/ejogajogAdminAPI/api/v1/admin/projectlist');
  if(response.statusCode!=200)
    throw Exception;
  else{
    _saveProjectList(response);
    return response.data;
  }
}

_saveProjectList(Response response)async{
  var data = ProjectListResponse.fromJson(response.data);
  Prefs.setString(Prefs.PROJECTS, Projects.encode(data.data!));
}

loadProjectListInBg() async {
  if(isNullOrEmpty(Prefs.getString(Prefs.PROJECTS)))
    retryProcess(3, _fetchProjectList);
}

