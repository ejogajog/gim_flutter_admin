import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:app/api/api_response.dart';
import 'package:app/ui/auth/project_data_handler.dart';
import 'package:app/ui/options/activities_options_page.dart';
import 'package:app/ui/options/features_model.dart';
import 'package:app/ui/widget/app_button.dart';
import 'package:app/ui/widget/base_widget.dart';
import 'package:app/ui/widget/editfield.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/navigation_util.dart';
import 'package:app/utils/prefs.dart';
import 'package:app/utils/ui_util.dart';
import 'package:app/utils/validation_util.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import 'login_screen_bloc.dart';

var mainContext;

class LoginScreenContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    mainContext = context;
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<LoginScreenBloc>(create: (_) => LoginScreenBloc()),
      ],
      child: LoginScreen(),
    );
  }
}

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends BasePageWidgetState<LoginScreen, LoginScreenBloc> {
  final _formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  onBuildCompleted() {
    if (mounted) bloc!.fetchCredentials();
  }

  @override
  List<Widget> getPageWidget() {
    return [
      LayoutBuilder(
        builder: (context, constraint) => Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: isMobileBrowser(constraint) ? constraint.maxWidth : 400,
                  padding: const EdgeInsets.all(20.0),
                  decoration: BoxDecoration(
                      color: ColorResource.colorWhite, borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: 10.0),
                      SvgPicture.asset('svg_img/gim_logo.svg', width: 36, height: 34),
                      Text(
                        'LOG IN',
                        style: GoogleFonts.roboto(
                          fontSize: 14.0,
                          fontWeight: FontWeight.w900,
                          color: ColorResource.colorMarineBlue,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 10.0),
                      Consumer<LoginScreenBloc>(
                        builder: (context, bloc, _) => Form(
                          key: _formKey,
                          child: AutofillGroup(
                            child: Column(
                              children: <Widget>[
                                TextEditText(
                                  labelText: 'EMAIL ID*',
                                  borderColor: ColorResource.colorBrownGrey,
                                  enableBorderColor: ColorResource.colorBrownGrey,
                                  textColor: ColorResource.colorBlack,
                                  labelTextColor: ColorResource.brownish_grey,
                                  keyboardType: TextInputType.emailAddress,
                                  maxlength: 40,
                                  behaveNormal: true,
                                  textEditingController: emailController..text = bloc.email,
                                  validationMessageRegexPair: {NOT_EMPTY_REGEX: 'Must not be empty'},
                                  onChanged: (text) => bloc.email = text,
                                  textCapitalization: TextCapitalization.none,
                                  autofillHints: const [AutofillHints.newUsername, AutofillHints.newPassword],
                                ),
                                SizedBox(height: 10.0),
                                TextEditText(
                                  obscured: true,
                                  behaveNormal: true,
                                  labelText: 'Password*',
                                  borderColor: ColorResource.colorBrownGrey,
                                  enableBorderColor: ColorResource.colorBrownGrey,
                                  textColor: ColorResource.colorBlack,
                                  textEditingController: passwordController..text = bloc.password,
                                  labelTextColor: ColorResource.brownish_grey,
                                  validationMessageRegexPair: {NOT_EMPTY_REGEX: 'Must not be empty'},
                                  onChanged: (text) => bloc.password = text,
                                  textCapitalization: TextCapitalization.none,
                                  keyboardType: TextInputType.visiblePassword,
                                  autofillHints: const [AutofillHints.password],
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      FilledColorButton(
                        fontSize: 15.0,
                        buttonText: 'Log In',
                        fontWeight: FontWeight.w500,
                        textColor: ColorResource.colorWhite,
                        backGroundColor: ColorResource.colorMarineBlue,
                        eventName: AnalyticsEvents.LOGIN_BTN_CLK,
                        onPressed: () async {
                          TextInput.finishAutofillContext();
                          await onLoginButtonPressed();
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      )
    ];
  }

  @override
  scaffoldBackgroundColor() => Colors.black;

  @override
  getContainerPadding() => EdgeInsets.all(10.0);

  @override
  getContainerDecoration() {
    return BoxDecoration(
        image: DecorationImage(
      fit: BoxFit.fill,
      image: AssetImage('images/bg_sign_in.webp'),
    ));
  }

  Future onLoginButtonPressed() async {
    hideSoftKeyboard(context);
    if (_formKey.currentState!.validate()) {
      submitDialog(context, dismissible: false);
      var response = await Provider.of<LoginScreenBloc>(context, listen: false).signIn();
      Navigator.pop(context);
      manipulateResponse(response);
    } else {
      _formKey.currentState!.reset();
    }
  }

  manipulateResponse(ApiResponse response) {
    switch (response.status) {
      case Status.LOADING:
        break;
      case Status.COMPLETED:
        navigate(response);
        break;
      case Status.ERROR:
        AnalyticsManager().logEventProp(
          AnalyticsEvents.LOGIN_FAILED,
          {UserProperties.USER_EMAIL: emailController.text},
        );
        showCustomSnackbar(response.message);
        break;
    }
  }

  navigate(response) async {
    await Prefs.setBoolean(Prefs.IS_LOGGED_IN, true);
    loadProjectListInBg();
    List<FeatureItem> items = bloc!.accessibleFeatures?.features();
    if (items.length == 1)
      navigateToScreen(context, items.first.pageUrl, pageName: items.first.eventName);
    else
      navigateNextScreen(context, ActivityOptionPage(bloc!.accessibleFeatures),
          pageName: AnalyticsEvents.ACC_FEATURE_SCREEN);
  }
}
