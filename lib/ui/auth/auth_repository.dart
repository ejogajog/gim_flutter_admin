import 'package:app/api/api_response.dart';

class AuthRepository {
  static var helper = ApiBaseHelper();

  static Future<ApiResponse> signIn(request) async {
    ApiResponse response = await helper.post("/ejogajog/api/v1/auth/adminLogIn", request);
    return response;
  }

  static Future<ApiResponse> signOut() async {
    ApiResponse response = await helper.get("/ejogajog/api/v2/common/logOut");
    return response;
  }

  static Future<ApiResponse> accessibleFeatures() async {
    ApiResponse response =
        await helper.get("/ejogajogAdminAPI/api/v1/flutter/permission");
    return response;
  }
}
