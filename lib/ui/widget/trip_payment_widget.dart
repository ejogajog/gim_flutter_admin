import 'package:flutter/material.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/styles.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/utils/color_const.dart';

class TripPaymentWidget extends StatefulWidget {
  final bool isEnterprise;
  final Function callback;
  final CustomerPreferences? presetInfo;

  TripPaymentWidget(this.isEnterprise, this.presetInfo, this.callback);

  @override
  _TripPaymentWidgetState createState() => _TripPaymentWidgetState();
}

class _TripPaymentWidgetState extends State<TripPaymentWidget> {
  var payTypePos = 0;
  var _choices = ['Cash', 'Invoice'];
  @override
  void initState() {
    payTypePos = widget.presetInfo?.tripDataSuggestionResponse?.paymentType ==
            null
        ? widget.isEnterprise
            ? 1
            : 0
        : widget.presetInfo?.tripDataSuggestionResponse?.paymentType == 'Cash'
            ? 0
            : 1;
    super.initState();
    widget.callback(_choices[payTypePos]);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 25.0),
        Text(localize('lbl_pay_type'), style: Styles.formLabel),
        SizedBox(height: 4.0),
        Column(
          children: [_buildTimeChoices()],
        ),
      ],
    );
  }

  Widget _buildTimeChoices() {
    return Container(
      height: 48,
      child: ListView.separated(
        scrollDirection: Axis.horizontal,
        itemCount: _choices.length,
        separatorBuilder: (BuildContext context, int index) =>
            SizedBox(width: 8.0),
        itemBuilder: (BuildContext context, int index) {
          return ChoiceChip(
              label: SizedBox(
                  width: 80,
                  child: Text(_choices[index],
                      style: TextStyle(fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center)),
              selected: payTypePos == index,
              selectedColor: Colors.blue,
              onSelected: (bool selected) {
                setState(() {
                  payTypePos = selected ? index : 0;
                  if (widget.callback != null)
                    widget.callback(_choices[payTypePos]);
                });
              },
              backgroundColor: ColorResource.grey_white,
              labelPadding: EdgeInsets.all(5.0),
              shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.all(Radius.circular(4.0))),
              labelStyle: TextStyle(
                  color: index == payTypePos
                      ? ColorResource.colorWhite
                      : ColorResource.colorPrimary),
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap);
        },
      ),
    );
  }
}
