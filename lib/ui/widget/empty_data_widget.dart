import 'package:flutter/material.dart';
import 'package:app/utils/color_const.dart';

class EmptyDataWidget extends StatelessWidget {
  final String? iconUrl;
  final String? message;

  const EmptyDataWidget({Key? key, this.iconUrl, this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          if(iconUrl != null)...[
            Padding(
              padding: const EdgeInsets.only(left: 50, right: 50, top: 50),
              child: AspectRatio(
                aspectRatio: 468 / 144,
                child: Image.asset(iconUrl!),
              ),
            ),
            SizedBox(height: 30),
          ],

          Text(
            '$message',
            style: TextStyle(
              fontSize: 18,
              color: ColorResource.semi_transparent_color_light,
            ),
          )
        ],
      ),
    );
  }
}