import 'package:flutter/material.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/utils/color_const.dart';

class CommentDialog extends StatefulWidget {
  final String title;

  CommentDialog(this.title);

  @override
  _CommentDialogState createState() => _CommentDialogState();
}

class _CommentDialogState extends State<CommentDialog> {
  TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: AlertDialog(
        title: Text(widget.title),
        content: Container(
          decoration: new BoxDecoration(
            shape: BoxShape.rectangle,
            border: new Border.all(
              color: Colors.black,
              width: 1.0,
            ),
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 4.0, right: 4.0, bottom: 4.0),
            child: TextField(
              decoration: new InputDecoration(
                border: InputBorder.none,
              ),
              style: TextStyle(color: ColorResource.primary_text),
              maxLines: 5,
              controller: _controller,
            ),
          ),
        ),
        actions: [
          ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side:
                            BorderSide(color: ColorResource.colorMarineBlue)))),
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                translate(context, 'cancel'),
                style: TextStyle(color: ColorResource.colorMarineBlue),
              ),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: ElevatedButton(
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(
                      ColorResource.colorMarineBlue),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(
                              color: ColorResource.colorMarineBlue)))),
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  translate(context, 'lbl_submit'),
                  style: TextStyle(color: Colors.white),
                ),
              ),
              onPressed: () {
                Navigator.pop(context, _controller.text);
              },
            ),
          ),
        ],
      ),
    );
  }
}
