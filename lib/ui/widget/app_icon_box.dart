import 'package:flutter/material.dart';
import 'package:app/ui/options/features_model.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/navigation_util.dart';
import 'package:google_fonts/google_fonts.dart';

class AppBoxIcon extends StatelessWidget {
  final FeatureItem? feature;

  AppBoxIcon(this.feature);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => navigateToScreen(context, feature?.pageUrl, pageName: feature?.eventName),
      child: Container(
        width: 160,
        height: 80,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          gradient: LinearGradient(
              colors: [
                const Color(0xFF43C6AC),
                const Color(0xFF165447),
              ],
              begin: const FractionalOffset(0.0, 0.0),
              end: const FractionalOffset(1.0, 0.0),
              stops: [0.0, 1.0],
              tileMode: TileMode.clamp),
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 10.0, top: 10.0),
          child: Text('${feature?.title}',
              style: GoogleFonts.roboto(
                  color: ColorResource.colorWhite,
                  fontSize: 14.0,
                  fontWeight: FontWeight.w700)),
        ),
      ),
    );
  }
}
