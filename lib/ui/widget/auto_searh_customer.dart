import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:app/ui/trip/create/repo/find_cust_repo.dart';
import 'package:app/ui/widget/form_field_validator.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/string_util.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class CustomerSearchWidget<T> extends StatefulWidget {
  String? value;
  String? labelText = "";
  String? hintText = "";
  final typeAheadController;
  List<T>? suggestionList = [];
  bool? immediateSuggestion;
  void Function(bool)? callBack;
  void Function(T)? onSuggestionItemSelected;
  var borderColor;
  bool? onlyForEnterprise;
  Map<String, String>? validationMessageRegexPair;

  CustomerSearchWidget(
      {Key? key,
      this.onSuggestionItemSelected,
      this.suggestionList,
      this.hintText,
      this.labelText,
      this.value,
      this.callBack,
      this.typeAheadController,
      this.immediateSuggestion = true,
      this.validationMessageRegexPair,
      this.onlyForEnterprise,
      this.borderColor = ColorResource.brownish_grey})
      : super(key: key);

  @override
  CustomerSearchWidgetState createState() => CustomerSearchWidgetState();
}

class CustomerSearchWidgetState extends State<CustomerSearchWidget> {
  TextFieldBloc? rootBloc;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((callback) {
      if (widget.validationMessageRegexPair != null)
        widget.typeAheadController?.addListener(() {
          if (!isNullOrEmpty(widget.typeAheadController.text))
            rootBloc?.currentText = widget.typeAheadController.text;
        });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final borderLayout = OutlineInputBorder(
      borderRadius: BorderRadius.circular(8.0),
      borderSide: BorderSide(
        color: widget.borderColor,
      ),
    );
    return ChangeNotifierProvider(
      create: (context) => TextFieldBloc(widget.validationMessageRegexPair!),
      child: Consumer<TextFieldBloc>(
        builder: (context, bloc, _) {
          rootBloc = bloc;
          return Container(
            margin: EdgeInsets.only(top: 10.0),
            child: TypeAheadFormField(
                hideOnLoading: true,
                hideOnEmpty: true,
                hideOnError: true,
                getImmediateSuggestions: widget.immediateSuggestion!,
                textFieldConfiguration: TextFieldConfiguration(
                  autofocus: false,
                  controller: widget.typeAheadController,
                  style: TextStyle(color: Colors.black, fontSize: 14.0, fontFamily: "roboto", fontWeight: FontWeight.w700),
                  decoration: InputDecoration(
                      enabledBorder: borderLayout,
                      border: borderLayout,
                      disabledBorder: borderLayout,
                      focusedBorder: borderLayout,
                      contentPadding: const EdgeInsets.only(
                          left: 15, top: 18, right: 10, bottom: 18),
                      hintText: widget.hintText),
                ),
                onSuggestionSelected: (dynamic suggestion) {
                  this.widget.typeAheadController.text = suggestion.toString();
                  this.widget.onSuggestionItemSelected!(suggestion);
                },
                itemBuilder: (context, dynamic suggestion) {
                  return Container(
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(color: ColorResource.light_grey),
                      ),
                    ),
                    child: ListTile(
                      dense: true,
                      contentPadding: EdgeInsets.fromLTRB(8.0, 4.0, 4.0, 4.0),
                      minLeadingWidth: 2.0,
                      title: Text(
                        suggestion.toString(),
                        style: GoogleFonts.roboto(
                            fontWeight: FontWeight.w300,
                            fontSize: 14.0,
                            color: ColorResource.colorMarineBlue),
                      ),
                    ),
                  );
                },
                suggestionsCallback: (pattern) async {
                  return await FindCustomerRepository.searchCustomer(pattern,widget.onlyForEnterprise! ? true : null);
                },
                transitionBuilder: (context, suggestionsBox, controller) {
                  return suggestionsBox;
                },
                validator: (value) {
                  if (widget.typeAheadController != null) {
                    bloc.currentText = widget.typeAheadController.text;
                  }
                  if (isNullOrEmpty(bloc.currentText)) {
                    bloc.currentText = "";
                  }
                  return bloc.errorText;
                }),
          );
        },
      ),
    );
  }
}

