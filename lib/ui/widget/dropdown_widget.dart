import 'package:flutter/material.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/image_util.dart';
import 'package:app/utils/string_util.dart';
import 'package:provider/provider.dart';

class DropDownWidget<T> extends StatefulWidget {
  List? dropDownItems = List.empty(growable: true);
  Function(T)? onItemSelcted;
  final labelTextColor;
  double? fontSize;
  final labelTextBgColor;
  String? labelText;
  final hintText;
  final textColor;
  final isOnlyRightBorder;
  T? selValue;
  EdgeInsets? padding;
  double? vMargin;
  var fillColor;
  var borderColor;
  final hintTextColor;

  DropDownWidget(
      {Key? key,
      this.dropDownItems,
      this.onItemSelcted,
      this.labelTextColor = ColorResource.brownish_grey,
      this.labelTextBgColor = Colors.white,
      this.labelText = "",
      this.fontSize = 14.0,
      this.hintText,
      this.selValue,
      this.fillColor = Colors.transparent,
      this.vMargin = 14,
      this.borderColor,
      this.textColor = Colors.black,
      this.hintTextColor = ColorResource.brownish_grey,
      this.isOnlyRightBorder = false,
      this.padding = const EdgeInsets.only(left: 8, top: 8, bottom: 8)})
      : super(key: key) {
    if (isNullOrEmptyList(dropDownItems)) dropDownItems = List.empty(growable: true);
  }

  @override
  State<StatefulWidget> createState() {
    return _DropDownWidget();
  }
}

class _DropDownWidget<T> extends State<DropDownWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var selValInd;
    if (widget.selValue != null) {
      selValInd = widget.dropDownItems!.indexWhere(
          (element) => element.toString().toLowerCase() == widget.selValue.toString().toLowerCase());
    }
    widget.borderColor = widget.borderColor ?? widget.fillColor;
    final double verticalMargin = widget.isOnlyRightBorder ? 0 : widget.vMargin!;
    return ChangeNotifierProvider<DropDownBloc<T>>(
      create: (context) => DropDownBloc<T>(
          (selValInd == null || selValInd == -1)
              ? null
              : widget.dropDownItems!.elementAt(selValInd)),
      child: Stack(
        children: <Widget>[
          Container(
            padding: widget.padding,
            margin: EdgeInsets.fromLTRB(0, verticalMargin, 0, verticalMargin),
            decoration: (widget.isOnlyRightBorder)
                ? BoxDecoration(
                    border: Border(
                      right: BorderSide(
                          width: 1.0, color: ColorResource.brownish_grey),
                    ),
                  )
                : BoxDecoration(
                    color: widget.fillColor,
                    borderRadius: BorderRadius.circular(2),
                    border: Border.all(color: widget.borderColor),
                  ),
            child: Consumer<DropDownBloc<T>>(
              builder: (context, bloc, child) => Center(
                child: DropdownButton<T>(
                  underline: Container(),
                  focusColor: widget.fillColor,
                  icon: Icon(
                    Icons.arrow_drop_down,
                    size: 24,
                    color: widget.textColor,
                  ),
                  hint: Text(widget.hintText ?? 'Please select',
                      style: TextStyle(
                          fontSize: 14.0,
                          fontFamily: "roboto",
                          color: widget.hintTextColor,
                          fontWeight: FontWeight.w700)),
                  style: TextStyle(
                      fontFamily: "roboto",
                      fontSize: 14.0,
                      fontWeight: FontWeight.w500,
                      color: widget.textColor),
                  isDense: true,
                  isExpanded: true,
                  value: bloc.dropdownValue,
                  onChanged: (T? newValue) {
                    bloc.dropdownValue = newValue;
                    widget.onItemSelcted!(newValue);
                  },
                  selectedItemBuilder: (context) => widget.dropDownItems!.map((value) {
                    return DropdownMenuItem<T>(
                      value: value,
                      child: Text(value.toString(),
                                maxLines: 1,
                                style: TextStyle(
                                    fontFamily: "roboto",
                                    fontSize: 12.0,
                                    color: widget.textColor))

                    );
                  }).toList(),
                  items: widget.dropDownItems!.map((value) {
                    return DropdownMenuItem<T>(
                      value: value,
                      child: Text(value.toString(),
                                maxLines: 1,
                                style: TextStyle(
                                    fontFamily: "roboto",
                                    fontSize: 12.0,
                                    color: ColorResource.colorBlack)));
                  }).toList(),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DropDownBloc<T> with ChangeNotifier {
  T? _dropdownValue;

  DropDownBloc(this._dropdownValue);

  T? get dropdownValue => _dropdownValue;

  set dropdownValue(T? value) {
    _dropdownValue = value;
    notifyListeners();
  }
}
