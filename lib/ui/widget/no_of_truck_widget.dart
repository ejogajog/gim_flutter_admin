import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/styles.dart';
import 'package:app/ui/trip/create/bloc/no_of_truck_bloc.dart';
import 'package:app/ui/trip/create/model/base_model.dart';
import 'package:app/ui/widget/auto_float_label_et.dart';
import 'package:app/ui/widget/counter_formatter.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/string_util.dart';
import 'package:provider/provider.dart';

class IncDecWidget extends StatelessWidget {
  final String? unit;
  final String? label;
  final List<DataInfo>? items;
  final Function? callback;
  final int? defaultNo;
  IncDecWidget(this.label, this.items, this.unit, {this.callback, this.defaultNo});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<IncDecBloc>(
            create: (_) => IncDecBloc(items, defaultNo, unit: unit, callback: callback)),
      ],
      child: NoOfTrucksWidget(label: label),
    );
  }
}

class NoOfTrucksWidget extends StatefulWidget {
  final String? label;

  NoOfTrucksWidget({this.label});

  @override
  _NoOfTrucksWidgetState createState() => _NoOfTrucksWidgetState();
}

class _NoOfTrucksWidgetState extends State<NoOfTrucksWidget> {

  IncDecBloc? _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = Provider.of<IncDecBloc>(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(height: 20.0),
        isNullOrEmpty(widget.label)
            ? SizedBox()
            : Text(
                localize(widget.label),
                style: Styles.formLabel,
              ),
        SizedBox(height: 5.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            OutlinedButton(
                onPressed: () => _bloc?.minusDataVal(),
                child: Icon(Icons.remove, size: 28, color: ColorResource.colorWhite),
                style: outlineButtonStyle),
            SizedBox(width: 8.0),
            Consumer<IncDecBloc>(builder: (context, bloc, _) {
              return Expanded(
                child: AutoFloatLabelTextField(
                  bloc.noOfValController,
                  textAlign: TextAlign.center,
                  textInputType: TextInputType.number,
                  inputFormatter: [
                    FilteringTextInputFormatter.digitsOnly,
                    LengthLimitingTextInputFormatter(3),
                    CounterValFormatter()
                  ],
                  callback: (text){
                    _bloc?.value = text;
                  },
                ),
              );
            }),
            SizedBox(width: 8.0),
            OutlinedButton(
                onPressed: () => _bloc?.plusDataVal(),
                child: Icon(Icons.add, size: 28, color: ColorResource.colorWhite),
                style: outlineButtonStyle),
          ],
        )
      ],
    );
  }

  var outlineButtonStyle = OutlinedButton.styleFrom(
    minimumSize: Size(80.0, 0.0),
    padding: EdgeInsets.fromLTRB(0.0, 14.0, 0.0, 14.0),
    backgroundColor: ColorResource.lightBlu,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(6.0),
    ),
    side: BorderSide(width: 2, color: ColorResource.lightBlu),
  );

  @override
  void dispose() {
    _bloc?.noOfValController?.dispose();
    super.dispose();
  }
}
