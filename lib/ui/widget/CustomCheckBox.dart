import 'package:flutter/material.dart';

class CustomCheckbox extends StatefulWidget {
  final String? label;
  final Function? onChange;
  final bool? isChecked;
  final double? size;
  final double? iconSize;
  final Color? selectedColor;
  final Color? selectedIconColor;
  final Color? borderColor;
  final Icon? checkIcon;

  CustomCheckbox(this.label,
      {this.isChecked,
      this.onChange,
      this.size,
      this.iconSize,
      this.selectedColor,
      this.selectedIconColor,
      this.borderColor,
      this.checkIcon});

  @override
  _CustomCheckboxState createState() => _CustomCheckboxState();
}

class _CustomCheckboxState extends State<CustomCheckbox> {
  bool _isSelected = false;

  @override
  void initState() {
    _isSelected = widget.isChecked ?? false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        GestureDetector(
          onTap: () {
            setState(() {
              _isSelected = !_isSelected;
              if (widget.onChange != null) widget.onChange!(_isSelected);
            });
          },
          child: AnimatedContainer(
            margin: EdgeInsets.all(4),
            duration: Duration(milliseconds: 500),
            curve: Curves.fastLinearToSlowEaseIn,
            decoration: BoxDecoration(
                color: _isSelected
                    ? widget.selectedColor ?? Colors.green
                    : Colors.transparent,
                borderRadius: BorderRadius.circular(3.0),
                border: Border.all(
                  color: widget.borderColor ?? Colors.green,
                  width: 1.5,
                )),
            width: widget.size ?? 24,
            height: widget.size ?? 24,
            child: _isSelected
                ? Icon(
                    Icons.check,
                    color: widget.selectedIconColor ?? Colors.white,
                    size: widget.iconSize ?? 18,
                  )
                : null,
          ),
        ),
        SizedBox(width: 5.0),
        Text(
          '${widget.label}',
          style: TextStyle(
            color: Colors.black,
            fontSize: 15,
            fontFamily: "roboto",
            fontWeight: FontWeight.w500,
          ),
        )
      ],
    );
  }
}
