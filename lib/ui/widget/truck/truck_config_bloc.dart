import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:app/ui/trip/create/model/base_model.dart';
import 'package:app/utils/string_util.dart';

class TruckConfigBloc extends ChangeNotifier {
  int _pos = 0;
  DataInfo? _value;
  List<DataInfo> item;
  Function? callback;
  final bool? enabled;
  var noOfValController = TextEditingController();

  TruckConfigBloc(this.item, defaultItem, this.enabled,{this.callback}) {
    _pos = item.indexOf(defaultItem);
    _setValue(sync: false);
  }

  plusDataVal() {
    if (_pos < (item.length - 1)) {
      ++_pos;
      _setValue();
    }
  }

  minusDataVal() {
    if (_pos > 0) {
      --_pos;
      _setValue();
    }
  }

  set value(String? text) {
    if (!isNullOrEmpty(text)) {
      var floor = double.tryParse(text!)?.floorToDouble();
      if (double.tryParse(text)?.ceil() != floor) {
        floor = floor! + .5;
      }
      _pos = item.indexWhere((element) => (element.toString() == floor.toString()) || (element.toString() == '${floor.toString()}.0'));
      if(_pos != -1){
        callback!(item[_pos]);
      }
    }
  }

  onFocusChange(focusNode) {
    if (!focusNode.hasFocus) _setValue(sync: false);
  }

  _setValue({sync = true}) {
    if(_pos > -1){
      _value = item[_pos];
      noOfValController.text = _value.toString();
      noOfValController.selection = TextSelection.fromPosition(
          TextPosition(offset: noOfValController.text.length));
      callback!(_value, sync:sync);
    }
  }

}
