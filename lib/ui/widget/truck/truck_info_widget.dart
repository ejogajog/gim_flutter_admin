import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:app/utils/app_constants.dart';
import 'package:provider/provider.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/model/master/truck_type.dart';
import 'package:app/ui/trip/create/model/truck_info.dart';
import 'package:app/ui/widget/truck/truck_info_bloc.dart';
import 'package:app/ui/widget/truck/truck_config_widget.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';

class TruckInfoWidget extends StatelessWidget {
  final Function? onEdit;
  final bool? isUsingTemp;
  final Function? callback;
  final TruckInfo? truckInfo;
  final CustomerPreferences? presetInfo;

  TruckInfoWidget(this.truckInfo, {this.callback, this.presetInfo, this.isUsingTemp = false, this.onEdit});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(providers: [
      ChangeNotifierProvider<TruckInfoBloc>(
          create: (_) => TruckInfoBloc(truckInfo, this.presetInfo,
              callback: callback, isUsingTemp: this.isUsingTemp, onEdit: this.onEdit))
    ], child: _TruckTypeWidget());
  }
}

class _TruckTypeWidget extends StatefulWidget {
  @override
  TruckTypeWidgetState createState() => TruckTypeWidgetState();
}

class TruckTypeWidgetState extends State<_TruckTypeWidget> {
  final _truckTypeScrollController = ScrollController();
  double _truckTypeScrollOffset = 0;
  TruckInfoBloc? bloc;

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<TruckInfoBloc>(context, listen: false);
    List<TruckType>? truckTypes = bloc?.truckTypes;
    return Column(
      children: [
        SizedBox(height: 20.0),
        Container(
            height: 110,
            padding: const EdgeInsets.only(top: 8, bottom: 6),
            decoration: BoxDecoration(
                border: Border.all(
                  color: ColorResource.brownish_grey,
                ),
                borderRadius: BorderRadius.all(Radius.circular(8)),
                color: ColorResource.colorWhite),
            child: Stack(
              children: [
                NotificationListener(
                  onNotification: (notification) {
                    if (notification is ScrollEndNotification) {
                      setState(() {
                        _truckTypeScrollOffset = _truckTypeScrollController.offset;
                      });
                    }
                    return false;
                  },
                  child: ListView.builder(
                    controller: _truckTypeScrollController,
                    itemCount: truckTypes?.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      TruckType? truckType = truckTypes?[index];
                      return InkWell(
                        onTap: () {
                          setState(() {
                            bloc!.truckType(index);
                          });
                        },
                        child: Container(
                          width: 120,
                          child: Column(
                            children: [
                              Stack(
                                alignment: AlignmentDirectional.topEnd,
                                children: [
                                  CachedNetworkImage(
                                    width: 70,
                                    height: 70,
                                    imageUrl: truckType?.colorImage ?? truckType?.image ?? "",
                                    errorWidget: (context, url, error) => Image.asset(
                                      'images/truck_placeholder.png',
                                      width: 70,
                                      height: 70,
                                    ),
                                  ),
                                  if (index == bloc?.selectedType) ...[
                                    SvgPicture.asset(
                                      'svg_img/green_check.svg',
                                      height: 28,
                                      width: 28,
                                    ),
                                  ],
                                ],
                              ),
                              Text(truckType?.text ?? '',
                                  textAlign: TextAlign.center,
                                  maxLines: 1,
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold,
                                      color: ColorResource.colorPrimaryDark)),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ),
                if (_truckTypeScrollOffset > 20) ...[
                  Positioned.fill(
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: CircleAvatar(
                        backgroundColor: Colors.black38,
                        child: IconButton(
                          onPressed: () {
                            _truckTypeScrollController.animateTo(
                              _truckTypeScrollOffset - 100,
                              duration: Duration(milliseconds: 500),
                              curve: Curves.fastLinearToSlowEaseIn,
                            );
                          },
                          icon: Icon(Icons.arrow_back_ios_rounded),
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
                if (_truckTypeScrollOffset == 0 ||
                    _truckTypeScrollOffset < _truckTypeScrollController.position.maxScrollExtent - 20) ...[
                  Positioned.fill(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: CircleAvatar(
                        backgroundColor: Colors.black38,
                        child: IconButton(
                          onPressed: () {
                            _truckTypeScrollController.animateTo(
                              _truckTypeScrollOffset + 100,
                              duration: Duration(milliseconds: 500),
                              curve: Curves.fastLinearToSlowEaseIn,
                            );
                          },
                          icon: Icon(Icons.arrow_forward_ios_rounded),
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ],
            )),
        SizedBox(height: 20.0),
        bloc!.prevSelTruck != bloc!.selectedType
            ? SizedBox()
            : Consumer<TruckInfoBloc>(builder: (context, bloc, child) => _buildDimen()),
        TruckConfigWidget('lbl_truck_size', bloc?.masterTruckSizes, bloc!.isEnabled,
            defItem: bloc!.defaultSize, callback: bloc!.truckSize),
        TruckConfigWidget('lbl_truck_len', bloc?.masterTruckLengths, bloc!.isEnabled,
            defItem: bloc!.defaultLen, callback: bloc!.truckLength),
      ],
    );
  }

  Widget _buildDimen() {
    return Wrap(
        spacing: 10.0,
        runSpacing: 10.0,
        children: List.generate(
            bloc!.truckSizes.length,
            (index) => ChoiceChip(
                label: SizedBox(
                  width: 120.0,
                  child: Text(
                    '${bloc!.truckSizes[index]} Ton, ${bloc!.truckLengths[index]} Feet',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: index == bloc!.selectedDimen ? ColorResource.colorWhite : ColorResource.colorPrimary,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                selected: bloc!.selectedDimen == index,
                selectedColor: Colors.blue,
                onSelected: (bool selected) {
                  setState(() {
                    bloc!.selectedDimen = selected ? index : 0;
                    bloc!.truckDimen = bloc!.selectedDimen;
                  });
                },
                backgroundColor: ColorResource.lightWhite,
                labelPadding: DimensionResource.chipPadding,
                shape: RoundedRectangleBorder(
                    side: BorderSide(color: ColorResource.light_grey),
                    borderRadius: BorderRadius.all(Radius.circular(4.0))),
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap)));
  }
}
