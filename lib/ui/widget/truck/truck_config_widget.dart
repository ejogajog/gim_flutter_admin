import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:app/styles.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/string_util.dart';
import 'package:app/ui/widget/auto_suggestion_widget.dart';
import 'package:app/ui/widget/decimal_formatter.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/ui/trip/create/model/base_model.dart';
import 'package:app/ui/widget/truck/truck_config_bloc.dart';

class TruckConfigWidget<T> extends StatelessWidget {
  final bool? enabled;
  final String? label;
  final DataInfo? defItem;
  final Function? callback;
  final List<DataInfo> items;

  TruckConfigWidget(this.label, this.items, this.enabled,
      {this.callback, this.defItem})
      : super(key: ValueKey('$enabled$label${defItem.toString()}'));

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<TruckConfigBloc>(
            create: (_) =>
                TruckConfigBloc(items, defItem, enabled, callback: callback)),
      ],
      child: ConfigWidget(label: label),
    );
  }
}

class ConfigWidget extends StatefulWidget {
  final String? label;

  ConfigWidget({this.label});

  @override
  _ConfigWidgetState createState() => _ConfigWidgetState();
}

class _ConfigWidgetState extends State<ConfigWidget> {
  TruckConfigBloc? _bloc;
  FocusNode _focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    _bloc = Provider.of<TruckConfigBloc>(context, listen: false);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(height: 20.0),
        Text(
          localize(widget.label),
          style: Styles.formLabel,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: OutlinedButton(
                  onPressed: () => _bloc?.minusDataVal(),
                  child: Icon(Icons.remove,
                      size: 28, color: ColorResource.colorWhite),
                  style: _outlineButtonStyle()),
            ),
            SizedBox(width: 8.0),
            Consumer<TruckConfigBloc>(builder: (context, bloc, _) {
              return Expanded(
                child: AutocompleteSuggestionWidget(
                  focusNode: _focusNode,
                  isEnabled: bloc.enabled,
                  suggestionList: _bloc?.item,
                  textAlign: TextAlign.center,
                  onFocusChange: _bloc?.onFocusChange,
                  textController: bloc.noOfValController,
                  suffixIcon: Icon(Icons.arrow_drop_down),
                  containFilter: false,
                  onTextChange: (text) {
                    _bloc?.value = text;
                  },
                  onSuggestionItemSelected: (item) {
                    _bloc?.value = item.toString();
                  },
                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                  inputFormatters: [
                    DecimalTextInputFormatter(maxVal: 40, decimalRange: 1),
                  ],
                  contentPad: EdgeInsets.only(left: 40, top: 20, right: 0, bottom: 20),
                ),
              );
            }),
            SizedBox(width: 8.0),
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: OutlinedButton(
                  onPressed: () => _bloc?.plusDataVal(),
                  child: Icon(Icons.add,
                      size: 28, color: ColorResource.colorWhite),
                  style: _outlineButtonStyle()),
            ),
          ],
        )
      ],
    );
  }

  _outlineButtonStyle() {
    var color = _bloc!.enabled! ? ColorResource.lightBlu : ColorResource.disBtn;
    return OutlinedButton.styleFrom(
        minimumSize: Size(80.0, 0.0),
        padding: EdgeInsets.fromLTRB(0.0, 14.0, 0.0, 14.0),
        backgroundColor: color,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(6.0),
        ),
        side: BorderSide(width: 2, color: color));
  }

}
