import 'package:app/bloc/base_bloc.dart';
import 'package:app/model/master/truck_size.dart';
import 'package:app/model/master/truck_type.dart';
import 'package:app/model/master/truck_dimension.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/ui/trip/create/model/truck_info.dart';
import 'package:app/utils/prefs.dart';
import 'package:app/utils/string_util.dart';

class TruckInfoBloc extends BaseBloc{
  Function? onEdit;
  bool? isUsingTemp;
  Function? callback;
  TruckInfo? _truckInfo;
  List<TruckSize> _presetTruckSizes = List<TruckSize>.empty(growable: true);
  List<TruckSize>? _masterTruckSizes;
  List<TruckType>? _masterTruckTypes;
  List<TruckDimensionLength> _presetTruckLengths = List<TruckDimensionLength>.empty(growable: true);
  List<TruckDimensionLength>? _masterTruckLengths;
  final Map<int, TruckDataSuggestions> _truckProp = {};

  int prevSelTruck = -1;
  int selectedType = -1;
  int selectedDimen = -1;

  CustomerPreferences? presetInfo;

  get truckTypes => _masterTruckTypes ?? [];

  get truckSizes => _presetTruckSizes;

  get masterTruckSizes => _masterTruckSizes ?? [];

  get truckLengths => _presetTruckLengths;

  get masterTruckLengths => _masterTruckLengths ?? [];

  TruckInfoBloc(this._truckInfo, this.presetInfo,
      {this.callback, this.isUsingTemp, this.onEdit}) {
    _masterTruckTypes = Prefs.getTrucksType();
    _masterTruckSizes = Prefs.getTrucksSizes();
    _masterTruckLengths = Prefs.getTrucksLength();
    _buildDataSet();
  }

  _buildDataSet() {
    if (presetInfo != null) {
      var tds = _defaultOrTempTds();
      if (tds == null) return;
      selectedType = _masterTruckTypes
          !.indexWhere((element) => element.id == tds.truckTypeId);
      if (selectedType != -1) {
        tds.build();
        prevSelTruck = selectedType;
        _truckProp[tds.truckTypeId] = tds;
        _presetTruckSizes = tds.sizeList;
        _presetTruckLengths = tds.lengthList;
        _truckType = truckTypes[selectedType];
        truckSize(truckSizes[selectedDimen = 0], sync: false);
        truckLength(truckLengths[selectedDimen], sync: false);
      }

      if (!isNullOrEmptyList(presetInfo?.truckDataSuggestions)) {
        presetInfo?.truckDataSuggestions?.forEach((element) {
          element.build();
          if (_truckProp.containsKey(element.truckTypeId)) {
            var trkSize = _truckProp[element.truckTypeId]?.sizeList[0];
            var trkLength = _truckProp[element.truckTypeId]?.lengthList[0];
            _truckProp[element.truckTypeId]?.lengthAndSizeResponses?.addAll(element.lengthAndSizeResponses!);
            _truckProp[element.truckTypeId]?.reBuild();
            _presetTruckSizes = element.sizeList;
            _presetTruckLengths = element.lengthList;
            for (int loop = 0; loop < _presetTruckSizes.length; loop++) {
              if (_presetTruckSizes[loop] == trkSize &&
                  _presetTruckLengths[loop] == trkLength) {
                _presetTruckSizes.removeAt(loop);
                _presetTruckLengths.removeAt(loop);
                _truckProp[element.truckTypeId]?.lengthAndSizeResponses?.removeAt(loop);
                _truckProp[element.truckTypeId]?.reBuild();
              }
            }
            _presetTruckSizes.insert(0, trkSize);
            _presetTruckLengths.insert(0, trkLength);
            return;
          }
          _truckProp[element.truckTypeId!] = element;
        });
      }
    }
  }

  _defaultOrTempTds() {
    var tds;
    if (isUsingTemp!) {
      tds = presetInfo?.tempTds;
      var defTds = presetInfo?.tripDataSuggestionResponse?.truckDataSuggestions;
      if (defTds != null) {
        int td = presetInfo!.truckDataSuggestions!.indexOf(defTds);
        if (td != -1) {
          var tdsObj = presetInfo!.truckDataSuggestions!.elementAt(td);
          if (tdsObj.lengthAndSizeResponses != null) {
            tdsObj.build();
            int length = tdsObj.sizeList.length;
            for (int loop = 0; loop < length; loop++) {
              if (tdsObj.sizeList[loop] == defTds.lengthAndSizeResponses?[0].truckSize &&
                  tdsObj.lengthList[loop] == defTds.lengthAndSizeResponses?[0].truckLength) {
                tdsObj.sizeList.removeAt(loop);
                tdsObj.lengthList.removeAt(loop);
              }
            }
          }
          if (tdsObj.lengthAndSizeResponses == null) tdsObj.lengthAndSizeResponses = [];
          tdsObj.lengthAndSizeResponses!.insert(0,TruckProp(
              truckSizeId: defTds.lengthAndSizeResponses?[0].truckSizeId,
              truckSize: defTds.lengthAndSizeResponses?[0].truckSize,
              truckLength: defTds.lengthAndSizeResponses?[0].truckLength));
          tdsObj.reBuild();
        } else
          presetInfo?.truckDataSuggestions!.add(defTds);
      }
    } else
      tds = presetInfo?.tripDataSuggestionResponse?.truckDataSuggestions;
    return tds;
  }

  set _truckType(item) {
    _truckInfo?.type = item;
    callback!(_truckInfo);
    var tds = item == null ? null : _truckProp[item.id];
    if (tds == null || tds.lengthAndSizeResponses == null) {
      _presetTruckSizes = [];
      _presetTruckLengths = [];
    } else {
      _presetTruckSizes = tds.sizeList;
      _presetTruckLengths = tds.lengthList;
    }
  }

  truckType(index){
    index = prevSelTruck == index ? -1 : index;
    prevSelTruck = index;
    selectedType = index;
    _truckType = index > -1 ? truckTypes[selectedType] : null;
    _resetDimen();
    onEdit?.call();
  }

  truckSize(item, {sync =true}) {
    _truckInfo?.size = item;
    callback!(_truckInfo);
    if(sync){
      selectedDimen = -1;
      notifyListeners();
      onEdit?.call();
    }
  }

  truckLength(item, {sync =true}) {
    _truckInfo?.length = item;
    callback!(_truckInfo);
    if(sync){
      selectedDimen = -1;
      notifyListeners();
      onEdit?.call();
    }
  }

  _resetDimen() {
    selectedDimen = -1;
    _truckInfo?.size = masterTruckSizes[23];
    _truckInfo?.length = masterTruckLengths[36];
  }

  set truckDimen(index) {
    _truckInfo?.size = _presetTruckSizes[index];
    _truckInfo?.length = _presetTruckLengths[index];
    callback!(_truckInfo);
    onEdit?.call();
  }

  truckInfo() {
    var truckInfo = TruckInfo();
    truckInfo.type = _truckInfo?.type;
    return truckInfo;
  }

  get isEnabled => selectedType != -1;

  get defaultSize => _truckInfo?.size ?? masterTruckSizes[23];
  get defaultLen => _truckInfo?.length ?? masterTruckLengths[36];

  get emptyPresets => presetInfo == null;

}