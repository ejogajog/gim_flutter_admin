import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:app/bloc/base_bloc.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/ui_util.dart';
import 'package:provider/provider.dart';

class BaseItemView<T> extends StatelessWidget{
  T? item;
  Function? onItemClick;
  int? position;
  BaseItemView({Key? key,this.item,this.onItemClick,this.position}):super(key:key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

Widget networkErrorBuilder(BuildContext context) {
  var width=MediaQuery.of(context).size.width;
  var statusBarWidth=MediaQuery.of(context).padding.top;
  return  Positioned(
    top: statusBarWidth,
    left: 0.0,
    child: new Material(
        color: ColorResource.pink_red,
        child: Container(height:kToolbarHeight,width:width,
            child: Center(child: Text('ERROR: Network connection unavailable',style: TextStyle(color: ColorResource.colorWhite),)))
    ),
  );
}
Widget networkErrorWithRetry(BuildContext context,VoidCallback onPressed,{msg}){
  var width=MediaQuery.of(context).size.width;
  var height=MediaQuery.of(context).size.height;
  return Container(
    width: width,
    height: height,
    color: Colors.white,
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SvgPicture.asset('svg_img/ic_warning.svg',width: 50,),
        SizedBox(height: 10,),
        Text(msg?? 'SOMETHING WENT WRONG'.toUpperCase(),style:
        TextStyle(color:HexColor("e93e53",),fontSize: 14.0 ),),
        SizedBox(height: 10,),
        ElevatedButton(
          onPressed:(){onPressed();},
          child: Text(localize('retry').toUpperCase(),style: TextStyle(color: ColorResource.colorMariGold,fontSize: 16),),
          style: ElevatedButton.styleFrom(backgroundColor: ColorResource.colorMarineBlue, padding: EdgeInsets.only(left:8.0,right: 8.0,top: 15.0,bottom: 15.0)),
        )
      ],
    ),
  );
}
abstract class BasePageWidgetState<T extends StatefulWidget,V extends BaseBloc> extends State<T> with WidgetsBindingObserver{
  final scaffoldState=GlobalKey<ScaffoldState>();
  bool isOffline = false;
  OverlayEntry? myOverlay;
  V? bloc;

  @override
  initState() {
    super.initState();
    bloc = Provider.of<V>(context, listen: false);
    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance.addPostFrameCallback((_) =>onBuildCompleted());
  }
  onBuildCompleted()=>null;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    didChangeLifeCycle(state);
    // print("State:"+state.toString());
  }
  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
  @override
  Widget build(BuildContext context){
    beforeBuildStarted();
    return  GestureDetector(
      onTap: ()=>hideSoftKeyboard(context),
      child: Scaffold(
        key: scaffoldState,
        appBar: getAppbar(),
        resizeToAvoidBottomInset: isResizeToBottomInset(),
        backgroundColor:scaffoldBackgroundColor() ,
        bottomNavigationBar: getBottomNaviagtionBar(),
        drawer: getNavigationDrawer(),
        floatingActionButton: getFloatingActionButton(),
        body: SafeArea(
          child: WillPopScope(
              onWillPop:onWillPop ,
              child: Container(
                padding: getContainerPadding(),
                height: double.maxFinite,
                width: double.infinity,
                decoration: getContainerDecoration(),
                child: Stack(
                    children: [
                      ...getPageWidget(),
                      Consumer<V>(
                          builder: (context,bloc,_)=> Visibility(
                            child: networkErrorWithRetry(context,onRetryClick,msg: bloc.errorMessage),
                            visible: bloc.shouldShowErrorPage,)),
                    ]
                ),
              ),
            ),
        ),
      ),
    );
  }
  beforeBuildStarted(){
    //print("build:"+this.runtimeType.toString());
  }
  getContainerPadding()=>EdgeInsets.all(0.0);
  getContainerDecoration()=>null;
  Future<bool> onWillPop()async=> true;
  List<Widget> getPageWidget()=>[Container()];
  getAppbar()=>null;
  isResizeToBottomInset()=>false;
  scaffoldBackgroundColor()=>Colors.white;
  Widget? getBottomNaviagtionBar()=>null;
  Widget? getNavigationDrawer()=>null;
  Widget? getFloatingActionButton()=>null;
  onRetryClick(){
    bloc?.shouldShowErrorPage=false;
  }
  didChangeLifeCycle(AppLifecycleState state) => null;
}

