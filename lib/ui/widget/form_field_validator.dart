import 'package:flutter/foundation.dart';

class TextFieldBloc with ChangeNotifier {
  String? _currentText = "";
  String? _errorText;

  Map<String, String?>? validationMessageRegexPair;

  get errorText => _errorText;

  set errorText(value) {
    _errorText = value;
  }

  TextFieldBloc(this.validationMessageRegexPair);

  get currentText => _currentText;

  set currentText(value) {
    _currentText = value;
    bool isValid = true;
    if (validationMessageRegexPair != null) {
      for (String key in validationMessageRegexPair!.keys) {
        RegExp regex = RegExp(key);
        if (!regex.hasMatch(value)) {
          isValid = false;
          _errorText = validationMessageRegexPair?[key];
          notifyListeners();
          break;
        }
      }
      if (isValid) {
        removeErrorMsg();
      }
    }
  }

  removeErrorMsg() {
    if (_errorText != null && _errorText!.isNotEmpty) {
      _errorText = null;
      notifyListeners();
    }
  }
}