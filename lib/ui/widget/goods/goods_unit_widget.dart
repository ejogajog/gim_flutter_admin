import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/model/master/goods_unit.dart';
import 'package:app/styles.dart';
import 'package:app/ui/widget/editfield.dart';
import 'package:app/ui/widget/goods/goods_unit_bloc.dart';
import 'package:app/utils/color_const.dart';
import 'package:provider/provider.dart';

class GoodsUnitWidget extends StatelessWidget {
  final double? defQntValue;
  final bool showGoodsQty;
  final Function goodsQnt;
  final GoodsUnit? defQntUnit;
  final Function goodsUnit;
  final Function? notifyIns;

  GoodsUnitWidget(
    this.goodsQnt,
    this.goodsUnit, {
    this.showGoodsQty = true,
    this.defQntUnit,
    this.defQntValue,
    this.notifyIns,
  });

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<GoodsUnitBloc>(
          create: (_) => GoodsUnitBloc(goodsQnt, goodsUnit,
              selUnit: this.defQntUnit, defQntVal: this.defQntValue, showGoodsQty: showGoodsQty, notifyIns: notifyIns),
        ),
      ],
      child: GoodsTypeUnitWidget(),
    );
  }
}

class GoodsTypeUnitWidget extends StatefulWidget {
  @override
  GoodsTypeUnitWidgetState createState() => GoodsTypeUnitWidgetState();
}

class GoodsTypeUnitWidgetState extends State<GoodsTypeUnitWidget> {
  GoodsUnitBloc? bloc;

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      bloc = Provider.of<GoodsUnitBloc>(context, listen: false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(height: 20.0),
        Text(localize('goods_qnt_unit'), style: Styles.formLabel),
        Consumer<GoodsUnitBloc>(
          builder: (context, bloc, child) => bloc.showGoodsQty
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: TextEditText(
                        labelText: '',
                        radius: 2.0,
                        textEditingController: bloc.qntValController,
                        textAlign: TextAlign.center,
                        behaveNormal: true,
                        fontWeight: FontWeight.w700,
                        borderColor: ColorResource.brownish_grey,
                        onChanged: (text) {
                          bloc.onQntChange(text);
                          if (bloc.notifyIns != null) {
                            bloc.notifyIns!();
                          }
                        },
                        maxlength: 5,
                        verticalPad: EdgeInsets.all(10.0),
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.digitsOnly,
                          LengthLimitingTextInputFormatter(99999),
                        ],
                      ),
                    ),
                    SizedBox(width: 20.0),
                    Expanded(
                      child: TextEditText(
                        labelText: '',
                        fontSize: 12,
                        isDropDown: true,
                        selValue: bloc.unit,
                        borderColor: ColorResource.brownish_grey,
                        suggestionDropDownItemList: Provider.of<GoodsUnitBloc>(context, listen: false).units,
                        searchDrodownCallback: (item) {
                          bloc.onUnitChange(item);
                          if (bloc.notifyIns != null) {
                            bloc.notifyIns!();
                          }
                        },
                      ),
                    )
                  ],
                )
              : TextEditText(
                  labelText: '',
                  fontSize: 12,
                  isDropDown: true,
                  selValue: bloc.unit,
                  borderColor: ColorResource.brownish_grey,
                  suggestionDropDownItemList: Provider.of<GoodsUnitBloc>(context, listen: false).units,
                  searchDrodownCallback: (item) {
                    bloc.onUnitChange(item);
                    if (bloc.notifyIns != null) bloc.notifyIns!();
                  },
                ),
        ),
      ],
    );
  }
}
