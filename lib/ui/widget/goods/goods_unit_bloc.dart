import 'package:flutter/cupertino.dart';
import 'package:app/bloc/base_bloc.dart';
import 'package:app/model/master/goods_unit.dart';
import 'package:app/utils/string_util.dart';

class GoodsUnitBloc extends BaseBloc{
  double? defQntVal;
  GoodsUnit? selUnit;
  bool showGoodsQty;
  final Function goodsQnt;
  final Function goodsUnit;
  final Function? notifyIns;
  final qntValController = TextEditingController();

  Map<String,dynamic> _units = {'Kg': GoodsUnit('KG','Kg','কেজি'),'Tons': GoodsUnit('TON','Tons','টন'),'Bags': GoodsUnit('BAGS','Bags','ব্যাগ'),'Cartons': GoodsUnit('CARTON','Cartons','কার্টন'),'Pieces': GoodsUnit('PCS','Pieces','পিস'),'CBM': GoodsUnit('CBM','CBM','সিবিএম')};

  GoodsUnitBloc(this.goodsQnt, this.goodsUnit, {this.selUnit, this.defQntVal,this.showGoodsQty = true, this.notifyIns}){
    _setDefQntVal();
  }

  get defQntValue => isNullOrEmptyDouble(defQntVal) ? '' : defQntVal.toString();

  get units => _units.keys.toList();

  get unit => selUnit?.name;

  onQntChange(value){
    goodsQnt(value);
  }

  onUnitChange(unit){
    goodsUnit(_units[unit].id);
  }

  _setDefQntVal(){
    qntValController.text = defQntValue;
    if(selUnit != null){
      goodsUnit(selUnit!.id);
    }
    notifyListeners();
  }
}