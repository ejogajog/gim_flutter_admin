import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/model/project/project.dart';
import 'package:app/ui/widget/CustomCheckBox.dart';
import 'package:app/ui/widget/editfield.dart';
import 'package:app/utils/prefs.dart';
import 'package:app/ui/widget/app_button.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/validation_util.dart';

class ProjectWidget extends StatefulWidget {
  final Function? onProject;
  final Function? onExpRate;
  final Function? createReason;
  final Function? onCreateTrip;
  ProjectWidget(this.onProject,this.onExpRate, this.createReason, this.onCreateTrip);
  @override
  ProjectWidgetState createState() => ProjectWidgetState();
}

class ProjectWidgetState extends State<ProjectWidget> {
  bool isProject = false;
  var _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(16.0,10,16,10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              CustomCheckbox(localize('project'), onChange: (val) {
                setState(() {
                  isProject = val;
                  widget.onProject!(0);
                });
              }),
              TextEditText(
                labelText: '',
                isDropDown: true,
                behaveNormal: true,
                isFilterWidget: true,
                isEnabled: isProject,
                suffixIcon: Icon(Icons.arrow_drop_down_outlined, color: ColorResource.colorMarineBlue),
                fontWeight: FontWeight.w300,
                isSearchableItemWidget: true,
                suggestionDropDownItemList: Prefs.getProjects(),
                onChanged: (data){
                  widget.onProject!(0);
                },
                searchDrodownCallback: (Projects? item){
                  widget.onProject!(item?.id);
                },
              ),
              SizedBox(height: 20),
              Text(localize('text_eta'),
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      color: ColorResource.colorMarineBlue,
                      fontSize: 14.0,
                      fontWeight: FontWeight.bold,
                      decoration: TextDecoration.none)),
              TextEditText(
                labelText: '',
                textAlign: TextAlign.center,
                behaveNormal: true,
                borderColor: ColorResource.platinum,
                keyboardType: TextInputType.number,
                onChanged: (text) {
                  widget.onExpRate!(text);
                },
                maxlength: 8,
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly,
                  LengthLimitingTextInputFormatter(8),
                ],
              ),
              SizedBox(height: 10),
              RichText(
                text: TextSpan(
                    text: 'Comment',
                    style: TextStyle(
                        color: ColorResource.colorMarineBlue,
                        fontWeight: FontWeight.bold,
                        fontSize: 14.0),
                    children: [
                      TextSpan(
                          text: ' *',
                          style: TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                              fontSize: 14.0))
                    ]),
              ),
              TextEditText(
                labelText: '',
                behaveNormal: true,
                borderColor: ColorResource.platinum,
                onChanged: (text) {
                  widget.createReason!(text);
                },
                maxLines: 5,
                maxlength: 150,
                showCounter: true,
                verticalPad: EdgeInsets.all(10.0),
                inputFormatters: <TextInputFormatter>[
                  LengthLimitingTextInputFormatter(150),
                ],
                validationMessageRegexPair: {
                  NOT_EMPTY_REGEX: translate(context, 'mne_val_msg')
                },
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  FilledColorButton(
                    fontSize : 16,
                    buttonText: localize('cancel'),
                    isFullwidth: false,
                    padding: EdgeInsets.fromLTRB(16, 20, 16, 20),
                    textColor: ColorResource.colorMarineBlue,
                    backGroundColor: ColorResource.CreamWhite,
                    onPressed: () async {
                      widget.onProject!(0);
                      widget.onExpRate!('0');
                      widget.createReason!('');
                      Navigator.pop(context, true);
                    },
                  ),
                  SizedBox(width: 2.0),
                  FilledColorButton(
                    fontSize : 16,
                    buttonText: localize('create_&_approve'),
                    isFullwidth: false,
                    padding: EdgeInsets.fromLTRB(16, 20, 16, 20),
                    textColor: ColorResource.colorWhite,
                    backGroundColor: ColorResource.colorMarineBlue,
                    onPressed: () async {
                      if(_formKey.currentState!.validate()){
                        Navigator.pop(context, true);
                        widget.onCreateTrip!();
                      }
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
