import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/styles.dart';
import 'package:app/ui/trip/create/bloc/trip_map_bloc.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/ui/widget/auto_search_location.dart';
import 'package:app/ui/widget/editfield.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/string_util.dart';
import 'package:app/utils/ui_util.dart';
import 'package:app/utils/validation_util.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

class LocationWidget extends StatelessWidget {
  final bool? isMultiple;
  final Function? callback;
  final Function? locations;
  final CustomerPreferences? presetInfo;

  LocationWidget(this.callback, this.presetInfo,
      {this.isMultiple = false, this.locations});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<TripMapBloc>(
          create: (_) => TripMapBloc(callback, isMultiple, presetInfo,
              locations: locations),
        ),
      ],
      child: _SetLocationWidget(),
    );
  }
}

class _SetLocationWidget extends StatefulWidget {
  @override
  _SetTripLocationWidgetState createState() => _SetTripLocationWidgetState();
}

class _SetTripLocationWidgetState extends State<_SetLocationWidget> {
  double mapHeight = 240;
  double iconSize = 24;
  TripMapBloc? bloc;
  bool isShown = false;

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<TripMapBloc>(context, listen: false);
    return Consumer<TripMapBloc>(
        builder: (context, bloc, _) => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                    localize(bloc.isMultiple! ? 'set_drop_loc' : 'set_pick_loc'),
                    style: Styles.formLabel),
                SizedBox(height: 5.0),
                Wrap(
                  children: [
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        bloc.isMultiple!
                            ? ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: bloc.listOfLocations.length,
                                itemBuilder: (context, index) => Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                        bloc.listOfLocations[index].name,
                                        style: Styles.selMedTextBold),
                                    TextEditText(
                                      labelText: '',
                                      readOnly: true,
                                      suffixIcon: InkWell(
                                          child: Icon(Icons.clear,
                                              color:
                                                  ColorResource.brownish_grey,
                                              size: 18.0),
                                          onTap: () => bloc
                                              .handleMultipleLocation(index)),
                                      textEditingController:
                                          TextEditingController(
                                              text: bloc.listOfLocations[index]
                                                      .toString() ??
                                                  ''),
                                      behaveNormal: true,
                                    ),
                                  ],
                                ),
                              )
                            : SizedBox(),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 5.0),
                          child: Text(bloc.selLblAddress,
                              style: Styles.selMedTextBold),
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: GooglePlacesSearchWidget(
                                isSrc: true,
                                companyId: bloc.companyId,
                                focusNode: bloc.focusNode,
                                callBack: _onFocus,
                                immediateSuggestion: false,
                                suffixIcon:
                                    bloc.isEditing ? Icons.check : Icons.close,
                                hintText: translate(
                                    context,
                                    bloc.isMultiple!
                                        ? 'eda_or_dp'
                                        : 'epa_or_dp'),
                                typeAheadController: bloc.addressController,
                                onSuggestionItemSelected: (dynamic value) {
                                  if (bloc.isDuplicateUnloadPoint(value)) {
                                    showToast(localize('dst_ads_exist'));
                                    return;
                                  }
                                  bloc.getPosUsingPlaceId(value);
                                  AnalyticsManager().logEventProp(
                                      bloc.isMultiple!
                                          ? AnalyticsEvents
                                              .UNLOADING_POINT_ITEM_CLK
                                          : AnalyticsEvents
                                              .LOADING_POINT_ITEM_CLK,
                                      {
                                        AnalyticProperties.ADDRESS:
                                            value.toString()
                                      });
                                },
                                validationMessageRegexPair: {
                                  NOT_EMPTY_REGEX:
                                      translate(context, 'mne_val_msg')
                                },
                                onChange: (data, {isEditing = true}) {
                                  bloc.onChange(data, isEditing: isEditing);
                                },
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 2.0),
                        Visibility(
                          visible: isShown,
                          child: LayoutBuilder(
                              builder: (context, constraint) => Stack(
                                    children: [
                                      GestureDetector(
                                        onHorizontalDragStart: (start) {
                                          bloc.adrInd = -1;
                                          bloc.flagUpdateOnDrag = true;
                                        },
                                        onVerticalDragStart: (start) {
                                          bloc.adrInd = -1;
                                          bloc.flagUpdateOnDrag = true;
                                        },
                                        child: Container(
                                          height: mapHeight,
                                          child: GoogleMap(
                                            onMapCreated: bloc.onCreated,
                                            initialCameraPosition:
                                                CameraPosition(
                                              target: bloc.markerPosition,
                                              zoom: 12.0,
                                            ),
                                            onCameraMove: bloc.onCameraMove,
                                            onCameraIdle: bloc.onCameraIdle,
                                            gestureRecognizers: <
                                                Factory<
                                                    OneSequenceGestureRecognizer>>[
                                              Factory<
                                                  OneSequenceGestureRecognizer>(
                                                () => EagerGestureRecognizer(),
                                              ),
                                            ].toSet(),
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                        top: (mapHeight - iconSize) / 2,
                                        right: isMobileBrowser(constraint)
                                            ? (constraint.maxWidth - iconSize) /
                                                2
                                            : (containerWidth -
                                                    (iconSize +
                                                        (2 *
                                                            horizontalPadding))) /
                                                2,
                                        child: Image.asset(
                                          bloc.isMultiple!
                                              ? "images/dropoff_pin.png"
                                              : "images/pickup_pin.png",
                                          height: 28,
                                          width: 24,
                                        ),
                                      )
                                    ],
                                  )),
                        ),
                        bloc.isMultiple!
                            ? Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  RichText(
                                    text: new TextSpan(
                                      children: [
                                        TextSpan(
                                          text:
                                              isShown ? "Hide map" : "Show map",
                                          style: TextStyle(
                                              color: Colors.blue,
                                              decoration:
                                                  TextDecoration.underline),
                                          recognizer: new TapGestureRecognizer()
                                            ..onTap = () {
                                              setState(() {
                                                isShown = !isShown;
                                              });
                                            },
                                        ),
                                      ],
                                    ),
                                  ),
                                  Tooltip(
                                    message:
                                        'Please provide the unloading point address',
                                    child: RichText(
                                      text: TextSpan(
                                        children: [
                                          TextSpan(
                                            text: "Add another unloading point",
                                            style: TextStyle(
                                                color: bloc.canAddAnoUnlPnt
                                                    ? ColorResource
                                                        .colorLightBlueGrey
                                                    : Colors.blue,
                                                decoration:
                                                    TextDecoration.underline),
                                            recognizer:
                                                new TapGestureRecognizer()
                                                  ..onTap = () {
                                                    if (bloc.canAddAnoUnlPnt)
                                                      return;
                                                    if (isNullOrEmpty(bloc
                                                        .addressController
                                                        .text)) {
                                                      showToast(localize(
                                                          'dst_ads_empty'));
                                                      return;
                                                    }
                                                    isShown = false;
                                                    bloc.handleMultipleLocation(
                                                        -1);
                                                    AnalyticsManager().logEvent(
                                                        AnalyticsEvents
                                                            .ADD_UNLOAD_POINT_BTN_CLK);
                                                  },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            : RichText(
                                text: new TextSpan(
                                  children: [
                                    TextSpan(
                                      text: isShown ? "Hide Map" : "Show Map",
                                      style: TextStyle(
                                          color: Colors.blue,
                                          decoration: TextDecoration.underline),
                                      recognizer: new TapGestureRecognizer()
                                        ..onTap = () {
                                          setState(() {
                                            isShown = !isShown;
                                          });
                                        },
                                    ),
                                  ],
                                ),
                              ),
                      ],
                    ),
                    Wrap(
                      spacing: 10.0,
                      children: List.generate(
                          bloc.addressList.length,
                          (index) => Padding(
                                padding: const EdgeInsets.only(top: 10.0),
                                child: ChipTheme(
                                    data: ChipTheme.of(context).copyWith(
                                        pressElevation: 10.0,
                                        selectedShadowColor: Colors.blue),
                                    child: ChoiceChip(
                                        label: Text(
                                            bloc.addressList[index].toString(),
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                color: index ==
                                                        bloc.selAddressIndex
                                                    ? ColorResource.colorWhite
                                                    : ColorResource
                                                        .colorPrimary,
                                                fontWeight: FontWeight.w500)),
                                        selected: bloc.selAddressIndex == index,
                                        selectedColor: Colors.blue,
                                        onSelected: (bool selected) {
                                          setState(() {
                                            if (bloc.isDuplicateUnloadPoint(
                                                bloc.addressList[index])) {
                                              bloc.selAddressIndex = -1;
                                              showToast(
                                                  localize('dst_ads_exist'));
                                              return;
                                            }
                                            bloc.isEditing = false;
                                            bloc.selAddressIndex = index;
                                            bloc.selPresetAddress =
                                                bloc.selAddressIndex;
                                            AnalyticsManager().logEventProp(
                                                bloc.isMultiple!
                                                    ? AnalyticsEvents
                                                        .UNLOADING_POINT_ITEM_CLK
                                                    : AnalyticsEvents.LOADING_POINT_ITEM_CLK,
                                                {
                                                  AnalyticProperties.ADDRESS:
                                                      bloc.selAddress.toString()
                                                });
                                          });
                                        },
                                        backgroundColor:
                                            ColorResource.grey_white,
                                        labelPadding:
                                            DimensionResource.chipPadding,
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(4.0))))),
                              )),
                    ),
                  ],
                ),
                SizedBox(height: 3.0),
              ],
            ));
  }

  _onFocus(FocusNode focusNode) {
    bloc?.modAddress(focusNode.hasFocus);
  }
}
