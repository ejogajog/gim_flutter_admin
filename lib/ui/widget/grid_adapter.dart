import 'package:flutter/material.dart';
import 'package:app/bloc/pagination_bloc.dart';
import 'package:app/ui/widget/base_widget.dart';
import 'package:app/utils/color_const.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class GridViewBuilder<T> extends StatefulWidget{
  List<T>? itemList;
  Function? onItemPressed;
  VoidCallback? loadMoreItem;
  BaseItemView<T> Function()? widgetType=()=>BaseItemView<T>();
  int? totalPage;
  int? currentPage;
  double? mainAxisExtent;
  LoadStatus? status;
  VoidCallback? onRefresh;
  final dividerColor;
  final totalNoOfItems;
  final bool? shouldKeepDefaultDivider;
  GridViewBuilder({Key? key,
    this.itemList,
    this.widgetType,
    this.onItemPressed,
    this.loadMoreItem,
    this.currentPage=1,
    this.totalPage=1,
    this.onRefresh,
    this.mainAxisExtent,
    this.totalNoOfItems,
    this.dividerColor=ColorResource.divider_color,
    this.shouldKeepDefaultDivider=false,
    this.status=LoadStatus.normal}):super(key:key){
  }

  @override
  State<StatefulWidget> createState() {
    return _GridViewBuilderState();
  }

}

class _GridViewBuilderState extends  State<GridViewBuilder>{
  ScrollController? controller;
  RefreshController? _refreshController;
  @override
  void initState() {
    super.initState();
    _refreshController = RefreshController(initialRefresh: false);
  }

  @override
  void dispose() {
    super.dispose();
    _refreshController?.dispose();
  }

  bool shouldLoadMore(){
    return widget.currentPage! < widget.totalPage!;
  }
  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      enablePullUp: true,
      footer: shouldLoadMore()? ClassicFooter(loadStyle: LoadStyle.HideAlways):CustomFooter( height:0,loadStyle: LoadStyle.HideAlways,builder:(context,mode)=>Text('')),
      onRefresh:()async{
        widget.status=LoadStatus.loading;
        if(widget.onRefresh != null)
          widget.onRefresh!();
        _refreshController?.refreshCompleted(resetFooterState: true);
      },
      onLoading:() {
        if(shouldLoadMore() && widget.loadMoreItem != null && widget.status != LoadStatus.loading) {
          widget.status = LoadStatus.loading;
          widget.loadMoreItem!();
        }
        _refreshController?.loadComplete();
      },
      controller: _refreshController!,
      child: GridView.builder(
          shrinkWrap: true,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2, mainAxisExtent: widget.mainAxisExtent),
          itemCount: widget.itemList!.length,
          controller: controller,
          itemBuilder:(context,index){
            if(index >= widget.totalNoOfItems){
              return Container();
            }
            final item = widget.itemList![index];
            BaseItemView child=widget.widgetType!();
            child.item=item;
            child.position=index;
            child.onItemClick=widget.onItemPressed;
            return child;
          }
      ),
    );
  }

}

enum LoadStatus {
  normal,
  error,
  loading,
  completed,
}

getGridList<T extends PaginationBloc>(T paginationBloc, itemWidget,
    {onItemClicked,mainAxisExtent,apiCallback,isSingleParameterCallback=true,shouldKeepDefaultDivider=false}) {
  return Expanded(
    child: Selector<T, int>(
        selector: (context, bloc) => bloc.itemList.length,
        builder: (context, list, _) =>
            GridViewBuilder(
              onRefresh: () {
                paginationBloc.reloadList(callback: apiCallback);
              },
              itemList: paginationBloc.itemList,
              currentPage: paginationBloc.currentPage,
              totalPage: paginationBloc.totalPage,
              shouldKeepDefaultDivider: shouldKeepDefaultDivider,
              widgetType: () => itemWidget(),
              mainAxisExtent: mainAxisExtent,
              totalNoOfItems: paginationBloc.totalNumberOfItems,
              onItemPressed:isSingleParameterCallback? (item,isEdit){
                if(onItemClicked!=null)onItemClicked(item,isEdit);
              } :(item,position){
                if(onItemClicked!=null)onItemClicked(item,position);
              },
              loadMoreItem: () async {
                paginationBloc.getListFromApi(callback: apiCallback);
              },
            )),
  );
}