import 'package:flutter/material.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/model/master/truck_dimension.dart';
import 'package:app/model/master/truck_size.dart';
import 'package:app/ui/trip/create/model/truck_info.dart';
import 'package:app/ui/widget/editfield.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/validation_util.dart';

class CustomDimenWidget extends StatefulWidget {
  bool _isCustom;
  final Function callback;
  final TruckInfo _truckInfo;
  final Function sizeCallback;
  final Function lengthCallback;
  final Color sizeFillColor;
  final Color lengthFillColor;
  final List<TruckSize> _masterTruckSizes;
  final List<TruckDimensionLength> _masterTruckLengths;

  CustomDimenWidget(
      this._isCustom,
      this.callback,
      this._truckInfo,
      this._masterTruckSizes,
      this._masterTruckLengths,
      this.sizeCallback,
      this.lengthCallback,
      this.sizeFillColor,
      this.lengthFillColor);

  @override
  _CustomDimenWidget createState() => _CustomDimenWidget();
}

class _CustomDimenWidget extends State<CustomDimenWidget> {
  double _chipWidth = 194;

  @override
  Widget build(BuildContext context) {
    return widget._isCustom
        ? ActionChip(
            label: Text('Custom',
                style: TextStyle(
                    color: ColorResource.colorPrimary,
                    fontWeight: FontWeight.bold)),
            onPressed: () {
              widget.callback(-2);
            },
            backgroundColor: ColorResource.grey_white,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(4.0))),
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap)
        : Container(
            width: _chipWidth,
            margin: EdgeInsets.all(2.0),
            padding: EdgeInsets.only(left: 7.0, right: 7.0),
            decoration: BoxDecoration(
                color: ColorResource.trnBlue,
                borderRadius: BorderRadius.circular(4.0)),
            child: Row(
              children: [
                Expanded(
                  child: TextEditText<TruckSize>(
                    labelText: '',
                    fontSize: 10,
                    vMargin: 4,
                    isDropDown: true,
                    hintText: localize('unit_ton'),
                    selValue: truckSize,
                    fillColor: widget.sizeFillColor,
                    textColor: widget.sizeFillColor == Colors.blue ? ColorResource.colorWhite : ColorResource.txtClr,
                    hintTextColor: ColorResource.warmGrey,
                    dropDownPad: const EdgeInsets.only(
                        left: 2, top: 0, right: 2, bottom: 0),
                    suggestionDropDownItemList: widget._masterTruckSizes,
                    searchDrodownCallback: (item) {
                      widget.sizeCallback(item, sync: true);
                    },
                    validationMessageRegexPair: {
                      NOT_EMPTY_REGEX: translate(context, 'mne_val_msg')
                    },
                  ),
                ),
                SizedBox(width: 4.0),
                Expanded(
                  child: TextEditText<TruckDimensionLength>(
                    labelText: '',
                    fontSize: 10,
                    vMargin: 4,
                    isDropDown: true,
                    fillColor: widget.lengthFillColor,
                    textColor: widget.lengthFillColor == Colors.blue ? ColorResource.colorWhite : ColorResource.txtClr,
                    hintTextColor: ColorResource.warmGrey,
                    hintText: localize('unit_feet'),
                    selValue: truckLength,
                    dropDownPad: const EdgeInsets.only(
                        left: 4, top: 0, right: 4, bottom: 0),
                    suggestionDropDownItemList: widget._masterTruckLengths,
                    searchDrodownCallback: (item) {
                      widget.lengthCallback(item, sync: true);
                    },
                    validationMessageRegexPair: {
                      NOT_EMPTY_REGEX: translate(context, 'mne_val_msg')
                    },
                  ),
                )
              ],
            ),
          );
  }

  get truckSize =>
      widget._truckInfo.size == null ? null : widget._truckInfo.size;

  get truckLength =>
      widget._truckInfo.length == null ? null : widget._truckInfo.length;
}
