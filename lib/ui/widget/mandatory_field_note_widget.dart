import 'package:flutter/material.dart';
import 'package:app/utils/color_const.dart';

class MandatoryFieldNote extends StatelessWidget {
  final textColor;

  const MandatoryFieldNote({Key? key, this.textColor=ColorResource.colorMarineBlue}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Align(
        alignment: Alignment.centerRight,
        child: Container(
            margin: EdgeInsets.only(right: 16.0, bottom: 6.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(
                  "*",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      fontFamily: "roboto",
                      fontWeight: FontWeight.w300,
                      fontSize: 18,
                      color: textColor),
                ),
                Text(
                  'Mandatory Fields',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      fontFamily: "roboto",
                      fontWeight: FontWeight.w300,
                      fontSize: 14,
                      color: textColor),
                ),
              ],
            )));
  }
}
