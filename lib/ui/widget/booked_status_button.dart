import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/ui/detail/trip_detail_bloc.dart';
import 'package:app/ui/detail/trip_status_request.dart';
import 'package:app/ui/detail/trip_status_updates.dart';
import 'package:app/ui/widget/app_button.dart';
import 'package:app/ui/widget/remark_dailog_widget.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/ui_util.dart';
import 'package:app/utils/widget_util.dart';

import '../../api/api_response.dart';
import '../../utils/analytics/analytic_manager.dart';
import '../../utils/analytics_const.dart';
import '../../utils/color_const.dart';
import '../detail/trip_details.dart';
import '../payment/model/trip_item.dart';
import 'base_widget.dart';

class BookedStatusButton extends StatefulWidget {
  final TripDetailBloc bloc;
  //TripItem tripItem;

  BookedStatusButton(this.bloc);

  @override
  _BookedStatusButtonState createState() => _BookedStatusButtonState(bloc);
}

class _BookedStatusButtonState
    extends BasePageWidgetState<BookedStatusButton, TripDetailBloc> {
  final TripDetailBloc bloc;

  _BookedStatusButtonState(this.bloc);

  @override
  Widget build(BuildContext context) {
    if (bloc.tripStatusUpdates == null) return Container();
    int index = bloc.maxStatusUpdateIndex();
    return Wrap(
      spacing: 10.0,
      children: [
        StatusButton(
            text: translate(context, 'on_way_to_lp'),
            isDisabled: index >= 1,
            prefixIcon:
                bloc.bookedTripUpdate == BookedTripStatus.OnTheWayToLoadPoint
                    ? tickWidget()
                    : crossWidget(index >= 1),
            onPressed: index < 1
                ? () {
                    setState(() {
                      bloc.bookedStatus = BookedTripStatus.OnTheWayToLoadPoint;
                    });
                    selectDateTime(context, onSelDateTime);
                  }
                : null),
        StatusButton(
            text: translate(context, 'trk_reach_to_lp'),
            isDisabled: index >= 1,
            prefixIcon: bloc.bookedTripUpdate ==
                    BookedTripStatus.TruckReachedAtLoadingPoint
                ? tickWidget()
                : crossWidget(index >= 1),
            onPressed: index < 1
                ? () {
                    setState(() {
                      bloc.bookedStatus =
                          BookedTripStatus.TruckReachedAtLoadingPoint;
                    });
                    selectDateTime(context, onSelDateTime);
                  }
                : null),
        //todo: code added
        StatusButton(
            text: translate(context, 'trk_waiting_for_load'),
            isDisabled: index > 2,
            prefixIcon:
                bloc.bookedTripUpdate == BookedTripStatus.WaitingForLoading
                    ? tickWidget()
                    : crossWidget(index > 2),
            onPressed: index <= 2
                ? () {
                    setState(() {
                      bloc.bookedStatus = BookedTripStatus.WaitingForLoading;
                    });
                    //todo:onpressd popup need to implement
                    _showLoadingRemarkDialog(context);
                    print(
                        "Remark of wait for loading is ${bloc.waitingForLoadingRemarks}");
                  }
                : null),
        //todo: code end for booked trip
        StatusButton(
            text: translate(context, 'trk_lop'),
            isDisabled: index >= 3,
            prefixIcon:
                bloc.bookedTripUpdate == BookedTripStatus.LoadingOnProcess
                    ? tickWidget()
                    : crossWidget(index >= 3),
            onPressed: index < 3
                ? () {
                    setState(() {
                      bloc.bookedStatus = BookedTripStatus.LoadingOnProcess;
                    });
                    selectDateTime(context, onSelDateTime);
                  }
                : null),
        StatusButton(
            text: translate(context, 'trk_load_done'),
            isDisabled: index >= 4,
            prefixIcon: bloc.bookedTripUpdate == BookedTripStatus.LoadingDone
                ? tickWidget()
                : crossWidget(index >= 4),
            onPressed: index < 4
                ? () {
                    setState(() {
                      bloc.bookedStatus = BookedTripStatus.LoadingDone;
                    });
                    selectDateTime(context, onSelDateTime);
                  }
                : null),
        StatusButton(
            text: translate(context, 'trk_started_dst'),
            isDisabled: index >= 5,
            prefixIcon:
                bloc.bookedTripUpdate == BookedTripStatus.StartedForDestination
                    ? tickWidget()
                    : crossWidget(index >= 5),
            onPressed: index < 5
                ? () {
                    setState(() {
                      bloc.bookedStatus =
                          BookedTripStatus.StartedForDestination;
                    });
                    selectDateTime(context, onSelDateTime);
                  }
                : null),
      ],
    );
  }

  _showLoadingRemarkDialog(BuildContext context) async {
    dynamic remark = await showDialog(
      context: context,
      builder: (BuildContext context) => CommentDialog(
        "Remarks",
      ),
    );

    if (remark != null) {
      setState(() {
        bloc.waitingLoadingRemarks = remark;
      });
    }
  }

  //todo

  // Future<void> _showWFLRemDialog(BuildContext context) {
  //   TextEditingController _controller = TextEditingController();
  //   return showDialog(
  //       context: context,
  //       builder: (BuildContext context) {
  //         return Padding(
  //           padding: const EdgeInsets.all(4.0),
  //           child: AlertDialog(
  //             title: Text('Remarks'),
  //             content: Container(
  //               decoration: new BoxDecoration(
  //                 shape: BoxShape.rectangle,
  //                 border: new Border.all(
  //                   color: Colors.black,
  //                   width: 1.0,
  //                 ),
  //               ),
  //               child: Padding(
  //                 padding: EdgeInsets.only(left: 4.0, right: 4.0, bottom: 4.0),
  //                 child: TextField(
  //                   decoration: new InputDecoration(
  //                     border: InputBorder.none,
  //                   ),
  //                   style: TextStyle(color: ColorResource.primary_text),
  //                   maxLines: 5,
  //                   controller: _controller,
  //                 ),
  //               ),
  //             ),
  //             actions: [
  //               ElevatedButton(
  //                 style: ButtonStyle(
  //                     backgroundColor:
  //                         MaterialStateProperty.all<Color>(Colors.white),
  //                     shape: MaterialStateProperty.all<RoundedRectangleBorder>(
  //                         RoundedRectangleBorder(
  //                             borderRadius: BorderRadius.circular(18.0),
  //                             side: BorderSide(
  //                                 color: ColorResource.colorMarineBlue)))),
  //                 child: Padding(
  //                   padding: EdgeInsets.all(8.0),
  //                   child: Text(
  //                     translate(context, 'cancel'),
  //                     style: TextStyle(color: ColorResource.colorMarineBlue),
  //                   ),
  //                 ),
  //                 onPressed: () {
  //                   setState(() {
  //                     bloc.bookedStatus = BookedTripStatus.None;
  //                   });
  //                   Navigator.pop(context);
  //                 },
  //               ),
  //               Padding(
  //                 padding: const EdgeInsets.only(right: 16.0),
  //                 child: ElevatedButton(
  //                   style: ButtonStyle(
  //                       backgroundColor: MaterialStateProperty.all<Color>(
  //                           ColorResource.colorMarineBlue),
  //                       shape:
  //                           MaterialStateProperty.all<RoundedRectangleBorder>(
  //                               RoundedRectangleBorder(
  //                                   borderRadius: BorderRadius.circular(18.0),
  //                                   side: BorderSide(
  //                                       color:
  //                                           ColorResource.colorMarineBlue)))),
  //                   child: Padding(
  //                     padding: EdgeInsets.all(8.0),
  //                     child: Text(
  //                       translate(context, 'lbl_submit'),
  //                       style: TextStyle(color: Colors.white),
  //                     ),
  //                   ),
  //                   onPressed: () {
  //                     if (_controller.text != null) {
  //                       setState(() {
  //                         bloc.waitingLoadingRemarks = _controller.text;
  //                       });
  //                     }
  //
  //                     onWaitingForLoadingStatusUpdate(
  //                         bloc.waitingForLoadingRemarks);
  //                     Navigator.pop(context);
  //                   },
  //                 ),
  //               ),
  //             ],
  //           ),
  //         );
  //       });
  // }

  ///todo
  //
  // onWaitingForLoadingStatusUpdate(String remark) async {
  //   var status = bloc.bookedTripStatus();
  //   print(
  //       "trip ID ${bloc.tripId}  trip update status ${status} comment ${remark}");
  //   TripStatusUpdateRequest request = TripStatusUpdateRequest(
  //       tripId: bloc.tripId, tripUpdateStatus: status, comment: '$remark');
  //   var response = await bloc.updateTripStatus(request);
  //   TripStatus tripStatus = TripStatus();
  //   tripStatus.status = status;
  //   tripStatus.comment = remark;
  //   tripStatus.createdAt = DateTime.now().millisecondsSinceEpoch;
  //
  //   copyTo(widget.tripItem, tripStatus);
  //   manipulateResponse(response, status);
  // }
  //
  // manipulateResponse(ApiResponse response, status) {
  //   bloc.actionTime = null;
  //   switch (response.status) {
  //     case Status.LOADING:
  //       break;
  //     case Status.COMPLETED:
  //       showToast(translate(context, 'msg_tsu'));
  //       AnalyticsManager().logEventProp(AnalyticsEvents.TRIP_STATUS_CLICK,
  //           {AnalyticProperties.TRIP_STATUS: status});
  //       bloc.bookedStatus = BookedTripStatus.None;
  //       break;
  //     case Status.ERROR:
  //       showSnackBar(scaffoldState.currentState, response.message);
  //       break;
  //   }
  // }
  //
  // copyTo(trip, TripStatus tripStatus) {
  //   print("copy to called");
  //   Clipboard.setData(
  //       new ClipboardData(text: bloc.clipBoardData(trip, tripStatus)));
  //   showToast(translate(context, 'msg_cop'));
  // }

  onSelDateTime(String dateTime) {
    bloc.actionTime = dateTime;
  }
}

Widget tickWidget() {
  return Icon(
    Icons.check,
    color: Colors.green,
    size: 24,
  );
}

Widget crossWidget(bool isDisabled) {
  return Icon(
    Icons.cancel,
    color: isDisabled ? Colors.black12 : Colors.grey,
  );
}
