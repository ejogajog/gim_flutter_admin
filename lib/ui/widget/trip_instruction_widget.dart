import 'package:flutter/material.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/ui/trip/create/bloc/create_trip_bloc.dart';
import 'package:app/ui/trip/create/bloc/instruction_bloc.dart';
import 'package:app/ui/widget/app_button.dart';
import 'package:app/ui/widget/intruction_item.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/color_const.dart';
import 'package:provider/provider.dart';

class TripInstructionsWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProxyProvider<CreateTripBloc, InstructionBloc>(
          create: (BuildContext context) => InstructionBloc(context.read<CreateTripBloc>()),
          update: (BuildContext context, CreateTripBloc tripBloc, InstructionBloc? insBloc){
            insBloc?.unloadPointUpdate();
            return insBloc!;
          },
        ),
      ],
      child: InstructionWidget(),
    );
  }
}

class InstructionWidget extends StatefulWidget {
  @override
  _TripInstructionsWidgetState createState() => _TripInstructionsWidgetState();
}

class _TripInstructionsWidgetState extends State<InstructionWidget> {
  InstructionBloc? bloc;

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<InstructionBloc>(context, listen: false);
    return Consumer<InstructionBloc>(
        builder: (context, bloc, _) => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(height: 20.0),
                  Text(localize('sel_ur_inst'),
                      style: TextStyle(
                        color: Color.fromRGBO(100, 100, 100, 1.0),
                        fontSize: 16,
                        fontFamily: "roboto",
                        fontWeight: FontWeight.w500,
                      )),
                  SizedBox(height: 5.0),
                  ..._buildInstructions(),
                  SizedBox(height: 5.0),
                  ..._buildEditableInst(),
                  bloc.isNewInsAdded
                      ? SizedBox(height: 5.0)
                      : FilledColorButton(
                          fontSize: 14.0,
                          isFullwidth: false,
                          textColor: ColorResource.colorWhite,
                          verticalMargin: 20.0,
                          buttonText: translate(context, 'ins_new_btn_txt'),
                          onPressed: () {
                            bloc.addNewIns();
                          },
                          eventName: AnalyticsEvents.ADD_NEW_INST_BTN_CLK,
                        )
                ]));
  }

  List<Widget> _buildInstructions() {
    bloc!.noEditInsList();
    return List.generate(bloc!.nonEditInst.length,
        (index) => Text(bloc!.nonEditInst[index].instruction!));
  }

  List<Widget> _buildEditableInst() {
    return List.generate(
        bloc!.editInst!.length,(index) => InstructionItem(index,_onSave,_onChecked,bloc!.editInst?[index]));
  }

  _onChecked() {
    bloc!.onChecked();
  }

  _onSave(index, content) {
    return bloc!.onSave(index, content);
  }
}
