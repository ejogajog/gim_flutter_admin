import 'package:flutter/material.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/model/master/goods_type.dart';
import 'package:app/styles.dart';
import 'package:app/ui/trip/create/bloc/product_type_bloc.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/ui/widget/editfield.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/ui_util.dart';
import 'package:app/utils/validation_util.dart';
import 'package:provider/provider.dart';

class ProductTypeWidget extends StatelessWidget {
  final Function? callback;
  final Function? notifyIns;
  final Function? validGoods;
  final CustomerPreferences? presetInfo;

  ProductTypeWidget(this.presetInfo, this.validGoods,{this.callback, this.notifyIns});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ProductTypeBloc>(
          create: (_) => ProductTypeBloc(this.presetInfo, this.validGoods,
              callback: callback, notifyIns: this.notifyIns),
        ),
      ],
      child: _TripProductWidget(),
    );
  }
}

class _TripProductWidget extends StatefulWidget {
  @override
  _TripProductWidgetState createState() => _TripProductWidgetState();
}

class _TripProductWidgetState extends State<_TripProductWidget> {
  ProductTypeBloc? bloc;
  final FocusNode focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<ProductTypeBloc>(context, listen: false);
    return Consumer<ProductTypeBloc>(
        builder: (context, bloc, _) => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 20.0),
                Text(localize('sel_prod_type'), style: Styles.formLabel),
                SizedBox(height: 6.0),
                Text(bloc.selGoods, style: Styles.selMedTextBold),
                SizedBox(height: 3.0),
                Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: LayoutBuilder(
                      builder: (context, constraint) =>
                          isMobileBrowser(constraint)
                              ? mobileUi()
                              : browserUi()),
                ),
              ],
            ));
  }

  Widget mobileUi() {
    return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextEditText(
            labelText: '',
            hintText: localize('sel_prod_type'),
            behaveNormal: true,
            fontWeight: FontWeight.w300,
            isFilterWidget: true,
            isSearchableItemWidget: true,
            suggestionDropDownItemList: bloc?.masterGoods,
            focusNode: focusNode,
            onChanged: (data) {
              bloc?.isGoodsValid = false;
            },
            onFocusChange: (focusNode) {
              bloc?.removeGoodsType(bloc?.prodEditController.text);
            },
            searchDrodownCallback: (item) {
              if(item == null)return;
              bloc?.prodEditController.text = item.toString();
              bloc?.productType = item;
              AnalyticsManager().logEventProp(
                  AnalyticsEvents.GOODS_TYPE_ITEM_CLK,
                  {AnalyticProperties.GOODS_ID: item.toString()});
            },
            validationMessageRegexPair: {
              NOT_EMPTY_REGEX: translate(context, 'mne_val_msg')
            },
            textEditingController: bloc?.prodEditController,
          ),
          Wrap(spacing: 10.0, children: _recentGoods())
        ]);
  }

  Widget browserUi() {
    return Wrap(
      spacing: 10.0,
      children: [
        TextEditText(
          labelText: '',
          hintText: localize('sel_prod_type'),
          behaveNormal: true,
          fontWeight: FontWeight.w300,
          isFilterWidget: true,
          isSearchableItemWidget: true,
          suggestionDropDownItemList: bloc?.masterGoods,
          onChanged: (data) {
            bloc?.isGoodsValid = false;
          },
          searchDrodownCallback: (item) {
            bloc?.prodEditController.text = item?.toString()??'';
            bloc?.productType = item;
            AnalyticsManager().logEventProp(AnalyticsEvents.GOODS_TYPE_ITEM_CLK,
                {AnalyticProperties.GOODS_ID: item.toString()});
          },
          validationMessageRegexPair: {
            NOT_EMPTY_REGEX: (bloc?.selGoods == localize('lbl_not_sel'))
                ? translate(context, 'mne_val_msg')
                : null
          },
          textEditingController: bloc?.prodEditController,
        ),
      ]..addAll(_recentGoods()),
    );
  }

  List<Widget> _recentGoods() {
    return List.generate(
        bloc?.goods.length,
        (index) => Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: ChoiceChip(
                  label: Text(
                    bloc!.goods[index].toString(),
                    maxLines: 1,
                    style: TextStyle(
                        color: index == bloc?.goodsIndex
                            ? ColorResource.colorWhite
                            : ColorResource.colorPrimary,
                        fontWeight: FontWeight.w500),
                    overflow: TextOverflow.ellipsis,
                  ),
                  selected: bloc!.goodsIndex == index,
                  selectedColor: Colors.blue,
                  onSelected: (bool selected) {
                    bloc!.goodsType = index;
                    if(bloc!.notifyIns != null)bloc!.notifyIns!();
                  },
                  backgroundColor: ColorResource.grey_white,
                  labelPadding: DimensionResource.chipPadding,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4.0))),
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap),
            ));
  }

  @override
  void dispose() {
    focusNode.dispose();
    bloc?.prodEditController.dispose();
    super.dispose();
  }
}
