import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/styles.dart';
import 'package:app/ui/trip/create/model/kam_user_model.dart';
import 'package:app/ui/widget/editfield.dart';
import 'package:app/ui/widget/kam/kam_user_bloc.dart';
import 'package:app/utils/validation_util.dart';
import 'package:provider/provider.dart';

class KamUserWidget extends StatelessWidget {
  final Function? callback;
  final Function? validKam;

  KamUserWidget(this.validKam, {this.callback});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<KamUserBloc>(
          create: (_) => KamUserBloc(validKam, callback: callback),
        ),
      ],
      child: KamUsersWidget(),
    );
  }
}

class KamUsersWidget extends StatefulWidget {
  @override
  KamUsersWidgetState createState() => KamUsersWidgetState();
}

class KamUsersWidgetState extends State<KamUsersWidget> {
  final FocusNode focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20.0),
          Text(localize('sel_kam_lbl'), style: Styles.formLabel),
          SizedBox(height: 6.0),
          Consumer<KamUserBloc>(
              builder: (context, bloc, child) => TextEditText(
                    labelText: '',
                    hintText: localize('sel_kam_hnt'),
                    behaveNormal: true,
                    fontWeight: FontWeight.w300,
                    isFilterWidget: true,
                    isSearchableItemWidget: true,
                    suggestionDropDownItemList: bloc.listOfKam,
                    focusNode: focusNode,
                    onChanged: (data) {
                      bloc.isKamValid = false;
                    },
                    onFocusChange: (focusNode) {
                      bloc.removeSelKam(bloc.kamValController.text);
                    },
                    searchDrodownCallback: (item) {
                      bloc.selKam = item;
                    },
                    validationMessageRegexPair: {
                      NOT_EMPTY_REGEX: translate(context, 'mne_val_msg')
                    },
                    textEditingController: bloc.kamValController,
                  ))
        ]);
  }
}
