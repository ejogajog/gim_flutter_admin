import 'package:flutter/cupertino.dart';
import 'package:app/api/api_response.dart';
import 'package:app/bloc/base_bloc.dart';
import 'package:app/ui/trip/create/model/kam_user_model.dart';
import 'package:app/ui/trip/create/repo/admin_repo.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/prefs.dart';
import 'package:app/utils/string_util.dart';

class KamUserBloc extends BaseBloc{
  bool _isValidKam = true;
  List<KamUser>? _kamUsers;
  final Function? callback;
  final Function? validKamFlag;
  final kamValController = TextEditingController();

  KamUserBloc( this.validKamFlag, {this.callback}){
    _fetchListOfKam();
  }

  get listOfKam => _kamUsers ?? List<KamUser>.empty();

  set selKam(item) {
    _isValidKam = true;
    validKamFlag!(_isValidKam);
    kamValController.text = item.email;
    notifyListeners();
    callback!(item);
  }

  set isKamValid(val){
    _isValidKam = val;
    validKamFlag!(_isValidKam);
  }

  removeSelKam(kamInp) {
    if(isNullOrEmpty(kamInp) || !_isValidKam) {
      _updateController('');
      callback!(null);
      notifyListeners();
    }
  }

  _updateController(text){
    kamValController.text = text;
    kamValController.selection = TextSelection.fromPosition(TextPosition(offset: kamValController.text.length));
  }

  _fetchListOfKam() async {
    ApiResponse apiResponse = await AdminRepository.getKamList();
    _kamUsers = KamUserModel.fromJson(apiResponse.data).kamUsers;
    _assignDefKam();
    notifyListeners();
  }

  _assignDefKam(){
    if(isKam){
      var kamId = Prefs.getIntWithDefValue(Prefs.USER_ID);
      KamUser? kamFound = _kamUsers?.firstWhere((obj) => obj.userId == kamId);
      if(kamFound != null){
        selKam = kamFound;
      }
    }
  }

  get isKam => Prefs.getString(Prefs.ADMIN_TYPE) == KAM;
}