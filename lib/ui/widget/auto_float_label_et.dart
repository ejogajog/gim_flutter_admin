import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:app/styles.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/image_util.dart';
import 'package:app/utils/validation_util.dart';

class AutoFloatLabelTextField<T> extends StatefulWidget {
  final List<T>? items;
  final bool? isDrop;
  final bool? withImg;
  final String? text;
  final int? maxLines;
  final int? maxLength;
  final bool? readOnly;
  final bool? isValidate;
  final String? hintText;
  final bool? borderLess;
  final String? labelText;
  final String? prefImgUrl;
  final Function? callback;
  final bool? searchFromApi;
  final bool? startForResult;
  final TextAlign? textAlign;
  final bool? onlyRightBorder;
  final String? suffixImgPath;
  final EdgeInsets? contentPad;
  final TextInputType? textInputType;
  final TextEditingController? controller;
  final TextCapitalization? textCapitalization;
  final List<TextInputFormatter>? inputFormatter;
  final HexColor textColor = ColorResource.colorBlack;

  AutoFloatLabelTextField(this.controller,
      {this.labelText,
      this.hintText,
      this.suffixImgPath,
      this.maxLines,
      this.readOnly = false,
      this.isValidate = false,
      this.onlyRightBorder = false,
      this.borderLess = false,
      this.maxLength,
      this.items,
      this.callback,
      this.text,
      this.isDrop = false,
      this.prefImgUrl,
      this.withImg = false,
      this.searchFromApi = false,
      this.startForResult = false,
      this.textInputType = TextInputType.text,
      this.inputFormatter,
      this.contentPad = DimensionResource.formFieldContentPadding,
      this.textAlign = TextAlign.start,
      this.textCapitalization = TextCapitalization.none});

  @override
  AutoFloatLabelTextFieldState createState() => AutoFloatLabelTextFieldState();
}

class AutoFloatLabelTextFieldState extends State<AutoFloatLabelTextField> {
  bool _autoValidate = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: !widget.onlyRightBorder!
          ? null
          : BoxDecoration(
              border: Border(
                right:
                    BorderSide(width: 1.0, color: ColorResource.brownish_grey),
              ),
              color: Colors.white,
            ),
      child: Stack(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: widget.borderLess! ? 8.0 : 0),
            child: TextFormField(
              autocorrect: false,
              textAlign: widget.textAlign!,
              readOnly: widget.readOnly!,
              maxLines: widget.maxLines,
              controller: widget.readOnly!
                  ? TextEditingController.fromValue(TextEditingValue(
                      text: widget.text ?? '',
                      selection:
                          TextSelection.collapsed(offset: widget.text!.length)))
                  : widget.controller,
              maxLength: widget.maxLength,
              inputFormatters: widget.inputFormatter,
              keyboardType: widget.textInputType,
              textCapitalization: widget.textCapitalization!,
              style: TextStyle(
                  fontFamily: "roboto",
                  fontSize: 14.0,
                  fontWeight: FontWeight.w500,
                  color: widget.textColor),
              decoration: InputDecoration(
                  isDense: true,
                  contentPadding: !widget.borderLess! ? widget.contentPad : null,
                  border: widget.borderLess != null && widget.borderLess!
                      ? InputBorder.none
                      : OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(6.0)),
                        ),
                  focusedBorder: widget.borderLess != null && widget.borderLess!
                      ? InputBorder.none
                      : OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(6.0)),
                          borderSide:
                              BorderSide(color: ColorResource.brownish_grey),
                        ),
                  alignLabelWithHint: true,
                  hintText: widget.hintText,
                  hintStyle: Styles.hintTextStyle,
                  suffixIcon: widget.isDrop!
                      ? Image.asset("images/ic_drop_down_arrow.png")
                      : null,
                  prefixIcon: widget.prefImgUrl == null
                      ? null
                      : Padding(
                          padding: EdgeInsets.all(8.0),
                          child: getNetworkImageProvider(
                              url: widget.prefImgUrl,
                              width: 32.0,
                              height: 20.0),
                        )),
              onTap: () {
                print('Handle On Tap');
              },
              onChanged: (text) {
                if (!widget.isDrop!) {
                  if (widget.callback != null) widget.callback!(text);
                  _autoValidate = text.isEmpty ? false : widget.isValidate!;
                }
              },
              autovalidateMode: widget.isValidate! ? AutovalidateMode.always : AutovalidateMode.disabled,
              validator: widget.isValidate! ? validateMobile : null,
              //_selectTime(context),
            ),
          ),
          widget.labelText == null
              ? SizedBox.shrink()
              : Positioned(
                  left: 18.0,
                  child: Container(
                      padding: EdgeInsets.only(left: 3.0, right: 3.0),
                      color: ColorResource.colorWhite,
                      child: Text(
                        widget.labelText ?? '',
                        style: TextStyle(
                          color: ColorResource.labelColor,
                          fontSize: 12.0,
                          fontFamily: "roboto",
                          fontWeight: FontWeight.w300,
                        ),
                      ))),
        ],
      ),
    );
  }
}
