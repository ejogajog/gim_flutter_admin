import 'package:flutter/material.dart';
import 'package:app/ui/widget/custom_truck_dimen.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/app_constants.dart';
import 'package:provider/provider.dart';
import 'package:app/styles.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/ui/trip/create/model/truck_info.dart';
import 'package:app/ui/trip/create/bloc/truck_type_bloc.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';

class TruckTypeWidget extends StatelessWidget {
  final bool? isUsingTemp;
  final Function? callback;
  final TruckInfo? truckInfo;
  final CustomerPreferences? presetInfo;

  TruckTypeWidget(this.truckInfo, {this.callback, this.presetInfo, this.isUsingTemp = false});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<TruckInfoBloc>(
          create: (_) =>
              TruckInfoBloc(truckInfo, this.presetInfo, callback: callback, isUsingTemp: this.isUsingTemp),
        ),
      ],
      child: _TruckTypeWidget(),
    );
  }
}

class _TruckTypeWidget extends StatefulWidget {
  @override
  _TripTruckTypeWidgetState createState() => _TripTruckTypeWidgetState();
}

class _TripTruckTypeWidgetState extends State<_TruckTypeWidget> {
  TruckInfoBloc? bloc;
  double _chipWidth = 160;

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<TruckInfoBloc>(context);
    return Consumer<TruckInfoBloc>(
        builder: (context, bloc, _) => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 20.0),
                Text(localize('sel_truck_type'), style: Styles.formLabel),
                SizedBox(height: 6.0),
                Text(bloc.truckDetail, style: Styles.selMedTextBold),
                SizedBox(height: 3.0),
                ...List<Widget>.generate(
                  bloc.truckTypes.length,
                  (int index) {
                    return Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            ChoiceChip(
                              labelPadding: DimensionResource.chipPadding,
                              label: SizedBox(
                                width: _chipWidth,
                                child: Text(bloc.truckTypes[index].toString(),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: index == bloc.selectedType
                                            ? ColorResource.colorWhite
                                            : ColorResource.colorPrimary,
                                        fontWeight: FontWeight.bold)),
                              ),
                              selected: bloc.selectedType == index,
                              selectedColor: Colors.blue,
                              onSelected: (bool selected) {
                                setState(() {
                                  bloc.selectedType = selected ? index : 0;
                                  bloc.truckType =
                                      bloc.truckTypes[bloc.selectedType];
                                  bloc.resetDimen();
                                  AnalyticsManager().logEventProp(
                                      AnalyticsEvents.TRUCK_TYPE_ITM_CLK, {
                                    AnalyticProperties.TRUCK_TYPE:
                                        bloc.truckTypes[index].toString()
                                  });
                                });
                              },
                              backgroundColor: ColorResource.colorWhite,
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                      color: ColorResource.light_grey,
                                      width: 1),
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(4.0),
                                      bottomLeft: Radius.circular(4.0))),
                              labelStyle: TextStyle(color: Colors.white),
                            ),
                            index == bloc.selectedType
                                ? Expanded(child: _buildDimen())
                                : Container()
                          ],
                        ),
                        SizedBox(height: 5.0),
                      ],
                    );
                  },
                ).toList()
              ],
            ));
  }

  Widget _buildDimen() {
    int length = bloc!.emptyPresets ? 1 : (bloc!.truckSizes.length+1);
    int customDimenPos = length - 1;
    return Container(
      height: 38,
      padding: EdgeInsets.only(left: 4.0, right: 4.0),
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(width: 1.0, color: Colors.lightBlue),
          right: BorderSide(width: 1.0, color: Colors.lightBlue),
          bottom: BorderSide(width: 1.0, color: Colors.lightBlue),
        ),
      ),
      child: ListView.separated(
        scrollDirection: Axis.horizontal,
        itemCount: length,
        shrinkWrap: true,
        separatorBuilder: (BuildContext context, int index) =>
            SizedBox(width: 8.0),
        itemBuilder: (BuildContext context, int index) {
          return ((index == customDimenPos))
              ? CustomDimenWidget(
                  bloc!.selectedDimen >= -1,
                  _changed,
                  bloc!.truckInfo(),
                  bloc!.masterTruckSizes,
                  bloc!.masterTruckLengths,
                  bloc!.truckSize,
                  bloc!.truckLength,
                  bloc!.sizeFillColor,
                  bloc!.lengthFillColor)
              : ChoiceChip(
                  label: Text(
                      '${bloc!.truckSizes[index]}, ${bloc!.truckLengths[index]}',
                      style: TextStyle(
                          color: index == bloc!.selectedDimen
                              ? ColorResource.colorWhite
                              : ColorResource.colorPrimary,
                          fontWeight: FontWeight.bold)),
                  selected: bloc!.selectedDimen == index,
                  selectedColor: Colors.blue,
                  onSelected: (bool selected) {
                    setState(() {
                      bloc!.selectedDimen = selected ? index : 0;
                      bloc!.truckDimen = bloc!.selectedDimen;
                      AnalyticsManager()
                          .logEventProp(AnalyticsEvents.TRUCK_DIMEN_BTN_CLK, {
                        AnalyticProperties.TRUCK_DIMEN:
                            '${bloc!.truckSizes[index]}, ${bloc!.truckLengths[index]}'
                      });
                    });
                  },
                  backgroundColor: ColorResource.grey_white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4.0))),
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap);
        },
      ),
    );
  }

  _changed(index) {
    setState(() {
      bloc?.onCustomTap(index);
    });
  }
}
