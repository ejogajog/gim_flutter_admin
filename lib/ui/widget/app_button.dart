import "package:flutter/material.dart";
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/string_util.dart';
import 'package:google_fonts/google_fonts.dart';

class FilledColorButton extends StatefulWidget{
  String?  buttonText;
  final isFilled;
  Color? textColor=ColorResource.colorMariGold;
  //NOTE: Please make sure to set onPressed with non null return otherwise it will keep on showing blue background color.
  var backGroundColor;
  VoidCallback? onPressed;
  var isFullwidth;
  var borderColor;
  var isOnlyTopRadius;
  bool? shouldHaveLeftRightMargin=true;
  double? fontSize;
  FocusNode? focusNode;
  double? innerPadding;
  double? verticalMargin;
  double? horizonatalMargin;
  final fontWeight;
  String? eventName;
  double? radius;
  EdgeInsets? padding;
  Map<String, dynamic>? eventProp;
  FilledColorButton({Key? key,this.buttonText="Button",this.isFilled=true,this.onPressed, this.fontWeight=FontWeight.bold,
    this.fontSize=18, this.isFullwidth=true,this.shouldHaveLeftRightMargin=true,this.radius = 7.0,this.textColor,this.innerPadding = 20, this.padding, this.verticalMargin=8,this.isOnlyTopRadius = false,
    this.horizonatalMargin=0.0,this.backGroundColor=ColorResource.colorMarineBlue,this.borderColor=ColorResource.colorMarineBlue, this.focusNode, this.eventName, this.eventProp}):super(key:key){

    if(!isFilled){
      if(textColor==null)
        textColor=ColorResource.colorMarineBlue;
      if(backGroundColor==ColorResource.colorMarineBlue)
        backGroundColor=Colors.white;
    }
    if(textColor==null)
      textColor=ColorResource.colorMariGold;
    if(this.buttonText?.isEmpty??true)
      this.buttonText="Button";
  }
  @override
  State<StatefulWidget> createState() =>_FilledColorButton();
}

class _FilledColorButton extends State<FilledColorButton>{
  @override
  Widget build(BuildContext context) {
    var width;
    if(widget.isFullwidth)
      width= double.infinity ;
    else
      width=null;
    var  leftRightMargin=0.0;
    if(widget.shouldHaveLeftRightMargin??false)
      leftRightMargin= 18.0;
    return SizedBox(
      width: width,
      child: Padding(
        padding:  EdgeInsets.symmetric(horizontal: widget.horizonatalMargin!,vertical:widget.verticalMargin!),
        child: MaterialButton(
          focusNode: widget.focusNode,
          shape: RoundedRectangleBorder(
            borderRadius: widget.isOnlyTopRadius ? BorderRadius.only(bottomLeft:Radius.circular(widget.radius!),bottomRight:Radius.circular(widget.radius!)) : BorderRadius.all(Radius.circular(widget.radius!)),
            side: BorderSide(color: isDisabled()?Colors.transparent:widget.borderColor,width: 1.0),

          ),
          onPressed: (){
            if(widget.onPressed != null)
              widget.onPressed!();
            if (!isNullOrEmpty(widget.eventName))
              AnalyticsManager().logEventProp(widget.eventName!, widget.eventProp);
          },
          child: FittedBox(child: Text(widget.buttonText!.toUpperCase(),style: TextStyle( fontSize: widget.fontSize,fontWeight: widget.fontWeight),),fit: BoxFit.fitWidth),
          padding: widget.padding ?? EdgeInsets.fromLTRB(2*widget.innerPadding!,widget.innerPadding!,2*widget.innerPadding!,widget.innerPadding!),
          textColor: widget.textColor,
          disabledColor: ColorResource.disBtn,
          color: isDisabled()? ColorResource.disBtn : widget.backGroundColor,
          disabledTextColor: ColorResource.marigold_opacity_20,
        ),
      ),
    );
  }
  isDisabled()=>widget.onPressed==null;
}

class CustomButton extends StatelessWidget{
  var prefixIcon;
  var suffixIcon;
  String? text;
  var padding;
  var textColor;
  var borderColor;
  var bgColor;
  VoidCallback? onPressed;
  var drawablePadding;
  bool? isForwardButton;
  bool? isRoundedCorner;
  bool? isAllCaps=true;
  double? fontSize;
  double? borderRadius;
  double? iconSize;
  FontWeight? fontWeight;
  final layoutAlignment;
  CustomButton({Key? key,
    this.text,
    this.suffixIcon,
    this.prefixIcon,
    this.padding,
    this.fontSize=14,
    this.borderColor=ColorResource.colorMarineBlue,
    this.bgColor=Colors.white,
    this.onPressed,
    this.iconSize = 12,
    this.drawablePadding=10.0,
    this.fontWeight=FontWeight.w500,
    this.borderRadius=5.0,
    this.isForwardButton=false,
    this.isRoundedCorner=true,
    this.isAllCaps=true,
    this.layoutAlignment=MainAxisAlignment.start,
    this.textColor}):super(key:key){
    if(textColor==null) textColor=ColorResource.colorMarineBlue;
    if(suffixIcon!=null && suffixIcon is IconData)
      suffixIcon=Icon(suffixIcon,color: textColor, size: iconSize);
    if(prefixIcon!=null && prefixIcon is IconData)
      prefixIcon=Icon(prefixIcon,color: textColor,size: iconSize);
    if(padding==null)
      padding=EdgeInsets.only(left: 5,right: 5,
          top: 3,bottom: 3);

  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        padding: padding,
        width: (isForwardButton??false)?double.infinity:null,
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          borderRadius: (isRoundedCorner??false)?BorderRadius.circular(borderRadius!):BorderRadius.circular(0.0),
          color: bgColor,
          border:Border.all(color: borderColor,width: 1),
        ),
        child: Row(
          children: <Widget>[
            SizedBox( width: 5),
            prefixIcon==null?Container():prefixIcon,
            SizedBox(width:prefixIcon==null?0: drawablePadding),
            Text((isAllCaps??false)?text!.toUpperCase():text!,style: GoogleFonts.roboto(color: textColor,fontSize: fontSize, fontWeight:fontWeight ),),
            SizedBox(width:suffixIcon==null? 5 : drawablePadding),
            suffixIcon==null?Container():(isForwardButton??false)?Expanded(child: Align(child: suffixIcon,alignment: Alignment.centerRight,)):suffixIcon
          ],
        ),
      ),
    );
  }

}

class StatusButton extends StatefulWidget{
  var prefixIcon;
  var suffixIcon;
  String? text;
  var padding;
  var textColor;
  var borderColor;
  var bgColor;
  VoidCallback? onPressed;
  var drawablePadding;
  bool? isForwardButton;
  bool? isRoundedCorner;
  bool? isAllCaps = true;
  double? fontSize;
  double? borderRadius;
  FontWeight? fontWeight;
  final layoutAlignment;
  bool? isDisabled;

  StatusButton({Key? key,
    this.text,
    this.isDisabled = false,
    this.suffixIcon,
    this.prefixIcon,
    this.padding,
    this.fontSize = 10,
    this.borderColor = ColorResource.colorMarineBlue,
    this.bgColor = Colors.white,
    this.onPressed,
    this.drawablePadding = 5.0,
    this.fontWeight = FontWeight.w500,
    this.borderRadius = 4.0,
    this.isForwardButton = false,
    this.isRoundedCorner = true,
    this.isAllCaps = true,
    this.layoutAlignment = MainAxisAlignment.start,
    this.textColor =  ColorResource.colorMarineBlue}) :super(key: key) {
    if (textColor == null) textColor = ColorResource.colorMarineBlue;
    if (suffixIcon != null && suffixIcon is IconData)
      suffixIcon = Icon(suffixIcon, color: textColor, size: 12,);
    if (prefixIcon != null && prefixIcon is IconData)
      prefixIcon = Icon(prefixIcon, color: textColor, size: 10,);
    if (padding == null)
      padding = EdgeInsets.only(left: 2, right: 2,
          top: 2, bottom: 2);
  }

  @override
  _StatusButtonState createState() => _StatusButtonState();

}

class _StatusButtonState extends State<StatusButton>{

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onPressed,
      child: Container(
        padding: widget.padding,
        width: 160,
        height: 40,
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          borderRadius: widget.isRoundedCorner!
              ? BorderRadius.circular(widget.borderRadius!)
              : BorderRadius.circular(0.0),
          color: widget.bgColor,
          border: Border.all(color: widget.isDisabled! ? ColorResource.divider_color : widget.borderColor, width: 1),
        ),
        child: Row(
          children: <Widget>[
            SizedBox(width: 5),
            widget.prefixIcon == null  ? Container() : widget.prefixIcon,
            SizedBox(width: widget.prefixIcon == null ? 0 : widget.drawablePadding),
            Expanded(
              child: Text(widget.isAllCaps! ? widget.text!.toUpperCase() : widget.text!,
                style: GoogleFonts.roboto(color: widget.isDisabled! ? ColorResource.divider_color : widget.textColor, fontSize: widget.fontSize, fontWeight: widget.fontWeight)),
            ),
            SizedBox(width: widget.suffixIcon == null ? 5 : widget.drawablePadding),
            widget.suffixIcon == null ? Container() : widget.isForwardButton!
                ? Expanded(child: Align(
              child: widget.suffixIcon, alignment: Alignment.centerRight,))
                : widget.suffixIcon
          ],
        ),
      ),
    );
  }
}


