import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app/ui/detail/trip_detail_bloc.dart';
import 'package:app/ui/widget/full_screen_image.dart';
import 'package:app/utils/navigation_util.dart';
import 'package:app/utils/string_util.dart';

class LoadingUnloadingImages extends StatefulWidget {
  final TripDetailBloc _tripDetailBloc;

  LoadingUnloadingImages(this._tripDetailBloc);

  @override
  _LoadingUnloadingImagesState createState() => _LoadingUnloadingImagesState();
}

class _LoadingUnloadingImagesState extends State<LoadingUnloadingImages> {
  @override
  Widget build(BuildContext context) {
    return (isNullOrEmptyList(widget._tripDetailBloc.loadingImages) &&
            isNullOrEmptyList(widget._tripDetailBloc.unLoadingImages))
        ? Container()
        : Row(
            children: <Widget>[
              isNullOrEmptyList(widget._tripDetailBloc.loadingImages)
                  ? Container()
                  : Flexible(
                      flex: 1,
                      fit: FlexFit.tight,
                      child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.transparent,
                          ),
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text("Loading Images",
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold)),
                              ),
                              Wrap(
                                  spacing: 10.0,
                                  runSpacing: 20.0,
                                  children: getLoadingImageWidget()),
                            ],
                          )),
                    ),
              SizedBox(
                width: 20,
              ),
              isNullOrEmptyList(widget._tripDetailBloc.unLoadingImages)
                  ? Container()
                  : Flexible(
                      flex: 1,
                      fit: FlexFit.tight,
                      child: Container(
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text("Unloading Images",
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold)),
                              ),
                              Wrap(
                                spacing: 10.0,
                                runSpacing: 20.0,
                                children: getUnLoadingImageWidget(),
                              ),
                            ],
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.transparent,
                          )),
                    ),
            ],
          );
  }

  getLoadingImageWidget() {
    if (widget._tripDetailBloc.loadingImages == null) return [Container()];
    List<Widget> images = List.empty(growable: true);
    widget._tripDetailBloc.loadingImages!.forEach((element) {
      images.add(GestureDetector(
        onTap: () =>
            navigateNextScreen(context, FullScreenImage(imageUrl: element)),
        child: CachedNetworkImage(
          imageUrl: element,
          height: 100,
          width: 100,
          placeholder: (context, url) => SizedBox(
            child: CircularProgressIndicator(),
            height: 10.0,
            width: 10.0,
          ),
          errorWidget: (context, url, error) => Icon(Icons.error),
        ) /*Image.network(
          element,
          filterQuality: FilterQuality.high,
          isAntiAlias: true,
          fit: BoxFit.cover,
          height: 100,
          width: 100,
          errorBuilder: (context, error, stackTrace) {
            return Text(
              '$error, $stackTrace',
              key: const Key('previewImage_errorText'),
            );
          },
        )*/
        ,
      ));
    });
    return images;
  }

  getUnLoadingImageWidget() {
    if (widget._tripDetailBloc.unLoadingImages == null) return [Container()];
    List<Widget> images = List.empty(growable: true);
    widget._tripDetailBloc.unLoadingImages!.forEach((element) {
      images.add(GestureDetector(
        onTap: () =>
            navigateNextScreen(context, FullScreenImage(imageUrl: element)),
        child: CachedNetworkImage(
          imageUrl: element,
          height: 100,
          width: 100,
          placeholder: (context, url) => SizedBox(
            child: CircularProgressIndicator(),
            height: 10.0,
            width: 10.0,
          ),
          errorWidget: (context, url, error) => Icon(Icons.error),
        ) /*Image.network(
          element,
          filterQuality: FilterQuality.high,
          isAntiAlias: true,
          fit: BoxFit.cover,
          height: 100,
          width: 100,
          errorBuilder: (context, error, stackTrace) {
            return Text(
              '$error, $stackTrace',
              key: const Key('previewImage_errorText'),
            );
          },
        )*/
        ,
      ));
    });
    return images;
  }
}
