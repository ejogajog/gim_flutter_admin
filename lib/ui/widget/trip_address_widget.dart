import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:app/styles.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/ui/widget/address_map_widget.dart';
import 'package:app/ui/trip/create/bloc/trip_address_bloc.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';

class AddressWidget extends StatelessWidget {
  bool? isEditTemp;
  Function? notifyParent;
  Function? validDistrict;
  CustomerPreferences? preference;
  List<BaseAddressesSuggestion?> loadPoint;
  List<BaseAddressesSuggestion?> unloadPoints;


  AddressWidget(this.notifyParent,this.preference,this.loadPoint,this.unloadPoints,{this.isEditTemp = false, this.validDistrict});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<TripAddressBloc>(
            create: (_) => TripAddressBloc(notifyParent, preference, this.loadPoint, this.unloadPoints,isEditTemp: this.isEditTemp, validDistrict: this.validDistrict)),
      ],
      child: _TripAddressWidget(),
    );
  }
}

class _TripAddressWidget extends StatefulWidget {
  @override
  _TripAddressWidgetState createState() => _TripAddressWidgetState();
}

class _TripAddressWidgetState extends State<_TripAddressWidget> {

  @override
  Widget build(BuildContext context) {
    return Consumer<TripAddressBloc>(
      builder: (context, bloc, child) => bloc == null
        ? SizedBox()
        :  Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 20.0),
                Text(localize('set_pick_loc'), style: Styles.formLabel),
                AddressMapWidget(ValueKey('lp1'),'epa_or_dp', 0,bloc.companyId, bloc.loadPoint, bloc.lpAdrHistory, bloc.lpSearchAdr, notifyParent: bloc.isEditTemp! ? bloc.notifyParent : null, validDistrict: bloc.validDistrict, isEditTemp: bloc.isEditTemp),
                SizedBox(height: 20.0),
                Text(localize('set_drop_loc'), style: Styles.formLabel),
                ...List.generate(bloc.noUnloadPoints, (pos) => AddressMapWidget(ValueKey('up${bloc.noUnloadPoints}$pos'),'eda_or_dp', pos, bloc.companyId, bloc.unloadPoints, bloc.unlPntHistory(pos), bloc.upSearchAdr, notifyParent: bloc.notifyParent, addUnloadPoints: bloc.addRemUnloadPoint, validDistrict: bloc.validDistrict, isEditTemp: bloc.isEditTemp)),
              ],
            ),
          );
  }
}
