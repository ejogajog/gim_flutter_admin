import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:app/ui/widget/auto_suggestion_widget.dart';
import 'package:app/ui/widget/dropdown_widget.dart';
import 'package:app/ui/widget/searchable_list.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/string_util.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class TextEditText<T> extends StatefulWidget {
  String? hintText = "";
  String? labelText = "";
  var borderColor;
  TextEditingController? textEditingController;
  var keyboardType;
  bool? obscured = false;
  Map<String, String?>? validationMessageRegexPair;
  var enableBorderColor;
  var focusedBorderColor;
  var textColor;
  var maxlength;
  var fillColor;
  var fontWeight;
  var textAlign;
  bool? isOptionalFieldWithValidation;
  bool? readOnly;
  bool? isEnabled;
  Widget? suffixIcon;
  ValueChanged<String>? onChanged;
  var onTap;
  var verticalPad;
  Color? suffixIconColor;
  String? errorText;
  bool? isFilterWidget;
  int? maxLines;
  var inputFormatters;
  bool? showCounter;

  //dropdown or autosuggestion item list
  List<T>? suggestionDropDownItemList;
  bool? isSearchableItemWidget;
  Function(T?)? searchDrodownCallback;
  final hintTextColor;
  final labelTextColor;
  double? fontSize;
  final labelTextBgColor;
  final behaveNormal;
  GlobalKey<FormFieldState>? formkey;
  bool? isHtmlText;
  int? errorMaxLines;
  final isDropDown;
  final withImg;
  TextCapitalization? textCapitalization;
  T? selValue;
  Function? isValid;
  EdgeInsets? dropDownPad;
  double? vMargin;
  FocusNode? focusNode;
  Function? onFocusChange;
  double? radius;
  bool? withClear;
  bool? containFilter;
  final List<String> autofillHints;

  TextEditText({
    Key? key,
    required this.labelText,
    this.hintText,
    this.textEditingController,
    this.focusNode,
    this.onFocusChange,
    this.keyboardType = TextInputType.text,
    this.suggestionDropDownItemList,
    this.isOptionalFieldWithValidation = false,
    this.obscured = false,
    this.isFilterWidget = false,
    this.isSearchableItemWidget = false,
    this.validationMessageRegexPair,
    this.enableBorderColor = ColorResource.brownish_grey,
    this.textColor = Colors.black,
    this.focusedBorderColor = ColorResource.brownish_grey,
    this.maxlength,
    this.selValue,
    this.readOnly = false,
    this.isEnabled = true,
    this.textAlign = TextAlign.left,
    this.suffixIcon,
    this.onChanged,
    this.fontSize = 14.0,
    this.searchDrodownCallback,
    this.onTap,
    this.suffixIconColor = ColorResource.colorLightBlueGrey,
    this.errorText,
    this.formkey,
    this.fillColor = Colors.white,
    this.borderColor = Colors.white,
    this.isValid,
    this.fontWeight,
    this.verticalPad,
    this.hintTextColor = ColorResource.brownish_grey,
    this.labelTextBgColor = Colors.white,
    this.labelTextColor = ColorResource.brownish_grey,
    this.behaveNormal = false,
    this.isHtmlText = false,
    this.errorMaxLines = 2,
    this.isDropDown = false,
    this.withImg = false,
    this.maxLines = 1,
    this.vMargin = 14,
    this.radius = 8.0,
    this.showCounter = false,
    this.inputFormatters,
    this.withClear = false,
    this.containFilter = true,
    this.dropDownPad = const EdgeInsets.only(left: 10, top: 10, bottom: 11),
    this.textCapitalization = TextCapitalization.none,
    this.autofillHints = const [],
  }) : super(key: key) {
    if (this.isSearchableItemWidget!) this.readOnly = true;
  }

  @override
  State<StatefulWidget> createState() => _TextEditText<T>();
}

class _TextEditText<T> extends State<TextEditText> {
  TextFieldBloc? rootBloc;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((callback) {
      if (widget.onTap != null && !widget.isSearchableItemWidget!)
        widget.textEditingController?.addListener(() {
          if (!isNullOrEmpty(widget.textEditingController?.text))
            rootBloc?.currentText = widget.textEditingController?.text;
        });
    });

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.textEditingController != null && !widget.readOnly!)
      widget.textEditingController?.selection = TextSelection.collapsed(
        offset: widget.textEditingController!.text.length,
      );
    if (widget.isSearchableItemWidget!) {
      return AutocompleteSuggestionWidget(
        labelText: widget.labelText,
        hintText: widget.hintText ?? "",
        suffixIcon: widget.suffixIcon,
        focusNode: widget.focusNode,
        isEnabled: widget.isEnabled,
        onTextChange: widget.onChanged,
        onFocusChange: widget.onFocusChange,
        withClear: widget.withClear,
        containFilter: widget.containFilter,
        textController: widget.textEditingController ?? TextEditingController(),
        suggestionList: widget.suggestionDropDownItemList,
        onSuggestionItemSelected: (item) {
          if (widget.searchDrodownCallback != null) {
            widget.searchDrodownCallback!(item);
          }
          widget.formkey?.currentState?.validate();
        },
        validationMessageRegexPair: widget.validationMessageRegexPair,
      );
    }

    if (widget.isDropDown) {
      return DropDownWidget(
        vMargin: widget.vMargin,
        selValue: widget.selValue,
        hintText: widget.hintText,
        labelText: widget.labelText,
        fontSize: widget.fontSize,
        padding: widget.dropDownPad,
        fillColor: widget.fillColor,
        borderColor: widget.borderColor,
        textColor: widget.textColor,
        hintTextColor: widget.hintTextColor,
        onItemSelcted: widget.searchDrodownCallback,
        dropDownItems: widget.suggestionDropDownItemList,
      );
    }
    return ChangeNotifierProvider<TextFieldBloc>(
      create: (context) => TextFieldBloc(
        widget.validationMessageRegexPair,
        widget.obscured,
        widget.isOptionalFieldWithValidation,
      ),
      child: Padding(
        padding: EdgeInsets.fromLTRB(0, 8.0, 0, 8.0),
        child: Consumer<TextFieldBloc>(
          builder: (context, bloc, _) {
            rootBloc = bloc;
            return Stack(
              alignment: Alignment.topLeft,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 3.0),
                  child: TextFormField(
                    autofillHints: widget.autofillHints,
                    maxLines: widget.maxLines,
                    textAlign: widget.textAlign,
                    onTap: () {
                      if (widget.isSearchableItemWidget!)
                        buildSearchableItemComponent<T>(bloc);
                      else if (widget.onTap != null) {
                        widget.onTap();
                      }
                    },
                    keyboardType: widget.keyboardType,
                    inputFormatters: widget.keyboardType == TextInputType.number
                        ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
                        : widget.inputFormatters,
                    controller: widget.textEditingController,
                    obscureText: bloc.passwordVisible,
                    readOnly: widget.readOnly!,
                    validator: (value) {
                      if (widget.textEditingController != null) {
                        bloc.currentText = widget.textEditingController!.text;
                      }
                      if (isNullOrEmpty(bloc.currentText)) {
                        bloc.currentText = "";
                      }
                      return bloc.errorText;
                    },
                    onChanged: (text) {
                      bloc.currentText = text;
                      widget.formkey?.currentState?.validate();
                      if (widget.onChanged != null) widget.onChanged!(text);
                      if (widget.isValid != null) widget.isValid!(isNullOrEmpty(bloc._errorText));
                    },
                    textCapitalization: widget.textCapitalization!,
                    decoration: InputDecoration(
                      filled: true,
                      errorMaxLines: widget.errorMaxLines,
                      errorStyle: TextStyle(fontSize: 12.0),
                      contentPadding: !widget.isFilterWidget!
                          ? widget.verticalPad
                          : const EdgeInsets.only(left: 14, top: 14, bottom: 14),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(widget.radius!),
                        borderSide: BorderSide(
                          color: widget.enableBorderColor,
                        ),
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(widget.radius!),
                          borderSide: BorderSide(
                            color: widget.borderColor,
                          )),
                      counterText: widget.showCounter! ? null : '',
                      labelText: widget.behaveNormal ? widget.labelText!.toUpperCase() : null,
                      hintText: widget.hintText,
                      hintStyle: TextStyle(color: widget.hintTextColor, fontSize: 14.0),
                      errorText: widget.errorText ?? bloc.errorText,
                      suffixIcon: widget.obscured!
                          ? IconButton(
                              padding: EdgeInsets.only(right: 20.0),
                              icon: Icon(
                                bloc.passwordVisible ? Icons.visibility_off : Icons.visibility,
                                color: widget.suffixIconColor,
                                size: 24.0,
                              ),
                              onPressed: () {
                                bloc.passwordVisible = !bloc.passwordVisible;
                              },
                            )
                          : widget.suffixIcon ?? null,
                      fillColor: widget.fillColor,
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(widget.radius!),
                        borderSide: BorderSide(
                          color: widget.focusedBorderColor,
                        ),
                      ),
                      labelStyle: GoogleFonts.roboto(
                        color: widget.labelTextColor,
                        fontWeight: FontWeight.w400,
                        fontSize: 13.0,
                      ),

                      // suffixIcon: Icon(Icons.airplay),
                    ),
                    style: TextStyle(color: widget.textColor, fontSize: widget.fontSize, fontWeight: widget.fontWeight),
                    maxLength: widget.maxlength,
                  ),
                ),
                Visibility(
                  visible: !widget.behaveNormal,
                  child: Positioned(
                    left: 10.0,
                    child: Container(
                        padding: EdgeInsets.only(left: 4.0, right: 4.0),
                        color: widget.labelTextBgColor,
                        child: Text(
                          widget.labelText!.toUpperCase(),
                          style: TextStyle(
                            color: widget.labelTextColor,
                            fontSize: 13.0,
                            fontFamily: "roboto",
                            fontWeight: FontWeight.w400,
                          ),
                        )),
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
    /*    return widget.onTap==null? contentWidget:GestureDetector(
      onTap: widget.onTap,
      child: AbsorbPointer( absorbing: true,child: contentWidget),
    );*/
  }

  buildSearchableItemComponent<T>(TextFieldBloc bloc) async {
    print(widget.suggestionDropDownItemList!.length);
    T? result = await Navigator.push<T>(
        context,
        MaterialPageRoute(
            builder: (context) => SearchableListWidget(
                  title: widget.labelText!,
                  itemList: widget.suggestionDropDownItemList,
                )));
    print("result:" + result.toString());
    if (result != null) {
      bloc.currentText = result.toString();
      widget.searchDrodownCallback!(result);
      widget.formkey?.currentState?.validate();
    }
  }
}

class TextFieldBloc with ChangeNotifier {
  String? _errorText;
  String? _currentText = "";
  bool? _passwordVisible = false;
  bool? _isRequiredTextField = false;
  bool? _isOptionalFieldWithValidation = false;

  bool get passwordVisible => _passwordVisible!;

  set passwordVisible(bool value) {
    _passwordVisible = value;
    notifyListeners();
  }

  get isRequiredTextField => _isRequiredTextField;

  set isRequiredTextField(value) {
    _isRequiredTextField = value;
  }

  Map<String, String?>? validationMessageRegexPair;

  get errorText => _errorText;

  set errorText(value) {
    _errorText = value;
  }

  TextFieldBloc(this.validationMessageRegexPair, this._passwordVisible, this._isOptionalFieldWithValidation) {
    if ((validationMessageRegexPair != null && validationMessageRegexPair!.length > 0)) _isRequiredTextField = true;
  }

  get currentText => _currentText;

  set currentText(value) {
    _currentText = value;
    bool isValid = true;
    if (_isRequiredTextField!) {
      if (_isOptionalFieldWithValidation! && value.toString().isEmpty) {
        removeErrorMsg();
      } else {
        for (String key in validationMessageRegexPair!.keys) {
          RegExp regex = RegExp(key);
          if (!regex.hasMatch(value)) {
            isValid = false;
            _errorText = validationMessageRegexPair![key];
            notifyListeners();
            break;
          }
        }

        if (isValid) {
          removeErrorMsg();
        }
      }
    }
  }

  removeErrorMsg() {
    if (_errorText != null && _errorText!.isNotEmpty) {
      _errorText = null;
      notifyListeners();
    }
  }
}
