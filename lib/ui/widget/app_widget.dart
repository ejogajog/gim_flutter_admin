import 'package:app/utils/route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:app/flavor/config.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/custom_scroll_behaviour.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:app/ui/widget/platform_specific_widget.dart';
import 'package:app/localization/app_translation_delegate.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class AppWidget extends PlatformSpecificWidget<CupertinoApp, MaterialApp> {
  final Widget? home;
  final AppTranslationsDelegate? newLocaleDelegate;

  FirebaseAnalytics analytics = FirebaseAnalytics.instance;
  static FirebaseAnalyticsObserver observer = FirebaseAnalyticsObserver(analytics: FirebaseAnalytics.instance);

  AppWidget({this.home, this.newLocaleDelegate});

  @override
  MaterialApp androidWidget(BuildContext context) {
    return MaterialApp(
      // key:key,
      scrollBehavior: CustomScrollBehavior(),
      navigatorKey: FlavorConfig.instance!.values.navigatorKey,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        dialogBackgroundColor: Colors.white,
        primaryColor: ColorResource.colorPrimary,
        fontFamily: 'roboto',
      ),
      onGenerateRoute: AdminPageRouter.generateRoute,
      navigatorObservers: <NavigatorObserver>[observer],
      title: "GIM Admin",
      home: home,
      localizationsDelegates: newLocaleDelegate == null
          ? [
              const AppTranslationsDelegate(),
              //provides localised strings
              GlobalMaterialLocalizations.delegate,
              //provides RTL support
              GlobalWidgetsLocalizations.delegate,
            ]
          : [
              newLocaleDelegate!,
              const AppTranslationsDelegate(),
              //provides localised strings
              GlobalMaterialLocalizations.delegate,
              //provides RTL support
              GlobalWidgetsLocalizations.delegate,
            ],
    );
  }
}
