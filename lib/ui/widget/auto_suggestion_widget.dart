import 'package:flutter/material.dart';
import 'package:app/ui/widget/form_field_validator.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/string_util.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class AutocompleteSuggestionWidget<T> extends StatefulWidget {
  var textAlign;
  final textColor;
  final borderColor;
  String? labelText = "";
  String? hintText = "";
  final labelTextColor;
  final double fontSize;
  var keyboardType;
  var textController;
  var inputFormatters;
  bool? isEnabled;
  bool? containFilter;
  final labelTextBgColor;
  Widget? suffixIcon;
  FocusNode? focusNode;
  Function(String)? onTextChange;
  Function? onFocusChange;
  EdgeInsets? contentPad;
  bool? withClear;
  List? suggestionList = List.empty(growable: true);
  void Function(T?)? onSuggestionItemSelected;
  Map<String, String?>? validationMessageRegexPair;

  AutocompleteSuggestionWidget(
      {Key? key,
      this.textAlign = TextAlign.left,
      this.focusNode,
      this.onFocusChange,
      this.onSuggestionItemSelected,
      this.labelTextBgColor = Colors.white,
      this.labelText,
      this.hintText,
      this.suffixIcon,
      this.onTextChange,
      this.textController,
      this.fontSize = 14.0,
      this.isEnabled = true,
      this.containFilter = true,
      this.textColor = Colors.blue,
      this.keyboardType = TextInputType.text,
      this.labelTextColor = ColorResource.brownish_grey,
      this.borderColor = ColorResource.brownish_grey,
      this.suggestionList,
      this.inputFormatters,
      this.withClear = false,
      this.contentPad = const EdgeInsets.only(left: 14, top: 14, bottom: 14),
      this.validationMessageRegexPair})
      : super(key: key);

  @override
  _AutocompleteTextFieldState createState() => _AutocompleteTextFieldState();
}

class _AutocompleteTextFieldState<T>
    extends State<AutocompleteSuggestionWidget<T>> {
  @override
  void initState() {
    super.initState();
    widget.focusNode?.addListener(() {
      if (widget.onFocusChange != null) widget.onFocusChange!(widget.focusNode);
    });
  }

  @override
  Widget build(BuildContext context) {
    final borderLayout = OutlineInputBorder(
      borderRadius: BorderRadius.circular(8.0),
      borderSide: BorderSide(
        color: widget.isEnabled! ? widget.borderColor : ColorResource.light_grey,
      ),
    );
    return ChangeNotifierProvider(
      create: (context) => TextFieldBloc(widget.validationMessageRegexPair),
      child: Consumer<TextFieldBloc>(
        builder: (context, bloc, _) => Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: TypeAheadFormField(
                  hideOnLoading: true,
                  hideOnEmpty: true,
                  hideOnError: true,
                  getImmediateSuggestions: false,
                  direction: AxisDirection.up,
                  autoFlipDirection: true,
                  debounceDuration: Duration(milliseconds: 300),
                  textFieldConfiguration: TextFieldConfiguration(
                    focusNode: widget.focusNode,
                    autofocus: false,
                    onChanged: widget.onTextChange,
                    controller: widget.textController,
                    inputFormatters: widget.inputFormatters,
                    textAlign: widget.textAlign,
                    keyboardType: widget.keyboardType,
                    enabled: widget.isEnabled!,
                    style: TextStyle(
                        color: widget.isEnabled!
                            ? Colors.black
                            : ColorResource.warmGrey,
                        fontSize: 14.0,
                        fontWeight: FontWeight.w500),
                    decoration: InputDecoration(
                        enabledBorder: borderLayout,
                        border: borderLayout,
                        suffixIcon: widget.suffixIcon,
                        disabledBorder: borderLayout,
                        focusedBorder: borderLayout,
                        contentPadding: widget.contentPad,
                        hintText: widget.hintText),
                  ),
                  onSuggestionSelected: (suggestion) {
                    widget.textController.text = suggestion.toString();
                    if (widget.onSuggestionItemSelected != null)
                      this.widget.onSuggestionItemSelected!(suggestion as T?);
                  },
                  itemBuilder: (context, suggestion) {
                    return Container(
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(color: ColorResource.light_grey),
                        ),
                      ),
                      child: ListTile(
                        dense: true,
                        contentPadding: EdgeInsets.fromLTRB(8.0, 4.0, 4.0, 4.0),
                        minLeadingWidth: 2.0,
                        title: Text(suggestion.toString(),
                            style: GoogleFonts.roboto(
                                fontWeight: FontWeight.w300,
                                fontSize: 14.0,
                                color: ColorResource.colorMarineBlue)),
                      ),
                    );
                  },
                  suggestionsCallback: (pattern) {
                    if (isNullOrEmpty(pattern))
                      this.widget.onSuggestionItemSelected!(null);
                    return getSuggestions(pattern);
                  },
                  validator: (value) {
                    if (widget.textController != null) {
                      bloc.currentText = widget.textController.text;
                    }
                    if (isNullOrEmpty(bloc.currentText)) {
                      bloc.currentText = "";
                    }
                    return bloc.errorText;
                  }),
            ),
            Visibility(
              visible: !isNullOrEmpty(widget.labelText),
              child: Positioned(
                  left: 15.0,
                  top: 3,
                  child: Container(
                      padding: EdgeInsets.only(left: 4.0, right: 4.0),
                      color: widget.labelTextBgColor,
                      child: Text(
                        widget.labelText == null
                            ? ""
                            : widget.labelText!.toUpperCase(),
                        style: TextStyle(
                          color: widget.labelTextColor,
                          fontSize: 12.0,
                          fontFamily: "roboto",
                          fontWeight: FontWeight.w400,
                        ),
                      ))),
            )
          ],
        ),
      ),
    );
  }

  getSuggestions<T>(String query) {
    if (isNullOrEmptyList(widget.suggestionList)) return;
    var matches = widget.suggestionList!.map((f) => (f)).toList();
    if (widget.containFilter!)
      matches.retainWhere(
          (s) => s.toString().toLowerCase().contains(query.toLowerCase()));
    else
      matches.retainWhere(
          (s) => s.toString().toLowerCase().startsWith(query.toLowerCase()));
    return matches;
  }
}
