import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/model/master/districts.dart';
import 'package:app/styles.dart';
import 'package:app/ui/trip/create/bloc/address_map_bloc.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/ui/widget/auto_search_location.dart';
import 'package:app/ui/widget/editfield.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/string_util.dart';
import 'package:app/utils/ui_util.dart';
import 'package:app/utils/validation_util.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

class AddressMapWidget extends StatelessWidget {
  int? index;
  ValueKey? key;
  int? companyId;
  bool? isEditTemp;
  String? hintTextKey;
  Function? notifyParent;
  Function? validDistrict;
  Function? addUnloadPoints;
  List<BaseAddressesSuggestion?>? addressPoints;
  List<BaseAddressesSuggestion>? _addressHistory;
  List<BaseAddressesSuggestion>? _searchAddressHistory;

  AddressMapWidget(this.key, this.hintTextKey, this.index, this.companyId,
      this.addressPoints, this._addressHistory, this._searchAddressHistory,
      {this.notifyParent, this.addUnloadPoints, this.validDistrict, this.isEditTemp})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<AddressMapBloc>(
          create: (_) => AddressMapBloc(hintTextKey, index, companyId,
              _addressHistory, _searchAddressHistory, addressPoints,
              notifyParent: notifyParent, addRemUnloadPoints: addUnloadPoints, validDistrict: this.validDistrict, isEditTemp:this.isEditTemp),
        ),
      ],
      child: _SetLocationWidget(),
    );
  }
}

class _SetLocationWidget extends StatefulWidget {
  @override
  _SetTripLocationWidgetState createState() => _SetTripLocationWidgetState();
}

class _SetTripLocationWidgetState extends State<_SetLocationWidget> {
  double mapHeight = 240;
  double iconSize = 24;
  bool isShown = false;
  bool isGestureEnable = false;
  final FocusNode focusNode = FocusNode();
  IconData _iconData = Icons.lock_outline;//IconData(0xf197, fontFamily: 'MaterialIcons');

  @override
  Widget build(BuildContext context) {
    return Consumer<AddressMapBloc>(
        builder: (context, bloc, _){
          var dstLbl = localize(bloc.isLoadPoint! ? 'sel_load_dst' : 'sel_ul_dst');
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 10.0),
              Wrap(
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 5.0),
                        child: Text(bloc.selLblAddress,
                            style: Styles.selMedTextBold),
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                            child: GooglePlacesSearchWidget(
                              isSrc: true,
                              companyId: bloc.companyId,
                              focusNode: bloc.focusNode,
                              callBack: _onFocus,
                              immediateSuggestion: false,
                              suffixIcon:
                              bloc.isEditing! ? Icons.check : Icons.close,
                              hintText: localize(bloc.hintTextKey),
                              typeAheadController: bloc.addressController,
                              onSuggestionItemSelected: (dynamic value) {
                                if (bloc.isDuplicateUnloadPoint(value)) {
                                  showToast(localize('dst_ads_exist'));
                                  return;
                                }
                                bloc.getPosUsingPlaceId(value);
                              },
                              validationMessageRegexPair: {
                                NOT_EMPTY_REGEX:
                                translate(context, 'mne_val_msg')
                              },
                              onChange: (data, {isEditing = true}) {
                                if(bloc.onChange(data, isEditing: isEditing)??false)
                                  showToast(localize('dst_ads_exist'));
                              },
                            ),
                          ),
                          bloc.isShowDelete
                              ? InkWell(
                            onTap: () => bloc.removeUnlPoint(),
                            child: Icon(
                              Icons.delete,
                              size: 28,
                              color: ColorResource.trip_above_24hourBg,
                            ),
                          )
                              : SizedBox()
                        ],
                      ),
                      SizedBox(height: 2.0),
                      isShown
                          ? LayoutBuilder(
                          builder: (context, constraint) => Stack(
                            children: [
                              GestureDetector(
                                onHorizontalDragStart: (start) {
                                  if(isGestureEnable){
                                    bloc.adrInd = -1;
                                    bloc.flagUpdateOnDrag = true;
                                  }
                                },
                                onVerticalDragStart: (start) {
                                  if(isGestureEnable) {
                                    bloc.adrInd = -1;
                                    bloc.flagUpdateOnDrag = true;
                                  }
                                },
                                child: Container(
                                  height: mapHeight,
                                  child: GoogleMap(
                                    scrollGesturesEnabled: isGestureEnable,
                                    onMapCreated: bloc.onCreated,
                                    initialCameraPosition:
                                    CameraPosition(
                                      target: bloc.markerPosition!,
                                      zoom: 12.0,
                                    ),
                                    onCameraMove: bloc.onCameraMove,
                                    onCameraIdle: bloc.onCameraIdle,
                                    gestureRecognizers: <
                                        Factory<
                                            OneSequenceGestureRecognizer>>[
                                      Factory<
                                          OneSequenceGestureRecognizer>(
                                            () =>
                                            EagerGestureRecognizer(),
                                      ),
                                    ].toSet(),
                                  ),
                                ),
                              ),
                              Positioned(
                                top: (mapHeight - iconSize) / 2,
                                right: isMobileBrowser(constraint)
                                    ? (constraint.maxWidth -
                                    iconSize) /
                                    2
                                    : (containerWidth -
                                    (iconSize +
                                        (2 *
                                            horizontalPadding))) /
                                    2,
                                child: Image.asset(
                                  bloc.isLoadPoint!
                                      ? "images/pickup_pin.png"
                                      : "images/dropoff_pin.png",
                                  height: 28,
                                  width: 24,
                                ),
                              ),
                              isGestureEnable ? SizedBox() : Container(
                                  width: double.infinity,
                                  height: mapHeight,
                                  color: Color.fromARGB(100, 22, 33, 44),
                                  padding: EdgeInsets.all(40)),
                              //IconData(0xe3ae, fontFamily: 'MaterialIcons');
                              Positioned(
                                child: Align(
                                    alignment: Alignment.topRight,
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: FloatingActionButton.small(onPressed: (){
                                        setState(() {
                                          _iconData = isGestureEnable ? Icons.lock_outline: Icons.lock_open_outlined;
                                          isGestureEnable = !isGestureEnable;
                                        });
                                      },
                                          child: Icon(_iconData, color: Colors.black),
                                          backgroundColor: Colors.white),
                                    )
                                ),
                              ),
                            ],
                          ))
                          : SizedBox(),
                      Row(
                        mainAxisAlignment:
                        MainAxisAlignment.spaceBetween,
                        children: [
                          Visibility(
                            visible: (bloc.isShowHistory || isShown),
                            child: RichText(
                              text: new TextSpan(
                                children: [
                                  TextSpan(
                                    text:
                                    isShown ? "Hide map" : "Show map",
                                    style: TextStyle(
                                        color: Colors.blue,
                                        decoration:
                                        TextDecoration.underline),
                                    recognizer: new TapGestureRecognizer()
                                      ..onTap = () {
                                        setState(() {
                                          isShown = !isShown;
                                        });
                                      },
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Visibility(
                            visible: bloc.isShowAddNewUnlPoint!,
                            child: Tooltip(
                              message:
                              'Please provide the unloading point address',
                              child: RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: "Add another unloading point",
                                      style: TextStyle(
                                          color: bloc.canAddAnoUnlPnt
                                              ? ColorResource
                                              .colorLightBlueGrey
                                              : Colors.blue,
                                          decoration:
                                          TextDecoration.underline),
                                      recognizer:
                                      new TapGestureRecognizer()
                                        ..onTap = () {
                                          if (bloc.canAddAnoUnlPnt)
                                            return;
                                          if (isNullOrEmpty(bloc
                                              .addressController
                                              .text)) {
                                            showToast(localize(
                                                'dst_ads_empty'));
                                            return;
                                          }
                                          isShown = false;
                                          bloc.addNewUnloadPoint();
                                          AnalyticsManager().logEvent(
                                              AnalyticsEvents
                                                  .ADD_UNLOAD_POINT_BTN_CLK);
                                        },
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                  Visibility(
                    visible: bloc.isShowHistory || isShown,
                    child: Wrap(
                      spacing: 10.0,
                      children: List.generate(
                          bloc.addressHistory.length,
                              (index) => Padding(
                            padding: const EdgeInsets.only(top: 10.0),
                            child: ChipTheme(
                                data: ChipTheme.of(context).copyWith(
                                    pressElevation: 10.0,
                                    selectedShadowColor: Colors.blue),
                                child: ChoiceChip(
                                    label: Text(
                                        bloc.addressHistory[index]
                                            .toString(),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            color: index ==
                                                bloc.selAddressIndex
                                                ? ColorResource.colorWhite
                                                : ColorResource
                                                .colorPrimary,
                                            fontWeight: FontWeight.w500)),
                                    selected: bloc.selAddressIndex == index,
                                    selectedColor: Colors.blue,
                                    onSelected: (bool selected) {
                                      setState(() {
                                        if (bloc.isDuplicateUp(index)) {
                                          showToast(
                                              localize('dst_ads_exist'));
                                          return;
                                        }
                                        bloc.isEditing = false;
                                        bloc.selAddressIndex = index;
                                        bloc.selPresetAddress =
                                            bloc.selAddressIndex;
                                        bloc.updateInstruction();
                                      });
                                    },
                                    backgroundColor:
                                    ColorResource.grey_white,
                                    labelPadding:
                                    DimensionResource.chipPadding,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(4.0))))),
                          )),
                    ),
                  ),
                ],
              ),
              (bloc.isEditTemp!) ? SizedBox() : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 20.0),
                  Text(dstLbl, style: Styles.formLabel),
                  SizedBox(height: 3.0),
                  TextEditText(
                    labelText: '',
                    hintText: dstLbl,
                    behaveNormal: true,
                    fontWeight: FontWeight.w300,
                    isFilterWidget: true,
                    withClear: true,
                    containFilter: false,
                    focusNode: focusNode,
                    suffixIcon: Icon(Icons.arrow_drop_down_outlined),
                    isSearchableItemWidget: true,
                    suggestionDropDownItemList: bloc.defDistricts,
                    searchDrodownCallback: (item) {
                      if(item == null)return;
                      bloc.districtEditController.text = item.toString();
                      bloc.district = item;
                    },
                    onChanged: (data) {
                      bloc.isDistrictValid = false;
                    },
                    onFocusChange: (FocusNode focusNode) {
                      if (!focusNode.hasFocus && !bloc.isValidDistrict!){
                        bloc.districtEditController.clear();
                      }
                      bloc.removeDistrict(bloc.districtEditController.text);
                    },
                    validationMessageRegexPair: {
                      NOT_EMPTY_REGEX: translate(context, 'mne_val_msg')
                    },
                    textEditingController: bloc.districtEditController,
                  ),
                ],),
              SizedBox(height: 3.0),
            ],
          );
        });
  }

  _onFocus(FocusNode focusNode) {
    Provider.of<AddressMapBloc>(context, listen: false)
        .modAddress(focusNode.hasFocus);
  }
}