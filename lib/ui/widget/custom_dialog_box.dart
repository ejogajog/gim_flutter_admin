import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:app/ui/widget/app_button.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/string_util.dart';

class CustomDialogBox extends StatefulWidget {
  final String? title;
  final String? imgPath;
  final String? buttonText;
  final String? descriptions;
  final Color? ttlTextColor;
  final String? negButtonText;
  final Widget? descriptionWidget;
  final Function? onFinishCallback;

  const CustomDialogBox(Key key,this.imgPath,
      {this.title, this.descriptions, this.buttonText = 'Okay', this.negButtonText, this.ttlTextColor, this.onFinishCallback, this.descriptionWidget})
      : super(key: key);

  @override
  _CustomDialogBoxState createState() => _CustomDialogBoxState();
}

class _CustomDialogBoxState extends State<CustomDialogBox> {
  static const double padding = 20;
  static const double avatarRadius = 45;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(padding),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: Stack(
        children: <Widget>[
          Container(
            width: 260,
            padding: EdgeInsets.only(
                left: padding,
                top: avatarRadius + padding,
                right: padding,
                bottom: padding),
            margin: EdgeInsets.only(top: avatarRadius),
            decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                color: Colors.white,
                borderRadius: BorderRadius.circular(padding),
                boxShadow: [
                  BoxShadow(
                      color: Colors.black,
                      offset: Offset(0, 4),
                      blurRadius: 4),
                ]),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  widget.title ?? '',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600, color: widget.ttlTextColor),
                ),
                SizedBox(
                  height: 15,
                ),
                widget.descriptionWidget ?? Text(
                  widget.descriptions ?? '',
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: HexColor('#6B7280')),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 22,
                ),
                isNullOrEmpty(widget.negButtonText)?
                FilledColorButton(buttonText: widget.buttonText, onPressed: (){
                  Navigator.pop(context);
                  if(widget.onFinishCallback != null)
                    widget.onFinishCallback!();
                },
                    radius: 40.0,
                    innerPadding: 14.0,textColor: ColorResource.colorWhite)
                    :Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: FilledColorButton(buttonText: widget.negButtonText, fontSize: 12, onPressed: (){
                        Navigator.pop(context);
                      },
                          radius: 30.0,
                          borderColor: ColorResource.light_grey,
                          backGroundColor: ColorResource.light_grey,
                          innerPadding: 10.0,textColor: ColorResource.colorMarineBlue),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: FilledColorButton(buttonText: widget.buttonText, fontSize: 12, onPressed: (){
                        Navigator.pop(context);
                        if(widget.onFinishCallback != null)
                          widget.onFinishCallback!();
                      },
                          radius: 30.0,
                          innerPadding: 10.0,textColor: ColorResource.colorWhite),
                    )
                  ],)
              ],
            ),
          ),
          Positioned(
            left: padding/2,
            right: padding,
            child: CircleAvatar(
              backgroundColor: Colors.transparent,
              radius: avatarRadius,
              child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(avatarRadius)),
                  child: widget.imgPath!.startsWith('svg') ? SvgPicture.asset(widget.imgPath!) : Image.asset(widget.imgPath!)),
            ),
          ),
        ],
      ),
    );
  }
}
