import 'package:flutter/services.dart';
import 'package:app/utils/app_constants.dart';

class CounterValFormatter extends TextInputFormatter {
  int? maxVal;
  CounterValFormatter({this.maxVal = MAX_TRIP});

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.length > 0) {
      int entVal = int.tryParse(newValue.text)??0;
      if (entVal >= 1 && entVal <= maxVal!) return newValue;
      return TextEditingValue(
          text: entVal > maxVal!
              ? '$maxVal'
              : entVal < 1
                  ? '1'
                  : newValue.text,
          selection: TextSelection.collapsed(offset: newValue.text.length));
    }
    return newValue;
  }
}
