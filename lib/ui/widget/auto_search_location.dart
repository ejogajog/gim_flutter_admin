import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:app/ui/trip/create/repo/google_repo.dart';
import 'package:app/ui/widget/form_field_validator.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/utils/string_util.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:translator/translator.dart';

class GooglePlacesSearchWidget<T> extends StatefulWidget {
  int? companyId;
  String? value;
  String? labelText = "";
  String? hintText = "";
  bool? isSrc;
  FocusNode? focusNode;
  final typeAheadController;
  bool? immediateSuggestion;
  Function? callBack;
  final void Function(T)? onSuggestionItemSelected;
  final borderColor;
  Map<String, String>? validationMessageRegexPair;
  Function? onChange;
  IconData? suffixIcon;

  GooglePlacesSearchWidget(
      {Key? key,
      this.companyId,
      this.onSuggestionItemSelected,
      this.hintText,
      this.labelText,
      this.value,
      this.isSrc,
      this.onChange,
      this.callBack,
      this.focusNode,
      this.typeAheadController,
      this.validationMessageRegexPair,
      this.immediateSuggestion = true,
      this.suffixIcon,
      this.borderColor = ColorResource.brownish_grey})
      : super(key: key);

  @override
  GooglePlacesSearchWidgetState createState() =>
      GooglePlacesSearchWidgetState();
}

class GooglePlacesSearchWidgetState extends State<GooglePlacesSearchWidget> {
  SuggestionsBoxController _suggestionsBoxController =
      SuggestionsBoxController();
  final translator = GoogleTranslator();

  @override
  void initState() {
    super.initState();
    widget.focusNode?.addListener(() {
      widget.callBack!(widget.focusNode);
    });
  }

  @override
  Widget build(BuildContext context) {
    final borderLayout = OutlineInputBorder(
      borderRadius: BorderRadius.circular(8.0),
      borderSide: BorderSide(
        color: widget.borderColor,
      ),
    );
    return ChangeNotifierProvider(
      create: (context) => TextFieldBloc(widget.validationMessageRegexPair!),
      child: Consumer<TextFieldBloc>(
          builder: (context, bloc, _) => Container(
                child: TypeAheadFormField(
                    hideOnLoading: true,
                    hideOnEmpty: true,
                    hideOnError: true,
                    minCharsForSuggestions: 1,
                    direction: AxisDirection.up,
                    suggestionsBoxController: _suggestionsBoxController,
                    getImmediateSuggestions: widget.immediateSuggestion!,
                    debounceDuration: Duration(milliseconds: 300),
                    textFieldConfiguration: TextFieldConfiguration(
                      focusNode: widget.focusNode,
                      autofocus: false,
                      onChanged: (data) {
                        if (widget.onChange != null) widget.onChange!(data);
                      },
                      controller: widget.typeAheadController,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 12.0,
                          fontWeight: FontWeight.w500),
                      decoration: InputDecoration(
                          suffixIcon: InkWell(
                              child: Icon(widget.suffixIcon,
                                  color: ColorResource.brownish_grey,
                                  size: 18.0),
                              onTap: () {
                                if (widget.suffixIcon == Icons.check) {
                                  FocusManager.instance.primaryFocus?.unfocus();
                                  if (widget.onChange != null)
                                    widget.onChange!(
                                        widget.typeAheadController.text,
                                        isEditing: false);
                                } else {
                                  widget.typeAheadController.clear();
                                  if (widget.onChange != null)
                                    widget.onChange!("");
                                }
                              }),
                          enabledBorder: borderLayout,
                          border: borderLayout,
                          disabledBorder: borderLayout,
                          focusedBorder: borderLayout,
                          contentPadding: const EdgeInsets.only(
                              left: 10, top: 14, right: 10, bottom: 14),
                          hintText: widget.hintText),
                    ),
                    onSuggestionSelected: (dynamic suggestion) {
                      this.widget.typeAheadController.text =
                          suggestion.toString();
                      this.widget.onSuggestionItemSelected!(suggestion);
                    },
                    itemBuilder: (context, dynamic suggestion) {
                      print("suggestion" + suggestion.toString());
                      return Container(
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(color: ColorResource.light_grey),
                          ),
                        ),
                        child: ListTile(
                          dense: true,
                          contentPadding:
                              EdgeInsets.fromLTRB(8.0, 4.0, 4.0, 2.0),
                          minLeadingWidth: 2.0,
                          leading: suggestion.id != null
                              ? SvgPicture.asset(
                                  'svg_img/ic_gim_gray.svg',
                                  height: 16.0,
                                  width: 16.0,
                                )
                              : Icon(CupertinoIcons.location_solid,
                                  color: Colors.grey, size: 16.0),
                          title: Text(
                            suggestion.toString(),
                            style: GoogleFonts.roboto(
                                fontWeight: FontWeight.w300,
                                fontSize: 14.0,
                                color: ColorResource.colorMarineBlue),
                          ),
                        ),
                      );
                    },
                    suggestionsCallback: (pattern) async {
                      return await _placesResult(pattern);
                    },
                    validator: (value) {
                      if (widget.typeAheadController != null) {
                        bloc.currentText = widget.typeAheadController.text;
                      }
                      if (isNullOrEmpty(bloc.currentText)) {
                        bloc.currentText = "";
                      }
                      return bloc.errorText;
                    }),
              )),
    );
  }

  _placesResult(String query) async {
    List results = await GoogleRepository.getPlaceSearchResult(query, widget.companyId);
    if(isNullOrEmptyList(results)){
      Translation translation = await translator.translate(query);
      var lanCode = translation.sourceLanguage.code;
      if(lanCode == 'bn'){
        query = (await translator.translate(query,to: 'en')).text;
        return await GoogleRepository.getPlaceSearchResult(query, widget.companyId);
      }
    }
    return results;
  }
}
