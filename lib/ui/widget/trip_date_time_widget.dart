import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/styles.dart';
import 'package:app/ui/trip/create/bloc/trip_date_time_bloc.dart';
import 'package:app/ui/widget/app_button.dart';
import 'package:app/utils/analytics/analytic_manager.dart';
import 'package:app/utils/analytics_const.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/utils/color_const.dart';
import 'package:app/ui/widget/picker/custom_time_picker.dart'
    as CustomPicker;
import 'package:app/ui/widget/picker/rounded_date_picker.dart'
    as RoundedDatePicker;
import 'package:provider/provider.dart';

class TripDateTimeWidget extends StatelessWidget {
  final Function? callback;

  TripDateTimeWidget({this.callback});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<TripDateTimeBloc>(
            create: (_) => TripDateTimeBloc(callback: callback)),
      ],
      child: SetTripDateTimeWidget(),
    );
  }
}

class SetTripDateTimeWidget extends StatefulWidget {
  @override
  _SetTripDateTimeWidgetState createState() => _SetTripDateTimeWidgetState();
}

class _SetTripDateTimeWidgetState extends State<SetTripDateTimeWidget> {
  List? hours;
  int selectedTime = 0;
  TripDateTimeBloc? bloc;

  @override
  void initState() {
    if (mounted) bloc = Provider.of(context, listen: false);
    super.initState();
    hours = bloc?.hourLoop();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<TripDateTimeBloc>(
        builder: (context, bloc, _) =>
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              SizedBox(height: 20.0),
              Text(localize('sel_date_time'), style: Styles.formLabel),
              SizedBox(height: 6.0),
              Text('${bloc.tripDate} ${bloc.tripTime}', style: Styles.selMedTextBold),
              SizedBox(height: 3.0),
              Container(
                  margin: const EdgeInsets.only(top: 4.0),
                  padding: const EdgeInsets.all(4.0),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4.0),
                      border: Border.all(color: ColorResource.light_grey)),
                  child: bloc.isCustom
                      ? _customDateTimeUi()
                      : _defaultDateTimeUi()),
            ]));
  }

  _defaultDateTimeUi() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Wrap(
          spacing: 5.0,
          runSpacing: 5.0,
          children: [
            Chip(
              padding: DimensionResource.chipPadding,
              label: Container(
                width: 180,
                child: Text(bloc!.tripDate),
              ),
              backgroundColor: Colors.blue,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(4.0))),
              labelStyle: TextStyle(
                color: Colors.white,
                fontSize: 15,
                fontFamily: "roboto",
                fontWeight: FontWeight.w500,
              ),
              labelPadding: DimensionResource.chipPadding,
            ),
            Chip(
              padding: DimensionResource.chipPadding,
              label: Text(bloc!.tripTime),
              backgroundColor: Colors.blue,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(4.0))),
              labelStyle: TextStyle(
                color: Colors.white,
                fontSize: 15,
                fontFamily: "roboto",
                fontWeight: FontWeight.w500,
              ),
              labelPadding: DimensionResource.chipPadding,
            ),
            ActionChip(
              label: Text(bloc!.todayTomorrow()),
              padding: DimensionResource.chipPadding,
              backgroundColor:
                  bloc!.isDefaultView ? Colors.blue : ColorResource.grey_white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(4.0))),
              labelStyle: TextStyle(
                color: bloc!.isDefaultView
                    ? Colors.white
                    : ColorResource.colorPrimary,
                fontSize: 15,
                fontFamily: "roboto",
                fontWeight: FontWeight.w500,
              ),
              labelPadding: DimensionResource.chipPadding,
              onPressed: () {
                selectedTime = 0;
                bloc!.defaultView(true, hour: hours![0]);
                AnalyticsManager().logEvent('${bloc!.todayTomorrow()}_${AnalyticsEvents.TODAY_TOM_BTN_CLK}');
              },
            ),
            GestureDetector(
              onTap: () {
                bloc!.custom = true;
                AnalyticsManager().logEvent(AnalyticsEvents.CALENDER_BTN_CLK);
              },
              child: Container(
                padding: DimensionResource.chipPadding,
                decoration: BoxDecoration(
                    color:
                        bloc!.isCustom ? Colors.blue : ColorResource.light_grey,
                    borderRadius: BorderRadius.all(Radius.circular(4.0))),
                child: Column(
                  children: [
                    Icon(Icons.calendar_today,
                        size: 16, color: ColorResource.warmGrey),
                    Text("Calender",
                        style: TextStyle(
                            fontSize: 8.0, color: ColorResource.warmGrey))
                  ],
                ),
              ),
            )
          ],
        ),
        SizedBox(height: 4.0),
        _buildTime()
      ],
    );
  }

  Widget _buildTime() {
    return Container(
      height: 48,
      width: containerWidth,
      child: ListView.separated(
        scrollDirection: Axis.horizontal,
        itemCount: hours!.length,
        separatorBuilder: (BuildContext context, int index) =>
            SizedBox(width: 10.0),
        itemBuilder: (BuildContext context, int index) {
          return ChoiceChip(
              label: Text(hours![index],
                  style: TextStyle(
                      color: index == selectedTime
                          ? ColorResource.colorWhite
                          : ColorResource.colorPrimary,
                      fontWeight: FontWeight.w500)),
              selected: selectedTime == index,
              selectedColor: Colors.blue,
              onSelected: (bool selected) {
                if(!bloc!.isDefaultView)return;
                selectedTime = selected ? index : 0;
                bloc!.defaultDateTime(hours![index]);
                AnalyticsManager().logEvent(AnalyticsEvents.LOAD_TIME_HOUR_BTN_CLK);
              },
              labelPadding: DimensionResource.chipPadding,
              backgroundColor: ColorResource.grey_white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(4.0))),
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap);
        },
      ),
    );
  }

  _customDateTimeUi() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Wrap(crossAxisAlignment: WrapCrossAlignment.center, children: [
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Chip(
                padding: DimensionResource.chipPadding,
                label: Container(
                  width: 180,
                  child: Text(bloc!.tripDate),
                ),
                backgroundColor: Colors.blue,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(4.0))),
                labelStyle: TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  fontFamily: "roboto",
                  fontWeight: FontWeight.w500,
                ),
                labelPadding: DimensionResource.chipPadding,
              ),
              SizedBox(height: 5.0),
              Container(
                width: 240,
                height: 420,
                child: RoundedDatePicker.showRoundedDatePicker(
                    context: context,
                    initialDatePickerMode: DatePickerMode.day,
                    initialDate: bloc?.selectedDate,
                    firstDate: DateTime(DateTime.now().year,
                        DateTime.now().month, DateTime.now().day),
                    lastDate: DateTime.now().add(Duration(days: 90)),
                    borderRadius: 8,
                    handleOk: selDate),
              )
            ],
          ),
          SizedBox(width: 20.0),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                width: 240,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Chip(
                      padding: DimensionResource.chipPadding,
                      label: Text(bloc?.tripTime),
                      backgroundColor: Colors.blue,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(4.0))),
                      labelStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                        fontFamily: "roboto",
                        fontWeight: FontWeight.w500,
                      ),
                      labelPadding: DimensionResource.chipPadding,
                    ),
                    GestureDetector(
                      onTap: () {
                        if(bloc!.isCustom)return;
                        bloc!.isCustom = true;
                      },
                      child: Container(
                        padding: const EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
                        decoration: BoxDecoration(
                            color: bloc!.isCustom
                                ? Colors.blue
                                : ColorResource.light_grey,
                            borderRadius:
                                BorderRadius.all(Radius.circular(4.0))),
                        child: Column(
                          children: [
                            Icon(Icons.calendar_today,
                                size: 16,
                                color: bloc!.isCustom
                                    ? ColorResource.colorWhite
                                    : ColorResource.warmGrey),
                            Text("Calender",
                                style: TextStyle(
                                    fontSize: 8.0,
                                    color: bloc!.isCustom
                                        ? ColorResource.colorWhite
                                        : ColorResource.warmGrey))
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 5.0),
              Container(
                width: 240,
                height: 420,
                child: Theme(
                  data: ThemeData(
                      primaryColor: Colors.white,
                      dialogBackgroundColor:
                          Color.fromRGBO(242, 242, 242, 1.0), colorScheme: ColorScheme.fromSwatch().copyWith(secondary: Color.fromRGBO(1, 154, 232, 1.0))),
                  child: CustomPicker.showRoundedTimePicker(
                      context: context,
                      borderRadius: 8,
                      initialTime: bloc!.getSelectedTime(),
                      handleOk: bloc!.selTime),
                ),
              ),
            ],
          ),
        ]),
        SizedBox(height: 5.0),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            FilledColorButton(
              isFilled: true,
              isFullwidth: false,
              textColor: ColorResource.colorWhite,
              buttonText: translate(context, 'txt_btn_cancel'),
              onPressed: () {
                selectedTime = 0;
                bloc!.defaultView(true, hour: hours![0]);
                bloc!.custom = false;
              },
            ),
            SizedBox(width: 5.0),
            FilledColorButton(
              isFilled: true,
              isFullwidth: false,
              textColor: ColorResource.colorWhite,
              buttonText: translate(context, 'txt_btn_ok'),
              onPressed: () {
                selectedTime = -1;
                bloc!.defaultView(false);
              },
            ),
          ],
        )
      ],
    );
  }

  selDate(date) {
    bloc?.selDate = date;
  }
}
