import 'package:app/model/master/districts.dart';
import 'package:app/model/master/master.dart';
import 'package:app/model/master/thanas.dart';
import 'package:app/ui/detail/trip_status_updates.dart';
import 'package:app/ui/options/features_model.dart';
import 'package:app/ui/payment/model/trip_item.dart' as PaymentInfo;
import 'package:app/ui/template/create/template_model.dart';
import 'package:app/ui/trip/create/model/customer_preference.dart';
import 'package:app/ui/trip/create/model/preset_instruction.dart';
import 'package:app/ui/trip/create/model/trip_model.dart';
import 'package:app/ui/trip/trip_item.dart';
import 'package:json_annotation/json_annotation.dart';

import '../ui/trip/create/model/create_trip_response.dart';

part 'base_response.g.dart';

@JsonSerializable(anyMap: true)
class BaseResponse<T> {
  BaseResponse();

  BaseResponse.instance(this.data);

  @_Converter()
  T? data;
  bool? success;
  String? message;
  String? errorCode;
  int? responseCode;

  factory BaseResponse.fromJson(Map<String, dynamic> json) =>
      _$BaseResponseFromJson<T>(json);

  Map<String, dynamic> toJson() => _$BaseResponseToJson(this);
}

@JsonSerializable()
class BaseTrip {
  int? requestedTripCount;
  int? bookedTripCount;
  int? liveTripCount;
  int? completedTripCount;

  BaseTrip();

  BaseTrip.counter(
      {this.requestedTripCount,
        this.bookedTripCount,
        this.liveTripCount,
        this.completedTripCount});

  BasePagination<TripItem>? tripListForSelectedTripFilter;

  factory BaseTrip.fromJson(Map<String, dynamic> json) =>
      _$BaseTripFromJson(json);

  Map<String, dynamic> toJson() => _$BaseTripToJson(this);

  getTotalTripCount() {
    return requestedTripCount! +
        bookedTripCount! +
        liveTripCount! +
        completedTripCount!;
  }
}

@JsonSerializable()
class BasePagination<T> {
  BasePagination(this.contentList);

  BasePagination.instance(
      this.contentList, this.totalPages, this.numberOfResults, this.nextPage);

  @_Converter()
  List<T>? contentList;
  int? totalPages;
  int? numberOfResults;
  int? nextPage;

  factory BasePagination.fromJson(Map<String, dynamic> json) =>
      _$BasePaginationFromJson<T>(json);

  Map<String, dynamic> toJson() => _$BasePaginationToJson(this);
}

class _Converter<T> implements JsonConverter<T, Object?> {
  const _Converter();

  @override
  T fromJson(Object? json) {
    if (T == TripItem) {
      return TripItem.fromJson(json as Map<String, dynamic>) as T;
    }else if (T == TripStatus) {
      return TripStatus.fromJson(json as Map<String, dynamic>) as T;
    }else if (T == TripStatusUpdates) {
      return TripStatusUpdates.fromJson(json as Map<String, dynamic>) as T;
    }else if (T == MasterDataResponse) {
      return MasterDataResponse.fromJson(json as Map<String, dynamic>) as T;
    }else if (T == Districts) {
      return Districts.fromJson(json as Map<String, dynamic>) as T;
    }else if (T == Thanas) {
      return Thanas.fromJson(json as Map<String, dynamic>) as T;
    }else if (T == PaymentInfo.TripItem) {
      return PaymentInfo.TripItem.fromJson(json as Map<String, dynamic>) as T;
    }else if(T == CustomerPreferences){
      return CustomerPreferences.fromJson(json as Map<String, dynamic>) as T;
    }else if(T == FeaturesModel){
      return FeaturesModel.fromJson(json as Map<String, dynamic>) as T;
    }else if(T == PresetInstruction){
      return PresetInstruction.fromJson(json as Map<String, dynamic>) as T;
    }else if(T == TemplateModel){
      return TemplateModel.fromJson(json as Map<String, dynamic>) as T;
    }else if(T == TripModel){
      return TripModel.fromJson(json as Map<String, dynamic>) as T;
    }else if(T == TripResponse){
      return TripResponse.fromJson(json as Map<String, dynamic>) as T;
    }
    return json as T;
  }

  @override
  Object? toJson(T? object) {
    return object;
  }
}

@JsonSerializable()
class StatusResponse {
  bool? status;

  StatusResponse();

  factory StatusResponse.fromJson(Map<String, dynamic> json) =>
      _$StatusResponseFromJson(json);
}

@JsonSerializable()
class IdValuePair {
  int? id = 0;
  int? value = 0;

  IdValuePair();

  factory IdValuePair.fromJson(Map<String, dynamic> json) =>
      _$IdValuePairFromJson(json);
}

@JsonSerializable()
class BaseTrackerInfo {
  bool? trackerAdded = false;
  bool? trackerActivated = false;

  BaseTrackerInfo();

  factory BaseTrackerInfo.fromJson(Map<String, dynamic> json) =>
      _$BaseTrackerInfoFromJson(json);
}

@JsonSerializable()
class BaseDistributor {
  String? distributorCompanyName;
  String? distributorMobileNumber;
  bool? distributor;
  int? distributorUserId;
}

convertDynamicListToStaticList<T>(List<dynamic>? items) {
  return items?.map(_Converter<T>().fromJson).toList();
}
