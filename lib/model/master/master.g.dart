// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'master.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MasterDataResponse _$MasterDataResponseFromJson(Map<String, dynamic> json) =>
    MasterDataResponse(
      (json['districts'] as List<dynamic>?)
          ?.map((e) => Districts.fromJson(e as Map<String, dynamic>))
          .toList(),
      (json['thanas'] as List<dynamic>?)
          ?.map((e) => Thanas.fromJson(e as Map<String, dynamic>))
          .toList(),
    )
      ..goodTypes = (json['goodTypes'] as List<dynamic>?)
          ?.map((e) => GoodsType.fromJson(e as Map<String, dynamic>))
          .toList()
      ..truckTypes = (json['truckTypes'] as List<dynamic>?)
          ?.map((e) => TruckType.fromJson(e as Map<String, dynamic>))
          .toList()
      ..truckDimensions = json['truckDimensions'] == null
          ? null
          : TruckDimensions.fromJson(
              json['truckDimensions'] as Map<String, dynamic>)
      ..truckSizes = (json['truckSizes'] as List<dynamic>?)
          ?.map((e) => TruckSize.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$MasterDataResponseToJson(MasterDataResponse instance) =>
    <String, dynamic>{
      'districts': instance.districts,
      'thanas': instance.thanas,
      'goodTypes': instance.goodTypes,
      'truckTypes': instance.truckTypes,
      'truckDimensions': instance.truckDimensions,
      'truckSizes': instance.truckSizes,
    };
