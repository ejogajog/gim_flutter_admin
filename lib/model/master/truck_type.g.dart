// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'truck_type.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TruckType _$TruckTypeFromJson(Map<String, dynamic> json) => TruckType(
      (json['id'] as num?)?.toInt(),
      json['text'] as String?,
      json['textBn'] as String?,
      json['image'] as String?,
      (json['sequence'] as num?)?.toInt(),
      json['colorImage'] as String?,
    );

Map<String, dynamic> _$TruckTypeToJson(TruckType instance) => <String, dynamic>{
      'id': instance.id,
      'text': instance.text,
      'textBn': instance.textBn,
      'image': instance.image,
      'colorImage': instance.colorImage,
      'sequence': instance.sequence,
    };
