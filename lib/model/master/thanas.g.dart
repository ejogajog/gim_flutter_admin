// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'thanas.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Thanas _$ThanasFromJson(Map<String, dynamic> json) => Thanas(
      (json['id'] as num?)?.toInt(),
      (json['districtId'] as num?)?.toInt(),
      json['name'] as String?,
      json['nameBn'] as String?,
    );

Map<String, dynamic> _$ThanasToJson(Thanas instance) => <String, dynamic>{
      'districtId': instance.districtId,
      'id': instance.id,
      'name': instance.name,
      'nameBn': instance.nameBn,
    };
