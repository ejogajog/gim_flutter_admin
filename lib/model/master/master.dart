import 'package:app/model/master/districts.dart';
import 'package:app/model/master/goods_type.dart';
import 'package:app/model/master/thanas.dart';
import 'package:app/model/master/truck_size.dart';
import 'package:app/model/master/truck_type.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:app/model/master/truck_dimension.dart';

part 'master.g.dart';

@JsonSerializable()
class MasterDataResponse{

  @JsonKey(name: 'districts')
  List<Districts>? districts;

  @JsonKey(name: 'thanas')
  List<Thanas>? thanas;

  @JsonKey(name: 'goodTypes')
  List<GoodsType>? goodTypes;

  @JsonKey(name: 'truckTypes')
  List<TruckType>? truckTypes;

  @JsonKey(name: 'truckDimensions')
  TruckDimensions? truckDimensions;

  List<TruckSize>? truckSizes;

  MasterDataResponse(this.districts,this.thanas);
  
  factory MasterDataResponse.fromJson(Map<String, dynamic> json) => _$MasterDataResponseFromJson(json);
  Map<String, dynamic> toJson() => _$MasterDataResponseToJson(this);
}