import 'dart:convert';

import 'package:app/localization/app_translation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'truck_type.g.dart';


@JsonSerializable()
class TruckType {
  int? id;
  String? text;
  String? textBn;
  String? image;
  String? colorImage;
  int? sequence;
  @JsonKey(ignore: true)
  bool isDefault = false;

  TruckType(this.id, this.text, this.textBn, this.image, this.sequence, this.colorImage,{this.isDefault = false});

  factory TruckType.fromJson(Map<String, dynamic> json) =>
      _$TruckTypeFromJson(json);

  Map<String, dynamic> toJson() => _$TruckTypeToJson(this);

  @override
  String toString() => '${isBangla() ? textBn : text}';

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is TruckType &&
              runtimeType == other.runtimeType &&
              id == other.id;

  @override
  int get hashCode => id.hashCode;

  static Map<String, dynamic> toMap(TruckType truckType) => {
    'id': truckType.id,
    'text': truckType.text,
    'image': truckType.image,
    'colorImage': truckType.colorImage,
    'textBn': truckType.textBn,
    'isDefault':truckType.isDefault
  };

  static String encode(List<TruckType> truckTypes) => json.encode(
    truckTypes
        .map<Map<String, dynamic>>((truckType) => TruckType.toMap(truckType))
        .toList(),
  );

  static List<TruckType> decode(String truckType) =>
      (json.decode(truckType) as List<dynamic>)
          .map<TruckType>((item) => TruckType.fromJson(item))
          .toList();
}