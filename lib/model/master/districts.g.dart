// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'districts.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Districts _$DistrictsFromJson(Map<String, dynamic> json) => Districts(
      (json['id'] as num?)?.toInt(),
    )
      ..text = json['text'] as String?
      ..textBn = json['textBn'] as String?;

Map<String, dynamic> _$DistrictsToJson(Districts instance) => <String, dynamic>{
      'id': instance.id,
      'text': instance.text,
      'textBn': instance.textBn,
    };
