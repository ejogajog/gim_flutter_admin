// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'truck_dimension.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TruckDimensions _$TruckDimensionsFromJson(Map<String, dynamic> json) =>
    TruckDimensions()
      ..length = (json['Length'] as List<dynamic>?)
          ?.map((e) => TruckDimensionLength.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$TruckDimensionsToJson(TruckDimensions instance) =>
    <String, dynamic>{
      'Length': instance.length,
    };

TruckDimensionLength _$TruckDimensionLengthFromJson(
        Map<String, dynamic> json) =>
    TruckDimensionLength(
      (json['id'] as num?)?.toInt(),
      json['value'] as String?,
    );

Map<String, dynamic> _$TruckDimensionLengthToJson(
        TruckDimensionLength instance) =>
    <String, dynamic>{
      'id': instance.id,
      'value': instance.value,
    };
