import 'dart:convert';

import 'package:app/localization/app_translation.dart';
import 'package:app/ui/trip/create/model/base_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'truck_dimension.g.dart';

@JsonSerializable()
class TruckDimensions {
  @JsonKey(name: 'Length')
  List<TruckDimensionLength>? length;

  TruckDimensions();

  factory TruckDimensions.fromJson(Map<String, dynamic> json) =>
      _$TruckDimensionsFromJson(json);

  Map<String, dynamic> toJson() => _$TruckDimensionsToJson(this);
}

@JsonSerializable()
class TruckDimensionLength extends DataInfo{
  int? id;
  String? value;
  @JsonKey(ignore: true)
  bool? isDefault = false;
  TruckDimensionLength(this.id, this.value, {this.isDefault = false});

  factory TruckDimensionLength.fromJson(Map<String, dynamic> json) =>
      _$TruckDimensionLengthFromJson(json);

  Map<String, dynamic> toJson() => _$TruckDimensionLengthToJson(this);

  @override
  String toString() {
    return isBangla()
        ? localize('number_decimal_count',
            dynamicValue: value.toString(), symbol: '%f')
        : value.toString();
  }

  @override
  get iD => id;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TruckDimensionLength &&
          runtimeType == other.runtimeType &&
          value == other.value;

  @override
  int get hashCode => value.hashCode;

  static Map<String, dynamic> toMap(TruckDimensionLength truckDimenLen) => {
        'id': truckDimenLen.id,
        'value': truckDimenLen.value,
        'isDefault':truckDimenLen.isDefault
      };

  static String encode(List<TruckDimensionLength> truckDimens) => json.encode(
        truckDimens
            .map<Map<String, dynamic>>(
                (truckDimension) => TruckDimensionLength.toMap(truckDimension))
            .toList(),
      );

  static List<TruckDimensionLength> decode(String truckDimen) => (json
          .decode(truckDimen) as List<dynamic>)
      .map<TruckDimensionLength>((item) => TruckDimensionLength.fromJson(item))
      .toList();
}
