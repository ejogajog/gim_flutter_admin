import 'dart:convert';

import 'package:app/localization/app_translation.dart';
import 'package:app/ui/trip/create/model/base_model.dart';
import 'package:json_annotation/json_annotation.dart';
part 'truck_size.g.dart';

@JsonSerializable()
class TruckSize extends DataInfo{

  int? id;
  double? size;
  @JsonKey(ignore: true)
  bool? isDefault = false;
  TruckSize(this.id,this.size, {this.isDefault = false});
  factory TruckSize.fromJson(Map<String, dynamic> json) => _$TruckSizeFromJson(json);
  Map<String, dynamic> toJson() => _$TruckSizeToJson(this);

  @override
  String toString() {
    return (isBangla() ? localize('number_decimal_count',dynamicValue: size.toString(),symbol: '%f'):size.toString());
  }

  @override
  get iD => id;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is TruckSize &&
              runtimeType == other.runtimeType &&
              (id == other.id || size == other.size);

  @override
  int get hashCode => id.hashCode;

  static Map<String, dynamic> toMap(TruckSize truckSize) => {
    'id': truckSize.id,
    'size': truckSize.size,
    'isDefault':truckSize.isDefault
  };

  static String encode(List<TruckSize> truckSizes) => json.encode(
    truckSizes
        .map<Map<String, dynamic>>((music) => TruckSize.toMap(music))
        .toList(),
  );

  static List<TruckSize> decode(String truckSize) =>
      (json.decode(truckSize) as List<dynamic>)
          .map<TruckSize>((item) => TruckSize.fromJson(item))
          .toList();
}