
import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
part 'districts.g.dart';

@JsonSerializable()
class Districts{
  int? id;
  String? text;
  String? textBn;
  Districts(this.id);
  factory Districts.fromJson(Map<String, dynamic> json) => _$DistrictsFromJson(json);
  Map<String, dynamic> toJson() => _$DistrictsToJson(this);

  @override
  String toString() {
    return '$text';
  }

  bool operator ==(dynamic other) =>
      other != null && this.id == other.id;

  @override
  int get hashCode => super.hashCode;

  static Map<String, dynamic> toMap(Districts district) => {
    'id': district.id,
    'text': district.text,
    'textBn': district.textBn,
  };

  static String encode(List<Districts> dst) => json.encode(
    dst
        .map<Map<String, dynamic>>((dst) => Districts.toMap(dst))
        .toList(),
  );

  static List<Districts> decode(String dst) =>
      (json.decode(dst) as List<dynamic>)
          .map<Districts>((item) => Districts.fromJson(item))
          .toList();
}

