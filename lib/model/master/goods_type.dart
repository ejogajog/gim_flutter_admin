import 'dart:convert';
import 'dart:core';
import 'package:app/localization/app_translation.dart';
import 'package:json_annotation/json_annotation.dart';
part 'goods_type.g.dart';

@JsonSerializable()
class GoodsType {
  int? id;
  String? text;
  String? image = '';
  String? textBn;
  @JsonKey(ignore: true)
  bool isDefault = false;
  GoodsType(this.id,this.text,this.image,this.textBn, {this.isDefault = false});
  factory GoodsType.fromJson(Map<String, dynamic> json) => _$GoodsTypeFromJson(json);
  Map<String, dynamic> toJson() => _$GoodsTypeToJson(this);
  @override
  String toString() {
    return isBangla() ? textBn ?? text ?? '' : text ?? '';
    //return text;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is GoodsType &&
              runtimeType == other.runtimeType &&
              toString() == other.toString();

  @override
  int get hashCode => toString().hashCode;

  static Map<String, dynamic> toMap(GoodsType goodsType) => {
    'id': goodsType.id,
    'text': goodsType.text,
    'image': goodsType.image,
    'textBn': goodsType.textBn,
    'isDefault': goodsType.isDefault
  };

  static String encode(List<GoodsType> goods) => json.encode(
    goods
        .map<Map<String, dynamic>>((goodsType) => GoodsType.toMap(goodsType))
        .toList(),
  );

  static List<GoodsType> decode(String goods) =>
      (json.decode(goods) as List<dynamic>)
          .map<GoodsType>((item) => GoodsType.fromJson(item))
          .toList();

}