
import 'package:json_annotation/json_annotation.dart';
part 'thanas.g.dart';

@JsonSerializable()
class Thanas{
  int? districtId;
  int? id;
  String? name;
  String? nameBn;
  Thanas(this.id,this.districtId, this.name,this.nameBn);
  factory Thanas.fromJson(Map<String, dynamic> json) => _$ThanasFromJson(json);
  Map<String, dynamic> toJson() => _$ThanasToJson(this);

  @override
  String toString() {
    return '$name';
  }

  bool operator ==(dynamic other) =>
      other != null && this.id == other.id && this.name == other.name;

  @override
  int get hashCode => super.hashCode;

}

