// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'base_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BaseResponse<T> _$BaseResponseFromJson<T>(Map json) => BaseResponse<T>()
  ..data = _Converter<T?>().fromJson(json['data'])
  ..success = json['success'] as bool?
  ..message = json['message'] as String?
  ..errorCode = json['errorCode'] as String?
  ..responseCode = (json['responseCode'] as num?)?.toInt();

Map<String, dynamic> _$BaseResponseToJson<T>(BaseResponse<T> instance) =>
    <String, dynamic>{
      'data': _$JsonConverterToJson<Object?, T>(
          instance.data, _Converter<T?>().toJson),
      'success': instance.success,
      'message': instance.message,
      'errorCode': instance.errorCode,
      'responseCode': instance.responseCode,
    };

Json? _$JsonConverterToJson<Json, Value>(
  Value? value,
  Json? Function(Value value) toJson,
) =>
    value == null ? null : toJson(value);

BaseTrip _$BaseTripFromJson(Map<String, dynamic> json) => BaseTrip()
  ..requestedTripCount = (json['requestedTripCount'] as num?)?.toInt()
  ..bookedTripCount = (json['bookedTripCount'] as num?)?.toInt()
  ..liveTripCount = (json['liveTripCount'] as num?)?.toInt()
  ..completedTripCount = (json['completedTripCount'] as num?)?.toInt()
  ..tripListForSelectedTripFilter =
      json['tripListForSelectedTripFilter'] == null
          ? null
          : BasePagination<TripItem>.fromJson(
              json['tripListForSelectedTripFilter'] as Map<String, dynamic>);

Map<String, dynamic> _$BaseTripToJson(BaseTrip instance) => <String, dynamic>{
      'requestedTripCount': instance.requestedTripCount,
      'bookedTripCount': instance.bookedTripCount,
      'liveTripCount': instance.liveTripCount,
      'completedTripCount': instance.completedTripCount,
      'tripListForSelectedTripFilter': instance.tripListForSelectedTripFilter,
    };

BasePagination<T> _$BasePaginationFromJson<T>(Map<String, dynamic> json) =>
    BasePagination<T>(
      (json['contentList'] as List<dynamic>?)
          ?.map(_Converter<T>().fromJson)
          .toList(),
    )
      ..totalPages = (json['totalPages'] as num?)?.toInt()
      ..numberOfResults = (json['numberOfResults'] as num?)?.toInt()
      ..nextPage = (json['nextPage'] as num?)?.toInt();

Map<String, dynamic> _$BasePaginationToJson<T>(BasePagination<T> instance) =>
    <String, dynamic>{
      'contentList': instance.contentList?.map(_Converter<T>().toJson).toList(),
      'totalPages': instance.totalPages,
      'numberOfResults': instance.numberOfResults,
      'nextPage': instance.nextPage,
    };

StatusResponse _$StatusResponseFromJson(Map<String, dynamic> json) =>
    StatusResponse()..status = json['status'] as bool?;

Map<String, dynamic> _$StatusResponseToJson(StatusResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
    };

IdValuePair _$IdValuePairFromJson(Map<String, dynamic> json) => IdValuePair()
  ..id = (json['id'] as num?)?.toInt()
  ..value = (json['value'] as num?)?.toInt();

Map<String, dynamic> _$IdValuePairToJson(IdValuePair instance) =>
    <String, dynamic>{
      'id': instance.id,
      'value': instance.value,
    };

BaseTrackerInfo _$BaseTrackerInfoFromJson(Map<String, dynamic> json) =>
    BaseTrackerInfo()
      ..trackerAdded = json['trackerAdded'] as bool?
      ..trackerActivated = json['trackerActivated'] as bool?;

Map<String, dynamic> _$BaseTrackerInfoToJson(BaseTrackerInfo instance) =>
    <String, dynamic>{
      'trackerAdded': instance.trackerAdded,
      'trackerActivated': instance.trackerActivated,
    };

BaseDistributor _$BaseDistributorFromJson(Map<String, dynamic> json) =>
    BaseDistributor()
      ..distributorCompanyName = json['distributorCompanyName'] as String?
      ..distributorMobileNumber = json['distributorMobileNumber'] as String?
      ..distributor = json['distributor'] as bool?
      ..distributorUserId = (json['distributorUserId'] as num?)?.toInt();

Map<String, dynamic> _$BaseDistributorToJson(BaseDistributor instance) =>
    <String, dynamic>{
      'distributorCompanyName': instance.distributorCompanyName,
      'distributorMobileNumber': instance.distributorMobileNumber,
      'distributor': instance.distributor,
      'distributorUserId': instance.distributorUserId,
    };
