import 'package:app/model/project/project.dart';
import 'package:json_annotation/json_annotation.dart';

part 'projects.g.dart';

@JsonSerializable()
class ProjectListResponse {
  List<Projects>? data;

  ProjectListResponse(this.data);

  factory ProjectListResponse.fromJson(Map<String, dynamic> json) =>
      _$ProjectListResponseFromJson(json);
  Map<String, dynamic> toJson() => _$ProjectListResponseToJson(this);
}
