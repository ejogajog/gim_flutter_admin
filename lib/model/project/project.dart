
import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
part 'project.g.dart';

@JsonSerializable()
class Projects{
  int? id;
  String? value;

  Projects();
  factory Projects.fromJson(Map<String, dynamic> json) => _$ProjectsFromJson(json);
  Map<String, dynamic> toJson() => _$ProjectsToJson(this);

  @override
  String toString() {
    return (value??'').trim();
  }

  bool operator ==(dynamic other) =>
      other != null && this.id == other.id && this.value == other.value;

  @override
  int get hashCode => super.hashCode;

  static Map<String, dynamic> toMap(Projects project) => {
    'id': project.id,
    'value': project.value,
  };

  static String encode(List<Projects> proj) => json.encode(
    proj
        .map<Map<String, dynamic>>((proj) => Projects.toMap(proj))
        .toList(),
  );

  static List<Projects> decode(String projName) =>
      (json.decode(projName) as List<dynamic>)
          .map<Projects>((item) => Projects.fromJson(item))
          .toList();
}

