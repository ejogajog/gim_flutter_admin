import 'package:flutter/material.dart';

enum Flavor { QA, PROD }

class FlavorValues {
  FlavorValues({
    required this.baseUrl,
    this.GOOGLE_API_KEY,
    this.MIX_PANEL_CODE,
    this.noOfTripRecords = 1500,
  });

  final String baseUrl;
  final String? GOOGLE_API_KEY;
  final String? MIX_PANEL_CODE;
  final int noOfTripRecords;
  final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();
}

class FlavorConfig {
  final Flavor flavor;
  final FlavorValues values;
  static FlavorConfig? _instance;

  factory FlavorConfig({required Flavor flavor, required FlavorValues values}) {
    _instance ??= FlavorConfig._internal(flavor, values);
    return _instance!;
  }

  FlavorConfig._internal(this.flavor, this.values);

  static FlavorConfig? get instance {
    return _instance;
  }

  static bool isProduction() => _instance?.flavor == Flavor.PROD;

  static bool isQA() => _instance?.flavor == Flavor.QA;
}
