import 'config.dart';
import 'package:app/app.dart';
import 'package:flutter/material.dart';
import 'package:app/firebase_options_qa.dart';
import 'package:app/utils/app_constants.dart';
import 'package:firebase_core/firebase_core.dart';

Future<void> main() async {
  FlavorConfig(
    flavor: Flavor.QA,
    values: FlavorValues(
      baseUrl: "https://qa2.gim.com.bd",
      MIX_PANEL_CODE: mpTestCode,
      noOfTripRecords: 200,
    ),
  );
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptionsQA.currentPlatform);
  runApp(MyApp());
}
