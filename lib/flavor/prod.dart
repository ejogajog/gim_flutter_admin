import 'config.dart';
import 'package:app/app.dart';
import 'package:flutter/material.dart';
import 'package:app/utils/app_constants.dart';
import 'package:app/firebase_options_prod.dart';
import 'package:firebase_core/firebase_core.dart';

Future<void> main() async {
  FlavorConfig(
    flavor: Flavor.PROD,
    values: FlavorValues(
      baseUrl: "https://gim.com.bd",
      MIX_PANEL_CODE: mpProdCode,
    ),
  );
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptionsPROD.currentPlatform);
  runApp(MyApp());
}
