import 'package:dio/dio.dart';
import 'package:app/utils/prefs.dart';
import 'package:app/utils/ui_util.dart';
import 'package:flutter/material.dart';
import 'package:app/api/http_util.dart';
import 'package:app/flavor/config.dart';
import 'package:app/model/base_response.dart';
import 'package:app/utils/app_constants.dart';

class ApiResponse<T> {
  Status status;
  var apiResponse;
  var data;
  String? message;

  ApiResponse.loading(this.message, this.apiResponse) : status = Status.LOADING;

  ApiResponse.completed(this.data, this.message) : status = Status.COMPLETED;

  ApiResponse.error(this.message, this.apiResponse) : status = Status.ERROR;

  ApiResponse.unhandledError(this.message) : status = Status.ERROR;


  @override
  String toString() {
    return "Status : $status \n Message : $message \n Data : $apiResponse";
  }
}

enum Status { LOADING, COMPLETED, ERROR }

class ApiBaseHelper {
  Future<ApiResponse> get(String url, {queryParameters = null}) async {
    var responseJson;
    try {
      Response response = await NetworkCommon.instance.dio.get(url,
          queryParameters: queryParameters,
          options: _getResponseReturnOption());
      debugPrint("ApiResponse:" + response.toString());
      responseJson = _returnResponse(response);
    } catch (ex) {
      return manageException(ex);
    }
    return responseJson;
  }


  Future<ApiResponse> post(String url, request, {isPut = false}) async {
    var responseJson;
    print("requestparam:" + request.toString());
    try {
      Response response;
      if (!isPut)
        response = await NetworkCommon.instance.dio.post(url, data: request, options: _getResponseReturnOption());
      else
        response = await NetworkCommon.instance.dio.put(url, data: request, options: _getResponseReturnOption());

      responseJson = _returnResponse(response);
    } catch (ex) {
      return manageException(ex);
    }
    return responseJson;
  }

  manageException(ex) {
    var errorMessage = 'SOMETHING WENT WRONG';
    showToast(errorMessage);
    try {
      if (ex is DioException) {
        errorMessage = "Connection timeout. Try again later";
      } else {
        errorMessage = 'ERROR: Network connection unavailable';
      }
    } catch (ex) {
      print(ex.toString());
    }

    return ApiResponse.unhandledError(errorMessage);
  }

  _getResponseReturnOption() {
    return Options(
        followRedirects: false,
        validateStatus: (status) {
          return true;
        }
    );
  }

  ApiResponse _returnResponse(Response response) {
    BaseResponse baseResponse;
    try {
      //response.data="lh<sg>";
      baseResponse = BaseResponse.fromJson(response.data);
    } catch (ex) {
      return manageException(ex);
    }
    switch (response.statusCode) {
      case 200:
        return ApiResponse.completed(response.data['data'], baseResponse.message);
      case 401:
        logoutFromApp();
        return ApiResponse.error(baseResponse.message, response.data);
        break;

      case 400:
      case 403:
      case 404:
      case 406:
      case 500:
      default:
        return ApiResponse.error(baseResponse.message, response.data);
    }
  }

  logoutFromApp({isNormalLogout = false}) async {
    print('logout with user stats');
    if (Prefs.getBoolean(Prefs.IS_LOGGED_IN, defaultValue: true)) {
      Future.delayed(const Duration(milliseconds: 550), () {
        FlavorConfig.instance!.values.navigatorKey.currentState!
            .pushNamedAndRemoveUntil(RouteConstants.authErrorRoute, (e) => false);
      });
    }
    Prefs.setBoolean(Prefs.IS_LOGGED_IN, false);
    Prefs.setBoolean(Prefs.IS_DISTRIBUTOR, false);
    Prefs.setBoolean(Prefs.PREF_IS_ENTERPRISE_USER, false);
    Prefs.setString(Prefs.PROJECTS, "");
    Prefs.remove(Prefs.PROJECTS);
  }

}
