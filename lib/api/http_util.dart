import 'package:dio/dio.dart';
import 'package:app/flavor/config.dart';
import 'package:app/utils/prefs.dart';

class NetworkCommon {
  late Dio _dio;

  Dio get dio => _dio;

  set dio(Dio value) {
    _dio = value;
  }

  static NetworkCommon? _instance;

  static NetworkCommon get instance => NetworkCommon();

  NetworkCommon._internal() {
    initConfig();
  }

  factory NetworkCommon() {
    _instance ??= NetworkCommon._internal();
    return _instance!;
  }

  initConfig() {
    dio = Dio();
    BaseOptions options = BaseOptions(
        followRedirects: false,
        baseUrl: FlavorConfig.instance!.values.baseUrl,
        receiveTimeout: Duration(milliseconds: 120000),
        connectTimeout: Duration(milliseconds: 90000),
        validateStatus: (status) => status! < 500,
        headers: {
          "authtoken": getAuthToken(),
          "Accept-Language": "en",
          "Accept": "application/json",
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/json"
        });
    dio = Dio(options);
    dio.interceptors.add(LogInterceptor(
      requestBody: true,
      responseBody: true,
      requestHeader: true,
    ));
  }
}

String? getAuthToken() => Prefs.getString(Prefs.token);
