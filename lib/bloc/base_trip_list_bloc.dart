import 'package:app/api/api_response.dart';
import 'package:app/model/base_response.dart';
import 'package:app/ui/trip/trip_map_item.dart';
import 'package:app/bloc/base_pagination_bloc.dart';

class BaseTripListBloc extends BasePaginationBloc<TripMapItem> {
  BaseTrip? trip;

  onTripListResponse(ApiResponse response, {callback}) {
    checkResponse(response, successCallback: () async {
      trip = BaseTrip.fromJson(response.data);
      List<TripMapItem> listOfTripItem = callback(trip!.tripListForSelectedTripFilter?.contentList);
      setPaginationItem(trip!.tripListForSelectedTripFilter!, listOfTripItem);
    });
  }
}
