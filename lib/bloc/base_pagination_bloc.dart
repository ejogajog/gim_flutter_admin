import 'package:app/bloc/base_bloc.dart';
import 'package:app/flavor/config.dart';
import 'package:app/model/base_response.dart';

class BasePaginationBloc<T> extends BaseBloc{
  int _currentPage= 1;
  int _size = FlavorConfig.instance!.values.noOfTripRecords;

  int get size => _size;

  set size(int value) {
    _size = value;
  }

  int _totalPage=0;
  int _totalNumberOfItems=0;
  Map<String,String> queryParam=Map();
  List<T>_itemList=[];
  List<T> get itemList => _itemList;

  set itemList(List<T> value) {
    if(value!=null && value.isNotEmpty)
      _itemList.addAll(value);
    print(this.runtimeType.toString()+":"+itemList.length.toString());
    notifyListeners();
  }

  int get totalNumberOfItems => _totalNumberOfItems;

  set totalNumberOfItems(int value) {
    if(value!=null)
      _totalNumberOfItems = value;
  }

  int get currentPage => _currentPage;

  set currentPage(int value) {
    _currentPage = value;
  }

  int get totalPage => _totalPage;

  set totalPage(int value) {
    _totalPage = value;
  }

  Map? getBaseQueryParam(){
    queryParam['size']=size.toString();
    queryParam['page']=currentPage.toString();
    return queryParam;
  }

  resetList(){
    currentPage = 1;
    // On reload filter/sort/search query params should not be cleared
    // queryParam.clear();
    getBaseQueryParam();
    itemList.clear();
    itemList=[];
  }

  setPaginationItem(BasePagination? pagination, List? tripMapItem){
    if(isLoading==true) isLoading=false;
    currentPage++;
    totalNumberOfItems = tripMapItem?.length??0;
    totalPage = pagination!.totalPages!;
    setContentList(tripMapItem);
  }

  setContentList(contentList){
    itemList = contentList;
  }
  getListFromApi({callback}) async{}


  reloadList({additionalQueryParam,callback}){
    resetList();
    if(additionalQueryParam!=null) queryParam.addAll(additionalQueryParam);
    getListFromApi(callback: callback);
  }

  clearItems(){
    _itemList.clear();
  }
}