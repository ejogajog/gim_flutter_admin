import 'package:flutter/material.dart';
import 'package:app/utils/color_const.dart';
import 'package:google_fonts/google_fonts.dart';

abstract class Styles {
  static TextStyle hintTextStyle = GoogleFonts.roboto(
    fontSize: 14,
    fontWeight: FontWeight.w500,
    color: Color.fromRGBO(221, 221, 221, 1.0),
  );

  static TextStyle tripDetailAddress = GoogleFonts.roboto(
    fontSize: 14,
    fontWeight: FontWeight.w300,
    color: Color.fromRGBO(0, 0, 0, 1.0),
  );

  static TextStyle tripDetailLabel = GoogleFonts.roboto(
    fontSize: 14,
    fontWeight: FontWeight.w300,
    color: Color.fromRGBO(100, 100, 100, 1.0),
  );

  static TextStyle tripDetailValue = GoogleFonts.roboto(
    fontSize: 16,
    fontWeight: FontWeight.w500,
    color: Color.fromRGBO(0, 0, 0, 1.0),
  );

  static TextStyle bookingDetailHeading = GoogleFonts.roboto(
    fontSize: 16,
    fontWeight: FontWeight.w900,
    color: Color.fromRGBO(0, 50, 80, 1.0),
  );

  static TextStyle bookingDetailLabel = GoogleFonts.roboto(
    fontSize: 20,
    fontWeight: FontWeight.w300,
    color: Color.fromRGBO(0, 50, 80, 1.0),
  );

  static TextStyle bookingDetailSubLabel = GoogleFonts.roboto(
    fontSize: 18,
    fontWeight: FontWeight.w300,
    color: Color.fromRGBO(0, 50, 80, 1.0),
  );

  static TextStyle totalTextStyle = GoogleFonts.roboto(
    fontSize: 24,
    fontWeight: FontWeight.w700,
    color: Color.fromRGBO(255, 189, 0, 1.0),
  );

  static TextStyle amountTextStyle = GoogleFonts.roboto(
    fontSize: 24,
    fontWeight: FontWeight.w700,
    color: Color.fromRGBO(0, 50, 82, 1.0),
  );

  static TextStyle blueMedTextBold = GoogleFonts.roboto(
    fontSize: 16,
    fontWeight: FontWeight.w700,
    color: Color.fromRGBO(0, 50, 80, 1.0),
  );

  static TextStyle blueBigTextBold = GoogleFonts.roboto(
    fontSize: 20,
    fontWeight: FontWeight.w700,
    color: Color.fromRGBO(0, 50, 80, 1.0),
  );

  static TextStyle textStyleGreenMed = GoogleFonts.roboto(
    fontSize: 16,
    fontWeight: FontWeight.w500,
    color: Color.fromRGBO(0, 170, 75, 1.0),
  );

  static TextStyle textStyleRedMed = GoogleFonts.roboto(
    fontSize: 16,
    fontWeight: FontWeight.w500,
    color: Color.fromRGBO(234, 22, 84, 1.0),
  );

  static TextStyle selMedTextBold = GoogleFonts.roboto(
    fontSize: 14,
    color: Colors.blue,
    fontWeight: FontWeight.w500,
  );

  static TextStyle hintLabelBrownish = GoogleFonts.roboto(
    fontSize: 12,
    fontWeight: FontWeight.w300,
    color: Color.fromRGBO(100, 100, 100, 1.0),
  );

  static TextStyle brownishValueBlack = GoogleFonts.roboto(
    fontSize: 16,
    fontWeight: FontWeight.w500,
    color: Color.fromRGBO(0, 0, 0, 1.0),
  );

  static TextStyle txtStyleBlackNormal = GoogleFonts.roboto(
    fontSize: 16,
    fontWeight: FontWeight.w300,
    color: Color.fromRGBO(0, 0, 0, 1.0),
  );

  static TextStyle txtStyleWhiteNormal = TextStyle(
    color: Color.fromRGBO(255, 255, 255, 1.0),
    fontSize: 14,
    fontFamily: "notosans",
    fontWeight: FontWeight.w300,
  );

  static TextStyle txtStyleBlackBangla = TextStyle(
    color: ColorResource.colorBlack,
    fontSize: 14,
    fontFamily: "notosans",
    fontWeight: FontWeight.w300,
  );

  static TextStyle tripNoStyle = GoogleFonts.roboto(
    fontSize: 12,
    fontWeight: FontWeight.w500,
    color: Color.fromRGBO(155, 155, 155, 1.0),
  );

  static TextStyle mapHintTextStyle = GoogleFonts.roboto(
    fontSize: 18,
    fontWeight: FontWeight.w300,
    color: Color.fromRGBO(221, 221, 221, 1.0),
  );

  static TextStyle detailLabel = GoogleFonts.roboto(
    fontSize: 14,
    fontWeight: FontWeight.w500,
    color: ColorResource.colorBlack,
  );

  static TextStyle detailValue = TextStyle(
    color: ColorResource.colorMarineBlue,
    fontSize: 14,
    fontFamily: "notosans",
    fontWeight: FontWeight.w300,
  );

  static TextStyle updateButton = TextStyle(
    color: ColorResource.colorMarineBlue,
    fontSize: 14,
    fontFamily: "notosans",
    fontWeight: FontWeight.w500,
  );

  static TextStyle hintLabel = TextStyle(
    color: ColorResource.labelColor,
    fontSize: 12,
    fontFamily: "notosans",
    fontWeight: FontWeight.w500,
  );

  static TextStyle formLabel = GoogleFonts.roboto(
    fontSize: 14,
    fontWeight: FontWeight.w500,
    color: Color.fromRGBO(100, 100, 100, 1.0),
  );
}
