import 'package:app/utils/prefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:app/ui/splash/splash.dart';
import 'package:app/ui/widget/app_widget.dart';
import 'package:app/localization/application.dart';
import 'package:app/localization/app_translation.dart';
import 'package:app/localization/app_translation_delegate.dart';

class MyApp extends StatefulWidget {
  @override
  MyAppState createState() => MyAppState();

  static restartApp(BuildContext context, {languageCode}) {
    final MyAppState state = context.findAncestorStateOfType<MyAppState>()!;
    state.restartApp(languageCode: languageCode);
  }
}

class MyAppState extends State<MyApp> with WidgetsBindingObserver {
  String languageCode = "en";
  AppTranslationsDelegate? _newLocaleDelegate;

  void restartApp({languageCode}) {
    this.setState(() {
      this.languageCode = languageCode ?? this.languageCode;
      _newLocaleDelegate = AppTranslationsDelegate(newLocale: Locale(languageCode));
      application.onLocaleChanged = onLocaleChange;
    });
  }

  @override
  void initState() {
    super.initState();
    initPreference();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    setState(() {});
  }

  static String getLanguageCode() => Prefs.getStringWithDefaultValue(Prefs.language_code, defaultValue: "en");

  initPreference() async {
    _newLocaleDelegate = AppTranslationsDelegate(newLocale: Locale(getLanguageCode()));
    await Prefs.init();
    languageCode = Prefs.getStringWithDefaultValue(Prefs.language_code, defaultValue: "en");
    if (languageCode == 'bd') languageCode = "bn";
    _newLocaleDelegate = AppTranslationsDelegate(newLocale: Locale(languageCode));
    application.onLocaleChanged = onLocaleChange;
    await AppTranslations.load(Locale(languageCode));
    print('init last line');
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarBrightness: Brightness.light,
      statusBarIconBrightness: Brightness.dark,
      systemNavigationBarColor: Colors.black,
      systemStatusBarContrastEnforced: false,
      systemNavigationBarContrastEnforced: false,
      systemNavigationBarDividerColor: Colors.black,
      systemNavigationBarIconBrightness: Brightness.light,
    ));
    return AppWidget(
      home: SplashScreen(),
      newLocaleDelegate: _newLocaleDelegate,
    );
  }

  void onLocaleChange(Locale locale) {
    setState(() {
      _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
    });
  }
}
